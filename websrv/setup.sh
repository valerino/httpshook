#!/bin/bash

echo -e "[ Update APT repositories ]"
sudo apt-get update

echo -e "[ Install and configure Build Essential Development Kit ]"
sudo apt-get install --yes build-essential

echo -e "[ Install and configure Python 2 ]"
sudo apt-get install --yes python

echo -e "[ Install and configure Development Kit for Python 2 ]"
sudo apt-get install --yes python-dev

echo -e "[ Install and configure Pip for Python 2 ]"
sudo apt-get install --yes python-pip

echo -e "[ Upgrade Pip for Python 2 ]"
sudo pip install --upgrade pip

echo -e "[ Install and configure MongoDB ]"
sudo apt-get install --yes mongodb

echo -e "[ Install and configure Flask for Python 2 ]"
sudo pip install flask

echo -e "[ Install and configure Pymongo ]"
sudo pip install pymongo

echo -e "[ Install and configure Pymongo for Flask for Python 2 ]"
sudo pip install flask-pymongo

echo -e "[ Install and configure Hpack for Python 2]"
sudo pip install hpack

echo -e "[ Install and configure Argparse for Python 2 ]"
sudo pip install argparse

echo -e "[ Install and configure Scapy for Python 2 ]"
sudo pip install scapy
