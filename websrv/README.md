README
======

DESCRIPTION
===========

This tool is a simple exfiltration Web server:

    - Serve the exploit with the agent to victim
    - Save exfiltrated data of agent from the victim
    - Look for login (username and password) from exfiltrated data to operator
    - Show exfiltrated data to operator

This tool was tested on Ubuntu 16.04 LTS.

LET'S GO
========

First run setup in order to install all packages:

    $ ./setup.sh

Then run MongoDB on a terminal like this:

    $ service mongodb start

Finally run exfiltration Web server on the same terminal like this:

    $ python ./websrv.py
    * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)

Please note:

1) websrv binds on port 5000, default on address 0.0.0.0, it can be changed directly in the Main

2) In order to serve the exploit, must to use this URL like Hook URL address (example iframe injection into HTTP responses):

    http://$websrvIP:5000/exploit

    NOTE: where $websrvIP is the exfiltration Web server's IP address.

3) In order to show exfiltrated data, open this URL from browser Web:

    http://$websrvIP:5000/logs

    NOTE: where $websrvIP is the exfiltration Web server's IP address.

4) In order to look for login from exfiltrated data, open this URL from browser Web:

    http://$websrvIP:5000/history

    NOTE: where $websrvIP is the exfiltration Web server's IP address.

Have a nice day! ;)
