from Cryptodome.Cipher import AES, PKCS1_v1_5
from Cryptodome.PublicKey import RSA
from Cryptodome import Random
from Cryptodome.Random import random
from Cryptodome.Util import number
import struct, binascii

"""@package FalconServer
FLCryptographer handles the encrypted and compressed packet received from an 
agent an tries to decrypt it.

The incoming packet must have the following format:
 - size_rsa_wrapped_aes_key_material, integer 4Bytes little endian
 - size_aes_padding, integer 4Bytes little endian
 - size_aes_encrypted_buffer_compressed, integer 4Bytes little endian
 - size_aes_encrypted_buffer_original, integer 4Bytes little endian
 - cmd_id, long 8Bytes little endian
 - aes_key_material, byte array with dimension equal to size_rsa_wrapped_aes_key_material
 - aes_compressed_encrypted_buffer, byte array with dimension equal to size_aes_encrypted_buffer_compressed
The two byte array of this packet are both encrypted:
 - aes_key_material with the RSA's public key of the receiver server
 - aes_compressed_encrypted_buffer with AES, the key and initial vector are stored
 in aes_key_material.
More details about the meaning of the fields can be found in README.md and below.

In the __init__ method, if parameter path_to_key is None, the module generate 
its own RSA keys and saves them into two files, named "falcon.priv" and "falcon.pub",
in the current folder. Keys are saved in PEM format.
( see http://www.ietf.org/rfc/rfc1421.txt and http://www.ietf.org/rfc/rfc1423.txt)
without passphrase.
Otherwise, reads and imports from path_to_key file a RSA key ( public or private).

The module provides two main (public) functions:
 - unpack(self, packet):
    given a received packet, module unpacks and decrypts it. Note that no 
    decompression is performed, just save in fields the decrypted data.
    A tuple comprehensive of decrypted, but still compressed, data and 
    size_aes_encrypted_buffer_original are returned.

 - pack(self,data,uncompressed_data_len):
    given a compressed data and its uncompressed length, this function make a
    crypted packet with the data specify above.
    The encrypted data is return.
"""
class FLCryptographer():
    """AES key length express in byte (256/8)"""
    AES_KEY_BYTE = 32
    """AES initial vector (IV) length express in byte (128/8) """
    AES_IV_BYTE  = 16
    """RSA key length express in byte (2048/8) """
    RSA_KEY_LEN  = 256

    def __init__(self, path_to_key=None):
        """Init function, initializes to default values and generates/reads RSA keys.

        FLCryptographer comes with the same fields of structure received in
        packet. In this function, these fields are setted on his default values:
         - 0, for numeric values ( each "size" member )
         - None, for each array of bytes data.
        Note that the field aes_key_material is represent as a dictionary, with
        the following keys:
         - 'key' : AES 32B encryption key;
         - 'iv'  : AES 16B Initial Vector;

        If path_to_key None, a new pair of RSA keys is generated, using 
        RSA_KEY_LEN constant ( in this case expressed in bits) for key length.
        Otherwise, reads from the file specified and imports the corresponding key.
        Pay attention, that no matter which kind of key is stored in file, if is
        correctly saved, Crypto module is able to understand if is a public or
        private key.
        """
        self.aes_key_material = { 'key' : None, 'iv'  : None }
        if path_to_key is None:
            # RSA key not exists, generate a new one
            self.rsa_key = RSA.generate(self.RSA_KEY_LEN*8) # generate requires bit instead of byte
            f = open('falcon.priv','w')
            f.write(self.rsa_key.exportKey('PEM'))
            f.close()
            f = open('falcon.pub','w')
            f.write(self.rsa_key.publickey().exportKey('PEM'))
            f.close()
        else:
            # RSA key exists, reads from file
            f = open(path_to_key,'r')
            self.rsa_key = RSA.importKey(f.read())
            f.close()

    def decrypt_AES(self, enc_buffer, padding):
        """Decrypt a buffer encrypted with AES CBC.

        Using AES in CBC mode, this function tries to decrypt the encrypted
        buffer using 'key' and 'iv' entries of aes_key_material variable.
        The 'padding' parameter is used for trimmed the decrypted buffer.
        Return decrypted data, without pad bytes
        """
        cipher = AES.new(self.aes_key_material['key'], AES.MODE_CBC, self.aes_key_material['iv'])
        decrypted = cipher.decrypt(enc_buffer)
        return decrypted[:len(decrypted)-padding]

    def encrypt_AES(self, plain):
        """Encrypt a buffer with AES CBC.

        Using AES in CBC mode, this function encrypts the buffer plain passed
        as parameter to function.
        The Key and IV parameter are generated, each time this function is call,
        using a secure Random generator ( Crypto.Random._UserFriendlyRNG, for
        details see http://pythonhosted.org/pycrypto/).
        If buffer length is not a multiplier of IV, then a random padding is added.
        Return data encrypted and how much bytes are used for padding.
        """
        self.aes_key_material['key'] = Random.new().read(self.AES_KEY_BYTE)
        self.aes_key_material['iv']  = Random.new().read(self.AES_IV_BYTE)
        if len(plain) % self.AES_IV_BYTE != 0:
            plain += Random.new().read( self.AES_IV_BYTE - (len(plain) % self.AES_IV_BYTE) )
        cipher = AES.new(self.aes_key_material['key'], AES.MODE_CBC, self.aes_key_material['iv'])
        return cipher.encrypt( plain), self.AES_IV_BYTE - (len(plain) % self.AES_IV_BYTE)
    
    def decrypt_AESkey(self, rsa_enc_buffer):
        """Decrypt the buffer, encrypted with RSA, that contains AES key.

        Using its own RSA's private key, try to decrypt the encrypted buffer
        and retrieves 'key' and 'iv' entries of aes_key_material variable for
        AES encryption.
        Results are stored into aes_key_material dictionary.
        """
        cipher = PKCS1_v1_5.new(self.rsa_key)
        rand = Random.new().read(self.AES_KEY_BYTE)
        aes_bytes = cipher.decrypt(rsa_enc_buffer,rand)

        if aes_bytes != rand:
            self.aes_key_material['key'] = aes_bytes[:self.AES_KEY_BYTE]
            self.aes_key_material['iv']  = aes_bytes[self.AES_KEY_BYTE: self.AES_KEY_BYTE+self.AES_IV_BYTE]
        else:
          print "!!! ERROR ON DECRYPT AES KEY!!!"

    def encrypt_AESKey(self):
        """Encrypts a buffer, with RSA, that contains AES key and AES Initial
        Vector (iv).

        Using its the client RSA's public key, pack in a buffer the AES's key
        (AES_KEY_BYTE length), the initial vector (IV, AES_IV_BYTE length) and
        an integer (4 byte length, unused always 0).
        Resulting buffer is returned to caller.
        """
        b = struct.pack('<%ds%ds' % ( self.AES_KEY_BYTE, self.AES_IV_BYTE,),
            self.aes_key_material['key'], self.aes_key_material['iv'])
        # Encrypt the session key with the public RSA key
        return PKCS1_v1_5.new(self.rsa_key).encrypt( b )

    def pack(self, compressed_data, uncompressed_len):
        """Create and encrypt data

        This function pack and encrypt data as per README.md.
        Data inside a packet has this format:
         - size_rsa_wrapped_aes_key_material, integer 4Bytes little endian, 
           represents the size of aes_key_material buffer;
         - size_aes_encrypted_buffer_compressed, integer 4Bytes little endian,
           represents the size of aes_compressed_encrypted_buffer;
         - size_aes_encrypted_buffer_original, integer 4Bytes little endian,
           represents the size original size ( decrypted and unconpressed ) of 
           aes_compressed_encrypted_buffer;
         - cmd_id, long 8Bytes little endian, represents an unique command identifier;
         - aes_key_material, byte array with dimension equal to size_rsa_wrapped_aes_key_material
           is encrypted with RSA public key of the client and contains three
           parameters used for encrypt aes_compressed_encrypted_buffer field:
            - AES_key, AES_KEY_BYTE key;
            - AES_iv, AES_IV_BYTE initial vector (default 16B length);
         - aes_compressed_encrypted_buffer, byte array with dimension equal to
           size_aes_encrypted_buffer_compressed, represent the data sends to 
           client, encrypted with AES CBC.
        """
        (aes_encrypted,aes_padding) = self.encrypt_AES(compressed_data)
        cmd_id = random.getrandbits(64)
        # !!! Integer fields have little endianess!!!
        return struct.pack("<LLLLQ%ds%ds" % ( self.RSA_KEY_LEN, len(aes_encrypted),),
              self.RSA_KEY_LEN, aes_padding, len(aes_encrypted), uncompressed_len,
              cmd_id, self.encrypt_AESKey(), aes_encrypted )
    
    def unpack(self, data):
        """Unpack and decrypt data received.

        Data inside a packet has this format:
         - size_rsa_wrapped_aes_key_material, integer 4Bytes little endian, 
           represents the size of aes_key_material buffer;
         - size_aes_padding, integer 4Bytes little endian, buffer is padded with
           these many bytes before encryption)
         - size_aes_encrypted_buffer_compressed, integer 4Bytes little endian,
           represents the size of aes_compressed_encrypted_buffer;
         - size_aes_encrypted_buffer_original, integer 4Bytes little endian,
           represents the size original size ( decrypted and unconpressed ) of 
           aes_compressed_encrypted_buffer;
         - cmd_id, long 8Bytes little endian, represents an unique command identifier;
         - aes_key_material, byte array with dimension equal to size_rsa_wrapped_aes_key_material
           is encrypted with RSA public key of the server and contains three
           parameters used for encrypt aes_compressed_encrypted_buffer field:
            - AES_key, AES_KEY_BYTE key;
            - AES_iv, AES_IV_BYTE initial vector (default 16B length);
         - aes_compressed_encrypted_buffer, byte array with dimension equal to
           size_aes_encrypted_buffer_compressed, represent the data sends to server,
           encrypted with AES CBC and compressed ( more details about compression
           in FLCompressor.py).
        """

        # !!! Integer fields have little endianess!!!
        size_rsa_wrapper_aes_key_material = struct.unpack('<L', data[:4])[0]
        aes_padding = struct.unpack('<L', data[4:8])[0]
        size_aes_encrypted_buffer_compressed = struct.unpack('<L', data[8:12])[0]
        size_aes_encrypted_buffer_original = struct.unpack('<L', data[12:16])[0]
        cmd_id = struct.unpack('<Q', data[16:24])[0]
        self.decrypt_AESkey(data[24:24+(size_rsa_wrapper_aes_key_material)])
        enc_buffer = data[24+size_rsa_wrapper_aes_key_material:24+size_rsa_wrapper_aes_key_material+size_aes_encrypted_buffer_compressed]
        """
        self.decrypt_AESkey(data[16:16+(size_rsa_wrapper_aes_key_material)])
        enc_buffer = data[16+size_rsa_wrapper_aes_key_material:16+size_rsa_wrapper_aes_key_material+size_aes_encrypted_buffer_compressed]
        """
        return (self.decrypt_AES(enc_buffer, aes_padding), size_aes_encrypted_buffer_original)

    def import_key_from_microsoft(self,file):
        """Read an RSA (private/public) key in Microsoft's format

        File must have the following structure:
        Common header ( in little endian):
            typedef struct _BCRYPT_RSAKEY_BLOB {
              ULONG Magic; -> Specifies the type of RSA key this BLOB represents.
              ULONG BitLength; -> The size, in bits, of the key.
              ULONG cbPublicExp; -> The size, in bytes, of the exponent of the 
                key.
              ULONG cbModulus; -> The size, in bytes, of the modulus of the key.
              ULONG cbPrime1; -> The size, in bytes, of the first prime number 
                of the key. This is only used for private key BLOBs.
              ULONG cbPrime2; -> The size, in bytes, of the second prime number
                of the key. This is only used for private key BLOBs.
            } BCRYPT_RSAKEY_BLOB;

        Possible values for Magic field:
          - Public   Key = 0x31415352L
          - Private  Key = 0x32415352L
          - Full RSA Key = 0x33415352L

        Data ( in big endian) changes in according of Magic field:
          - Public Key:
            BCRYPT_RSAKEY_BLOB{
              PublicExponent[cbPublicExp] 
              Modulus[cbModulus] 
            }

          - Private Key:
            BCRYPT_RSAKEY_BLOB{
              PublicExponent[cbPublicExp]
              Modulus[cbModulus]
              Prime1[cbPrime1]
              Prime2[cbPrime2]
            }

          - Full RSA Key
            BCRYPT_RSAKEY_BLOB{
              PublicExponent[cbPublicExp]
              Modulus[cbModulus]
              Prime1[cbPrime1]
              Prime2[cbPrime2]
              Exponent1[cbPrime1]
              Exponent2[cbPrime2]
              Coefficient[cbPrime1]
              PrivateExponent[cbModulus]
            }
        """
        with open(file) as f:
            blob = f.read()
        # header is in little endian...
        parsing_len = 24

        BCRYPT_RSAPUBLIC_MAGIC = 0x31415352L # Public key
        BCRYPT_RSAPRIVATE_MAGIC = 0x32415352L # Private Key
        BCRYPT_RSAFULLPRIVATE_MAGIC = 0x33415352L # FULL RSA Key

        header = struct.unpack("<LLLLLL",blob[:parsing_len])
        if ( header[0] < BCRYPT_RSAPUBLIC_MAGIC or
              header[0] > BCRYPT_RSAFULLPRIVATE_MAGIC):
          # !!!! ERROR !!!!
            raise ValueError('Magic number %d not supported' % (header[0]))

        # common part in each case
        # extract public exponent 'e'
        pubExp = struct.unpack(">%ds"%(header[2]),blob[parsing_len:parsing_len+header[2]])
        parsing_len += header[2]
        # extract module 'n'
        module = struct.unpack(">%ds"%(header[3]),blob[parsing_len:parsing_len+header[3]])
        parsing_len += header[3]
        (pubExp,module) = (long(binascii.hexlify(pubExp[0]),16),long(binascii.hexlify(module[0]),16))
        # check Magic field:
        if header[0] >= BCRYPT_RSAPRIVATE_MAGIC:
            (p,q) = struct.unpack(">%ds%ds"%(header[4],header[5]),
                             blob[parsing_len:parsing_len+header[4]+header[5]])
            (p,q) = (long(binascii.hexlify(p),16),long(binascii.hexlify(q),16))
            parsing_len += header[4] + header[5]
            # private exponent is not include in struct, we must find it!
            d = number.inverse( pubExp, (p-1)*(q-1))
            if header[0] == BCRYPT_RSAFULLPRIVATE_MAGIC:
                # add length of Exponent1, Exponent2 and Coefficient
                parsing_len += header[4] + header[5] + header[4]
                d = struct.unpack(">%ds" % (module), blob[parsing_len:parsing_len+module])
            # import as private key
            self.rsa_key = RSA.construct((module,pubExp, d,))
        else:
          # import as public key
          self.rsa_key = RSA.construct((module,pubExp,))
