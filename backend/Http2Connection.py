import sys
import binascii
import base64

from pymongo import MongoClient

from FLStream import Http2Stream
from Http2Frame import Http2Frame
from Http1Parser import parse_http1_message
import Utilities


class Http2Connection:
    HTTP_1_X = "HTTP/1"

    NEW_CONN = 0x0
    MANAGE_OK = 0x1
    TIME_FAIL = 0x2  # actually not used
    HTTP1_REQ = 0x4
    GENER_ERR = 0x8
    MINOR_STREAM = 0x10
    MISSED_BYTES = 0x20

    def __init__(self, act_pkt=None, raw_reference=None):
        self.stream_ids = {}
        self.referer = None
        self.packet = act_pkt
        self.act_ctx = [act_pkt[0]] if act_pkt else None
        self.acq_time = act_pkt[1][0]['time'] if act_pkt else 0
        self.raw_reference = raw_reference

    def reassembly_streams(self, remains=0):
        c2 = MongoClient('mongodb://localhost:27017/c2').c2.bufs
        # packet = c2.find_one({'_id': ObjectId(self.act_ctx)})
        packet = self.packet
        ret_val = self.MANAGE_OK
        if packet is not None:
            for pkt in self.packet[1]:
                if type(remains) is dict:  # ret_val == self.MISSED_BYTES:
                    ret_val, remains = self.__unpack__(base64.b64decode(remains['buf'] + pkt['buf']['buf']),
                                                       remains['dir'], pkt['buf']['seq'])
                else:
                    ret_val, remains = self.__unpack__(base64.b64decode(pkt['buf']['buf'])[remains:],
                                                       pkt['buf']['dir'], pkt['buf']['seq'])
                if ret_val == self.NEW_CONN:
                    return ret_val, remains  # i
                elif ret_val == self.GENER_ERR:
                    print "General error"
                    if type(remains) is dict:
                        ret_val, remains = self.__unpack__(base64.b64decode(pkt['buf']['buf']), pkt['buf']['dir'],
                                                           pkt['buf']['seq'])
        return ret_val, remains

    def __unpack__(self, buf, _dir, _seq):
        actual_pos = Http2Connection.check_magic(buf)
        if actual_pos == -1:
            # print "HTTP1 frame ", _dir
            http1 = parse_http1_message(_dir, buf)
            return self.HTTP1_REQ, 0
        if actual_pos >= 0 and not self.stream_ids:
            actual_pos += len(Http2Stream.HTTP_2)
        elif actual_pos is None:
            actual_pos = 0
        # elif actual_pos == 0 and self.stream_ids:
        #     return self.NEW_CONN, 0
        _continue = len(buf[actual_pos:]) >= 9  # at least one header
        while _continue:
            (_len, _type, _flags, _streamId) = Http2Stream.unpack_frame_header(buf[actual_pos:])
            if Http2Frame.is_valid_frame(_type, _streamId):
                # if self.check_stream_id(_streamId, _dir, _type):
                #     return self.NEW_CONN, actual_pos
                if _streamId not in self.stream_ids:
                    # found a new stream
                    self.stream_ids.update({_streamId: Http2Stream()})
                actual_pos += 9
                if len(buf[actual_pos:]) - _len < 0:
                    self.stream_ids[_streamId].add_frame(_type, _seq, buf[actual_pos:], _dir, _flags)
                    # clear end stream flag but maintains others
                    self.stream_ids[_streamId].set_flags(_dir, _type, -1, ~5)
                    return self.MISSED_BYTES, {'buf': base64.b64encode(Http2Stream.pack_frame_header(_len - len(buf[actual_pos:]), _type,
                                                                                    _flags, _streamId)),
                                               'dir': _dir, 'ctx': self.act_ctx, 'seq': _seq}
                try:
                    self.stream_ids[_streamId].add_frame(_type, _seq, buf[actual_pos:actual_pos + _len], _dir, _flags)
                except Exception:
                    self.stream_ids[_streamId].add_frame(Http2Frame.UNRECOGNIZED, _seq,
                                                         "Error on decoding: {}\nFail data:{}"
                                                         .format(sys.exc_info(), buf[actual_pos:actual_pos + _len]),
                                                         _dir)
                    print "Error on decoding: {}\nFailed data:{}".format(sys.exc_info(),
                                                                         buf[actual_pos:actual_pos + _len])
                actual_pos += _len
                if actual_pos + 9 > len(buf):  # at least a valid header
                    _continue = False
                    if len(buf[actual_pos:]) > 0:
                        self.stream_ids[_streamId].add_frame(Http2Frame.UNRECOGNIZED, _seq,
                                                             "Remaining data len {}<br>Remaining data {}".format(
                                                                 len(buf[actual_pos:]), binascii.hexlify(
                                                                     buf[actual_pos:])), _dir)
            else:  # no http2 frame or some error occurs
                return self.GENER_ERR, 0
        return self.MANAGE_OK, 0

    def get_referer(self):
        if self.referer:
            return self.referer
        else:
            for stream in self.stream_ids:
                headers = self.stream_ids[stream].follow_up_headers('request')
                headers_d = Http2Connection.from_list_to_dict(headers)
                if headers_d and (':authority' in headers_d or 'referer' in headers_d):
                    self.referer = Utilities.escape_http(
                        headers_d[':authority'] if ':authority' in headers_d else headers_d['referer'])
                    break  # one is just enough
                headers += self.stream_ids[stream].follow_up_headers('response')
                headers_d = Http2Connection.from_list_to_dict(headers)
                if headers_d and (':authority' in headers_d or 'referer' in headers_d):
                    self.referer = Utilities.escape_http(
                        headers_d[':authority'] if ':authority' in headers_d else headers_d['referer'])
                    break  # one is just enough
        return self.referer

    def get_raw_reference(self):
        return self.raw_reference

    def set_raw_reference(self, raw_reference):
        self.raw_reference = raw_reference

    def follow_stream_data(self):
        total_data = {'text': [], 'image': [], '???': []}
        for stream_id in self.stream_ids:
            header = self.stream_ids[stream_id].follow_up_headers('request')
            content_type, data = self.stream_ids[stream_id].follow_up_data(header, 'request')
            if data:
                if content_type in total_data:
                    total_data[content_type].append(data)
                else:
                    total_data['???'].append(data)
            header += self.stream_ids[stream_id].follow_up_headers('response')
            content_type, data = self.stream_ids[stream_id].follow_up_data(header, 'response')
            if data:
                if content_type in total_data:
                    total_data[content_type].append(data)
                else:
                    total_data['???'].append(data)
        return total_data

    def is_streams_finished(self):
        ret_val = True
        if self.stream_ids:
            for stream in self.stream_ids:
                ret_val &= self.stream_ids[stream].is_finish_flag_set()
        return ret_val

    def get_acq_time(self):
        return self.acq_time

    def get_ctx(self):
        return self.act_ctx

    def update_packet(self, pkt):
        self.packet = pkt

    def get_packet(self):
        return self.packet

    def check_stream_id(self, stream_id, _dir, _type):
        if stream_id > (max(self.stream_ids) if self.stream_ids else 0) or stream_id == 0:
            return False
        else:
            if stream_id in self.stream_ids:
                return self.stream_ids[stream_id].is_finish_flag_set(_dir, _type)
        return False

    def gets_unfinished_streams(self):
        def shadow_check(stream):
            return not self.stream_ids[stream].is_finish_flag_set('request') or \
                   not self.stream_ids[stream].is_finish_flag_set('response')

        return list(filter(shadow_check, self.stream_ids.keys() if 0 not in self.stream_ids
                    else self.stream_ids.keys()[1:]))

    def get_streams(self):
        return self.stream_ids.keys()

    def check_if_inside(self, other):
        aux_bool = {}
        ret_val = True
        for stream in self.stream_ids:
            if stream is not 0:  # 0 is used for connection exchanges, so it's never terminated
                if stream in other.stream_ids:
                    if other.stream_ids[stream].are_elements_present(['HEADERS', 'DATA'], 'request'):
                        aux_bool.update({stream: not self.stream_ids[stream].is_finish_flag_set('request')})
                    elif other.stream_ids[stream].are_elements_present(['HEADERS', 'DATA'], 'response'):
                        aux_bool.update({stream: not self.stream_ids[stream].is_finish_flag_set('response')})

        def shadow_lambda_check(pred, curr_idx):
            return pred and aux_bool[curr_idx]

        if self.stream_ids:
            for stream in other.stream_ids:
                if stream not in self.stream_ids and stream is not 0:  # case 0 see above
                    if stream < max(self.stream_ids):  # equal case excluded by above "not in" condition
                        ret_val = False
                        break
                if other.stream_ids[stream].are_elements_present(['HEADERS', 'DATA'], 'request') and \
                        stream in self.stream_ids and (
                        self.stream_ids[stream].get_seq('request') >= other.stream_ids[stream].get_seq('request') > -1):
                    ret_val = False
                    break
                elif other.stream_ids[stream].are_elements_present(['HEADERS', 'DATA'], 'response') and \
                        stream in self.stream_ids and (
                        self.stream_ids[stream].get_seq('response') >= other.stream_ids[stream].get_seq('response') > -1):
                    ret_val = False
                    break

        ret_val = reduce(shadow_lambda_check, aux_bool, ret_val)

        if ret_val:
            for stream in aux_bool:
                if stream in other.stream_ids:
                    self.stream_ids[stream].add_data(other.stream_ids[stream])
            # now add streams which aren't present in self.stream_ids
            for stream in other.stream_ids:
                if stream not in self.stream_ids:
                    self.stream_ids.update({stream: other.stream_ids[stream]})
            # other_ctx = other.get_ctx()
            # if other_ctx['request'] and (self.act_ctx['request'] != other_ctx['request']):
            #     if self.act_ctx['request']:
            #         self.act_ctx['request'] += other_ctx['request']
            #     else:
            #         self.act_ctx['request'] = other_ctx['request']
            # if other_ctx['response']:
            #     if self.act_ctx['response'] and (self.act_ctx['response'] != other_ctx['response']):
            #         self.act_ctx['response'] += other_ctx['response']
            #     else:
            #         self.act_ctx['response'] = other_ctx['response']
            if other.get_ctx():
                self.act_ctx.append(other.get_ctx())
        return ret_val

    def serialize(self):
        # ret_val = {'data': '', 'referer': self.get_referer(), 'unfinished_stream': [], 'ctx': self.act_ctx}
        # for stream in self.stream_ids:
        #     if stream is not 0:
        #         if not self.stream_ids[stream].is_finish_flag_set():
        #                 ret_val['unfinished_stream'].append(stream)
        ret_val = {'unfinished_stream': self.gets_unfinished_streams(),
                   'data': map(Http2Stream.serialize, self.stream_ids.items()),
                   'referer': self.get_referer(),
                   'acq_time': self.acq_time,
                   'ctx': self.act_ctx
                   }
        return ret_val

    @staticmethod  # TODO add consistency checks
    def deserialize(obj):
        ret_val = Http2Connection(raw_reference=obj['_id'])
        ret_val.act_ctx = obj['data']['ctx']
        ret_val.acq_time = obj['data']['acq_time']
        ret_val.referer = obj['data']['referer']
        ret_val.stream_ids = dict(map(Http2Stream.deserialize, obj['data']['data']))
        return ret_val

    @staticmethod
    def check_magic(buff):
        if buff.find(Http2Connection.HTTP_1_X) >= 0:
            return -1
        sm_off = buff.find(Http2Stream.HTTP_2)
        return sm_off if sm_off >= 0 else None

    @staticmethod
    def from_list_to_dict(_list):
        return {k: v for k, v in _list}
