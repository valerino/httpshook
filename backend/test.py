import os, struct

import unittest

from FLCryptographer import FLCryptographer
from FLCompressor import FLCompressor
from Cryptodome.Cipher import AES, PKCS1_v1_5
from Cryptodome.PublicKey import RSA
from Cryptodome import Random
import cStringIO
import lzo

class MyTest(unittest.TestCase):
    def save_key_on_file(self, key, key_type='priv'):
        if key_type is 'priv':
            f = open('falcon.priv','w')
        else:
            f = open('falcon.pub','w')
        f.write(key)
        f.close()

    def read_key_on_file(self, key_type='priv'):
        if key_type is 'priv':
            f = open('falcon.priv','r')
        else:
            f = open('falcon.pub','r')
        key = f.read()
        f.close()
        return RSA.importKey(key)

    def remove_key_file(self):
        if os.path.exists('falcon.priv'):
            os.remove('falcon.priv')
        if os.path.exists('falcon.pub'):
            os.remove('falcon.pub')

    def test_cryptoConstructor(self):
        flcFirst = FLCryptographer()
        self.assertTrue(os.path.isfile("falcon.priv"))
        firstGenerateKey = flcFirst.rsa_key
        flcSecond = FLCryptographer('./falcon.priv')
        self.assertEqual(firstGenerateKey.exportKey('PEM'),flcSecond.rsa_key.exportKey('PEM'))
        self.remove_key_file()

    def test_unpack(self):
        flc = FLCryptographer()
        aes_k = b'Thirty2 byte key'*2
        aes_iv = Random.new().read(16)
        plain_msg = b'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id.'
       
        cipher = AES.new(aes_k, AES.MODE_CBC, aes_iv)
        ciph_msg = cipher.encrypt(plain_msg)
        ciph_msg_len = len(ciph_msg)

        rsa_msg = struct.pack('<%ds%dsI'%(32,16,), aes_k, aes_iv, 45)
        f = open('falcon.pub','r')
        rsa_key = RSA.importKey(f.read())
        f.close()
        
        # Encrypt the session key with the public RSA key
        cipher_rsa = PKCS1_v1_5.new(rsa_key.publickey())
        rsa_msg_enc = cipher_rsa.encrypt(rsa_msg)

        total_msg = struct.pack("<III%ds%ds"%( len(rsa_msg_enc),len(ciph_msg),),
            FLCryptographer.RSA_KEY_LEN, 1, ciph_msg_len, rsa_msg_enc, ciph_msg)

        (unpack_data, original_len) = flc.unpack(total_msg)

        self.assertEqual(unpack_data, plain_msg)
        self.assertEqual(original_len, len(unpack_data))
        self.assertEqual(original_len, len(plain_msg))
        self.remove_key_file()

    def test_pack(self):
        plain_msg = b'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id.'

        rsa_key = RSA.generate(2048)
        self.save_key_on_file(rsa_key.publickey().exportKey('PEM'),'pub')
        flc = FLCryptographer('falcon.pub')

        packed_msg = flc.pack(plain_msg, len(plain_msg))

        size_rsa_wrapper_aes_key_material = struct.unpack('<I', packed_msg[:4])[0]
        size_aes_encrypted_buffer_compressed = struct.unpack('<I', packed_msg[4:8])[0]
        size_aes_encrypted_buffer_original = struct.unpack('<I', packed_msg[8:12])[0]
        cipher_rsa = PKCS1_v1_5.new(rsa_key)
        aes_key_plain = cipher_rsa.decrypt(packed_msg[12:12+size_rsa_wrapper_aes_key_material],1)
        aes_key_material = {
                            'key': aes_key_plain[:FLCryptographer.AES_KEY_BYTE],
                            'iv' : aes_key_plain[FLCryptographer.AES_KEY_BYTE:FLCryptographer.AES_KEY_BYTE+FLCryptographer.AES_IV_BYTE],
                            'pad': aes_key_plain[FLCryptographer.AES_KEY_BYTE+FLCryptographer.AES_IV_BYTE:]
                           }
        cipher = AES.new(aes_key_material['key'], AES.MODE_CBC, aes_key_material['iv'])
        aes_compressed_encrypted_buffer = packed_msg[12+size_rsa_wrapper_aes_key_material:]

        self.assertEqual(plain_msg, cipher.decrypt(aes_compressed_encrypted_buffer))

        self.remove_key_file()

    def test_uncompression(self):
        original = b'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        compressed_buf = lzo.compress(original,False)
        uncompr = FLCompressor().decompress(compressed_buf, len(original))
        self.assertEqual(uncompr,original)
        self.assertEqual(len(uncompr),len(original))

    def test_import_key(self):
        flc = FLCryptographer()
        flc.import_key_from_microsoft('/home/czoia/VirtualBox VMs/shared/falcon/poc/websrv/websrv/testkeys/srv_priv.bin')
        f = open('/home/czoia/projects/falcon/falcon-poc/shared/libutils/testbed/test_buffers/whole_post_buffer.bin')
        packed,original_size = flc.unpack(f.read())
        f = open('/home/czoia/projects/falcon/falcon-poc/shared/libutils/testbed/test_buffers/compressed.bin')
        read = f.read()
        f.close()
        self.assertEqual(len(read), len(packed))
        # self.assertEqual(FLCompressor().decompress(packed, original_size), read)

    def test_completeRecv(self):
        plain_msg = b'Lorem ipsu'
        server = FLCryptographer()
        server_rsa_pub = self.read_key_on_file('pub')
        client_aes = { 'key' : Random.new().read(32),
                       'iv'  : Random.new().read(16),
                       'pad' : 1}
        cipher = AES.new(client_aes['key'], AES.MODE_CBC, client_aes['iv'])
        ciph_txt = cipher.encrypt( lzo.compress(plain_msg, False, len(plain_msg)))
        aes_enc = server_rsa_pub.encrypt( struct.pack('<32s16sI',
                    client_aes['key'], client_aes['iv'], client_aes['pad']),0)
        (unpack_data,expected_len) = server.unpack(struct.pack('<III%ds%ds'%(len(aes_enc),len(ciph_txt)),len(aes_enc),
                                       len(ciph_txt),len(plain_msg),aes_enc,ciph_txt))

        self.assertEqual(expected_len, len(plain_msg))
        unpack_data = FLCompressor().decompress(unpack_data)
        # self.assertEqual(unpack_data,plain_msg)
        self.assertEqual(len(unpack_data), expected_len)
        self.remove_key_file()

if __name__ == '__main__':
    unittest.main()