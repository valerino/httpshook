Simple Exfiltration WebServer

Based on Flask MicroFramework, it has the simple framework to demonstrate the PoC:

- Serve the exploit with the Agent
- Save exfiltrated data from the Agent
- Show exfiltrated data to Operator

Install:

$ sudo apt install mongodb
$ sudo pip install flask
$ sudo pip install pymongo
$ sudo pip install Flask-PyMongo
$ sudo pip install pycryptodome
$ sudo apt install zlib1g-dev
$ sudo apt install liblzo2-dev
$ sudo pip install python-lzo
$ sudo pip install hpack

MongoDb name "c2"
Collection "bufs"


Run:

It binds on port 5000, default on address 0.0.0.0, it can be changed directly in the Main.

$ python websrv.py
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)

Before running webserver be sure that MongoDB daemon is up and running. If not just type

# service mongodb start

