import Utilities
import sys
import binascii
import hpack
import struct

from Http2Frame import Http2Frame


class Http2Stream:
    # Starting Http frame identifier
    HTTP_2 = "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"

    # Accepted content-types
    ACCEPTED_IMAGES = ['image/gif', 'image/jpeg', 'image/jpg', 'image/png',
                       'image/svg+xml', 'image/tiff', 'image/x-icon']
    ACCEPTED_TEXT = ['text/html', 'text/html;charset=UTF-8', 'text/plain',
                     'text/html;charset=utf-8', 'text/html;charset=iso-8859-1',
                     'text/plain;charset=utf-8', 'text/plain;charset=iso-8859-1',
                     'text/javascript', 'application/x-javascript;charset=utf-8',
                     'text/javascript;charset=utf-8', 'text/css', 'text/css;charset=utf-8',
                     'application/x-www-form-urlencoded', 'application/json',
                     'application/json;charset=utf-8', 'application/json;charset=iso-8859-1',
                     'application/xhtml+xml', 'application/xhtml+xml;charset=utf-8',
                     'application/xhtml+xml;charset=iso-8859-1', 'text/csv', 'text/csv;charset=utf-8',
                     'text/csv;charset=iso-8859-1', 'application/javascript;charset=utf-8',
                     'application/x-www-form-urlencoded;charset=utf-8', 'application/x-www-form-urlencoded',
                     'html', 'htm', 'xhtml', 'css', 'js', 'php', 'xml', 'csv']
    ACCEPTED_ENCODING = ['gzip', 'deflate', 'br']

    def __init__(self):
        self.frames = {
            'request': {
                'DATA': [],
                'HEADERS': [],
                'PRIORITY': [],
                'RST_STREAM': [],
                'SETTINGS': [],
                'PUSH_PROMISE': [],
                'PING': [],
                'GO_AWAY': [],
                'WINDOW_UPDATE': [],
                'CONTINUATION': [],
                'UNRECOGNIZED': [],
            },
            'response': {
                'DATA': [],
                'HEADERS': [],
                'PRIORITY': [],
                'RST_STREAM': [],
                'SETTINGS': [],
                'PUSH_PROMISE': [],
                'PING': [],
                'GO_AWAY': [],
                'WINDOW_UPDATE': [],
                'CONTINUATION': [],
                'UNRECOGNIZED': [],
            },
        }
        self.father = 0
        self.decoder = hpack.Decoder()

    def add_frame(self, frame_type, seq, frame, _dir, flags=0):
        try:
            if type(frame_type) is int:
                frame_type = Http2Frame.frame_code_to_string(frame_type)
            data_to_add = ''
            if frame_type is 'SETTINGS':
                for i in range(0, len(frame), 6):
                    identifier = int(binascii.hexlify(frame[i:i+2]), 16)
                    value = int(binascii.hexlify(frame[i+2:i+6]), 16)
                    data_to_add += '{{{}:{}}}'.format(identifier, value)
            elif frame_type is 'PRIORITY':
                self.father = int(binascii.hexlify(frame[0:4]), 16) & ~(1 << 31)
                data_to_add = frame
            else:
                data_to_add = frame
            self.frames[_dir][frame_type].append(Http2Frame(frame_type, seq, flags, data_to_add))
            return True
        except Exception as e:
            print "Some error occurs when try to add frame"
            print e
            exc_type, exc_value, exc_traceback = sys.exc_info()  # most recent (if any) by default
            print 'type {}, line nr {}'.format(exc_traceback.tb_lineno, exc_type.__name__)
            print "!!!! ERROR END   !!!!"
        return False

    def follow_up_headers(self, _dir):
        http_headers = ''
        for header in self.frames[_dir]['HEADERS']:
            flags = header.get_flags()
            header = header.get_data()
            pad_len = 0
            if flags & Http2Frame.PAD_FLAG:
                pad_len = int(binascii.hexlify(header[0]), 16)
                header = header[1:]  # pad len
            if flags & Http2Frame.PRIORITY_FLAG:
                header = header[5:]  # stream dependency and weight
            header_len = len(header)
            http_headers += (header[:header_len - pad_len])
        for header in self.frames[_dir]['CONTINUATION']:
            http_headers += header.get_data()
        if len(http_headers) > 0:
            try:
                return self.decoder.decode(http_headers)
            except Exception as e:
                print "Exception in decoding header"
                print e
                exc_type, exc_value, exc_traceback = sys.exc_info()  # most recent (if any) by default
                print 'line nr ', exc_traceback.tb_lineno
                print 'type', exc_type.__name__
                print "!!!! ERROR END   !!!!"
        return []

    def follow_up_data(self, http2_headers, _dir):
        total_data = ''
        headers_d = dict(http2_headers)
        content_length = None
        if 'content-length' in headers_d:
            try:
                content_length = int(headers_d['content-length'])
            except ValueError:
                headers_d.pop('content-length')

        for data in self.frames[_dir]['DATA']:
            flags = data.get_flags()
            data = data.get_data()
            pad_len = 0
            if flags & Http2Frame.PAD_FLAG:
                pad_len = int(binascii.hexlify(data[0]), 16)
                data = data[1:]
            data_len = len(data)
            # if content_length:
            #     delta = (len(total_data) + data_len - pad_len)
            #     if delta > content_length:
            #         delta -= content_length
            #         total_data += (data[:data_len - pad_len - delta])
            #         print "ALERT: Remaining data: {}".format(delta)
            #     else:
            #         total_data += (data[:data_len - pad_len])
            # else:
            total_data += (data[:data_len - pad_len])

        resource_type = headers_d['content-type'] if 'content-type' in headers_d \
            else Utilities.infer_type(total_data, headers_d[':path'] if ':path' in headers_d else None)

        correct, total_data = Utilities.decompress_content(headers_d['content-encoding'], total_data) \
            if 'content-encoding' in headers_d else Utilities.translate_data(total_data, resource_type)

        if resource_type and len(total_data) > 0:
            resource_type = resource_type.replace(" ", "").lower()
            if resource_type in Http2Stream.ACCEPTED_IMAGES:
                if not total_data.startswith('<img'):
                    correct, total_data = Utilities.prepare_img_data(resource_type, total_data)
                resource_type = 'image'
            elif resource_type in Http2Stream.ACCEPTED_TEXT:
                if not correct:
                    correct, total_data = Utilities.decompress_content(headers_d['content-encoding']
                                                                       if 'content-encoding' in headers_d
                                                                       else Utilities.infer_type(total_data),
                                                                       total_data)
                resource_type = 'text'
            elif resource_type in Http2Stream.ACCEPTED_ENCODING:
                resource_type = Utilities.infer_type(total_data, headers_d[':path'] if ':path' in headers_d else None)
                correct, total_data = Utilities.translate_data(total_data, resource_type)
            else:
                print "Unrecognized content-type {}".format(resource_type)
                correct = False
        else:
            resource_type = '???'
        try:
            if resource_type is '???' or not correct:
                total_data = total_data.decode('UTF-8')
        except UnicodeDecodeError:
            total_data = binascii.hexlify(total_data)
        return resource_type, total_data

    def is_finish_flag_set(self, _dir):
        if self.frames[_dir]['DATA']:
            return (self.frames[_dir]['DATA'][-1].get_flags() & Http2Frame.END_STREAM_FLAG) \
                   == Http2Frame.END_STREAM_FLAG
        if self.frames[_dir]['HEADERS']:
            return (self.frames[_dir]['HEADERS'][-1].get_flags() & Http2Frame.END_STREAM_FLAG) \
                   == Http2Frame.END_STREAM_FLAG
        return False

    def is_finish_headers(self, _dir):
        if self.frames[_dir]['HEADERS']:
            return (self.frames[_dir]['HEADERS'][-1].get_flags() & Http2Frame.END_HEADERS_FLAG) \
                   == Http2Frame.END_HEADERS_FLAG
        return False

    def set_flags(self, _dir, _type, pos, value):
        if type(_type) is int:
            _type = Http2Frame.frame_code_to_string(_type)
        if _dir in self.frames and _type in self.frames[_dir] and len(self.frames[_dir]) > 0:
            self.frames[_dir][_type][pos].set_flags(value)

    def get_father(self):
        return self.father

    def add_data(self, data):
        for elem in data.frames['request']:
            self.frames['request'][elem] += data.frames['request'][elem]
        for elem in data.frames['response']:
            self.frames['response'][elem] += data.frames['response'][elem]

    def are_elements_present(self, _type, _dir):
        if type(_type) is not list:
            _type = [_type]
        if type(_dir) is not list:
            _dir = [_dir]
        ret_val = False
        for x in _dir:
            for y in _type:
                ret_val |= len(self.frames[x][y]) > 0
        return ret_val

    def get_seq(self, _dir=None):
        ret_val = -1  # seq must be positive, -1 is not accepted value so we are sure we are safe
        if _dir:
            for frame in self.frames[_dir]:
                if self.frames[_dir][frame]:
                    aux_max = max(self.frames[_dir][frame], key=lambda x: x.get_seq()).get_seq()
                    if aux_max > ret_val:
                        ret_val = aux_max
        else:
            for frame in self.frames['request']:
                if self.frames['request'][frame]:
                    aux_max = max(self.frames['request'][frame], key=lambda x: x.get_seq()).get_seq()
                    if aux_max > ret_val:
                        ret_val = aux_max
            for frame in self.frames['response']:
                if self.frames['response'][frame]:
                    aux_max = max(self.frames['response'][frame], key=lambda x: x.get_seq()).get_seq()
                    if aux_max > ret_val:
                        ret_val = aux_max
        return ret_val

    @staticmethod
    def serialize(obj):
        ret_val = {
            'request': {
                'DATA': [],
                'HEADERS': [],
                'PRIORITY': [],
                'RST_STREAM': [],
                'SETTINGS': [],
                'PUSH_PROMISE': [],
                'PING': [],
                'GO_AWAY': [],
                'WINDOW_UPDATE': [],
                'CONTINUATION': [],
                'UNRECOGNIZED': [],
            },
            'response': {
                'DATA': [],
                'HEADERS': [],
                'PRIORITY': [],
                'RST_STREAM': [],
                'SETTINGS': [],
                'PUSH_PROMISE': [],
                'PING': [],
                'GO_AWAY': [],
                'WINDOW_UPDATE': [],
                'CONTINUATION': [],
                'UNRECOGNIZED': [],
            },
            'stream_id': obj[0]
        }

        for frame in obj[1].frames['request']:
            ret_val['request'][frame] = map(Http2Frame.serialize, obj[1].frames['request'][frame])
            ret_val['response'][frame] = map(Http2Frame.serialize, obj[1].frames['response'][frame])
        return ret_val

    @staticmethod
    def deserialize(obj):
        ret_val = Http2Stream()
        for frame in obj['request']:
            ret_val.frames['request'][frame] = map(Http2Frame.deserialize, obj['request'][frame])
            ret_val.frames['response'][frame] = map(Http2Frame.deserialize, obj['response'][frame])
        return obj['stream_id'], ret_val

    @staticmethod
    def unpack_frame_header(buff):
        _len = int(binascii.hexlify(buff[:3]), 16)
        _type = int(binascii.hexlify(buff[3]), 16)
        _flags = int(binascii.hexlify(buff[4]), 16)
        _streamId = int(binascii.hexlify(buff[5:9]), 16)
        return _len, _type, _flags, _streamId

    @staticmethod
    def pack_frame_header(_len, _type, _flags, _stream_id):
        return binascii.unhexlify(
            struct.pack('>18s', '{0:06x}{1:02x}{2:02x}{3:08x}'.format(_len, _type, _flags, _stream_id)))

    @staticmethod
    def exclude_header_metadata(http2_headers, _dir):
        aux = http2_headers[:]  # prevents removing elements from parameter
        # the following metadata headers are exclusive of the other counterpart
        # e.g. elements in 'response' must be present only in 'request' header and viceversa
        exclude_headers = {'response': [':method', ':scheme', ':authority', ':path'],
                           'request': [':status']}
        for metadata in exclude_headers[_dir]:
            for k, v in aux:
                if metadata == k:
                    aux.remove((k, v))
        return aux
