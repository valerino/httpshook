import sys, io, binascii
import gzip, hpack, base64
from bson.objectid import ObjectId
from pymongo import MongoClient


"""@package FalconServer

"""
class FLHttpDecoder():
    # Frame types
    DATA_FRAME          = 0x00
    HEADERS_FRAME       = 0x01
    PRIORITY_FRAME      = 0x02
    RST_STREAM_FRAME    = 0x03
    SETTINGS_FRAME      = 0x04
    PUSH_PROMISE_FRAME  = 0x05
    PING_FRAME          = 0x06
    GO_AWAY_FRAME       = 0x07
    WINDOW_UPDATE_FRAME = 0x08
    CONTINUATION_FRAME  = 0x09

    # Flags values
    END_STREAM_FLAG     = 0x01
    END_HEADERS_FLAG    = 0X04
    PAD_FLAG            = 0x08
    PRIORITY_FLAG       = 0x20

    # Starting Http frame identifier
    HTTP_1_X = "HTTP/1"
    HTTP_2   = "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"

    # Accepted content-types
    ACCEPTED_IMAGES = ['image/gif', 'image/jpeg', 'image/png', 'image/svg+xml']
    ACCEPTED_TEXT   = ['text/html', 'text/html;charset=UTF-8', 'text/plain', 
                        'text/plain;charset=UTF-8', 'text/javascript;charset=UTF-8', 
                        'text/css;charset=UTF-8']

    def __init__(self):
        self.decoder = {
            FLHttpDecoder.DATA_FRAME          : self.decodeDataFrame,
            FLHttpDecoder.HEADERS_FRAME       : self.decodeHeaderFrame,
            FLHttpDecoder.PRIORITY_FRAME      : self.decodePriorityFrame,
            FLHttpDecoder.RST_STREAM_FRAME    : self.decodeRSTStream,
            FLHttpDecoder.SETTINGS_FRAME      : self.decodeSettingFrame,
            FLHttpDecoder.PUSH_PROMISE_FRAME  : self.decodePushPromiseFrame,
            FLHttpDecoder.PING_FRAME          : self.decodePingFrame,
            FLHttpDecoder.GO_AWAY_FRAME       : self.decodeGoAwayFrame,
            FLHttpDecoder.WINDOW_UPDATE_FRAME : self.decodeWinUpdFrame,
            FLHttpDecoder.CONTINUATION_FRAME  : self.decodeContinuationFrame,
        }
        self.out = ''
        self.hDecoder = hpack.Decoder()
        self.mongo = MongoClient('mongodb://localhost:27017/c2').c2.bufs
        self.isHttp2 = False
        self.headers = {}
        self.imageData = b''
        self.jsData    = ''
        self.cssData   = ''
        self.htmlData  = ''

    def qualcosa(self, packet):
        aux = self.discriminateAndExcludeHttp(packet)
        _continue = True
        actualPos = 0
        while _continue:
            (_len, _type, _flags, _streamId) = self.decodePacket(aux[actualPos:])
            if _type in self.decoder or self.isHttp2:
                actualPos+=9 # header length
                try:
                    self.decoder[_type](aux[actualPos:actualPos+_len], _flags)
                except Exception:
                    self.out += "Something Error: {}".format(sys.exc_info())
                    self.out += "Failed data:"
                    self.out +="<pre>{}</pre>".format(aux[actualPos:actualPos+_len])
                actualPos+=_len
            else:
                self.out += "Not http2 frame. Raw data:"
                self.out += "<pre>{}</pre>".format(aux[actualPos:])
                actualPos = len(aux)
            if actualPos+9 > len(aux): # at least a valid header
                _continue = False
                self.out += "Remaining data len {}<br>Remaining data {}".format(len(aux[actualPos:]),binascii.hexlify(aux[actualPos:]))
        return self.out

    def gunzip_bytes_obj(self, bytes_obj):
        in_ = io.BytesIO()
        in_.write(bytes_obj)
        in_.seek(0)
        gunzipped_bytes_obj = b''
        with gzip.GzipFile(fileobj=in_, mode='rb') as fo:
            gunzipped_bytes_obj = fo.read()
        return gunzipped_bytes_obj

    def assembly(self, objectId):
        x = self.mongo.find_one({'_id' : ObjectId(objectId)})
        _in  = sorted( x['in'], key=lambda buf: buf['seq'])
        _out = sorted( x['out'], key=lambda buf: buf['seq'])
        total_in  = b''
        total_out = b''
        if len(_in):
            for y in _in:
                total_in += base64.b64decode(y['buf'])
        if len(_out):
            for y in _out:
                total_out += base64.b64decode(y['buf'])
        return (total_in, total_out)

    def discriminateAndExcludeHttp(self, buff):
        sm_off = buff.find(self.HTTP_2)
        if sm_off < 0:
            if buff.find(self.HTTP_1_X) > -1:
                print "Is HTTP/1.x"
                self.isHttp2 = False
            return buff
        else:
            self.isHttp2 = True
            # if magic is found, then a new session has begun!
            self.flush()
            self.hDecoder = hpack.Decoder()
            self.headers = {}
            sm_off += len(self.HTTP_2)
            self.out+="Is http2"
            return buff[sm_off:]

    def decodePacket(self, buff):
        _len      = int(binascii.hexlify(buff[:3]), 16)
        _type     = int(binascii.hexlify(buff[3]), 16)
        _flags    = int(binascii.hexlify(buff[4]), 16)
        _streamId = int(binascii.hexlify(buff[5:9]), 16)
        self.out += "<h4>Generic header start</h4><pre>"
        self.out += "\n  Packet  len          {:d}".format(_len)
        self.out += "\n  Packet type          {:d}".format(_type)
        self.out += "\n  Flags                {:d}".format(_flags)
        self.out += "\n  Stream ID            {:d}".format(_streamId)
        self.out += "\n  Payload              {}".format(binascii.hexlify(buff[9:9+_len]))
        self.out += "\n  Remaining data (len) {:d}".format(len(buff[9+_len:]))
        self.out += "\n  Remaining data       {}".format(binascii.hexlify(buff[9+_len:]))
        self.out += "</pre><h4>Generic header end</h4>"
        return _len, _type, _flags, _streamId

    def decodeDataFrame(self, frame_payload, flags):
        """
         +---------------+
         |Pad Length? (8)|
         +---------------+-----------------------------------------------+
         |                            Data (*)                         ...
         +---------------------------------------------------------------+
         |                           Padding (*)                       ...
         +---------------------------------------------------------------+
        """
        END_STREAM_FLAG = 0x01
        PAD_FLAG = 0x08
        self.out += "<h4>Data START (len {:d})</h4>".format(len(frame_payload))
        self.out += "<pre>Check for custom flags: ({:x})".format(flags)
        if flags & END_STREAM_FLAG:
            self.out += "\n  END STREAM"
        if flags & PAD_FLAG:
            self.out += "\n  PADDED"
        pad_len = 0
        if flags & self.PAD_FLAG:
            pad_len = int(binascii.hexlify(frame_payload[0]), 16)
            frame_payload = frame_payload[1:]
            self.out += "\n  Pad length: {:d}".format(pad_len)
        frame_len = len(frame_payload)
        self.out += "\nData: {}".format(binascii.hexlify(frame_payload[:frame_len-pad_len]))
        self.out += "</pre>"

        if 'content-type' in self.headers:
            if self.headers['content-type'].replace(" ", "") in self.ACCEPTED_TEXT:
                if 'content-encoding' in self.headers:
                    if 'gzip' in self.headers['content-encoding']:
                        self.out += "\nUncompressed data gzip({:d},{:d}, {:d}):\n".format(frame_len, pad_len,frame_len-pad_len)
                        if 'javascript' in self.headers['content-type'].lower():
                            self.addJavascript(frame_payload[:frame_len-pad_len])
                        elif 'css' in self.headers['content-type'].lower():
                            self.addCSS(frame_payload[:frame_len-pad_len])
                        else:
                            self.addHTML(frame_payload[:frame_len-pad_len])
                    else:
                        self.out += "\nUncompressed data:\n"
                        self.out += compressionType(binascii.hexlify(frame_payload[:frame_len-pad_len]))
                else:
                    self.out += "\nNo content encoding found in headers"
            elif self.headers['content-type'] in self.ACCEPTED_IMAGES:
                self.imageData += frame_payload[:frame_len-pad_len]
                self.out += "Image len: {}, image len header {}".format(len(self.imageData), self.headers['content-length'] if 'content-length' in self.headers else -1)
                if len(self.imageData) >= int(self.headers['content-length']  if 'content-length' in self.headers else 0):
                    self.addImage()
            else :
                self.out += "\nUnrecognized content-type({}):\n".format(self.headers['content-type'])
                self.out += "<pre>{}</pre>".format(binascii.hexlify(frame_payload[:frame_len-pad_len]))
        else:
            self.out += "\nUnrecognized data:\n"
            self.out += "<pre>{}</pre>".format(binascii.hexlify(frame_payload[:frame_len-pad_len]))
        if flags & PAD_FLAG:
            self.out += "\n  Padding: "+binascii.hexlify(frame_payload[frame_len-pad_len:])
        self.out += "<h4>Data END</h4>"

    def decodeHeaderFrame(self, frame_payload, flags ):
        """
         +---------------+
         |Pad Length? (8)|
         +-+-------------+-----------------------------------------------+
         |E|                 Stream Dependency? (31)                     |
         +-+-------------+-----------------------------------------------+
         |  Weight? (8)  |
         +-+-------------+-----------------------------------------------+
         |                   Header Block Fragment (*)                 ...
         +---------------------------------------------------------------+
         |                           Padding (*)                       ...
         +---------------------------------------------------------------+
        """
        self.out += "<h4>Headers START</h4>"
        self.out += "<pre>Check for custom flags: ({0:x} ({0:b}) )".format(flags)
        pad_len = 0
        if flags & self.END_STREAM_FLAG:
            self.out += "\n  END STREAM"
        if flags & self.END_HEADERS_FLAG:
            self.out += "\n  END HEADERS"
        if flags & self.PAD_FLAG:
            pad_len = int(binascii.hexlify(frame_payload[0]),16)
            self.out += "\n  PADDED, len = {:x}".format(pad_len)
            frame_payload = frame_payload[1:]
        if flags & self.PRIORITY_FLAG:
            self.out += "\n  PRIORITY, depends on {:d}".format(int(binascii.hexlify(frame_payload[:4]),16))
            frame_payload = frame_payload[4:]
        frame_len = len(frame_payload)
        try:
            aux_dict = dict((aux[0],aux[1]) for aux in self.hDecoder.decode(frame_payload[:frame_len-pad_len]))
            if len(self.headers) == 0:
                self.headers = aux_dict
            else:
                for key in aux_dict:
                    self.out += "\nKey {} aux_value {}".format(key,aux_dict[key])
                    if key == 'content-type' and key in self.headers and self.headers[key] != aux_dict[key]:
                        if ( self.headers['content-type'] in self.ACCEPTED_IMAGES 
                             and len(self.imageData) > 0):
                            self.out += "Possible incomplete images, added because of change header type..,"
                            self.addImage()
                    self.headers[key] = aux_dict[key]
            self.out += ''.join(["\n {{ {}:{} }}".format(k,v) for k,v in self.headers.iteritems()])
            if flags & self.PAD_FLAG:
                self.out += "\n  Padding: "+binascii.hexlify(frame_payload[frame_len-pad_len:])
        except:
            self.out += "\nHeader parser error {}".format(sys.exc_info())
        self.out += "</pre><h4>Headers END</h4>"

    def decodePriorityFrame(self, frame_payload, flags=None):
        """
         +-+-------------------------------------------------------------+
         |E|                  Stream Dependency (31)                     |
         +-+-------------+-----------------------------------------------+
         |   Weight (8)  |
         +-+-------------+
        """
        self.out += "<h4>Priority START</h4><pre>"
        self.out += "\n  Stream dependency: {:d}".format(int(binascii.hexlify(frame_payload[:4]),16))
        self.out += "\n  Weight           : {:d}".format(int(binascii.hexlify(frame_payload[5]),16))
        self.out += "</pre><h4>Priority END</h4>"

    def decodeRSTStream(self, frame_payload, flags=None):
        """
         +---------------------------------------------------------------+
         |                        Error Code (32)                        |
         +---------------------------------------------------------------+
        """
        self.out += "<h4>RST STREAM START</h4><pre>"
        self.out += "\n  Error code: {:d}".format(int(binascii.hexlify(frame_payload),16))
        self.out += "</pre><h4>RST STREAM END</h4>"

    def decodeSettingFrame(self, frame_payload, flags=None):
        """
         +-------------------------------+
         |       Identifier (16)         |
         +-------------------------------+-------------------------------+
         |                        Value (32)                             |
         +---------------------------------------------------------------+
        """
        chunks = [frame_payload[i:i+6] for i in range(0,len(frame_payload),6)]
        self.out += "<h4>Setting START (total settings {:d})</h4><pre>".format(len(chunks))
        if flags is not None and flags != 0:
            self.out += "<h5>Flags</h5>ACK: {}".format(flags)
        for chunk in chunks:
            identifier = int(binascii.hexlify(chunk[:2]),16)
            val = int(binascii.hexlify(chunk[2:]),16)
            if identifier == 0x01:
                self.out += "\n  SETTINGS_HEADER_TABLE_SIZE {:d}".format(val)
            elif identifier == 0x02:
                self.out += "\n  SETTINGS_ENABLE_PUSH {:d}".format(val)
            elif identifier == 0x03:
                self.out += "\n  SETTINGS_MAX_CONCURRENT_STREAMS {:d}".format(val)
            elif identifier == 0x04:
                self.out += "\n  SETTINGS_INITIAL_WINDOW_SIZE {:d}".format(val)
            elif identifier == 0x05:
                self.out += "\n  SETTINGS_MAX_FRAME_SIZE {:d}".format(val)
            elif identifier == 0x06:
                self.out += "\n  SETTINGS_MAX_HEADER_LIST_SIZE {:d}".format(val)
            else:
                self.out += "\n  error decoding identifier {0:d} with value {1:d}".format(identifier,val)
        self.out += "</pre><h4>Setting END</h4>"

    def decodePushPromiseFrame(self, frame_payload, flags=None):
        self.out += "<h4>Push Promise START</h4><pre>\n"
        self.out += self.hDecoder.decode(frame_payload)
        self.out += "</pre><h4>Push Promise END</h4>"

    def decodePingFrame(self, frame_payload, flag):
        self.out += "<h4>Ping START</h4><pre>"
        if flag & 0x01:
            self.out += "\n  Ping response"
        else:
            self.out += "\n  Ping request"
        self.out += "\n  Data (opaque): {}".format(binascii.hexlify(frame_payload))
        self.out += "</pre><h4>Ping END</h4>"

    def decodeGoAwayFrame(self, frame_payload, flags=None):
        self.out += "<h4>Go Away START</h4><pre>"
        self.out += "\n  Last stream-ID : {:d}".format(int(binascii.hexlify(frame_payload[:4]),16))
        self.out += "\n  Error Code     : {:d}".format(int(binascii.hexlify(frame_payload[4:8]),16))
        if len(frame_payload) > 8:
            self.out += "\n  Debug data : {}".format(binascii.hexlify(frame_payload[8:]))
        self.out += "</pre><h4>Go Away END</h4>"

    def decodeWinUpdFrame(self, frame_payload, flags=None):
        self.out += "<h4>Window Update START</h4><pre>"
        self.out += "\n  Window size increment: {}".format(int(binascii.hexlify(frame_payload[:4]),16))
        self.out += "</pre><h4>Window Update END</h4>"

    def decodeContinuationFrame(self, frame_payload, flags=None):
        self.out += "<h4>Continuation START</h4><pre>\n"
        self.out += self.hDecoder.decode(frame_payload)
        self.out += "</pre><h4>Continuation END</h4>"

    def getOut(self):
        return self.out

    def resetOut(self):
        self.out = b''

    def addImage(self):
        self.out += "<img src='data:{};base64,{}'>".format(self.headers['content-type'],base64.b64encode(self.imageData))
        self.imageData = b''

    def addJavascript(self, frame=None, flush=False):
        if frame:
            self.jsData += frame
            self.out += "jsData len: {}, jsData len header {}".format(len(self.jsData), self.headers['content-length'] if 'content-length' in self.headers else -1)
        if len(self.jsData) >= int(self.headers['content-length'] if 'content-length' in self.headers else 0) or flush:
            self.out += "<div style='overflow:auto; height: 150px; border: 1px solid black;'><pre>"
            try:
                self.out += "Javascript\n"
                aux_text = self.gunzip_bytes_obj(self.jsData)
                aux_text = aux_text.replace("<","&lt;")
                aux_text = aux_text.replace(">","&gt;")
                self.out += aux_text
            except Exception:
                self.out += "Decode JS frame error: {}".format(sys.exc_info())
                self.out += "\nFailed data:\n{}".format(binascii.hexlify(self.jsData))
            self.out += "</div></pre>"
            self.jsData = ''

    def addCSS(self, frame=None, flush=False):
        if frame:
            self.cssData += frame
            self.out += "cssData len: {}, cssData len header {}".format(len(self.cssData), self.headers['content-length'] if 'content-length' in self.headers else -1)
        if len(self.cssData) >= int(self.headers['content-length'] if 'content-length' in self.headers else 0) or flush:
            self.out += "<div style='overflow:auto; height: 150px; border: 1px solid black;'><pre>"
            try:
                self.out += "CSS\n"
                aux_text = self.gunzip_bytes_obj(self.cssData)
                aux_text = aux_text.replace("<","&lt;")
                aux_text = aux_text.replace(">","&gt;")
                self.out += aux_text
            except Exception:
                self.out += "Decode CSS frame error: {}".format(sys.exc_info())
                self.out += "\nFailed data:\n{}".format(binascii.hexlify(self.cssData))
            self.out += "</div></pre>"
            self.cssData = ''

    def addHTML(self, frame=None, flush=False):
        if frame:
            self.htmlData += frame
            self.out += "htmlData len: {}, htmlData len header {}".format(len(self.htmlData), self.headers['content-length'] if 'content-length' in self.headers else -1)
        if len(self.htmlData) >= int(self.headers['content-length'] if 'content-length' in self.headers else 0) or flush:
            try:
                self.out += "<div style='overflow:auto; height: 150px; border: 1px solid black;'><pre>"
                self.out += self.gunzip_bytes_obj(self.htmlData)
            except Exception:
                self.out += "Decode HTML frame error: {}".format(sys.exc_info())
                self.out += "Failed data:"
                self.out +="{}".format(binascii.hexlify(self.htmlData))
            self.out += "</pre></div>"
            self.htmlData = ''

    def flush(self):
        if len(self.htmlData) > 0:
            self.out += "Possible incomplete htmls, added because incomplete data are requested...."
            self.addHTML(flush=True)
        if len(self.imageData) > 0:
            self.out += "Possible incomplete images, added because incomplete data are requested...."
            self.addImage()
        if len(self.cssData) > 0:
            self.out += "Possible incomplete CSS, added because incomplete data are requested...."
            self.addCSS(flush=True)
        if len(self.jsData) > 0:
            self.out += "Possible incomplete js, added because incomplete data are requested...."
            self.addJavascript()