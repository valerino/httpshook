import lzo


"""@package FalconServer
FLCompressor handles the compressed packet received from an agent an tries 
uncompress it.

The incoming packet is compressed using Windows LZNT1 compression algorithm
(see https://msdn.microsoft.com/en-us/library/jj665697.aspx), this class is 
just a wrapper for python lzo library (https://pypi.python.org/pypi/python-lzo/1.11).
"""
class FLCompressor():

    def __init__(self):
        pass

    def decompress(self,data, original_size):
        """Function for decompress data

        This function decompress data passed as input with lznt1 algorithm.
        Return uncompressed data.
        """
        return lzo.decompress(data, False, original_size)

    def compress(self,data):
        """Function for compress data

        This function compress data passed as input with lznt1 algorithm.
        Returns a tuple composed by compressed data without header metadata
        """
        return lzo.compress(data, 1, False)