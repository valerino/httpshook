import base64
import binascii
import brotli
import gzip
import io
import zlib


def gzip_unzip(bytes_obj):
    in_ = io.BytesIO()
    in_.write(bytes_obj)
    in_.seek(0)
    with gzip.GzipFile(fileobj=in_, mode='rb') as fo:
        gunzipped_bytes_obj = fo.read(len(bytes_obj))
    return gunzipped_bytes_obj


def decompress_content(encoding, total_data):
    try:
        if 'gzip' in encoding:
            return True, gzip_unzip(total_data if not total_data.startswith('1f8b') else binascii.unhexlify(total_data))
        elif 'deflate' in encoding or 'zlib' in encoding:
            return True, zlib.decompress(total_data[2:-4])
        elif 'br' in encoding:
            return True, brotli.decompress(total_data)
        elif 'compress' in encoding:
            return False, total_data  # TODO self.lzw_unzip(total_data)
    except Exception:
        pass
    return False, total_data


def prepare_img_data(encoding, total_data):
    if 'image' not in encoding:
        encoding += 'image/'
    return True, "<img src='data:{};base64,{}'>".format(encoding, base64.b64encode(total_data))


def prepare_txt_data(encoding, total_data):  # first parameter only for dictionary uses
    return True, total_data.replace('<script', '&lt;;script').replace("</script", "&lt;script")


def infer_type(data=None, path=None):
    if data:
        magic = {
            '\x47\x49\x46\x38\x37\x61': 'image/gif',
            '\x47\x49\x46\x38\x39\x61': 'image/gif',
            '\xFF\xD8\xFF': 'image/jpeg',
            '\x89\x50\x4E\x47\x0D\x0A\x1A\x0A': 'image/png',
            '\x42\x40': 'image/bmp',
            '\x3c\x3f\x78\x6d\x6c\x20': 'xml',
            '\x78\x01': 'zlib',
            '\x78\x9C': 'zlib',
            '\x78\xDA': 'zlib',
            '\x1F\x8B': 'gzip',
            '\x3C\x3F\x78\x6D\x6C\x20': 'xml',
        }
        for number in magic:
            if data.startswith(number) or data.startswith(binascii.hexlify(number)):
                return magic[number]
    if path:
        path = path.lower()
        splitted_path = path.split('.')
        if len(splitted_path) > 1:
            splitted_path = splitted_path[1].split('?')
            if splitted_path[0] in ['html', 'htm', 'xhtml', 'css', 'js', 'php', 'xml', 'csv']:
                return splitted_path[0]
            elif splitted_path[0] in ['png', 'jpg', 'jpeg', 'gif', 'svg', 'bmp', 'thm']:
                return 'image/'+splitted_path[0]
    return None


def translate_data(data, type_of_magic):
    from_magic = {
        'image/gif': prepare_img_data,
        'image/jpg': prepare_img_data,
        'image/jpeg': prepare_img_data,
        'image/svg': prepare_img_data,
        'image/bmp': prepare_img_data,
        'image/png': prepare_img_data,
        'image': prepare_img_data,
        'xml': prepare_txt_data,
        'text': prepare_txt_data,
        'zlib': decompress_content,
        'gzip': decompress_content,
    }
    if type_of_magic and data:
        if len(data) > 0:
            if type_of_magic in from_magic:
                return from_magic[type_of_magic](type_of_magic, data)
            # some images have charset included in resource type...we need to split
            type_of_magic = type_of_magic.split(';')[0]
            if type_of_magic in from_magic:
                return from_magic[type_of_magic](type_of_magic, data)
    return False, data


def escape_http(referer):
    import re
    m = re.search('https?://(.+)/$', referer)
    return m.group(1) if m else referer
