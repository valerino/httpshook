from BaseHTTPServer import BaseHTTPRequestHandler
from httplib import HTTPResponse
from StringIO import StringIO

"""
N.B. the backdoor modify some fields of HTTP1 header, like:
user-agent -> from Mozilla to Gozilla
accept-encoding -> accept-emcoding 
"""


def parse_http1_message(_dir, data):
    if _dir is 'request':
        print "Request HTTP1"
        request = Request(data)
        return request.get_header()
    elif _dir is 'response':
        response = HTTPResponse(Response(data))
        response.begin()
        print "Response HTTP1"
        # TODO can be that http1 header is splitted in more than one packet, so the remaining line
        # can causes an exception. For now, comment!
        # print response.read(len(data))
        # print response.getheaders()
        return "Http1"  # response.getheaders()


class Response:
    def __init__(self, data):
        self.data = StringIO(data)

    def makefile(self, *args, **kwargs):
        return self.data


class Request(BaseHTTPRequestHandler):

    def __init__(self, request):
        self.rfile = StringIO(request)
        self.raw_requestline = self.rfile.readline()
        self.parse_request()

    def get_header(self):
        for k in self.headers:
            print "{} : {}".format(k, self.headers[k])
        return self.headers
