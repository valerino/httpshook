import binascii


class Http2Frame:
    # Max Stream ID
    MAX_STREAM_ID = 0x7FFFFFFF

    # Frame types
    DATA_FRAME          = 0x00
    HEADERS_FRAME       = 0x01
    PRIORITY_FRAME      = 0x02
    RST_STREAM_FRAME    = 0x03
    SETTINGS_FRAME      = 0x04
    PUSH_PROMISE_FRAME  = 0x05
    PING_FRAME          = 0x06
    GO_AWAY_FRAME       = 0x07
    WINDOW_UPDATE_FRAME = 0x08
    CONTINUATION_FRAME  = 0x09
    UNRECOGNIZED        = 0x0A

    # Flags values
    END_STREAM_FLAG     = 0x01
    ACK_FLAG            = 0x01
    END_HEADERS_FLAG    = 0x04
    PAD_FLAG            = 0x08
    PRIORITY_FLAG       = 0x20

    def __init__(self, frame_type, seq, flags=0, frame_data=None):
        self.type = frame_type
        self.data = frame_data
        self.flags = flags
        self.seq = seq

    def get_type(self):
        return self.type

    def get_data(self):
        return self.data

    def get_flags(self):
        return self.flags

    def set_flags(self, flag):
        self.flags &= flag

    def get_seq(self):
        return self.seq

    @staticmethod
    def serialize(obj):
        return {'type': obj.type, 'seq': obj.seq, 'flags': obj.flags, 'data': binascii.hexlify(obj.data)}

    @staticmethod
    def deserialize(obj):
        return Http2Frame(obj['type'], obj['seq'], obj['flags'], binascii.unhexlify(obj['data']))

    @staticmethod
    def frame_code_to_string(frame_code):
        _map = {0x00: 'DATA', 0x01: 'HEADERS', 0x02: 'PRIORITY', 0x03: 'RST_STREAM',
                0x04: 'SETTINGS', 0x05: 'PUSH_PROMISE', 0x06: 'PING', 0x07: 'GO_AWAY',
                0x08: 'WINDOW_UPDATE', 0x09: 'CONTINUATION'}
        return _map[frame_code] if frame_code in _map else 'UNRECOGNIZED'

    @staticmethod
    def is_valid_frame(_type, stream_id):
        return Http2Frame.frame_code_to_string(_type) is not 'UNRECOGNIZED' and stream_id <= Http2Frame.MAX_STREAM_ID
