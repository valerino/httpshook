#!/usr/bin/env python2

from netifaces import ifaddresses, AF_PACKET, AF_INET
from sys import exit

class IfaceIO(object):
    """
    Interface I/O Class.
    """

    # Constructor
    def __init__(self, interface):
        # Network interface
        self.__interface__ = interface

        # Network interface's MAC address
        self.__mac__ = None

        # Network interface's IP address
        self.__ip__ = None

    # Get the MAC address
    def getMAC(self):
        if self.__mac__ == None:
            try:
                # Get the MAC address from the Network interface
                self.__mac__ = ifaddresses(self.__interface__)[AF_PACKET][0]['addr']
            except:
                print("[!] Can't resolve MAC address of %s network interface." % (self.__interface__))

                # Exit with negative status code
                exit(-1)

        return self.__mac__

    # Get the IP address
    def getIP(self):
        if self.__ip__ == None:
            try:
                # Get the IP address from the Network interface
                self.__ip__ = ifaddresses(self.__interface__)[AF_INET][0]['addr']
            except:
                print("[!] Can't resolve IP address of %s network interface." % (self.__interface__))

                # Exit with negative status code
                exit(-1)

        return self.__ip__
