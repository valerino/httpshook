#!/usr/bin/env python2

from signal import signal, SIGINT, SIGTERM, SIGHUP, SIGUSR1, SIGUSR2
from sys import exit

class SigIO(object):
    """
    Signal IO Class.
    """

    def __init__(self, arpio, nfqueue):
        # ArpIO object
        self.__arpio__ = arpio

        # NFQueueIO object
        self.__nfqueue__ = nfqueue

    # Set action for given signals
    def set_action(self):
        # Catch Interrupt signal
        signal(SIGINT, self.__signal_handler__)

        # Catch Termination signal
        signal(SIGTERM, self.__signal_handler__)

        # Catch Hangup signal
        signal(SIGHUP, self.__signal_handler__)

        # Catch User 1 signal
        signal(SIGUSR1, self.__signal_handler__)

        # Catch User 2 signal
        signal(SIGUSR2, self.__signal_handler__)

    # Handle the signal catched
    def __signal_handler__(self, signal_number, frame_stack):
        print("\n[!] Interrupt.")

        # Stop ARP cache poisoning attack against the victim; start the antidote for the victim against ARP cache poisoning attack.
        self.__arpio__.stop_poison()

        # Stop Netfilter queue scheduler
        self.__nfqueue__.stop_scheduler()

        # Exit with positive status code
        exit(0)
