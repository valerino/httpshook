#!/usr/bin/env python2

class IPForwardIO(object):
    """
    IP forwarding I/O Class.
    """

    # IP packet forwarding path on file system
    __PATH_FILE = "/proc/sys/net/ipv4/ip_forward"

    # IP packet forwarding's enable flag
    __ENABLE_VALUE__ = "1"

    # Constructor
    def __init__(self):
        # Original value of IP forward
        self.__orig_value__ = None

    # Enable IP packet forwarding on Network interface
    def enable(self):
        # Open the file in read-only mode
        fd = open(self.__PATH_FILE, 'r')

        # Get and save che current value
        self.__orig_value__ = fd.read()[:-1]

        # Close the file
        fd.close()

        # If the current value is disabled, then enable it
        if self.__orig_value__ != self.__ENABLE_VALUE__:
            # Open the file in write-only mode
            fd = open(self.__PATH_FILE, 'w')

            # Write and flush enable flag in the file
            fd.write(self.__ENABLE_VALUE__)
            fd.flush()

            # Close the file
            fd.close()

    # Disable IP packet forwarding on Network interface
    def disable(self):
        # If the past value was disabled, then disable it
        if self.__orig_value__ != self.__ENABLE_VALUE__:
            # Open the file in write-only mode
            fd = open(self.__PATH_FILE, 'w')

            # Write and flush disable flag in the file
            fd.write(self.__orig_value__)
            fd.flush()

            # Close the file
            fd.close()
