#!/bin/bash

echo -e "[ Update APT repositories ]"
sudo apt-get update

echo -e "[ Install and configure Build Essential Development Kit ]"
sudo apt-get install --yes build-essential

echo -e "[ Install and configure Python 2 ]"
sudo apt-get install --yes python

echo -e "\n[ Install and configure Development Kit for Python 2 ]"
sudo apt-get install --yes python-dev

echo -e "\n[ Install and configure Pip for Python 2]"
sudo apt-get install --yes python-pip

echo -e "[ Upgrade Pip for Python 2 ]"
sudo pip install --upgrade pip

echo -e "\n[ Install and configure Argparse for Python 2 ]"
sudo pip install argparse

echo -e "\n[ Install and configure Netifaces for Python 2 ]"
sudo pip install netifaces

echo -e "\n[ Install and configure Scapy for Python 2 ]"
sudo pip install scapy

echo -e "\n[ Install Netfilter IPTables firewall ]"
sudo apt-get install --yes iptables

echo -e "\n[ Install and configure Netfilter Queue Development Kit ]"
sudo apt-get install --yes libnetfilter-queue-dev

echo -e "\n[ Install and configure Netfilter Queue for Python 2 ]"
sudo pip install NetfilterQueue
