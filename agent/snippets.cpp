// 
// /*
//   boringSslWrite() hook (chrome)
// */
// int hookedBoringSslWrite(void *fd, void *handshake, void *buffer, int amount) {
//   typedef int(*_boringSslWrite)(void *, void *, void *, int);
//   if (buffer && amount) {
//     // dump data pre encryption
//     OutputDebugStringA("***************BoringSslWrite()**********************\n");
//     OutputDebugStringA((PCHAR)buffer);
//   }
//   // call original
//   _boringSslWrite boringSslWrite = (_boringSslWrite)hkBoringSslWrite.trampolineAddress;
//   int res = boringSslWrite(fd, handshake, buffer, amount);
//   return res;
// }
// 
// /*
// boringSslRead() hook (chrome)
// */
// int hookedBoringSslRead(void *fd, void *handshake, void *buffer, int amount, int peak) {
//   typedef int(*_boringSslRead)(void *, void *, void *, int, int);
//   // call original
//   _boringSslRead boringSslRead = (_boringSslRead)hkBoringSslRead.trampolineAddress;
//   int res = boringSslRead(fd, handshake, buffer, amount, peak);
//   if (res > 0 && amount) {
//     // dump data pre encryption
//     OutputDebugStringA("***************BoringSslRead()**********************\n");
//     OutputDebugStringA((PCHAR)buffer);
//   }
//   return res;
// }
// 
// /*
// install boringSSL hooks (chrome)
// */
// void installBoringSslHooks() {
//   OutputDebugStringA("Installing BoringSSL hooks!\n");
//   // NOTE: patterns valid (at least) for : Versione 62.0.3202.62 (Build ufficiale) (a 64 bit)
//   unsigned char boringSslWrite[] = {
//     0x48, 0x89, 0x5C, 0x24, 0x08, 0x48, 0x89, 0x6C, 0x24, 0x10, 0x48, 0x89, 0x74, 0x24, 0x18, 0x57,
//     0x41, 0x56, 0x41, 0x57, 0x48, 0x83, 0xEC, 0x30, 0x83, 0x22, 0x00, 0x4D, 0x8B, 0xF8, 0x48, 0x8B, 0x41 };
//   PBYTE writeAddr = procFindPattern(GetCurrentProcessId(), L"chrome.dll", TH32CS_SNAPMODULE, boringSslWrite, sizeof(boringSslWrite));
//   if (writeAddr) {
//     apihkInstallHook2(writeAddr, (PBYTE)hookedBoringSslWrite, &hkBoringSslWrite);
//   }
//   else {
//     OutputDebugStringA("Cannot find BoringSSL write pattern!\n");
//   }
//   unsigned char boringSslRead[] = {
//     0x48, 0x89, 0x5C, 0x24, 0x08, 0x48, 0x89, 0x6C, 0x24, 0x10, 0x48, 0x89, 0x74, 0x24, 0x18, 0x57,
//     0x41, 0x54, 0x41, 0x55, 0x41, 0x56, 0x41, 0x57, 0x48, 0x83, 0xEC, 0x30, 0x45, 0x33, 0xE4, 0x41, 0x8B };
//   PBYTE readAddr = procFindPattern(GetCurrentProcessId(), L"chrome.dll", TH32CS_SNAPMODULE, boringSslRead, sizeof(boringSslRead));
//   if (readAddr) {
//     apihkInstallHook2(readAddr, (PBYTE)hookedBoringSslRead, &hkBoringSslRead);
//   }
//   else {
//     OutputDebugStringA("Cannot find BoringSSL read pattern!\n");
//   }
// }

// 
// // boringssl hooks
// apiHookStruct hkBoringSslWrite = { 0 };
// apiHookStruct hkBoringSslRead = { 0 };
// 
// // Wininet hooks
// apiHookStruct hkInternetWriteFile = { 0 };
// 
// BOOL hookedInternetWriteFile(HINTERNET hFile, LPCVOID lpBuffer, DWORD dwNumberOfBytesToWrite, LPDWORD lpdwNumberOfBytesWritten) {
//   BOOL res = FALSE;
//   BOOL(WINAPI * internetWriteFile)(HINTERNET hFile, LPCVOID lpBuffer, DWORD dwNumberOfBytesToWrite, LPDWORD lpdwNumberOfBytesWritten);
// 
//   // call original
//   internetWriteFile = (BOOL(WINAPI*) (HINTERNET hFile, LPCVOID lpBuffer, DWORD dwNumberOfBytesToWrite, LPDWORD lpdwNumberOfBytesWritten))hkInternetWriteFile.trampolineAddress;
//   res = internetWriteFile(hFile, lpBuffer, dwNumberOfBytesToWrite, lpdwNumberOfBytesWritten);
//   if (res == 0 && dwNumberOfBytesToWrite) {
//     // dump data post decryption
//     OutputDebugStringA("***************InternetWriteFile()**********************\n");
//     OutputDebugStringA((PCHAR)lpBuffer);
//   }
//   return res;
// }
// 
// /**
// * install Wininet hoos
// */
// void installWininetHooks() {
//   OutputDebugStringA("installing Wininet hooks\n");
//   HMODULE wininet = GetModuleHandleA("wininet.dll");
//   if (wininet) {
//     apihkInstallHook2((PBYTE)GetProcAddress(wininet, "InternetWriteFile"), (PBYTE)hookedInternetWriteFile, &hkInternetWriteFile);
//   }
//   else {
//     OutputDebugStringA("Cannot find wininet.dll!\n");
//   }
// }
// 
