/*!
* \file winsock_hook.h
*
* \author valerino
* \date dicembre 2017
*
* winsock2 hooks
*/

#pragma once
/*!
* \brief install hooks on winsock2.dll
*
* \param LPVOID trampolineMem if provided, is a pointer to trampolines memory allocated out of the sandbox with ws2_32GetTrampolinesMemoryInRemoteProcess() available trough trampolineMem (WSASend())
* \note ws2_32CheckHook() should be called first to check if the hook can be placed or it's already placed
* \return 0 on success, ERROR_HOOK_FAILED if hooking failed
*/
int ws2_32InstallHooks(LPVOID trampolineMem);

/*!
 * \brief uninstall hooks on winsock2.dll
 * \return void
 */
void ws2_32UninstallHooks();

/*!
 * \brief check if hook is possible/already placed
 *
 * \return 0 on success, ERROR_ALREADY_HOOKED if process is already hooked or ERROR_HOOK_FAILED if api to hook is not found
 */
ULONG ws2_32CheckHook();

/*!
* \brief allocate memory in the remote process for the trampolines needed to hook functions
* \note the target functions code will be deprotected using VirtualProtectEx()
* \param DWORD pid the target pid
* \param uint8_t * * mem a big enough array of pointers filled with memory pointers on successful return
* \param uint32_t * num on succesful return, number of memory pointers in the mem array
* \return ULONG 0 on success
*/
ULONG ws2_32GetTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t* num);

/*!
* \brief free memory allocated with ncryptGetTrampolinesMemoryInRemoteProcess(), to be used on failure
*
* \param DWORD pid the target pid
* \param uint8_t * * mem memory blocks array
* \param uint32_t num number of filled memory blocks
* \return void
*/
void ws2_32FreeTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t num);

