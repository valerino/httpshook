/*!
 * \file cc_cmd.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * handle commands received from c&c
 */
#pragma once

#include <stdint.h>
#ifdef _WIN32 
#include <windows.h>
#endif
#include <exfiltrateutils.h>

 /*!
  * \brief run command from c&c, handed over by the check commands thread:
  *
  * \param uint8_t * data the json buffer
  * \param uint32_t size json size in bytes (ignored)
  * \param uint64_t cmdId a command unique id
  * \param void* resered a pointer to an application defined data, or NULL
  * \return uint32_t 0 on success
  */
uint32_t ccRunCommand(uint8_t* data, uint32_t size, uint64_t cmdId, void* reserved);
