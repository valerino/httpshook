/*!
* \file ncrypt_hook.h
*
* \author valerino
* \date novembre 2017
*
* ncrypt hooks to capture SSL
*/

#pragma once

/*!
* \brief install hooks on ncrypt.dll
*
* \param LPVOID trampolineMem if provided, is a pointer to trampolines memory allocated out of the sandbox with ncryptGetTrampolinesMemoryInRemoteProcess() available trough trampolineMem[0] (SslEncryptPacket) and trampolineMem[1] (SslDecryptPacket)
* \return 0 on success, ERROR_HOOK_FAILED if hooking failed
*/
int ncryptInstallHooks(LPVOID trampolineMem);

/*!
 * \brief uninstall hooks on ncrypt.dll
 *
 * \return void
 */
void ncryptUninstallHooks();

/*!
* \brief check if hook is possible/already placed
*
* \return 0 on success, ERROR_ALREADY_HOOKED if process is already hooked or ERROR_HOOK_FAILED if api to hook is not found
*/
ULONG ncryptCheckHook();

/*!
 * \brief allocate memory in the remote process for the trampolines needed to hook functions
 * \note the target functions code will be deprotected using VirtualProtectEx()
 * \param DWORD pid the target pid
 * \param uint8_t * * mem a big enough array of pointers filled with memory pointers on successful return
 * \param uint32_t * num on succesful return, number of memory pointers in the mem array
 * \return ULONG 0 on success
 */
ULONG ncryptGetTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t* num);

/*!
 * \brief free memory allocated with ncryptGetTrampolinesMemoryInRemoteProcess(), to be used on failure
 *
 * \param DWORD pid the target pid
 * \param uint8_t * * mem memory blocks array
 * \param uint32_t num number of filled memory blocks
 * \return void
 */
void ncryptFreeTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t num);
