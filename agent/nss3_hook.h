/*!
* \file nss3_hook.h
*
* \author valerino
* \date novembre 2017
*
* nss3 hooks to capture SSL
*/

#pragma once

/*!
* \brief install hooks on nss3.dll
*
* \return 0 on success, ERROR_ALREADY_HOOKED if process is already hooked or ERROR_HOOK_FAILED if hooking failed
*/
int nssInstallHooks();

/*!
 * \brief uninstall hooks on nss3.dll
 *
 * \return void
 */
void nssUninstallHooks();

/*!
* \brief check if hook is possible/already placed
*
* \return 0 on success, ERROR_ALREADY_HOOKED if process is already hooked or ERROR_HOOK_FAILED if api to hook is not found
*/
ULONG nssCheckHook();
