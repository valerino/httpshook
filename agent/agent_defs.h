/*!
 * \file agent_defs.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * agent global definitions, mostly for debugging
 */
#pragma once

 // define when debugging, to speedup injection directly into browsers without passing by svchost which takes quite many tries on windows 10 due to the many
 // svchost.exe found. remember to disable this when deploying!!!!!!!
//#define DBG_INJECTION_INTO_BROWSER_ONLY

