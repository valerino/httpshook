/*!
 * \file bcrypthook.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * bcrypt.dll hooks to capture SSL
 */

#pragma once
 /*!
  * \brief install hooks on bcrypt.dll
  *
  * \return void
  */
BOOL installBcryptHooks();
