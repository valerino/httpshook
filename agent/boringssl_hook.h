/*!
* \file boringssl_hook.h
*
* \author valerino
* \date dicembre 2017
*
* hooks into boring ssl (statically linked into chrome) to capture SSL traffic
*/

#pragma once

/*!
* \brief install hooks on boringsll
*
* \param BOOL isOpera TRUE to target opera browser, FALSE for chrome
* \return 0 on success, ERROR_ALREADY_HOOKED if process is already hooked or ERROR_HOOK_FAILED if hooking failed
*/
int boringSslInstallHooks(BOOL isOpera);

/*!
 * \brief uninstall boringssl hooks
 *
 * \return void
 */
void boringSslUninstallHooks();

/*!
* \brief check if hook is possible/already placed
*
* \param isOpera TRUE for opera, FALSE for chrome
* \return 0 on success, ERROR_ALREADY_HOOKED if process is already hooked or ERROR_HOOK_FINGERPRINT_MISMATCH if api to hook is not found
*/
ULONG boringSslCheckhook(BOOL isOpera);