rem ** debugging script, embeds configuration in .\testprobe.json in the vanilla built (NOT ENCRYPTED) binaries
rd /s /q test-%1
mkdir test-%1
copy x64\%1\probe.exe test-%1\probe.exe
copy x64\%1\dropper.exe test-%1\dropper.exe
copy x64\%1\agent.dll test-%1\agent.dll
copy %1\agent.dll test-%1\agent32.dll
python ..\shared\embed_cfg.py --dll test-%1\probe.exe --json_in .\testprobe.json --url dummy

