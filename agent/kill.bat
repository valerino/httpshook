rem debugging script, kills all browsers instances, explorer.exe and delete files in %temp% and Startup
rem explorer.exe must be re-run manually once done.

taskkill /F /IM microsoftedgecp.exe
taskkill /F /IM microsoftedge.exe
taskkill /F /IM firefox.exe
taskkill /F /IM chrome.exe
taskkill /F /IM opera.exe
taskkill /F /IM explorer.exe
taskkill /F /IM svchost.exe
taskkill /F /IM iexplore.exe
taskkill /F /IM dropper.exe
taskkill /F /IM probe.exe
del /ah %userprofile%\desktop*
del /q %temp%\*
del /q "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup"\*.exe
