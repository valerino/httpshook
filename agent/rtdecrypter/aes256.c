/*
*   A byte-oriented AES-256-CTR implementation.
*   Based on the code available at http://www.literatecode.com/aes256
*   Complies with RFC3686, http://tools.ietf.org/html/rfc3686
*
*/
/* everything will here be linked into '.text1' section */
#pragma code_seg(push, r1, ".text1")

#include "aes256.h"

static uint8_t gf_alog(uint8_t x);
static uint8_t gf_log(uint8_t x);
static uint8_t gf_mulinv(uint8_t x);
static uint8_t rj_sbox(uint8_t x);
static uint8_t rj_xtime(uint8_t x);
static void aes_subBytes(uint8_t *buf);
static void aes_addRoundKey(uint8_t *buf, uint8_t *key);
static void aes_addRoundKey_cpy(uint8_t *buf, uint8_t *key, uint8_t *cpk);
static void aes_shiftRows(uint8_t *buf);
static void aes_mixColumns(uint8_t *buf);
static void aes_expandEncKey(uint8_t *k, uint8_t rc);
static void ctr_inc_ctr(uint8_t *val);
static void ctr_clock_keystream(aes256_context *ctx, uint8_t *ks);

/* -------------------------------------------------------------------------- */
static uint8_t gf_alog(uint8_t x) /* calculate anti-logarithm gen 3 */
{
	uint8_t y = 1, i;

	for (i = 0; i < x; i++)
		y ^= rj_xtime(y);

	return y;
} /* gf_alog */

/* -------------------------------------------------------------------------- */
static uint8_t gf_log(uint8_t x) /* calculate logarithm gen 3 */
{
	uint8_t y, i = 0;

	if (x > 0)
		for (i = y = 1; i > 0; i++) {
			y ^= rj_xtime(y);
			if (y == x)
				break;
		}

	return i;
} /* gf_log */

/* -------------------------------------------------------------------------- */
static uint8_t gf_mulinv(uint8_t x) /* calculate multiplicative inverse */
{
	return (x > 0) ? gf_alog(255 - gf_log(x)) : 0;
} /* gf_mulinv */

/* -------------------------------------------------------------------------- */
static uint8_t rj_sbox(uint8_t x) {
	uint8_t y, sb;

	sb = y = gf_mulinv(x);
	y = (uint8_t)((y << 1) | (y >> 7)) & 0xFF;
	sb ^= y;
	y = (uint8_t)((y << 1) | (y >> 7)) & 0xFF;
	sb ^= y;
	y = (uint8_t)((y << 1) | (y >> 7)) & 0xFF;
	sb ^= y;
	y = (uint8_t)((y << 1) | (y >> 7)) & 0xFF;
	sb ^= y;

	return (sb ^ 0x63);
} /* rj_sbox */

static uint8_t rj_xtime(uint8_t x) {
	return ((x << 1) & 0xFF) ^ (0x1b * ((x & 0x80) >> 7));
} /* rj_xtime */

/* -------------------------------------------------------------------------- */
static void aes_subBytes(uint8_t *buf) {
	register uint8_t i;

	for (i = 0; i < 16; i++)
		buf[i] = rj_sbox(buf[i]);

} /* aes_subBytes */

/* -------------------------------------------------------------------------- */
static void aes_addRoundKey(uint8_t *buf, uint8_t *key) {
	register uint8_t i;

	for (i = 0; i < 16; i++)
		buf[i] ^= key[i];

} /* aes_addRoundKey */

/* -------------------------------------------------------------------------- */
static void aes_addRoundKey_cpy(uint8_t *buf, uint8_t *key, uint8_t *cpk) {
	register uint8_t i = 16;

	for (i = 0; i < 16; i++) {
		cpk[i] = key[i];
		buf[i] ^= key[i];
		cpk[16 + i] = key[16 + i];
	}

} /* aes_addRoundKey_cpy */

/* -------------------------------------------------------------------------- */
static void aes_shiftRows(uint8_t *buf) {
	register uint8_t i = buf[1], j = buf[3], k = buf[10], l = buf[14];

	buf[1] = buf[5];
	buf[5] = buf[9];
	buf[9] = buf[13];
	buf[13] = i;
	buf[3] = buf[15];
	buf[15] = buf[11];
	buf[11] = buf[7];
	buf[7] = j;
	buf[10] = buf[2];
	buf[2] = k;
	buf[14] = buf[6];
	buf[6] = l;

} /* aes_shiftRows */

/* -------------------------------------------------------------------------- */
static void aes_mixColumns(uint8_t *buf) {
	register uint8_t i, a, b, c, d, e;

	for (i = 0; i < 16; i += 4) {
		a = buf[i];
		b = buf[i + 1];
		c = buf[i + 2];
		d = buf[i + 3];
		e = a ^ b ^ c ^ d;
		buf[i] ^= e ^ rj_xtime(a ^ b);
		buf[i + 1] ^= e ^ rj_xtime(b ^ c);
		buf[i + 2] ^= e ^ rj_xtime(c ^ d);
		buf[i + 3] ^= e ^ rj_xtime(d ^ a);
	}

} /* aes_mixColumns */

/* -------------------------------------------------------------------------- */
static void aes_expandEncKey(uint8_t *k, uint8_t rc) {
	register uint8_t i;

	k[0] ^= rj_sbox(k[29]) ^ rc;
	k[1] ^= rj_sbox(k[30]);
	k[2] ^= rj_sbox(k[31]);
	k[3] ^= rj_sbox(k[28]);

	for (i = 4; i < 16; i += 4) {
		k[i] ^= k[i - 4];
		k[i + 1] ^= k[i - 3];
		k[i + 2] ^= k[i - 2];
		k[i + 3] ^= k[i - 1];
	}
	k[16] ^= rj_sbox(k[12]);
	k[17] ^= rj_sbox(k[13]);
	k[18] ^= rj_sbox(k[14]);
	k[19] ^= rj_sbox(k[15]);

	for (i = 20; i < 32; i += 4) {
		k[i] ^= k[i - 4];
		k[i + 1] ^= k[i - 3];
		k[i + 2] ^= k[i - 2];
		k[i + 3] ^= k[i - 1];
	}

} /* aes_expandEncKey */

/* -------------------------------------------------------------------------- */
void aes256_init(aes256_context *ctx, uint8_t *k) {
	register uint8_t i;

	for (i = 0; i < sizeof(ctx->key); i++)
		ctx->enckey[i] = k[i];

} /* aes256_init */

/* -------------------------------------------------------------------------- */
void aes256_done(aes256_context *ctx) {
	register uint8_t i;

	for (i = 0; i < sizeof(ctx->key); i++) {
		ctx->key[i] = ctx->enckey[i] = 0;
		ctx->blk.nonce[i % sizeof(ctx->blk.nonce)] = 0;
		ctx->blk.iv[i % sizeof(ctx->blk.iv)] = 0;
		ctx->blk.ctr[i % sizeof(ctx->blk.ctr)] = 0;
	}

} /* aes256_done */

/* -------------------------------------------------------------------------- */
void aes256_encrypt_ecb(aes256_context *ctx, uint8_t *buf) {
	uint8_t i, rcon = 1;

	aes_addRoundKey_cpy(buf, ctx->enckey, ctx->key);
	for (i = 1; i < 14; ++i) {
		aes_subBytes(buf);
		aes_shiftRows(buf);
		aes_mixColumns(buf);
		if ((i & 1) == 1)
			aes_addRoundKey(buf, &ctx->key[16]);
		else {
			aes_expandEncKey(ctx->key, rcon);
			rcon = rj_xtime(rcon);
			aes_addRoundKey(buf, ctx->key);
		}
	}
	aes_subBytes(buf);
	aes_shiftRows(buf);
	aes_expandEncKey(ctx->key, rcon);
	aes_addRoundKey(buf, ctx->key);

} /* aes256_encrypt */

/* -------------------------------------------------------------------------- */
static void ctr_inc_ctr(uint8_t *val) {
	if (val != NULL)
		if (++val[3] == 0)
			if (++val[2] == 0)
				if (++val[1] == 0)
					val[0]++;

} /* ctr_inc_ctr */

/* -------------------------------------------------------------------------- */
static void ctr_clock_keystream(aes256_context *ctx, uint8_t *ks) {
	uint8_t i;
	uint8_t *p = (uint8_t *)&ctx->blk;

	if ((ctx != NULL) && (ks != NULL)) {
		for (i = 0; i < sizeof(ctx->blk); i++)
			ks[i] = p[i];

		aes256_encrypt_ecb(ctx, ks);
		ctr_inc_ctr(&ctx->blk.ctr[0]);
	}

} /* ctr_clock_keystream */

/* -------------------------------------------------------------------------- */
void aes256_setCtrBlk(aes256_context *ctx, rfc3686_blk *blk) {
	uint8_t i, *p = (uint8_t *)&ctx->blk, *v = (uint8_t *)blk;

	if ((ctx != NULL) && (blk != NULL))
		for (i = 0; i < sizeof(ctx->blk); i++)
			p[i] = v[i];

} /* aes256_setCtrBlk */

/* -------------------------------------------------------------------------- */
void aes256_encrypt_ctr(aes256_context *ctx, uint8_t *buf, size_t sz) {
	uint8_t key[sizeof(ctx->blk)];
	size_t  i;
	uint8_t j = sizeof(key);

	for (i = 0; i < sz; i++) {
		if (j == sizeof(key)) {
			j = 0;
			ctr_clock_keystream(ctx, key);
		}
		buf[i] ^= key[j++];
	}

} /* aes256_encrypt_ctr */

#pragma code_seg(pop, r1)
