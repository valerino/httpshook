#pragma once
/*
* Copyright 1995-2016 The OpenSSL Project Authors. All Rights Reserved.
*
* Licensed under the OpenSSL license (the "License").  You may not use
* this file except in compliance with the License.  You can obtain a copy
* in the file LICENSE in the source distribution or at
* https://www.openssl.org/source/license.html
*/

#ifdef __cplusplus
extern "C" {
#endif

#define SHA_LONG unsigned int

#define SHA_LBLOCK      16
#define SHA_CBLOCK      (SHA_LBLOCK*4)
#define SHA_LAST_BLOCK  (SHA_CBLOCK-8)
#define SHA_DIGEST_LENGTH 20

	typedef struct SHAstate_st {
		SHA_LONG h0, h1, h2, h3, h4;
		SHA_LONG Nl, Nh;
		SHA_LONG data[SHA_LBLOCK];
		unsigned int num;
	} SHA_CTX;

#define SHA256_CBLOCK   (SHA_LBLOCK*4)

	typedef struct SHA256state_st {
		SHA_LONG h[8];
		SHA_LONG Nl, Nh;
		SHA_LONG data[SHA_LBLOCK];
		unsigned int num, md_len;
	} SHA256_CTX;

	int SHA256_Init(SHA256_CTX *c);
	int SHA256_Update(SHA256_CTX *c, const void *data, size_t len);
	int SHA256_Final(unsigned char *md, SHA256_CTX *c);
	unsigned char *SHA256(const unsigned char *d, size_t n, unsigned char *md);

#define SHA224_DIGEST_LENGTH    28
#define SHA256_DIGEST_LENGTH    32
#define SHA384_DIGEST_LENGTH    48
#define SHA512_DIGEST_LENGTH    64

#ifdef __cplusplus
}
#endif

