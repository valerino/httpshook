/*!
 * \file llistutlils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * standard windows linked list stuff (ripped from WDK)
 */
#ifndef __lists_h__
#define __lists_h__

#ifndef InitializeListHead
#define InitializeListHead(ListHead) (\
	(ListHead)->Flink = (ListHead)->Blink = (ListHead))
#endif

#ifndef IsListEmpty
#define IsListEmpty(ListHead) \
	((ListHead)->Flink == (ListHead))
#endif

#ifndef InsertTailList
#define InsertTailList(ListHead,Entry) {\
	PLIST_ENTRY _EX_Blink;\
	PLIST_ENTRY _EX_ListHead;\
	_EX_ListHead = (ListHead);\
	_EX_Blink = _EX_ListHead->Blink;\
	(Entry)->Flink = _EX_ListHead;\
	(Entry)->Blink = _EX_Blink;\
	_EX_Blink->Flink = (Entry);\
	_EX_ListHead->Blink = (Entry);\
}
#endif

#ifndef RemoveEntryList
#define RemoveEntryList(Entry) {\
	PLIST_ENTRY _EX_Blink;\
	PLIST_ENTRY _EX_Flink;\
	_EX_Flink = (Entry)->Flink;\
	_EX_Blink = (Entry)->Blink;\
	_EX_Blink->Flink = _EX_Flink;\
	_EX_Flink->Blink = _EX_Blink;\
}
#endif

#ifndef RemoveHeadList
#define RemoveHeadList(ListHead) \
	(ListHead)->Flink;\
{RemoveEntryList((ListHead)->Flink)}
#endif

#ifndef RemoveTailList
#define RemoveTailList(ListHead) \
	(ListHead)->Blink;\
{RemoveEntryList((ListHead)->Blink)}
#endif

#ifndef RemoveHeadList
#define RemoveHeadList(ListHead) \
	(ListHead)->Flink;\
{RemoveEntryList((ListHead)->Flink)}
#endif

#endif