/*!
* \file osutils_ptr.h
*
* \author valerino
* \date novembre 2017
*
* import definitions for osutils
*/
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

	/*
	the following pointers are available after calling osInitialize(), and remains valid also after calling osFinalize()
	*/
	typedef HMODULE(WINAPI* _LOADLIBRARYA) (PCHAR lpLibFileName);
	extern _LOADLIBRARYA ptrLoadLibraryA;

	typedef BOOL(WINAPI* FREELIBRARY)(HMODULE hLibModule);
	extern FREELIBRARY ptrFreeLibrary;

	typedef DWORD(WINAPI* GETMODULEFILENAMEA)(HMODULE hModule, PCHAR lpFilename, DWORD nSize);
	extern GETMODULEFILENAMEA ptrGetModuleFileNameA;

	typedef DWORD(WINAPI* GETMODULEFILENAMEW)(HMODULE hModule, PWCHAR lpFilename, DWORD nSize);
	extern GETMODULEFILENAMEW ptrGetModuleFileNameW;

	typedef HMODULE(WINAPI* GETMODULEHANDLEA)(PCHAR lpModuleName);
	extern GETMODULEHANDLEA ptrGetModuleHandleA;

	typedef HMODULE(WINAPI* GETMODULEHANDLEW)(PWCHAR lpModuleName);
	extern GETMODULEHANDLEW ptrGetModuleHandleW;

	typedef void*(WINAPI* _GETPROCADDRESS)(HMODULE hModule, PCHAR lpProcName);
	extern _GETPROCADDRESS ptrGetProcAddress;

	typedef HANDLE(WINAPI* CREATEMUTEXA)(LPSECURITY_ATTRIBUTES lpMutexAttributes, BOOL bInitialOwner, PCHAR lpName);
	extern CREATEMUTEXA ptrCreateMutexA;

	typedef HANDLE(WINAPI* OPENMUTEXA)(DWORD dwDesiredAccess, BOOL bInheritHandle, PCHAR lpName);
	extern OPENMUTEXA ptrOpenMutexA;

	typedef HANDLE(WINAPI* CREATEEVENTA)(LPSECURITY_ATTRIBUTES lpEventAttributes, BOOL bManualReset, BOOL bInitialState, PCHAR lpName);
	extern CREATEEVENTA ptrCreateEventA;

	typedef BOOL(WINAPI* SETEVENT)(HANDLE hEvent);
	extern SETEVENT ptrSetEvent;

	typedef BOOL(WINAPI* RELEASEMUTEX)(HANDLE hMutex);
	extern RELEASEMUTEX ptrReleaseMutex;

#ifdef __cplusplus
}
#endif

