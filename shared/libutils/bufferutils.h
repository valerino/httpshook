#pragma once
/*!
 * \file bufferutils.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * buffer manipulation utilities
 */

 /*!
 * \brief replace a string in a buffer, if found
 *
 * \param const char * stringToReplace string to find in the input buffer
 * \param const char * replacement the replacement string (beware, size must not be >stringToReplace!)
 * \param void* input the input buffer
 * \param uint32_t len the input size
 * \return int number of replaced strings
 */
int bufferTryReplaceString(const char* stringToReplace, const char* replacement, void* input, uint32_t len);

/*!
* \brief zap a string in a buffer, replacing with spaces (if found)
*
* \param const char * stringToZap string to find in the input buffer
* \param PVOID input the input buffer
* \param uint32_t len the input size
* \return int number of zapped strings
*/
int bufferTryZapString(const char* stringToZap, void* input, uint32_t len);

/*!
* \brief zap multiple strings in a buffer, replacing with spaces (if found)
*
* \param const char ** stringsToZap array of string (pointers) to find in the input buffer, last entry must be NULL
* \param PVOID input the input buffer
* \param uint32_t len the input size
* \return void
*/
int bufferTryZapStrings(const char**stringsToZap, void* input, uint32_t len);

/*!
* \brief memmem() with wildcard
*
* \param const void * l
* \param size_t l_len
* \param const void * s
* \param size_t s_len
* \param uint8_t wildcard if a character in s is == wildcard, it is skipped
* \return void *
*/
extern "C" void * bufferMemMemWildcard(const void *l, size_t l_len, const void *s, size_t s_len, uint8_t wildcard);

/*!
* \brief memcmp with wildcard
*
* \param const void * s1
* \param const void * s2
* \param size_t n
* \param uint8_t wildcard if a character in s2 is == wildcard, it is skipped
* \return
*/
extern "C" int bufferMemCmpWildcard(const void* s1, const void* s2, size_t n, uint8_t wildcard);

/*!
* \brief memmem implementation
*
* \param const void * l
* \param size_t l_len
* \param const void * s
* \param size_t s_len
* \return void *
*/
extern "C" void * bufferMemMem(const void *l, size_t l_len, const void *s, size_t s_len);
