/*!
 * \file hashutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * hashing utilities
 */
#pragma once

#include <stdint.h>

 /*!
  * \brief hashes string (ror13 + uppercase/lowercase compensation) (works for unicode strings too)
  * \note position independent (PIC) code, will be linked in '.text2' section
  * \param uint8_t * c the string
  * \param uint32_t len string len, default pass 0 to expect 0-terminated buffer
  * \return uint32_t the hash, same for uppercase or lowercase
  */
extern "C" uint32_t hashRor13(uint8_t* c, uint32_t len);
