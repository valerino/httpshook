/*!
 * \file cryptutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * cryptographic utilities
 */
#pragma once

#ifndef BCRYPT_KEY_HANDLE 
typedef void* BCRYPT_KEY_HANDLE;
#endif 

/* rsa keys should be RSA-2048 */
#define RSA_KEY_BITS 2048

/* aes key material used to build an aes key */
#define AES_KEY_BITS 256
#define AES_IV_BITS  128
typedef struct _aes_key_material {
	uint8_t k[AES_KEY_BITS / 8]; /* 32 bytes */
	uint8_t iv[AES_IV_BITS / 8]; /* 16 bytes */
	uint8_t* reserved; /* internal */
} aes_key_material;

/*!
 * \brief initializes an AES-256 CBC key to be used with cryptAesEncrypt() and cryptAesDecrypt()
 *
 * \param aes_key_material * km to be initialized. NOTE: this must be resident in memory as long as the BCRYPT_KEY_HANDLE is used!
 *		The same aes_key_material must then be passed to cryptCloseKey() when the key has to be discarded.
 * \param BOOL random if TRUE, km will be initialized with random values. either, it expects k and iv already set
 * \param BCRYPT_KEY_HANDLE* k on successful return, aes key handle to be closed with cryptCloseKey()
 * \return uint32_t 0 on success (NTSTATUS)
 */
uint32_t cryptAesInitializeKey(aes_key_material* km, BOOL random, BCRYPT_KEY_HANDLE* k);

/*!
 * \brief unwrap an aes_key_material struct wrapped with cryptRsaWrapAesKey()
 *
 * \param BCRYPT_KEY_HANDLE rsaPriv an rsa private key
 * \param uint8_t * wrapped input buffer, rsa wrapped aes_key_material struct to be unwrapped
 * \param uint32_t size size of the input buffer
 * \param aes_key_material * km on successful return, filled aes_key_material struct. NOTE: this must be resident in memory as long as the BCRYPT_KEY_HANDLE is used!
 *		The same aes_key_material must then be passed to cryptCloseKey() when the key has to be discarded.
 * \param BCRYPT_KEY_HANDLE* k on successful return, ready to use aes key to be closed with cryptCloseKey()
 * \return uint32_t 0 on success (NTSTATUS)
 */
uint32_t cryptRsaUnwrapAesKey(BCRYPT_KEY_HANDLE rsaPriv, uint8_t* wrapped, uint32_t size, aes_key_material* km, BCRYPT_KEY_HANDLE* k);

/*!
 * \brief wrap an aes_key_material struct with RSA
 *
 * \param BCRYPT_KEY_HANDLE rsaRecipientPub recipient's public RSA key
 * \param uint32_t keyLenBytes size of the public key, in bytes (i.e. 256 for 2048 bit key)
 * \param aes_key_material * km the aes_key_material to be wrapped
 * \param uint8_t * * wrapped on successful return, the wrapped aes_key_material (to be freed with memHeapFree())
 * \param uint32_t * size on successful return, size of the wrapped aes_key_material buffer
 * \return uint32_t 0 on success (NTSTATUS)
 */
uint32_t cryptRsaWrapAesKey(BCRYPT_KEY_HANDLE rsaRecipientPub, uint32_t keyLenBytes, aes_key_material* km, uint8_t** wrapped, uint32_t* size);

/*!
 * \brief generate a random buffer
 *
 * \param uint8_t * buf receives the buffer
 * \param uint32_t size size of the buffer to generate
 * \return uint32_t 0 on success
 */
uint32_t cryptGenerateRandomBuffer(uint8_t* buf, uint32_t size);

/*!
 * \brief generate a random RSA keypair
 *
 * \param uint32_t bits key bits, i.e. 2048
 * \param uint8_t * * priv on successful return, private key buffer to be freed with memHeapFree()
 * \param uint32_t * privSize on successful return, size of the private key buffer
 * \param uint8_t * * pub on successful return, public key buffer to be freed with memHeapFree()
 * \param uint32_t * pubSize on successful return, size of the public key buffer
 * \return uint32_t 0 on success (NTSTATUS)
 * \sa https://msdn.microsoft.com/en-us/library/windows/desktop/aa375531(v=vs.85).aspx has instructions to generate BCRYPT_RSAPUBLIC_BLOB, BCRYPT_RSAPRIVATE_BLOB from outside Windows OS
 */
uint32_t cryptRsaGenerateKeypair(uint32_t bits, uint8_t** priv, uint32_t* privSize, uint8_t** pub, uint32_t* pubSize);

/*!
 * \brief encrypt buffer using AES-256 CBC
 *
 * \param k a key from cryptAesInitializeKeyMaterial() or cryptAesUnwrapAesKeyMaterial()
 * \param iv an initialization vector of AES_IV_BITS / 8 size
 * \param uint8_t * buf the input buffer
 * \param uint32_t size the input size
 * \param aes_key_material * km a filled aes_key_material struct
 * \param uint8_t * * out on successful return, the encrypted buffer (to be freed with memHeapFree())
 * \param uint32_t * outSize on successful return, the encrypted buffer size
 * \return uint32_t 0 on success (NTSTATUS)
 */
uint32_t cryptAesEncrypt(BCRYPT_KEY_HANDLE k, uint8_t* iv, uint8_t* buf, uint32_t size, uint8_t** out, uint32_t* outSize);

/*!
* \brief decrypt buffer encrypted using AES-256 CBC
*
* \param k a key from cryptAesInitializeKey() or cryptAesUnwrapAesKey()
* \param iv an initialization vector of AES_IV_BITS / 8 size
* \param uint8_t * buf the input buffer
* \param uint32_t size the input size
* \param aes_key_material * km a filled aes_key_material struct
* \param uint8_t * * out on successful return, the decrypted buffer (to be freed with memHeapFree()). The buffer is guaranteed to have bunch of \0\0 in the end!
* \param uint32_t * outSize on successful return, the decrypted buffer size
* \param BOOL forcePkcs1Padding if TRUE, expects a pkcs1 padded buffer
* \return uint32_t 0 on success (NTSTATUS)
*/
uint32_t cryptAesDecrypt(BCRYPT_KEY_HANDLE k, uint8_t* iv, uint8_t* buf, uint32_t size, uint8_t** out, uint32_t* outSize, BOOL forcePkcs1Padding = FALSE);

/*!
 * \brief close a key handle
 *
 * \param BCRYPT_KEY_HANDLE key the key (it's ok to pass NULL)
 * \param aes_key_material km if it's an aes key, key material to be freed
 * \return void
 */
void cryptCloseKey(BCRYPT_KEY_HANDLE key, aes_key_material* km = NULL);

/*!
 * \brief import an RSA key from BCRYPT_RSA_PUBLIC_BLOB or BCRYPT_RSA_PRIVATE_BLOB
 *
 * \param uint8_t * blob the key blob
 * \param uint32_t size size of the key blob
 * \param BCRYPT_KEY_HANDLE * key on successful return, key to be closed with cryptCloseKey()
 * \param BOOL privateKey if TRUE, blob is a private key (default FALSE)
 * \param uint32_t keyLenBytes if not NULL, on successful return the key size in bytes
 * \return uint32_t
 */
uint32_t cryptRsaImportKey(uint8_t* blob, uint32_t size, BCRYPT_KEY_HANDLE* key, BOOL privateKey = FALSE, uint32_t* keyLenBytes = NULL);

/*!
 * \brief initializes api, must be called once per process
 * \note ubsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
 * \calls osInitialize()
 * \return uint32_t 0 on success
 */
uint32_t cryptInitialize();

/*!
 * \brief finalizes api
 * \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
 * \calls osFinalize()
 */
void cryptFinalize();
