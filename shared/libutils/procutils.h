/*!
* \file procutils.h
*
* \author valerino
* \date novembre 2017
*
* process related utilities
*/

#pragma once
#include <stdint.h>
#include "listutils.h"

#include "procutils_ptr.h"

/*!
 * \class processEntry
 *
 * \brief contains the PROCESSENTRY32 for a process in the linkedlist created by procGetProcessesList()
 *
 * \author valerino
 * \date novembre 2017
 */
typedef struct _processEntry {
	PROCESSENTRY32A proc; /* the process entry */
	LIST_ENTRY chain; /* internal */
} processEntry;

/*!
* \class threadEntry
*
* \brief contains the THREADENTRY32 for a thread in the linkedlist created by procGetThreadsList()
*
* \author valerino
* \date novembre 2017
*/
typedef struct _threadEntry {
	THREADENTRY32 thread; /* the thread entry */
	LIST_ENTRY chain; /* internal */
} threadEntry;

/*!
* \class moduleEntry
*
* \brief contains the MODULEENTRY32 for a module in the linkedlist created by procGetModulesList()
*
* \author valerino
* \date novembre 2017
*/
typedef struct _moduleEntry {
	MODULEENTRY32 mod; /**< the module entry */
	LIST_ENTRY chain; /**< internal */
} moduleEntry;

/*!
 * \brief retrieve running processes
 *
 * \param PLIST_ENTRY processes on successful return, a linkedlist of processEntry. must be freed with procFreeProcessesList()
 * \param PULONG num on successful return, number of processes in list
 * \return ULONG 0 on success
 */
ULONG procGetProcessesList(PLIST_ENTRY processes, PULONG num);

/*!
 * \brief free list allocated with procGetProcessesList()
 *
 * \param PLIST_ENTRY processes list head
 * \return void
 */
void procFreeProcessesList(PLIST_ENTRY processes);

/*!
* \brief retrieve modules running inside a process
*
* \param PLIST_ENTRY processes on successful return, a linkedlist of moduleEntry. must be freed with procFreeModulesList()
* @param: DWORD pid target process id
* @param: ULONG flag scan be TH32CS_SNAPMODULE or TH32CS_SNAPMODULE32 or both (refer to CreateToolhelp32Snapshot() function documentation)
* \param PULONG num on successful return, number of modules in list
* \return ULONG 0 on success
*/
ULONG procGetModulesList(PLIST_ENTRY modules, DWORD pid, ULONG flags, PULONG num);

/*!
* \brief free list allocated with procGetModulesList()
*
* \param PLIST_ENTRY processes list head
* \return void
*/
void procFreeModulesList(PLIST_ENTRY modules);

/*!
* \brief retrieve running threads inside a process
*
* \param PLIST_ENTRY processes on successful return, a linkedlist of moduleEntry. must be freed with procFreeModulesList()
* @param: DWORD pid target process id
* \param PULONG num on successful return, number of modules in list
* \return ULONG 0 on success
*/
ULONG procGetThreadsList(PLIST_ENTRY threads, DWORD pid, PULONG num);

/*!
* \brief free list allocated with procGetThreadsList()
*
* \param PLIST_ENTRY processes list head
* \return void
*/
void procFreeThreadsList(PLIST_ENTRY threads);

/*!
 * \brief enable/disable privilege on a process
 *
 * \param HANDLE token the process token
 * \param const PWCHAR privilege privilege name, i.e. SE_DEBUG_PRIVILEGE
 * \param BOOL enable TRUE to enable
 * \return ULONG 0 on success
 */
ULONG procSetPrivilege(HANDLE token, const PWCHAR privilege, BOOL enable);

/*!
 * \brief enable/disable privilege on the current process token
 *
 * \param const PWCHAR privilege privilege name, i.e. SE_DEBUG_PRIVILEGE
 * \param BOOL enable TRUE to enable
 * \return ULONG 0 on success
 */
ULONG procSetCurrentProcessPrivilege(const PWCHAR privilege, BOOL enable);

/*!
 * \brief start a process
 *
 * \param const PWCHAR path path to the executable
 * \param const PWCHAR params if not NULL, parameters
 * \param BOOL hidden TRUE to pass SW_HIDE to CreateProcess() API
 * \return ULONG 0 on success
 */
ULONG procExecute(const PWCHAR path, const PWCHAR params, BOOL hidden);

/*!
* \brief get rot-13 hash of the current process
*
* \note procInitialize() must be called before!
*
* \return extern " uint32_t
*/
uint32_t procGetCurrentProcessHash();

/*!
* \brief initializes api, must be called once per process
* \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
* \calls strInitialize(), osInitialize()
* \return uint32_t 0 on success
*/
ULONG procInitialize();

/*!
* \brief finalizes api
* \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
* \calls strFinalize(), osFinalize()
* \return uint32_t
*/
void procFinalize();

/*!
 * \brief find pattern in process memory
 *
 * \param DWORD pid the target pid
 * \param moduleHash module name (i.e.e chrome.dll) hash obtained through hash_ror13() on the unicode string
 * \param ULONG flags can be TH32CS_SNAPMODULE or TH32CS_SNAPMODULE32 or both (refer to CreateToolhelp32Snapshot() function documentation)
 * \param PBYTE toSearch pattern to search for
 * \param DWORD toSearchSize size of pattern to search
 * \param uint8_t wildcard a character to use as wildcard
 * \param DWORD useWildcard if TRUE, wildcard memmem() is used. either, standard memmem() is used
 * \return uint8_t*
 */
extern "C" uint8_t* procFindPatternByHash(DWORD pid, uint32_t moduleHash, DWORD flags, PBYTE toSearch, DWORD toSearchSize, uint8_t wildcard, BOOL useWildcard);

/*!
* \brief get pointer to PROCESS ENVIRONMENT BLOCK (https://msdn.microsoft.com/en-us/library/windows/desktop/aa813706(v=vs.85).aspx)
* \note position independent (PIC) code, will be linked in '.text2' section, can be called without calling the initialize function first
* \return PVOID
*/
extern "C" PVOID procGetPeb();

/*!
* \brief get the specific module entry, if it's mapped into the specified process
* \param ULONG pid process id to search within
* \param uint32_t hash an hash obtained through hash_ror13() on the unicode string
* \param ULONG flags can be TH32CS_SNAPMODULE or TH32CS_SNAPMODULE32 or both (refer to CreateToolhelp32Snapshot() function documentation)
* \param MODULEENTRY32 * modEntry on successful return, filled with the desired module information
* \return  ULONG 0 on success
*/
extern "C" ULONG procGetModuleEntryByHash(ULONG pid, uint32_t hash, ULONG flags, MODULEENTRY32* modEntry);

/*!
* \brief get the specific module entry, if it's mapped into the specified process
* \param ULONG pid process id to search within
* \param PWCHAR moduleName module name substring
* \param ULONG flags can be TH32CS_SNAPMODULE or TH32CS_SNAPMODULE32 or both (refer to CreateToolhelp32Snapshot() function documentation)
* \param MODULEENTRY32 * modEntry on successful return, filled with the desired module information
* \return  ULONG 0 on success
*/
extern "C" ULONG procGetModuleEntry(ULONG pid, const PWCHAR moduleName, ULONG flags, MODULEENTRY32* modEntry);

/*!
 * \brief get module base by hash, if it's mapped into the current process
 *
 * \param DWORD hash an hash obtained through hash_ror13() on the unicode string module (dll) name (i.e. 'something.dll')
 * \return extern " HMODULE
 */
extern "C" HMODULE procFindModuleByHash(uint32_t hash);

/*!
 * \brief find a process (may be more than 1 entry) in the running processes list, by ror13 hash
 *
 * \param DWORD hash an hash obtained through hash_ror13() on the utf8 (ansi) string of the process exe (i.e. 'something.exe')
 * \param uint32_t * pids on successful return, filled with found process ids matching the hash
 * \param uint32_t pidDwordSize maximum size of the pids array, in DWORDs
 * \param uint32_t * numPids on successful return, number of found processes
 * \return ULONG 0 on success
 */
extern "C" ULONG procFindProcessByHash(uint32_t hash, uint32_t* pids, uint32_t pidDwordSize, uint32_t* numPids);
