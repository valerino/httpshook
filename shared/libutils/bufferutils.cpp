/*!
 * \file bufferutils.cpp
 *
 * \author valerino
 * \date dicembre 2017
 *
 * buffer manipulation utilities
 */

#ifdef _WIN32
#include <windows.h>
#endif
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "dbgutils.h"
#include "strutils.h"

 /* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
extern "C" void * bufferMemMem(const void *l, size_t l_len, const void *s, size_t s_len) {
	register char *cur, *last;
	const char *cl = (const char *)l;
	const char *cs = (const char *)s;

	/* we need something to compare */
	if (l_len == 0 || s_len == 0)
		return NULL;

	/* "s" must be smaller or equal to "l" */
	if (l_len < s_len)
		return NULL;

	/* special case where s_len == 1 */
	if (s_len == 1)
		return (void*)memchr(l, (int)*cs, l_len);

	/* the last position where its possible to find "s" in "l" */
	last = (char *)cl + l_len - s_len;

	for (cur = (char *)cl; cur <= last; cur++)
		if (cur[0] == cs[0] && memcmp(cur, cs, s_len) == 0)
			return cur;

	return NULL;
}

extern "C" int bufferMemCmpWildcard(const void* s1, const void* s2, size_t n, uint8_t wildcard) {
	unsigned char u1, u2;
	unsigned char* _s1 = (unsigned char*)s1;
	unsigned char* _s2 = (unsigned char*)s2;

	for (; n--; _s1++, _s2++) {
		u1 = *(unsigned char *)_s1;
		u2 = *(unsigned char *)_s2;
		if (u2 != wildcard) {
			if (u1 != u2) {
				return (u1 - u2);
			}
		}
	}
	return 0;
}

extern "C" void * bufferMemMemWildcard(const void *l, size_t l_len, const void *s, size_t s_len, uint8_t wildcard) {
	register char *cur, *last;
	const char *cl = (const char *)l;
	const char *cs = (const char *)s;

	/* we need something to compare */
	if (l_len == 0 || s_len == 0)
		return NULL;

	/* "s" must be smaller or equal to "l" */
	if (l_len < s_len)
		return NULL;

	/* special case where s_len == 1 */
	if (s_len == 1) {
		if (wildcard) {
			return (void*)l;
		}
		return (void*)memchr(l, (int)*cs, l_len);
	}

	/* the last position where its possible to find "s" in "l" */
	last = (char *)cl + l_len - s_len;

	for (cur = (char *)cl; cur <= last; cur++)
		if (cur[0] == cs[0] && bufferMemCmpWildcard(cur, cs, s_len, wildcard) == 0)
			return cur;

	return NULL;
}

int bufferTryReplaceString(const char* stringToReplace, const char* replacement, void* input, uint32_t len) {
	size_t l = strlen(stringToReplace);
	int found = 0;

	if (l < len) {
		size_t sublen = strlen(replacement);
		char* ptr = (char*)bufferMemMem((const char*)input, len, stringToReplace, sublen);
		if (ptr) {
			// patch!! 
			DBG_PRINT(DBG_LEVEL_VERBOSE, "***** REPLACING: %s => %s *****", stringToReplace, replacement);
			memcpy(ptr, replacement, sublen);
			found++;
		}
	}
	return found;
}

int bufferTryZapString(const char* stringToZap, void* input, uint32_t len) {
	size_t l = strlen(stringToZap);
	int found = 0;
	if (l < (size_t)len) {
		// find the accept encoding field
		char* ptr = (char*)bufferMemMem((const char*)input, len, stringToZap, l);
		if (ptr) {
			found++;
			// patch!!
			DBG_PRINT(DBG_LEVEL_VERBOSE, "***** ZAPPING: %s *****", stringToZap);
			while (*ptr != '\r') {
				*ptr = ' ';
				ptr++;
			}
		}
	}
	return found;
}

int bufferTryZapStrings(const char**stringsToZap, void* input, uint32_t len) {
	const char** s = stringsToZap;
	int found = 0;
	while (*s) {
		found += bufferTryZapString(*s, input, len);
		s++;
	}
	return found;
}
#pragma code_seg(pop, r1)
