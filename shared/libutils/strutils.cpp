/*!
* \file strutils.cpp
*
* \author valerino
* \date novembre 2017
*
* string utilities
*/

#include <windows.h>
#include <stdarg.h>
#include <stdint.h>
#include "strutils.h"
#include "memutils.h"
#include "peutils.h"
#include "procutils.h"
#include "timeutils.h"
#include "dbgutils.h"
#include "refloader.h"
#include "osutils.h"

WNSPRINTFA strPrintfA = NULL;
WNSPRINTFW strPrintfW = NULL;
WVNSPRINTFA strVPrintfA = NULL;
WVNSPRINTFW strVPrintfW = NULL;
STRSTRIA strInStringA = NULL;
STRSTRIW strInStringW = NULL;
STRRCHRW strRightChrW = NULL;
STRRCHRA strRightChrA = NULL;
STRLENW strLenW = NULL;

HMODULE shlwapi = NULL;
int __strInitializeCalled = 0;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
ULONG strInitialize() {
	if (__strInitializeCalled > 0) {
		return 0;
	}
	__strInitializeCalled++;

	uint32_t res = osInitialize();
	if (res != 0) {
		strFinalize();
		return res;
	}

	// get shlwapi
	shlwapi = procFindModuleByHash(0x9bac604d); // => s h l w a p i . d l l
	if (!shlwapi) {
		char dll[] = { 's','h','l','w','a','p','i','.','d','l','l','\0' };
		shlwapi = ptrLoadLibraryA(dll);
		if (!shlwapi) {
			res = GetLastError();
			strFinalize();
			return res;
		}
	}

	// get kernel32
	HMODULE kernel32 = procFindModuleByHash(KERNEL32DLL_HASH_UNICODE); // = > k e r n e l 3 2 . d l l

	// get pointers
	dynamic_import_ptr ptrs[] = {
		{ (ULONG_PTR**)&strPrintfA, shlwapi, 0xed7767ca}, // = > wnsprintfA
		{ (ULONG_PTR**)&strPrintfW, shlwapi, 0xed7767e0}, // = > wnsprintfW
		{ (ULONG_PTR**)&strVPrintfA, shlwapi, 0xad775fe0}, // => wvnsprintfA
		{ (ULONG_PTR**)&strVPrintfW, shlwapi, 0xad775ff6}, // => wvnsprintfW
		{ (ULONG_PTR**)&strInStringA, shlwapi, 0xab9d4f4a}, // = > StrStrIA
		{ (ULONG_PTR**)&strInStringW, shlwapi, 0xab9d4f60}, // = > StrStrIW
		{ (ULONG_PTR**)&strRightChrA, shlwapi,  0x89e53cca }, // => StrRChrA
		{ (ULONG_PTR**)&strRightChrW, shlwapi, 0x89e53ce0 }, // => StrRChrW
		{ (ULONG_PTR**)&strLenW, kernel32, 0x9bc13b41 }, // => lstrlenW
		{ 0,0,0 }
	};
	if (peResolveDynamicImports(ptrs) != 0) {
		strFinalize();
		return ERROR_NOT_FOUND;
	}

	return 0;
}

void strFinalize() {
	if (__strInitializeCalled == 0) {
		return;
	}
	__strInitializeCalled--;

	if (__strInitializeCalled == 0) {
		__strInitializeCalled--;
		if (__strInitializeCalled == 0) {
			if (shlwapi) {
				ptrFreeLibrary(shlwapi);
				shlwapi = NULL;
			}
			osFinalize();
		}
	}
}

ULONG strToWchar(const PCHAR value, PWCHAR* out) {
	*out = NULL;
	int cbUcs2 = MultiByteToWideChar(CP_UTF8, 0, value, -1, NULL, 0);
	if (cbUcs2 == 0) {
		return GetLastError();
	}

	PWCHAR val = (PWCHAR)memHeapAlloc(cbUcs2 * sizeof(WCHAR) + sizeof(WCHAR));
	if (!val) {
		return ERROR_OUTOFMEMORY;
	}
	MultiByteToWideChar(CP_UTF8, 0, value, -1, val, cbUcs2 + sizeof(WCHAR));
	*out = val;
	return 0;
}

void strUnixNowTimestamp(PCHAR out, int outSize, BOOL localTime) {
	__int64 t = timeNowUnix(localTime);
	strPrintfA(out, outSize, "%I64d", t);
}

void strChromeNowTimestamp(PCHAR out, int outSize, BOOL localTime) {
	__int64 t = timeNowChrome(localTime);
	strPrintfA(out, outSize, "%I64d", t);
}

BYTE strHexCharToHexByte(CHAR c) {
	BYTE hexvals[] = { 0x00, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };

	if (c >= 0x30 && c <= 0x39) {
		// numeric
		return(c - 0x30);
	}
	else if (c >= 0x41 && c <= 0x46) {
		c -= 0x40;
		return hexvals[c];
	}
	else if (c >= 0x61 && c <= 0x66) {
		c -= 0x60;
		return hexvals[c];
	}
	return 0;
}

ULONG strToHexA(const PCHAR hexString, PBYTE out, ULONG outSize) {
	uint32_t len = (uint32_t)strlen(hexString);

	if (outSize < (len / 2)) {
		return ERROR_INSUFFICIENT_BUFFER;
	}

	ULONG i = 0;
	ULONG j = 0;
	while (i < len) {
		out[j] = strHexCharToHexByte(hexString[i]) << 4;
		out[j] |= strHexCharToHexByte(hexString[i + 1]);
		i += 2;
		j++;
	}
	return 0;
}

ULONG strToHexW(const PWCHAR hexString, PBYTE out, ULONG outSize) {
	ULONG len = lstrlen(hexString);

	if (outSize < (len / 2)) {
		return ERROR_INSUFFICIENT_BUFFER;
	}

	ULONG i = 0;
	ULONG j = 0;
	while (i < len) {
		out[j] = strHexCharToHexByte((CHAR)hexString[i]) << 4;
		out[j] |= strHexCharToHexByte((CHAR)hexString[i + 1]);
		i += 2;
		j++;
	}
	return 0;
}

uint32_t strFromHex(unsigned char * in, size_t insz, char * out, size_t outsz) {
	if (outsz < (insz * 2) + 1) {
		return ERROR_INSUFFICIENT_BUFFER;
	}

	unsigned char * pin = in;
	const char * hex = "0123456789ABCDEF";
	char * pout = out;
	for (size_t i = 0; i < insz - 1; ++i) {
		*pout++ = hex[(*pin >> 4) & 0xF];
		*pout++ = hex[(*pin++) & 0xF];
	}
	*pout++ = hex[(*pin >> 4) & 0xF];
	*pout++ = hex[(*pin) & 0xF];
	*pout = 0;
	return 0;
}

uint32_t strToHex(const char* string, uint8_t* out, size_t outSize) {
	size_t len = strlen(string);
	if (outSize < (len / 2)) {
		return ERROR_INSUFFICIENT_BUFFER;
	}
	memset(out, 0, outSize);
	size_t index = 0;
	while (index < len) {
		char c = string[index];
		int value = 0;
		if (c >= '0' && c <= '9') {
			value = (c - '0');
		}
		else if (c >= 'A' && c <= 'F') {
			value = (10 + (c - 'A'));
		}
		else if (c >= 'a' && c <= 'f') {
			value = (10 + (c - 'a'));
		}
		else {
			return ERROR_INVALID_DATA;
		}
		out[(index / 2)] += value << (((index + 1) % 2) * 4);

		index++;
	}
	return 0;
}
#pragma code_seg(pop, r1)
