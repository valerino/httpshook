/*!
 * \file jsonutils.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * json utilities (uses jsmn, https://github.com/zserge/jsmn)
 */
#pragma once
#include <jsmn/jsmn.h>

 /*! @brief used by json* functions */
typedef struct _jsonStruct {
	/*! @brief jsmn tokens memory pointer*/
	jsmntok_t* t;
	/*! @brief maximum number of tokens in jsmn tokens memory*/
	int maxTokens;
	/*! @brief the jsmn parser*/
	jsmn_parser p;
	/*! @brief after parsing, how many tokens in this json*/
	int numTokens;
	/*! @brief current index during walking the tokens. always < numTokens */
	int currentIdx;
	/*! @brief the json buffer */
	char* j;

} jsonStruct;

#define JSON_ERROR_BASE 10000

/*! @brief returned by jsonGet*() when tokens exhausted */
#define JSON_ERROR_NO_MORE_TOKENS JSON_ERROR_BASE + 1

/*! @brief invalid json */
#define JSON_ERROR_INVALID JSON_ERROR_BASE + 2

/*! @brief returned by jsonGet*() when the expected type is wrong */
#define JSON_ERROR_WRONG_TYPE JSON_ERROR_BASE + 3

/*!
 * \brief initializes json parser
 *
 * \param jsonStruct* s on successful return, filled struct to be used in subsequent jsonutils calls
 * \param num how many (maximum) tokens we expect in the jsons we process
 * \param json the json buffer to be parsed
 * \return int 0 on success
 */
uint32_t jsonInitialize(jsonStruct* s, int num, char* json);

/*!
 * \brief finalizes json parser
 *
 * \param jsonStruct* s from jsonInitialize()
 */
void jsonFinalize(jsonStruct* s);

/*!
 * \brief gets the next string in tokens array
 *
 * \param jsonStruct* s from jsonInitialize()
 * \param char ** str on successful return, pointer to the next string to be freed with memHeapFree()
 * \return int 0 on success
 */
int jsonGetNextString(jsonStruct* s, char** str);

/*!
* \brief gets string value of the named token in tokens array ("name": "value")
*
* \param jsonStruct* s from jsonInitialize()
* \param name token to find
* \param char ** str on successful return, pointer to value to be freed with memHeapFree()
* \return int 0 on success
*/
int jsonGetStringValue(jsonStruct* s, char* name, char** value);
