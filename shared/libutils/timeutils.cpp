/*!
 * \file timeutils.cpp
 *
 * \author valerino
 * \date novembre 2017
 *
 * time utilities
 */
#include <windows.h>
#include "timeutils.h"

 /* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

 /*!
  * \brief get current time as filetime
  *
  * \param FILETIME * ft on return, the time
  * \param BOOL local TRUE for localtime, FALSE for UTC
  * \return void
  */
void now(FILETIME* ft, BOOL local) {
	FILETIME systemFt;

	// get utc time
	GetSystemTimeAsFileTime(&systemFt);
	if (local) {
		// convert to local
		FileTimeToLocalFileTime(&systemFt, ft);
	}
	else {
		// use utc
		ft->dwHighDateTime = systemFt.dwHighDateTime;
		ft->dwLowDateTime = systemFt.dwLowDateTime;
	}
}

__int64 timeNowChrome(int local) {
	FILETIME ft;

	// get now
	now(&ft, local);

	// convert to microseconds since jan-1-1601
	LARGE_INTEGER date;
	date.HighPart = ft.dwHighDateTime;
	date.LowPart = ft.dwLowDateTime;
	return date.QuadPart / 10;
}

__int64 timeNowUnix(int local) {
	FILETIME ft;

	// get now
	now(&ft, local);

	// convert to seconds since jan-1-1970
	LARGE_INTEGER date, adjust;
	date.HighPart = ft.dwHighDateTime;
	date.LowPart = ft.dwLowDateTime;
	adjust.QuadPart = 11644473600000 * 10000;
	date.QuadPart -= adjust.QuadPart;
	return date.QuadPart / 10000000;
}
#pragma code_seg(pop, r1)

