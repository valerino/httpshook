/*!
 * \file compressutils.cpp
 *
 * \author valerino
 * \date novembre 2017
 *
 * compression utilities
 */

#include <windows.h>
#include <stdint.h>
#include <memutils.h>
#include <dbgutils.h>
#include "compressutils.h"

 /* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
uint32_t compressInitialize(uint8_t** wrkmem) {
#ifdef USE_MINILZO
	// allocate work memory
	uint32_t allocSize = LZO1X_1_MEM_COMPRESS + sizeof(lzo_align_t);
	uint8_t* w = (uint8_t*)memHeapAlloc(allocSize);
	if (!w) {
		return ERROR_OUTOFMEMORY;
	}
	*wrkmem = w;
	DBG_PRINT(DBG_LEVEL_INFO, "compressutils initialized!");
	return lzo_init();
#endif
}

void compressFinalize(uint8_t* wrkmem) {
#ifdef USE_MINILZO
	memHeapFree(wrkmem);
#endif
}

uint32_t compressPack(uint8_t* wrkmem, void* in, uint32_t size, uint8_t** out, uint32_t* outSize) {
	if (!wrkmem || !in || !size || !out || !outSize) {
		return ERROR_INVALID_PARAMETER;
	}
	*outSize = 0;
	uint32_t res = 0;

#ifdef USE_MINILZO
	// allocate memory for compressed buffer
	uint32_t compressedAlloc = size + (size / 16) + 64 + 3;
	*out = (uint8_t*)memHeapAlloc(compressedAlloc);
	if (!*out) {
		return ERROR_OUTOFMEMORY;
	}

	// perform compression
	lzo_uint s;
	res = lzo1x_1_compress((uint8_t*)in, size, *out, &s, wrkmem);
	if (res != LZO_E_OK) {
		memHeapFree(*out);
		*out = NULL;
	}
	*outSize = (uint32_t)s;

#endif
	return res;
}

uint32_t compressUnpack(uint8_t* wrkmem, void* in, uint32_t size, uint32_t originalSize, uint8_t** out, uint32_t* outSize) {
	if (!wrkmem || !in || !size || !originalSize || !out || !outSize) {
		return ERROR_INVALID_PARAMETER;
	}
	uint32_t res = 0;
	*outSize = 0;
#ifdef USE_MINILZO
	// allocate memory for decompressed buffer
	uint32_t allocSize = originalSize + sizeof(lzo_align_t);
	*out = (uint8_t*)memHeapAlloc(allocSize + 32);
	if (!*out) {
		return ERROR_OUTOFMEMORY;
	}

	// perform decompression
	lzo_uint s;
	res = lzo1x_decompress((uint8_t*)in, size, *out, &s, wrkmem);
	if (res == LZO_E_INPUT_NOT_CONSUMED) {
		// take it as ok!
		res = LZO_E_OK;
	}
	if (res != LZO_E_OK) {
		memHeapFree(*out);
		*out = NULL;
	}

	// doublecheck
	if (s != originalSize) {
		memHeapFree(*out);
		*out = NULL;
		res = ERROR_INVALID_DATA;
	}
	*outSize = (uint32_t)s;
#endif

	return res;
}
#pragma code_seg(pop, r1)
