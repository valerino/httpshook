/*!
 * \file osutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * os specific utilities
 */
#pragma once

#include <stdint.h>
#include <AclAPI.h>
#include "osutils_ptr.h"

 /*!
  * \brief checks for a specific windows version
  *
  * \param uint16_t major
  * \param uint16_t minor
  * \return BOOL
  */
BOOL osIsWindowsAtLeast(uint16_t major, uint16_t minor);

/*!
 * \brief check for at least windows 7
 *
 * \return BOOL
 */
BOOL osIsWindows7OrGreater();

/*!
 * \brief check for at least windows 8.1
 *
 * \return BOOL
 */
BOOL osIsWindows81OrGreater();

/*!
* \brief initializes api, must be called once per process
* \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
* \return uint32_t 0 on success
*/
ULONG osInitialize();

/*!
* \brief finalizes api
* \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
* \return uint32_t
*/
void osFinalize();

/*!
 * \brief initializes a security attributes with a NULL DACL, to grant all access to everyone
 *
 * \param LPSECURITY_ATTRIBUTES sa
 * \param PSECURITY_DESCRIPTOR sd
 * \return ULONG 0 on success
 */
ULONG osInitializeSecurityAttributesWithNullDacl(LPSECURITY_ATTRIBUTES sa, PSECURITY_DESCRIPTOR sd);