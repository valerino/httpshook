/*!
 * \file b64utils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * base64 encoder/decoder
 */
#pragma once

 /*!
 * \brief initializes api, must be called once per process
 * \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
 * \calls osInitialize()
 * \return uint32_t 0 on success
 */
uint32_t base64Initialize();

/*!
 * \brief finalizes api
 * \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
 * \calls osFinalize()
 * \return uint32_t
 */
void base64Finalize();

/*!
 * \brief encode buffer to base64
 *
 * \param uint8_t * data buffer to be encoded
 * \param uint32_t size encoded size
 * \param char * * b64 on successful return, base64 buffer to be freed with memHeapFree()
 * \param uint32_t * b64size on successful return, size of the allocated b64 buffer
 * \return uint32_t 0 on success
 */
uint32_t base64Encode(uint8_t* data, uint32_t size, char** b64, uint32_t* b64size);

/*!
 * \brief decode base64 buffer
 *
 * \param char * b64 base64 buffer to be decoded, string
 * \param char * * decoded on successful return, decoded buffer to be freed with memHeapFree()
 * \param uint32_t * decodedSize on successful return, size of the allocated buffer
 * \return uint32_t 0 on success
 */
uint32_t base64Decode(char* b64, uint8_t** decoded, uint32_t* decodedSize);