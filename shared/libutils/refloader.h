/*!
 * \file refloader.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * reflective loader and utilities, inspired by metasploit :)
 */
#pragma once

#define DEREF( name )*(UINT_PTR *)(name)
#define DEREF_64( name )*(DWORD64 *)(name)
#define DEREF_32( name )*(DWORD *)(name)
#define DEREF_16( name )*(WORD *)(name)
#define DEREF_8( name )*(BYTE *)(name)

typedef BOOL(WINAPI * DLLMAIN)(HINSTANCE, DWORD, LPVOID);
typedef HMODULE(WINAPI * LOADLIBRARYA)(LPCSTR);
typedef FARPROC(WINAPI * GETPROCADDRESS)(HMODULE, LPCSTR);
typedef LPVOID(WINAPI * VIRTUALALLOC)(LPVOID, SIZE_T, DWORD, DWORD);
typedef DWORD(NTAPI * NTFLUSHINSTRUCTIONCACHE)(HANDLE, PVOID, ULONG);
typedef LPVOID(WINAPI * VIRTUALLOCK)(LPVOID, SIZE_T);
typedef BOOL(WINAPI* VIRTUALFREE)(LPVOID, SIZE_T, DWORD);

/* hashes */
#define KERNEL32DLL_HASH_UNICODE		0x6A4ABC5B
#define KERNEL32DLL_HASH_ANSI			0x6e2bca17
#define KERNELBASEDLL_HASH_UNICODE		0x2defbae3
#define NTDLL_HASH_UNICODE			0x3CFA685D

#define LOADLIBRARYA_HASH				0x8a8b4676
#define GETPROCADDRESS_HASH				0x1acaee7a
#define VIRTUALALLOC_HASH				0x302ebe1c
#define NTFLUSHINSTRUCTIONCACHE_HASH	0xd95a3b7f
#define VIRTUALLOCK_HASH				0xed7326c1
#define VIRTUALFREE_HASH				0xe183277b

/*! \brief calling DllMain() with this executes the PE loader, which maps the DLL in memory */
#define DLLMAIN_RUN_PELOADER			-1

/*! \brief calling DllMain() with this executes a function which, if returns FALSE, causes the PE memory to be freed (load check fails)
	this should be called right after calling DllMain() with DLLMAIN_RUN_PELOADER and before calling DllMain() with DLL_PROCESS_ATTACH as normal */
#define DLLMAIN_RUN_LOADCHECK			-2

	/*!
	 * \brief get PE entrypoint address RVA (offset from imagebase).
	 *
	 * \note this also checks the peImage is the same architecture than the reflective loader
	 *
	 * \param VOID * peImage PE base address
	 * \note position independent (PIC) code, will be linked in '.text2' section
	 * \return extern " DWORD 0 on error
	 */
extern "C" DWORD reflectivePEGetEntrypointRVA(VOID * peImage);

/*!
 * \brief this is a PE loader, which fully maps a PE DLL in memory resolving the imports and relocations.
 *	once returned from this function, the PE entrypoint can be called.
 *
 * \param ULONG_PTR peBuffer a buffer representing a PE DLL file (32 or 64) (the imagebase)
 * \param ULONG_PTR * newBase on successful return, the new imagebase the PE is mapped to.
 * \note actually, the newBase returned is the same as peBuffer since the function expects dll_buffer to be an already executable buffer. no further allocations are done (TODO ?)
 * \note also, actually this function expects file alignment = section alignments to correctly load and map the PE (TODO: support ?)
 * \return BOOL TRUE if the mapping succeeded
 */
BOOL reflectivePELoader(ULONG_PTR peBuffer, ULONG_PTR* newBase);
