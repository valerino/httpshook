/*!
* \file jsonutils.cpp
*
* \author valerino
* \date dicembre 2017
*
* json utilities (uses jsmn, https://github.com/zserge/jsmn)
*/
#ifdef _WIN32
#include <windows.h>
#endif
#include <stdio.h>
#include <stdint.h>
#include "jsonutils.h"
#include <memutils.h>

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

uint32_t jsonInitialize(jsonStruct* s, int num, char* json) {
	memset(s, 0, sizeof(jsonStruct));

	// allocate tokens memory
	jsmntok_t* mem = (jsmntok_t*)memHeapAlloc((num + 1) * sizeof(jsmntok_t));
	if (!mem) {
		return ERROR_OUTOFMEMORY;
	}

	// allocate json buffer
	char* p = (char*)memHeapAlloc((int)strlen(json) + 1);
	if (!p) {
		memHeapFree(mem);
		return ERROR_OUTOFMEMORY;
	}
	strcpy(p, json);

	// parse json
	jsmn_init(&s->p);
	int res = jsmn_parse(&s->p, p, strlen(p), mem, num);
	if (res < 0) {
		memHeapFree(mem);
		memHeapFree(p);
		return JSON_ERROR_INVALID;
	}
	s->numTokens = res;
	s->maxTokens = num;
	s->j = p;
	s->t = mem;
	return 0;
}

void jsonFinalize(jsonStruct* s) {
	if (s) {
		if (s->t) {
			memHeapFree(s->t);
		}
		if (s->j) {
			memHeapFree(s->j);
		}
	}
}

int jsonGetNextString(jsonStruct* s, char** str) {
	*str = NULL;
	if (s->currentIdx == s->numTokens) {
		return JSON_ERROR_NO_MORE_TOKENS;
	}
	if (s->t[s->currentIdx].type != JSMN_STRING) {
		s->currentIdx++;
		return JSON_ERROR_WRONG_TYPE;
	}

	// copy the string
	int len = s->t[s->currentIdx].end - s->t[s->currentIdx].start;
	char* p = (char*)memHeapAlloc(len + 1);
	if (!p) {
		return ERROR_OUTOFMEMORY;
	}
	char* start = s->j + s->t[s->currentIdx].start;
	memcpy(p, start, len);
	s->currentIdx++;
	*str = p;
	return 0;
}

int jsonGetStringValue(jsonStruct* s, char* name, char** value) {
	int err = 0;
	while (err != JSON_ERROR_NO_MORE_TOKENS) {
		char* str = NULL;
		err = jsonGetNextString(s, &str);
		if (str && (strcmp(str, name) == 0)) {
			// get the next value
			memHeapFree(str);
			err = jsonGetNextString(s, value);

			// found, we also reset the current index
			s->currentIdx = 0;
			break;
		}
	}
	return err;
}
#pragma code_seg(pop, r1)
