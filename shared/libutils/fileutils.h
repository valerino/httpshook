/*!
 * \file fileutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * file related utilities
 */

#pragma once

 /*!
  * \brief read whole file to buffer
  *
  * \param PWCHAR path path to file to be read
  * \param PULONG fileSize on successful return, the allocated buffer size
  * \return void* pointer to buffer, must be freed with memHeapFree(), NULL on error
  */
void* fileToBuffer(PWCHAR path, PULONG fileSize);

/*!
 * \brief create file with buffer content
 *
 * \param const PWCHAR path path to the file to be written, will be overwritten if exists
 * \param const void * buffer buffer to write
 * \param ULONG size size of buffer
 * \param DWORD flags flags for CreateFile(), default FILE_ATTRIBUTE_NORMAL
 * \return ULONG 0 on success
 */
ULONG fileFromBuffer(const PWCHAR path, const void* buffer, ULONG size, DWORD flags = FILE_ATTRIBUTE_NORMAL);

/*!
 * \brief generate a string with a path in the user temp folder with random name
 *
 * \param const PWCHAR path destination string buffer
 * \param int pathSize size of the destination string buffer, in characters
 * \param const PWCHAR ext an extension, without dot (i.e. L"exe", L"tmp", ...)
 * \return void
 */
void fileTempPath(const PWCHAR path, int pathSize, const PWCHAR ext);

/*!
* \brief generate a string with a path in the user temp folder with the given name
*
* \param const PWCHAR path destination string buffer
* \param int pathSize size of the destination string buffer, in characters
* \param const PWCHAR filename (i.e. setup.exe) to be generated
* \return void
*/
void fileTempPathName(const PWCHAR path, int pathSize, const PWCHAR name);

/*!
* \brief initializes api, must be called once per process
* \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
* \calls strInitialize(), cryptInitialize(), procInitialize()
* \return uint32_t 0 on success
*/
ULONG fileInitialize();

/*!
* \brief finalizes api
* \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
* \calls strFinalize(), cryptFinalize(), procFinalize()
* \return uint32_t
*/
void fileFinalize();

/*!
* \brief delete ourself via a .bat as soon as the current process exits
* \return ULONG 0 on success
*/
ULONG fileDeleteSelfOnExit();

/*!
 * \brief purge a path from a specific type of file (not recursive)
 *
 * \param const PWCHAR path path to be scanned
 * \param const PWCHAR mask a mask, or NULL ('*.*' is used)
 * \return ULONG
 */
ULONG filePurgeFolder(const PWCHAR path, const PWCHAR mask);
