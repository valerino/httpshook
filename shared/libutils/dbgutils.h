/*!
 * \file dbgutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * debug print primitives
 */

#include <stdio.h>

#define DBG_LEVEL_VERBOSE 4
#define DBG_LEVEL_ERROR 3
#define DBG_LEVEL_WARNING 2
#define DBG_LEVEL_INFO 1

 /*!
  * \brief dbgprint, ansi, format support.
  * \note strInitialize() must be called first!
  *
  * \param int level
  * \param const char * file
  * \param const char * function
  * \param int line
  * \param const char * format
  * \param ...
  * \return void
  */
#ifdef __cplusplus
extern "C" {
#endif
	void dbgOut(int level, const char* file, const char* function, int line, const char* format, ...);
#ifdef __cplusplus
}
#endif

#if defined (_DEBUG) || defined (DBG_PRINT_ENABLED)
/** @brief macro to call dbgOut, does nothing on release builds */
#define DBG_PRINT(lvl, ...) dbgOut (lvl, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
#else
#define DBG_PRINT(lvl, ...)
#endif

/*!
 * \brief sets the global debug level, lower masks higher (INFO masks VERBOSE)
 *
 * \param int level one of the DBG_LEVEL_* (default is VERBOSE)
 * \return void
 */
void dbgSetLevel(int level);
