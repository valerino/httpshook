/*!
* \file memutils.h
*
* \author valerino
* \date novembre 2017
*
* memory allocation utilities
*/

#pragma once
#include <stdint.h>

/*!
* \brief check if a pointer is valid performing size/boundary checks
*
* \param const LPVOID buffer_bgn buffer to validate begin
* \param SIZE_T buffer_size buffer to validate size
* \param const LPVOID field_bgn pointer to something inside the (buffer + buffer_size)
* \param SIZE_T field_size size of something
* \return bool TRUE if the pointer is valid, FALSE if it goes out of range
*/
BOOL memValidatePtr(const LPVOID buffer_bgn, SIZE_T buffer_size, const LPVOID field_bgn, SIZE_T field_size);

/*!
 * \brief allocate memory from the process heap
 *
 * \param uint32_t size size to be allocated
 * \return uint8_t* pointer to allocated buffer, must be freed with memHeapFree()
 */
void* memHeapAlloc(uint32_t size);

/*!
 * \brief free memory allocated with memHeapAlloc()
 *
 * \param void* ptr pointer to the allocated block. it's ok to pass NULL
 * \return void
 */
void memHeapFree(void* ptr);

/**
* memHeapRealloc
*
* \brief reallocates a memory block allocated with memHeapAlloc(), to be freed with memHeapFree()
* \param void* ptr the already allocated block. if NULL, the function behaves exactly as memHeapAlloc() using newSize as size.
* \param uint32_t newSize the new size for the block at ptr
* \return  void* if successful, the reallocated memory (guaranteed to be zeroed). if NULL, GetLastError() is set to ERROR_OUT_OF_MEMORY.
*/
void* memHeapRealloc(void* ptr, uint32_t newSize);
