/*!
* \file compressutils.h
*
* \author valerino
* \date novembre 2017
*
* compression utilities
*/

#pragma once
#include <stdint.h>

#define USE_MINILZO
#ifdef USE_MINILZO
#include <minilzo/minilzo.h>
#endif

/*!
 * \brief initializes the compression support, use compressFinalize() to release resources
 * \note can be called multiple time per process, to initialize different workmemories (to support multithreading)
 * uint8_t* wrkMem on successful return, workmemory to be passed to compressFinalize() when done
 * \return uint32_t 0 on success
 */
uint32_t compressInitialize(uint8_t** wrkMem);

/*!
* \brief release compression resources
* uint8_t* wrkMem workmemory returned by compressInitialize()
*/
void compressFinalize(uint8_t* wrkMem);

/*!
 * \brief compress a buffer, using minilzo if USE_MINILZO is defined or using RtlCompressBuffer (opensource implementation: https://github.com/coderforlife/ms-compress)
 *
 * \param uint8_t* wrkmem workmemory returned by compressInitialize()
 * \param void * in the decompressed input buffer
 * \param uint32_t size size of the input buffer
 * \param uint8_t * * out on successful return, the compressed buffer (to be freed with memHeapFree())
 * \param uint32_t * outSize on successful return, the compressed buffer size
 * \return uint32_t 0 on success
 */
uint32_t compressPack(uint8_t* wrkmem, void* in, uint32_t size, uint8_t** out, uint32_t* outSize);

/*!
* \brief decompress a buffer, using minilzo if USE_MINILZO is defined or using RtlDecompressBuffer (opensource implementation: https://github.com/coderforlife/ms-compress)
*
* \param uint8_t* wrkmem workmemory returned by compressInitialize()
* \param void * in the compressed input buffer
* \param uint32_t size compressed size of the input buffer
* \param uint32_t originalSize original size of the input buffer
* \param uint8_t * * out on successful return, the compressed buffer (to be freed with memHeapFree()). The buffer is guaranteed to have bunch of \0\0 in the end!
* \param uint32_t * outSize on successful return, the compressed buffer size
* \return uint32_t 0 on success
*/
uint32_t compressUnpack(uint8_t* wrkmem, void* in, uint32_t size, uint32_t originalSize, uint8_t** out, uint32_t* outSize);
