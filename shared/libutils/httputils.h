/*!
 * \file httputils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * http request utilities using winhttp
 */
#pragma once
#include <winhttp.h>

#ifndef WINHTTP_ACCESS_TYPE_AUTOMATIC_PROXY
#define WINHTTP_ACCESS_TYPE_AUTOMATIC_PROXY 4
#endif

 /* returned by httpOpenHandles() */
typedef struct _http_handles {
	HINTERNET connect;
	HINTERNET session;
	HINTERNET req;
} http_handles;

/*!
* \brief initializes api, must be called once per process
* \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
* \note calls strInitialize(), osInitialize()
* \return uint32_t 0 on success
*/
ULONG httpInitialize();

/*!
* \brief finalizes api
* subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
* \note calls strFinalize(), osFinalize()
* \return uint32_t
*/
void httpFinalize();

/*!
 * \brief initializes an http/s connection
 *
 * \param const PWCHAR url the target url
 * \param const PWCHAR httpCommand "POST" or "GET
 * \param const PWCHAR referrer if not NULL, referrer
 * \param http_handles * handles on successful return, http handles to be used in subsequent calls. to be closed with httpCloseHandles()
 * \return uint32_t
 */
uint32_t httpOpenHandles(const PWCHAR url, const PWCHAR referrer, const PWCHAR httpCommand, http_handles* handles);

/*!
* \brief releases resources from openHttpHandles()
*
* \param http_handles * handles from httpOpenHandles()
* \return void
*/
void httpCloseHandles(http_handles* handles);

/*!
* \brief get response status
*
* \param handles from httpOpenHandles()
* \param uint32_t* httpStatus status on successful return, the status value (200, 400, ....)
* \return uint32_t 0 on success
*/
uint32_t httpQueryResponseStatus(http_handles* handles, uint32_t* httpStatus);

/*!
 * \brief send a request, wraps WinHttpSendRequest(), handles resend for ignoring HTTPS missing certificates
 *
 * \param handles from httpOpenHandles()
 * \param const PWCHAR headers if not NULL, any header to be sent (must be a double 0 terminated string array, each including boundary and termination)
 * \param void * lpOptional look at WinHttpSendRequest() documentation, must include the boundary already set in headers if any, and termination
 * \param uint32_t dwOptionalLength look at WinHttpSendRequest() documentation, size of the data to be sent with the request
 * \param uint32_t dwTotalLength look at WinHttpSendRequest() documentation, usually = dwOptionalLength unless you want to send further data
 * \param uintptr_t dwContext look at WinHttpSendRequest() documentation, usually 0
 * \return uint32_t 0 on success
 */
uint32_t httpSendRequest(http_handles* handles, const PWCHAR headers, void* lpOptional, uint32_t dwOptionalLength, uint32_t dwTotalLength, uintptr_t dwContext);

/*!
* \brief download response after a request, in memory or to file
*
* \param handles from httpOpenHandles()
* \param const PWCHAR outPath if not NULL, path to the file to store the response. if NULL, the response will be stored in memory at the response buffer
* \param uint8_t* response on successful return, pointer to the response buffer, ignored when outPath is not NULL. guaranteed to be zero-terminated, must be freed with memHeapFree()
* \param uint32_t* respSize on successful return, size of the response buffer, ignored when outPath is not NULL
* \param uint32t* httpStatus on return, the HTTP status code
* \return uint32_t 0, or an error defined in winerror.h
*/
uint32_t httpDownloadResponse(http_handles* handles, const PWCHAR outPath, uint8_t** response, uint32_t* respSize, uint32_t* httpStatus);

/*!
 * \brief perform GET to download a file (basically a shortcut for httpDownloadResponse())
 *
 * \param const PWCHAR url the url to download from
 * \param const PWCHAR downloadPath if not NULL, a path to download file to (data and size are ignored)
 * \param const PWCHAR referrer if not NULL, referrer
 * \param const PWCHAR headers if not NULL, any header to be sent (must be a double 0 terminated string array, each including boundary and termination)
 * \param uint8_t * * data if downloadPath is NULL, on successful return buffer with the downloaded file (must be freed with memHeapFree())
 * \param uint32_t * size if donwloadPath is NULL, on successful return size of the downloaded file
 * \param uint32t* httpStatus on return, the HTTP status code
 * \return uint32_t 0 on success
 */
uint32_t httpGet(const PWCHAR url, const PWCHAR referrer, const PWCHAR headers, const PWCHAR downloadPath, uint8_t**data, uint32_t* size, uint32_t* httpStatus);

/*!
* \brief perform POST to a upload buffer (basically a shortcut for httpSendRequest())
*
* \param const PWCHAR url the url to post to
* \param const PWCHAR referrer if not NULL, referrer
* \param const PWCHAR headers if not NULL, any header to be sent
* \param uint8_t * data buffer to be uploaded
* \param uint32_t size size of the buffer to be uploaded
* \param handles on successful return, handles to be closed with httpCloseHandles()
* \return uint32_t 0 on success
*/
uint32_t httpPost(const PWCHAR url, const PWCHAR referrer, const PWCHAR headers, uint8_t* data, uint32_t size, http_handles* handles);
