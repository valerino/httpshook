/*!
 * \file injutils_ptr.h
 *
 * \author valerino
 * \date dicembre 2017
 *
 * import definitions for injutils
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

	/*
	the following pointers are available after calling injectInitialize(), and remains valid also after calling injectFinalize()
	*/
	typedef HANDLE(WINAPI* CREATEREMOTETHREAD)(HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter,
		DWORD dwCreationFlags, LPDWORD lpThreadId);
	extern CREATEREMOTETHREAD ptrCreateRemoteThread;

	typedef HANDLE(WINAPI* OPENPROCESS)(DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwProcessId);
	extern OPENPROCESS ptrOpenProcess;

	typedef BOOL(WINAPI* WRITEPROCESSMEMORY)(HANDLE hProcess, LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize, SIZE_T * lpNumberOfBytesWritten);
	extern WRITEPROCESSMEMORY ptrWriteProcessMemory;

	typedef BOOL(WINAPI* READPROCESSMEMORY)(HANDLE hProcess, LPCVOID lpBaseAddress, LPVOID lpBuffer, SIZE_T nSize, SIZE_T * lpNumberOfBytesRead);
	extern READPROCESSMEMORY ptrReadProcessMemory;

	typedef BOOL(WINAPI* VIRTUALPROTECT)(LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, PDWORD lpflOldProtect);
	extern VIRTUALPROTECT ptrVirtualProtect;

	typedef BOOL(WINAPI* VIRTUALPROTECTEX)(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, PDWORD lpflOldProtect);
	extern VIRTUALPROTECTEX ptrVirtualProtectEx;

	typedef LPVOID(WINAPI* VIRTUALALLOCEX)(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
	extern VIRTUALALLOCEX ptrVirtualAllocEx;

	typedef BOOL(WINAPI* VIRTUALFREEEX)(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);
	extern VIRTUALFREEEX ptrVirtualFreeEx;

	typedef BOOL(WINAPI* VIRTUALFREE)(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);
	extern VIRTUALFREE ptrVirtualFree;

	typedef HANDLE(WINAPI* OPENTHREAD)(DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwThreadId);
	extern OPENTHREAD ptrOpenThread;

	typedef DWORD(WINAPI* QUEUEUSERAPC)(PAPCFUNC pfnAPC, HANDLE hThread, ULONG_PTR dwData);
	extern QUEUEUSERAPC ptrQueueUserApc;

	typedef BOOL(WINAPI* SETTHREADCONTEXT)(HANDLE hThread, CONTEXT * lpContext);
	extern SETTHREADCONTEXT ptrSetThreadContext;

	typedef BOOL(WINAPI* GETTHREADCONTEXT)(HANDLE hThread, LPCONTEXT lpContext);
	extern GETTHREADCONTEXT ptrGetThreadContext;

	typedef DWORD(WINAPI* RESUMETHREAD)(HANDLE hThread);
	extern RESUMETHREAD ptrResumeThread;

	typedef DWORD(WINAPI* SUSPENDTHREAD)(HANDLE hThread);
	extern SUSPENDTHREAD ptrSuspendThread;

	typedef SIZE_T(WINAPI* VIRTUALQUERY)(LPCVOID lpAddress, PMEMORY_BASIC_INFORMATION lpBuffer, SIZE_T dwLength);
	extern VIRTUALQUERY ptrVirtualQuery;

	typedef BOOL(WINAPI* FLUSHINSTRUCTIONCACHE)(HANDLE hProcess, LPCVOID lpBaseAddress, SIZE_T dwSize);
	extern FLUSHINSTRUCTIONCACHE ptrFlushInstructionCache;

	typedef LPVOID(WINAPI* VIRTUALALLOC)(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
	extern VIRTUALALLOC ptrVirtualAlloc;

	typedef UINT(WINAPI* GETSYSTEMDIRECTORYA)(PCHAR lpBuffer, UINT uSize);
	extern GETSYSTEMDIRECTORYA ptrGetSystemDirectoryA;

	typedef SIZE_T(WINAPI* VIRTUALQUERYEX)(HANDLE hProcess, LPCVOID lpAddress, PMEMORY_BASIC_INFORMATION lpBuffer, SIZE_T dwLength);
	extern VIRTUALQUERYEX ptrVirtualQueryEx;

#if defined(_WIN64)
	typedef BOOL(WINAPI* WOW64GETTHREADCONTEXT)(HANDLE hThread, PWOW64_CONTEXT lpContext);
	extern WOW64GETTHREADCONTEXT ptrWow64GetThreadContext;

	typedef BOOL(WINAPI* WOW64SETTHREADCONTEXT)(HANDLE hThread, WOW64_CONTEXT *lpContext);
	extern WOW64SETTHREADCONTEXT ptrWow64SetThreadContext;

	typedef UINT(WINAPI* GETSYSTEMWOW64DIRECTORYA)(PCHAR lpBuffer, UINT uSize);
	extern GETSYSTEMWOW64DIRECTORYA ptrGetSystemWow64DirectoryA;
#endif

#ifdef __cplusplus
}
#endif
