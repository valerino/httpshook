/*!
* \file cryptutils.cpp
*
* \author valerino
* \date novembre 2017
*
* cryptographic utilities
*/

#include <windows.h>
#include <bcrypt.h>
#include "fileutils.h"
#include "memutils.h"
#include "peutils.h"
#include "procutils.h"
#include "cryptutils.h"
#include "dbgutils.h"
#include "strutils.h"
#include "osutils.h"
#include <stdint.h>

BCRYPT_ALG_HANDLE algAes = NULL;
BCRYPT_ALG_HANDLE algRsa = NULL;
ULONG aesObjectLen = 0;
/*
	dynamically imported bcrypt functions
*/
HMODULE bcrypt = NULL;
int __cryptInitializeCalled = 0;

typedef NTSTATUS(WINAPI* BCRYPTGENRANDOM)(BCRYPT_ALG_HANDLE hAlgorithm, PUCHAR pbBuffer, ULONG cbBuffer, ULONG dwFlags);
BCRYPTGENRANDOM ptrBCryptGenRandom = NULL;

typedef NTSTATUS(WINAPI* BCRYPTOPENALGORITHMPROVIDER)(BCRYPT_ALG_HANDLE *phAlgorithm, LPCWSTR pszAlgId, LPCWSTR pszImplementation, ULONG dwFlags);
BCRYPTOPENALGORITHMPROVIDER ptrBCryptOpenAlgorithmProvider = NULL;

typedef NTSTATUS(WINAPI* BCRYPTCLOSEALGORITHMPROVIDER)(BCRYPT_ALG_HANDLE hAlgorithm, ULONG dwFlags);
BCRYPTCLOSEALGORITHMPROVIDER ptrBCryptCloseAlgorithmProvider = NULL;

typedef NTSTATUS(WINAPI* BCRYPTGENERATEKEYPAIR)(BCRYPT_ALG_HANDLE hAlgorithm, BCRYPT_KEY_HANDLE *phKey, ULONG dwLength, ULONG dwFlags);
BCRYPTGENERATEKEYPAIR ptrBCryptGenerateKeyPair = NULL;

typedef NTSTATUS(WINAPI* BCRYPTFINALIZEKEYPAIR)(BCRYPT_KEY_HANDLE hKey, ULONG dwFlags);
BCRYPTFINALIZEKEYPAIR ptrBCryptFinalizeKeyPair = NULL;

typedef NTSTATUS(WINAPI* BCRYPTSETPROPERTY)(BCRYPT_HANDLE hObject, LPCWSTR pszProperty, PUCHAR pbInput, ULONG cbInput, ULONG dwFlags);
BCRYPTSETPROPERTY ptrBCryptSetProperty = NULL;

typedef NTSTATUS(WINAPI* BCRYPTGETPROPERTY)(BCRYPT_HANDLE hObject, LPCWSTR pszProperty, PUCHAR pbOutput, ULONG cbOutput, ULONG *pcbResult, ULONG dwFlags);
BCRYPTGETPROPERTY ptrBCryptGetProperty = NULL;

typedef NTSTATUS(WINAPI* BCRYPTENCRYPT)(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID *pPaddingInfo, PUCHAR pbIV, ULONG cbIV, PUCHAR pbOutput, ULONG cbOutput, ULONG *pcbResult, ULONG dwFlags);
BCRYPTENCRYPT ptrBCryptEncrypt = NULL;

typedef NTSTATUS(WINAPI* BCRYPTDECRYPT)(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID *pPaddingInfo, PUCHAR pbIV, ULONG cbIV, PUCHAR pbOutput, ULONG cbOutput, ULONG *pcbResult, ULONG dwFlags);
BCRYPTENCRYPT ptrBCryptDecrypt = NULL;

typedef NTSTATUS(WINAPI* BCRYPTGENERATESYMMETRICKEY)(BCRYPT_ALG_HANDLE hAlgorithm, BCRYPT_KEY_HANDLE *phKey, PUCHAR pbKeyObject, ULONG cbKeyObject, PUCHAR pbSecret, ULONG cbSecret, ULONG dwFlags);
BCRYPTGENERATESYMMETRICKEY ptrBCryptGenerateSymmetricKey = NULL;

typedef NTSTATUS(WINAPI* BCRYPTEXPORTKEY)(BCRYPT_KEY_HANDLE   hKey, BCRYPT_KEY_HANDLE   hExportKey, LPCWSTR pszBlobType, PUCHAR   pbOutput, ULONG   cbOutput, ULONG   *pcbResult, ULONG   dwFlags);
BCRYPTEXPORTKEY ptrBCryptExportKey = NULL;

typedef NTSTATUS(WINAPI* BCRYPTIMPORTKEY)(BCRYPT_ALG_HANDLE hAlgorithm, BCRYPT_KEY_HANDLE hImportKey, LPCWSTR pszBlobType, BCRYPT_KEY_HANDLE *phKey, PUCHAR pbKeyObject, ULONG cbKeyObject, PUCHAR pbInput,
	ULONG cbInput, ULONG dwFlags);
BCRYPTIMPORTKEY ptrBCryptImportKey = NULL;

typedef NTSTATUS(WINAPI* BCRYPTIMPORTKEYPAIR)(BCRYPT_ALG_HANDLE hAlgorithm, BCRYPT_KEY_HANDLE hImportKey, LPCWSTR pszBlobType, BCRYPT_KEY_HANDLE *phKey, PUCHAR pbInput, ULONG cbInput, ULONG dwFlags);
BCRYPTIMPORTKEYPAIR ptrBCryptImportKeyPair = NULL;

typedef NTSTATUS(WINAPI* BCRYPTDESTROYKEY)(BCRYPT_KEY_HANDLE hKey);
BCRYPTDESTROYKEY ptrBCryptDestroyKey = NULL;

typedef NTSTATUS(WINAPI* BCRYPTHASHDATA)(BCRYPT_HASH_HANDLE hHash, PUCHAR pbInput, ULONG cbInput, ULONG dwFlags);
BCRYPTHASHDATA ptrBCryptHashData = NULL;

typedef NTSTATUS(WINAPI* BCRYPTCREATEHASH)(BCRYPT_ALG_HANDLE hAlgorithm, BCRYPT_HASH_HANDLE *phHash, PUCHAR pbHashObject, ULONG cbHashObject, PUCHAR pbSecret, ULONG cbSecret, ULONG dwFlags);
BCRYPTCREATEHASH ptrBCryptCreateHash = NULL;

typedef NTSTATUS(WINAPI* BCRYPTDESTROYHASH)(BCRYPT_HASH_HANDLE hHash);
BCRYPTDESTROYHASH ptrBCryptDestroyHash = NULL;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
uint32_t encryptDecrypt(BCRYPT_KEY_HANDLE k, uint8_t* iv, uint8_t* buf, uint32_t size, uint8_t** out, uint32_t* outSize, BOOL encrypt, BOOL rsa = FALSE) {
	ULONG cbResult;
	*outSize = 0;
	*out = NULL;
	NTSTATUS res = 0;
	uint8_t _iv[AES_IV_BITS / 8] = { 0 };
	BOOL hasIv = (iv != NULL);
	if (hasIv) {
		// avoid function modifies iv, as msdn says
		memcpy(_iv, iv, AES_IV_BITS / 8);
	}

	// calculate needed size
	if (encrypt) {
		res = ptrBCryptEncrypt(k, buf, size, NULL, hasIv ? _iv : NULL, hasIv ? (AES_IV_BITS / 8) : 0, NULL, 0, &cbResult, rsa ? BCRYPT_PAD_PKCS1 : BCRYPT_BLOCK_PADDING);
	}
	else {
		res = ptrBCryptDecrypt(k, buf, size, NULL, hasIv ? _iv : NULL, hasIv ? (AES_IV_BITS / 8) : 0, NULL, 0, &cbResult, rsa ? BCRYPT_PAD_PKCS1 : BCRYPT_BLOCK_PADDING);
	}
	if (res != 0) {
		return res;
	}

	// allocate memory
	uint32_t allocSize = cbResult;
	*out = (uint8_t*)memHeapAlloc(cbResult + 32);
	if (!*out) {
		return ERROR_OUTOFMEMORY;
	}

	// perform encryption/decryption
	if (encrypt) {
		res = ptrBCryptEncrypt(k, buf, size, NULL, hasIv ? _iv : NULL, hasIv ? (AES_IV_BITS / 8) : 0, *out, allocSize, &cbResult, rsa ? BCRYPT_PAD_PKCS1 : BCRYPT_BLOCK_PADDING);
	}
	else {
		res = ptrBCryptDecrypt(k, buf, size, NULL, hasIv ? _iv : NULL, hasIv ? (AES_IV_BITS / 8) : 0, *out, allocSize, &cbResult, rsa ? BCRYPT_PAD_PKCS1 : BCRYPT_BLOCK_PADDING);
	}
	*outSize = cbResult;
	return res;
}

uint32_t cryptAesEncrypt(BCRYPT_KEY_HANDLE k, uint8_t* iv, uint8_t* buf, uint32_t size, uint8_t** out, uint32_t* outSize) {
	return encryptDecrypt(k, iv, buf, size, out, outSize, TRUE);
}

uint32_t cryptAesDecrypt(BCRYPT_KEY_HANDLE k, uint8_t* iv, uint8_t* buf, uint32_t size, uint8_t** out, uint32_t* outSize, BOOL forcePkcs1Padding) {
	BOOL rsa = FALSE;
	if (forcePkcs1Padding) {
		rsa = TRUE;
	}
	return encryptDecrypt(k, iv, buf, size, out, outSize, FALSE, rsa);
}

uint32_t cryptAesInitializeKey(aes_key_material* km, BOOL random, BCRYPT_KEY_HANDLE* k) {
	if (!km) {
		return ERROR_INVALID_PARAMETER;
	}
	*k = NULL;

	if (random) {
		// generate random key and iv
		cryptGenerateRandomBuffer(km->k, AES_KEY_BITS / 8);
		cryptGenerateRandomBuffer(km->iv, AES_IV_BITS / 8);
	}

	// create the simmetric key
	km->reserved = (uint8_t*)memHeapAlloc(aesObjectLen);
	if (!km->reserved) {
		return ERROR_OUTOFMEMORY;
	}
	ULONG keylen = AES_KEY_BITS / 8;
	uint32_t res = ptrBCryptGenerateSymmetricKey(algAes, k, km->reserved, aesObjectLen, km->k, keylen, 0);
	if (res != 0) {
		memHeapFree(km->reserved);
		km->reserved = NULL;
	}
	return res;
}

uint32_t cryptRsaUnwrapAesKey(BCRYPT_KEY_HANDLE rsaPriv, uint8_t* wrapped, uint32_t size, aes_key_material* km, BCRYPT_KEY_HANDLE* k) {
	if (!k) {
		return ERROR_INVALID_PARAMETER;
	}

	*k = NULL;

	// decrypt the key material 
	// size here must be = the key length, or the function will fail!
	uint8_t* out;
	uint32_t outSize;
	uint32_t res = encryptDecrypt(rsaPriv, NULL, wrapped, size, &out, &outSize, FALSE, TRUE);
	if (res != 0) {
		return res;
	}
	memcpy(km, out, sizeof(aes_key_material));
	memHeapFree(out);

	// use the key material to obtain a key handle
	return cryptAesInitializeKey(km, FALSE, k);
}

uint32_t cryptRsaWrapAesKey(BCRYPT_KEY_HANDLE rsaRecipientPub, uint32_t keyLenBytes, aes_key_material* km, uint8_t** wrapped, uint32_t* size) {
	// encrypt keymaterial buffer with rsa
	uint32_t res = encryptDecrypt(rsaRecipientPub, NULL, (uint8_t*)km, sizeof(aes_key_material), wrapped, size, TRUE, TRUE);
	return res;
}

uint32_t cryptGenerateRandomBuffer(uint8_t* buf, uint32_t size) {
	return ptrBCryptGenRandom(NULL, buf, size, BCRYPT_USE_SYSTEM_PREFERRED_RNG);
}

void cryptCloseKey(BCRYPT_KEY_HANDLE key, aes_key_material* km) {
	if (key) {
		ptrBCryptDestroyKey(key);

		if (km) {
			// aes key, also free the key object
			memHeapFree(km->reserved);
		}
	}
}

uint32_t cryptRsaImportKey(uint8_t* blob, uint32_t size, BCRYPT_KEY_HANDLE* key, BOOL privateKey, uint32_t* keyLenBytes) {
	*key = NULL;
	WCHAR str_RSAPUBLICBLOB[] = { (WCHAR)'R', (WCHAR)'S', (WCHAR)'A', (WCHAR)'P', (WCHAR)'U', (WCHAR)'B', (WCHAR)'L', (WCHAR)'I', (WCHAR)'C', (WCHAR)'B', (WCHAR)'L', (WCHAR)'O', (WCHAR)'B', (WCHAR)'\0' };
	WCHAR str_RSAPRIVATEBLOB[] = { (WCHAR)'R', (WCHAR)'S', (WCHAR)'A', (WCHAR)'P', (WCHAR)'R', (WCHAR)'I', (WCHAR)'V', (WCHAR)'A', (WCHAR)'T', (WCHAR)'E', (WCHAR)'B', (WCHAR)'L', (WCHAR)'O', (WCHAR)'B', (WCHAR)'\0' };
	WCHAR str_KeyLength[] = { (WCHAR)'K', (WCHAR)'e', (WCHAR)'y', (WCHAR)'L', (WCHAR)'e', (WCHAR)'n', (WCHAR)'g', (WCHAR)'t', (WCHAR)'h', (WCHAR)'\0' };

	// default to import a public key
	PWCHAR keytype = str_RSAPUBLICBLOB;
	if (privateKey) {
		// key is a private key
		keytype = str_RSAPRIVATEBLOB;
	}

	// import
	NTSTATUS res = ptrBCryptImportKeyPair(algRsa, NULL, keytype, key, blob, size, 0);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "error BCryptImportKeyPair, ntstatus=%x", res);
		return res;
	}

	// get the key length (=block size)
	if (keyLenBytes) {
		*keyLenBytes = 0;
		ULONG blockLen;
		ULONG cbRes;
		res = ptrBCryptGetProperty(*key, str_KeyLength, (PBYTE)&blockLen, sizeof(ULONG), &cbRes, 0);
		if (res != 0) {
			DBG_PRINT(DBG_LEVEL_ERROR, "error BCryptGetProperty(length), ntstatus=%x", res);
			cryptCloseKey(*key);
			*key = NULL;
		}
		else {
			// size is in bits, we need bytes
			*keyLenBytes = blockLen / 8;
		}
	}

	return res;
}

uint32_t cryptRsaGenerateKeypair(uint32_t bits, uint8_t** priv, uint32_t* privSize, uint8_t** pub, uint32_t* pubSize) {
	*privSize = 0;
	*pubSize = 0;
	*priv = NULL;
	*pub = NULL;

	WCHAR str_RSAPUBLICBLOB[] = { (WCHAR)'R', (WCHAR)'S', (WCHAR)'A', (WCHAR)'P', (WCHAR)'U', (WCHAR)'B', (WCHAR)'L', (WCHAR)'I', (WCHAR)'C', (WCHAR)'B', (WCHAR)'L', (WCHAR)'O', (WCHAR)'B', (WCHAR)'\0' };
	WCHAR str_RSAPRIVATEBLOB[] = { (WCHAR)'R', (WCHAR)'S', (WCHAR)'A', (WCHAR)'P', (WCHAR)'R', (WCHAR)'I', (WCHAR)'V', (WCHAR)'A', (WCHAR)'T', (WCHAR)'E', (WCHAR)'B', (WCHAR)'L', (WCHAR)'O', (WCHAR)'B', (WCHAR)'\0' };

	// generate keys
	BCRYPT_KEY_HANDLE hKey;
	NTSTATUS res = ptrBCryptGenerateKeyPair(algRsa, &hKey, bits, 0);
	if (res != 0) {
		return res;
	}
	ptrBCryptFinalizeKeyPair(hKey, 0);

	// calculate size
	ULONG pb;
	ULONG pr;
	ptrBCryptExportKey(hKey, NULL, str_RSAPUBLICBLOB, NULL, 0, &pb, 0);
	*pubSize = pb;
	ptrBCryptExportKey(hKey, NULL, str_RSAPRIVATEBLOB, NULL, 0, &pr, 0);
	*privSize = pr;

	// allocate memory for keys
	*priv = (unsigned char*)memHeapAlloc(*privSize);
	*pub = (unsigned char*)memHeapAlloc(*pubSize);
	if (!priv || !pub) {
		memHeapFree(*priv);
		memHeapFree(*pub);
		return ERROR_OUTOFMEMORY;
	}

	// export
	ptrBCryptExportKey(hKey, NULL, str_RSAPUBLICBLOB, *pub, *pubSize, (PULONG)pubSize, 0);
	ptrBCryptExportKey(hKey, NULL, str_RSAPRIVATEBLOB, *priv, *privSize, (PULONG)privSize, 0);

	// done
	ptrBCryptDestroyKey(hKey);
	return 0;
}

void cryptFinalize() {
	if (__cryptInitializeCalled == 0) {
		return;
	}
	__cryptInitializeCalled--;

	if (__cryptInitializeCalled == 0) {
		if (algAes) {
			ptrBCryptCloseAlgorithmProvider(algAes, 0);
			algAes = NULL;
		}
		if (algRsa) {
			ptrBCryptCloseAlgorithmProvider(algRsa, 0);
			algRsa = NULL;
		}
		if (bcrypt) {
			ptrFreeLibrary(bcrypt);
			bcrypt = NULL;
		}
		osFinalize();
	}
}

uint32_t cryptInitialize() {
	if (__cryptInitializeCalled > 0) {
		return 0;
	}
	__cryptInitializeCalled++;

	uint32_t res = osInitialize();
	if (res != 0) {
		cryptFinalize();
		return res;
	}
	// ensure we have bcrypt
	bcrypt = procFindModuleByHash(0x7832c05); // => b c r y p t . d l l
	if (!bcrypt) {
		char dll[] = { 'b','c','r','y','p','t','.','d','l','l','\0' };
		bcrypt = ptrLoadLibraryA(dll);
		if (!bcrypt) {
			res = GetLastError();
			cryptFinalize();
			return res;
		}
	}

	// get pointers
	dynamic_import_ptr ptrs[] = {
		{ (ULONG_PTR**)&ptrBCryptGenRandom, bcrypt, 0x8e9d74b }, // => BCryptGenRandom
		{ (ULONG_PTR**)&ptrBCryptOpenAlgorithmProvider, bcrypt, 0x2f675270 }, // => BCryptOpenAlgorithmProvider
		{ (ULONG_PTR**)&ptrBCryptCloseAlgorithmProvider, bcrypt, 0x79bf17bc }, // => BCryptCloseAlgorithmProvider
		{ (ULONG_PTR**)&ptrBCryptGenerateKeyPair, bcrypt, 0xcf575b20 }, // => BCryptGenerateKeyPair
		{ (ULONG_PTR**)&ptrBCryptFinalizeKeyPair, bcrypt, 0x574f971f }, // => BCryptFinalizeKeyPair
		{ (ULONG_PTR**)&ptrBCryptSetProperty, bcrypt, 0xaf5a941e }, // => BCryptSetProperty
		{ (ULONG_PTR**)&ptrBCryptGetProperty, bcrypt, 0xaf5a941b }, // => BCryptGetProperty
		{ (ULONG_PTR**)&ptrBCryptDecrypt, bcrypt, 0xf81d1d6b }, // => BCryptDecrypt
		{ (ULONG_PTR**)&ptrBCryptEncrypt, bcrypt, 0x78211d70 }, // => BCryptEncrypt
		{ (ULONG_PTR**)&ptrBCryptGenerateSymmetricKey, bcrypt, 0xed13f12e }, // => BCryptGenerateSymmetricKey
		{ (ULONG_PTR**)&ptrBCryptExportKey, bcrypt, 0x92a2eb75 }, // => BCryptExportKey
		{ (ULONG_PTR**)&ptrBCryptImportKey, bcrypt, 0x96a2ea15 }, // => BCryptImportKey		{ (ULONG_PTR**)&ptrBCryptGenRandom, bcrypt, 0x8e9d74b },
		{ (ULONG_PTR**)&ptrBCryptImportKeyPair, bcrypt, 0xd0e969fc }, // => BCryptImportKeyPair
		{ (ULONG_PTR**)&ptrBCryptDestroyKey, bcrypt, 0x64203f50 }, // => BCryptDestroyKey
		{ (ULONG_PTR**)&ptrBCryptHashData, bcrypt, 0xc14f1df1 }, // => BCryptHashData
		{ (ULONG_PTR**)&ptrBCryptCreateHash, bcrypt, 0x4f439300 }, // => BCryptCreateHash
		{ (ULONG_PTR**)&ptrBCryptDestroyHash, bcrypt, 0xf4532049 }, // => BCryptDestroyHash
		{0,0,0}
	};
	if (peResolveDynamicImports(ptrs) != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot resolve cryptutils imports!");
		cryptFinalize();
		return ERROR_NOT_FOUND;
	}

	// open algorithm handles
	WCHAR str_RSA[] = { (WCHAR)'R', (WCHAR)'S', (WCHAR)'A', (WCHAR)'\0' };
	WCHAR str_AES[] = { (WCHAR)'A', (WCHAR)'E', (WCHAR)'S', (WCHAR)'\0' };
	res = ptrBCryptOpenAlgorithmProvider(&algRsa, str_RSA, NULL, 0);
	if (res != 0) {
		cryptFinalize();
		return res;
	}
	res = ptrBCryptOpenAlgorithmProvider(&algAes, str_AES, NULL, 0);
	if (res != 0) {
		cryptFinalize();
		return res;
	}

	// set aes properties
	WCHAR str_ChainingMode[] = { (WCHAR)'C', (WCHAR)'h', (WCHAR)'a', (WCHAR)'i', (WCHAR)'n', (WCHAR)'i', (WCHAR)'n', (WCHAR)'g', (WCHAR)'M', (WCHAR)'o', (WCHAR)'d', (WCHAR)'e', (WCHAR)'\0' };
	WCHAR str_ChainingModeCBC[] = { (WCHAR)'C', (WCHAR)'h', (WCHAR)'a', (WCHAR)'i', (WCHAR)'n', (WCHAR)'i', (WCHAR)'n', (WCHAR)'g', (WCHAR)'M', (WCHAR)'o', (WCHAR)'d', (WCHAR)'e', (WCHAR)'C', (WCHAR)'B', (WCHAR)'C', (WCHAR)'\0' };
	WCHAR str_ObjectLength[] = { (WCHAR)'O', (WCHAR)'b', (WCHAR)'j', (WCHAR)'e', (WCHAR)'c', (WCHAR)'t', (WCHAR)'L', (WCHAR)'e', (WCHAR)'n', (WCHAR)'g', (WCHAR)'t', (WCHAR)'h', (WCHAR)'\0' };
	ptrBCryptSetProperty(algAes, str_ChainingMode, (PBYTE)str_ChainingModeCBC, sizeof(str_ChainingModeCBC), 0);
	ULONG cbRes;
	ptrBCryptGetProperty(algAes, str_ObjectLength, (PBYTE)&aesObjectLen, sizeof(ULONG), &cbRes, 0);
	DBG_PRINT(DBG_LEVEL_INFO, "cryptutils initialized!");
	return 0;
}
/* everything here will be linked into '.text2' section */
#pragma code_seg(pop, r1)
