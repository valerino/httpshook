#pragma once

/*!
* \file timeutils.h
*
* \author valerino
* \date novembre 2017
*
* time utilities
*/

#ifdef __cplusplus
extern "C" {
#endif


	/*!
	 * \brief get time in microseconds since jan-1-1601
	 *
	 * \param local: true for localtime, false for UTC
	 * \return __int64
	 */
	__int64 timeNowChrome(int local);

	/*!
	 * \brief get time in seconds since jan-1-1970
	 *
	 * \param int local true for localtime, false for UTC
	 * \return __int64
	 */
	__int64 timeNowUnix(int local);

#ifdef __cplusplus
}
#endif

