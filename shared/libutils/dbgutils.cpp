/*!
* \file dbgutils.cpp
*
* \author valerino
* \date novembre 2017
*
* debug print primitives. compile with DBG_PRINT_ENABLED. strInitialize() must be called
*/

#ifdef _WIN32
#include <windows.h>
#endif

#include "dbgutils.h"
#include "memutils.h"
#include "strutils.h"
#include <stdio.h>

/*<! \brief the debug level */
static int _g_level = DBG_LEVEL_VERBOSE;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
void dbgOut(int level, const char* file, const char* function, int line, const char* format, ...) {
	if (level > _g_level) {
		return;
	}

	// format buffer
	char buf[1024] = { 0 };
	va_list ap;
	va_start(ap, format);
	strVPrintfA(buf, sizeof(buf), (char*)format, ap);
	va_end(ap);

	char lvl[32] = { 0 };
	switch (level) {
		case DBG_LEVEL_VERBOSE:
			strcpy(lvl, "DBG");
			break;
		case DBG_LEVEL_ERROR:
			strcpy(lvl, "ERR");
			break;
		case DBG_LEVEL_WARNING:
			strcpy(lvl, "WRN");
			break;
		case DBG_LEVEL_INFO:
			strcpy(lvl, "INF");
			break;
	}

	// print the debug string
	char dbgString[1024] = { 0 };
	strPrintfA(dbgString, sizeof(dbgString), "([**%s**]%s/%s/%d/TID:%d) %s\n", lvl, file, function, line, GetCurrentThreadId(), buf);
	OutputDebugStringA(dbgString);
}

void dbgSetLevel(int level) {
	_g_level = level;
}
/* everything here will be linked into '.text2' section */
#pragma code_seg(pop, r1)
