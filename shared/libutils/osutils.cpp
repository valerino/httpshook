/*!
* \file osutils.cpp
*
* \author valerino
* \date novembre 2017
*
* os specific utilities
*/
#include <windows.h>
#include <stdint.h>
#include "procutils.h"
#include "peutils.h"
#include "dbgutils.h"
#include "refloader.h"
#include "osutils.h"

int __osInitializeCalled = 0;

extern "C" _LOADLIBRARYA ptrLoadLibraryA = NULL;
extern "C" FREELIBRARY ptrFreeLibrary = NULL;
extern "C" GETMODULEFILENAMEA ptrGetModuleFileNameA = NULL;
extern "C" GETMODULEFILENAMEW ptrGetModuleFileNameW = NULL;
extern "C" GETMODULEHANDLEA ptrGetModuleHandleA = NULL;
extern "C" GETMODULEHANDLEW ptrGetModuleHandleW = NULL;
extern "C" _GETPROCADDRESS ptrGetProcAddress = NULL;

typedef DWORD(WINAPI* GETSECURITYINFO)(HANDLE handle, SE_OBJECT_TYPE ObjectType, SECURITY_INFORMATION SecurityInfo, PSID *ppsidOwner,
	PSID *ppsidGroup, PACL *ppDacl, PACL *ppSacl, PSECURITY_DESCRIPTOR *ppSecurityDescriptor);
GETSECURITYINFO ptrGetSecurityInfo = NULL;

typedef BOOL(WINAPI* ALLOCATEANDINITIALIZESID)(PSID_IDENTIFIER_AUTHORITY pIdentifierAuthority, BYTE nSubAuthorityCount, DWORD dwSubAuthority0,
	DWORD dwSubAuthority1, DWORD dwSubAuthority2, DWORD dwSubAuthority3, DWORD dwSubAuthority4, DWORD dwSubAuthority5, DWORD dwSubAuthority6,
	DWORD dwSubAuthority7, PSID *pSid);
ALLOCATEANDINITIALIZESID ptrAllocateAndInitializeSid = NULL;

typedef DWORD(WINAPI* SETENTRIESINACLA)(ULONG cCountOfExplicitEntries, PEXPLICIT_ACCESSA pListOfExplicitEntries, PACL OldAcl, PACL *NewAcl);
SETENTRIESINACLA ptrSetEntriesInAclA = NULL;

typedef DWORD(WINAPI* SETSECURITYINFO)(HANDLE handle, SE_OBJECT_TYPE ObjectType, SECURITY_INFORMATION SecurityInfo, PSID psidOwner, PSID psidGroup,
	PACL pDacl, PACL pSacl);
SETSECURITYINFO ptrSetSecurityInfo = NULL;

typedef PVOID(WINAPI* FREESID)(PSID pSid);
FREESID ptrFreeSid = NULL;

typedef BOOL(WINAPI* INITIALIZESECURITYDESCRIPTOR)(PSECURITY_DESCRIPTOR pSecurityDescriptor, DWORD dwRevision);
INITIALIZESECURITYDESCRIPTOR ptrInitializeSecurityDescriptor = NULL;

typedef BOOL(WINAPI* SETSECURITYDESCRIPTORDACL)(PSECURITY_DESCRIPTOR pSecurityDescriptor, BOOL bDaclPresent,
	PACL pDacl, BOOL bDaclDefaulted);
SETSECURITYDESCRIPTORDACL ptrSetSecurityDescriptorDacl = NULL;

extern CREATEMUTEXA ptrCreateMutexA = NULL;
extern OPENMUTEXA ptrOpenMutexA = NULL;;
extern CREATEEVENTA ptrCreateEventA = NULL;;
extern SETEVENT ptrSetEvent = NULL;
extern RELEASEMUTEX ptrReleaseMutex = NULL;

static HMODULE advapi32 = NULL;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
ULONG osInitialize() {
	ULONG gle = 0;

	if (__osInitializeCalled > 0) {
		return 0;
	}
	__osInitializeCalled++;

	// we have loadlibrary in kernel32
	HMODULE kernel32 = procFindModuleByHash(KERNEL32DLL_HASH_UNICODE);
	ptrLoadLibraryA = (_LOADLIBRARYA)peGetProcAddress(kernel32, 0x8a8b4676); // => LoadLibraryA

	// find libraries
	advapi32 = procFindModuleByHash(0xc78a43f4); // = > a d v a p i 3 2 . d l l
	if (!advapi32) {
		char dll[] = { 'a','d','v','a','p','i','3','2','.','d','l','l','\0' };
		advapi32 = ptrLoadLibraryA(dll);
		if (!advapi32) {
			gle = GetLastError();
			osFinalize();
			return gle;
		}
	}

	// get pointers
	dynamic_import_ptr ptrs[] = {
		{ (ULONG_PTR**)&ptrFreeLibrary, kernel32, 0xecc6c96f }, // => FreeLibrary
		{ (ULONG_PTR**)&ptrGetModuleFileNameA, kernel32, 0xf3cf5f59 }, // => GetModuleFileNameA
		{ (ULONG_PTR**)&ptrGetModuleFileNameW, kernel32, 0xf3cf5f6f}, // => GetModuleFileNameW
		{ (ULONG_PTR**)&ptrGetModuleHandleA, kernel32, 0x61eebcec }, // => GetModuleHandleA
		{ (ULONG_PTR**)&ptrGetModuleHandleW, kernel32, 0x61eebd02 }, // => GetModuleHandleW
		{ (ULONG_PTR**)&ptrGetProcAddress, kernel32, 0x1acaee7a }, // => GetProcAddress
		{ (ULONG_PTR**)&ptrGetSecurityInfo, advapi32, 0x4d82e78b }, // => GetSecurityInfo
		{ (ULONG_PTR**)&ptrAllocateAndInitializeSid, advapi32, 0xe1e923c8 }, // => AllocateAndInitializeSid
		{ (ULONG_PTR**)&ptrSetEntriesInAclA, advapi32, 0xd950a235 }, // => SetEntriesInAclA
		{ (ULONG_PTR**)&ptrSetSecurityInfo, advapi32, 0x4d83178b }, // => SetSecurityInfo
		{ (ULONG_PTR**)&ptrFreeSid, advapi32, 0x8d64652d }, // => FreeSid
		{ (ULONG_PTR**)&ptrInitializeSecurityDescriptor, advapi32, 0xa513b4bf }, // => InitializeSecurityDescriptor
		{ (ULONG_PTR**)&ptrSetSecurityDescriptorDacl, advapi32, 0x256a9004 }, // => SetSecurityDescriptorDacl
		{ (ULONG_PTR**)&ptrCreateEventA, kernel32, 0xcf41a678 }, // => CreateEventA
		{ (ULONG_PTR**)&ptrCreateMutexA, kernel32, 0xed61943c }, // => CreateMutexA
		{ (ULONG_PTR**)&ptrOpenMutexA, kernel32, 0x7bffe248 }, // => OpenMutexA
		{ (ULONG_PTR**)&ptrSetEvent, kernel32, 0xaf886c1e }, // => SetEvent
		{ (ULONG_PTR**)&ptrReleaseMutex, kernel32, 0xb31f4dac }, // => ReleaseMutex
		{ 0,0,0 }
	};
	if (peResolveDynamicImports(ptrs) != 0) {
		osFinalize();
		return ERROR_NOT_FOUND;
	}
	return 0;
}

void osFinalize() {
	if (__osInitializeCalled == 0) {
		return;
	}
	__osInitializeCalled--;

	if (__osInitializeCalled == 0) {
		if (advapi32) {
			ptrFreeLibrary(advapi32);
		}
	}
}

BOOL osIsWindowsAtLeast(uint16_t major, uint16_t minor) {
	OSVERSIONINFOEXW osvi = { 0 };
	osvi.dwOSVersionInfoSize = sizeof(osvi);
	GetVersionEx((LPOSVERSIONINFOW)&osvi);
	if (osvi.dwMajorVersion < major) {
		return FALSE;
	}
	if (osvi.dwMinorVersion < minor) {
		return FALSE;
	}
	return TRUE;
}

BOOL osIsWindows7OrGreater() {
	return osIsWindowsAtLeast(6, 1);
}

BOOL osIsWindows81OrGreater() {
	return osIsWindowsAtLeast(6, 2);
}

ULONG osInitializeSecurityAttributesWithNullDacl(LPSECURITY_ATTRIBUTES sa, PSECURITY_DESCRIPTOR sd) {
	memset(sa, 0, sizeof(SECURITY_ATTRIBUTES));
	memset(sd, 0, sizeof(SECURITY_DESCRIPTOR));

	// initialize the security descriptor
	if (!ptrInitializeSecurityDescriptor(sd, SECURITY_DESCRIPTOR_REVISION)) {
		return GetLastError();
	}

	// set the security descriptor with a NULL dacl (all access granted)
	if (!ptrSetSecurityDescriptorDacl(sd, TRUE, NULL, FALSE)) {
		return GetLastError();
	}
	sa->nLength = sizeof(SECURITY_ATTRIBUTES);
	sa->lpSecurityDescriptor = sd;
	sa->bInheritHandle = TRUE;
	return 0;
}
#pragma code_seg(pop, r1)

