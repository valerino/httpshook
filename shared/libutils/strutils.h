/*!
 * \file strutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * string utilities
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

	/*!
	 * \brief convert string to UTF-16 string (WCHAR)
	 *
	 * \param const PCHAR value the UTF8 string to be converted
	 * \param PWCHAR * out on successful return, pointer to the converted string. must be freed with memHeapFree()
	 * \return ULONG 0 on success
	 */
	ULONG strToWchar(const PCHAR value, PWCHAR* out);

	/*!
	 * \brief get a string representing current time in UNIX format (seconds since jan-1-1970)
	 *
	 * \param PCHAR out buffer for the time string
	 * \param int outSize size of buffer
	 * \param BOOL localTime TRUE to use localtime, default UTC
	 * \return void
	 */
	void strUnixNowTimestamp(PCHAR out, int outSize, BOOL localTime = FALSE);

	/*!
	* \brief get a string representing current time in CHROME format (microseconds since jan-1-1601)
	*
	* \param PCHAR out buffer for the time string
	* \param int outSize size of buffer
	* \param BOOL localTime TRUE to use localtime, default UTC
	* \return void
	*/
	void strChromeNowTimestamp(PCHAR out, int outSize, BOOL localTime = FALSE);


	/*!
	 * \brief convert byte array to hexadecimal string ({0xa,0xb,0xc,0xd,0xe} => "ABCDE")
	 *
	 * \param unsigned char * in the hex buffer
	 * \param size_t insz size of the input buffer
	 * \param char * out on successful return, the hex string
	 * \param size_t outsz size of the output buffer
	 * \return uint32_t 0 on success
	 */
	uint32_t strFromHex(unsigned char * in, size_t insz, char * out, size_t outsz);

	/*!
	 * \brief convert hexadecimal string to byte array ("ABCDE" => {0xa,0xb,0xc,0xd,0xe})
	 *
	 * \param const char * string the string to convert
	 * \param uint8_t * out on successful return, the hex buffer
	 * \param size_t outSize size of the output buffer
	 * \return uint32_t 0 on success
	 */
	uint32_t strToHex(const char* string, uint8_t* out, size_t outSize);

	/*!
	* \brief initializes api, must be called once per process
	* \note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
	* \calls procInitialize(), osInitialize()
	* \return uint32_t 0 on success
	*/
	ULONG strInitialize();

	/*!
	* \brief finalizes api
	* \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
	* \calls procFinalize(), osFinalize()
	* \return uint32_t
	*/
	void strFinalize();

	/*
	the following pointers are available after calling strInitialize(), and remains valid also after calling strFinalize()
	*/

	/*!
	* \brief snprintf alike, unicode version
	*
	* \param PCHAR pszDest
	* \param int cchDest
	* \param PCHAR pszFmt
	* \param ...
	* \return int
	*/
	typedef int (WINAPI* WNSPRINTFW)(PWCHAR pszDest, int cchDest, PWCHAR pszFmt, ...);
	extern WNSPRINTFW strPrintfW;

	/*!
	* \brief snprintf alike, ansi version
	*
	* \param PCHAR pszDest
	* \param int cchDest
	* \param PCHAR pszFmt
	* \param ...
	* \return int
	*/
	typedef int (WINAPI* WNSPRINTFA)(PCHAR pszDest, int cchDest, PCHAR pszFmt, ...);
	extern WNSPRINTFA strPrintfA;

	/*!
	* \brief snvprintf alike, ansi version
	*
	* \param PCHAR pszDest
	* \param int cchDest
	* \param PCHAR pszFmt
	* \param va_list arglist
	* \return int
	*/
	typedef int (WINAPI* WVNSPRINTFA)(PCHAR pszDest, int cchDest, PCHAR pszFmt, va_list arglist);
	extern WVNSPRINTFA strVPrintfA;

	/*!
	* \brief snvprintf alike, unicode version
	*
	* \param PCHAR pszDest
	* \param int cchDest
	* \param PCHAR pszFmt
	* \param va_list arglist
	* \return int
	*/
	typedef int (WINAPI* WVNSPRINTFW)(PWCHAR pszDest, int cchDest, PWCHAR pszFmt, va_list arglist);
	extern WVNSPRINTFW strVPrintfW;

	/*!
	* \brief strstri() alike
	*
	* \param PCHAR pszFirst
	* \param PCHAR pszSrch
	* \return PCHAR
	*/
	typedef PCHAR(WINAPI* STRSTRIA)(PCHAR pszFirst, PCHAR pszSrch);
	extern STRSTRIA strInStringA;

	/*!
	* \brief strstri() alike, unicode version
	*
	* \param PWCHAR pszFirst
	* \param PWCHAR pszSrch
	* \return PWCHAR
	*/
	typedef PWCHAR(WINAPI* STRSTRIW)(PWCHAR pszFirst, PWCHAR pszSrch);
	extern STRSTRIW strInStringW;

	/*!
	* \brief strrchr() alike, unicode version
	*
	* \param PWCHAR start
	* \param PWCHAR end usually NULL
	* \return WCHAR character to search (last occurrence)
	*/
	typedef PWCHAR(WINAPI* STRRCHRW)(PWCHAR start, PWCHAR end, WCHAR match);
	extern STRRCHRW strRightChrW;

	/*!
	* \brief strrchr() alike
	*
	* \param PCHAR start
	* \param PCHAR end usually NULL
	* \return CHAR character to search (last occurrence)
	*/
	typedef PCHAR(WINAPI* STRRCHRA)(PCHAR start, PCHAR end, CHAR match);
	extern STRRCHRA strRightChrA;

	/*!
	* \brief wcslen() alike
	*
	* \param PWCHAR lpString
	* \return int
	*/
	typedef int (WINAPI* STRLENW)(PWCHAR lpString);
	extern STRLENW strLenW;

	/*!
	* \brief snvprintf() alike, ansi version
	*
	 * \param char * buffer
	 * \param unsigned int buffer_len
	 * \param const char * fmt
	 * \param va_list va
	 * \return int
	 */
	int strVsnPrintfA(char *buffer, unsigned int buffer_len, const char *fmt, va_list va);

	/*!
	 * \brief sprintf() alike, ansi version
	 *
	 * \param char * buffer
	 * \param unsigned int buffer_len
	 * \param const char * fmt
	 * \param ...
	 * \return int
	 */
	int strSnPrintfA(char *buffer, unsigned int buffer_len, const char *fmt, ...);

#ifdef __cplusplus
}
#endif
