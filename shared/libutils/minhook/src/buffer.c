﻿/*
 *  MinHook - The Minimalistic API Hooking Library for x64/x86
 *  Copyright (C) 2009-2017 Tsuda Kageyu.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 *  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 *  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 *  OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <windows.h>
#include "buffer.h"

 // ADDED(valerino): to use dynamically imported functions (must call injectInitialize())
#include <injutils_ptr.h>
#include <procutils_ptr.h>
#include <dbgutils.h>

// Size of each memory block. (= page size of VirtualAlloc)
#define MEMORY_BLOCK_SIZE 0x1000

// Max range for seeking a memory block. (= 1024MB)
#define MAX_MEMORY_RANGE 0x40000000

// Memory protection flags to check the executable address.
#define PAGE_EXECUTE_FLAGS \
    (PAGE_EXECUTE | PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY)

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

//-------------------------------------------------------------------------
VOID InitializeBuffer(VOID) {
	// Nothing to do for now.
}

//-------------------------------------------------------------------------
VOID UninitializeBuffer(VOID) {}

//-------------------------------------------------------------------------
#if defined(_M_X64) || defined(__x86_64__)
static LPVOID FindPrevFreeRegion(LPVOID pAddress, LPVOID pMinAddr, DWORD dwAllocationGranularity, BOOL remote, HANDLE hp) {
	ULONG_PTR tryAddr = (ULONG_PTR)pAddress;

	// Round down to the allocation granularity.
	tryAddr -= tryAddr % dwAllocationGranularity;

	// Start from the previous allocation granularity multiply.
	tryAddr -= dwAllocationGranularity;

	while (tryAddr >= (ULONG_PTR)pMinAddr) {
		MEMORY_BASIC_INFORMATION mbi;
		SIZE_T qres = 0;
		if (remote) {
			qres = ptrVirtualQueryEx(hp, (LPVOID)tryAddr, &mbi, sizeof(mbi));
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "ptrVirtualQueryEx(), address=%p, qres=%d", pAddress, qres);
		}
		else {
			qres = ptrVirtualQuery((LPVOID)tryAddr, &mbi, sizeof(mbi));
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "ptrVirtualQuery(), address=%p, qres=%d", pAddress, qres);
		}
		if (qres == 0)
			break;

		if (mbi.State == MEM_FREE)
			return (LPVOID)tryAddr;

		if ((ULONG_PTR)mbi.AllocationBase < dwAllocationGranularity)
			break;

		tryAddr = (ULONG_PTR)mbi.AllocationBase - dwAllocationGranularity;
	}

	return NULL;
}
#endif

//-------------------------------------------------------------------------
#if defined(_M_X64) || defined(__x86_64__)
static LPVOID FindNextFreeRegion(LPVOID pAddress, LPVOID pMaxAddr, DWORD dwAllocationGranularity, BOOL remote, HANDLE hp) {
	ULONG_PTR tryAddr = (ULONG_PTR)pAddress;

	// Round down to the allocation granularity.
	tryAddr -= tryAddr % dwAllocationGranularity;

	// Start from the next allocation granularity multiply.
	tryAddr += dwAllocationGranularity;

	while (tryAddr <= (ULONG_PTR)pMaxAddr) {
		MEMORY_BASIC_INFORMATION mbi;
		SIZE_T qres = 0;
		if (remote) {
			qres = ptrVirtualQueryEx(hp, (LPVOID)tryAddr, &mbi, sizeof(mbi));
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "ptrVirtualQueryEx(), address=%p, qres=%d", pAddress, qres);
		}
		else {
			qres = ptrVirtualQuery((LPVOID)tryAddr, &mbi, sizeof(mbi));
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "ptrVirtualQuery(), address=%p, qres=%d", pAddress, qres);
		}
		if (qres == 0)
			break;

		if (mbi.State == MEM_FREE)
			return (LPVOID)tryAddr;

		tryAddr = (ULONG_PTR)mbi.BaseAddress + mbi.RegionSize;

		// Round up to the next allocation granularity.
		tryAddr += dwAllocationGranularity - 1;
		tryAddr -= tryAddr % dwAllocationGranularity;
	}

	return NULL;
}
#endif

//-------------------------------------------------------------------------
static void* GetMemoryBlock(LPVOID pOrigin, BOOL remote, HANDLE hp) {
	void* pBlock;
#if defined(_M_X64) || defined(__x86_64__)
	ULONG_PTR minAddr;
	ULONG_PTR maxAddr;

	SYSTEM_INFO si;
	GetSystemInfo(&si);
	minAddr = (ULONG_PTR)si.lpMinimumApplicationAddress;
	maxAddr = (ULONG_PTR)si.lpMaximumApplicationAddress;

	// pOrigin ± 512MB
	if ((ULONG_PTR)pOrigin > MAX_MEMORY_RANGE && minAddr < (ULONG_PTR)pOrigin - MAX_MEMORY_RANGE)
		minAddr = (ULONG_PTR)pOrigin - MAX_MEMORY_RANGE;

	if (maxAddr > (ULONG_PTR)pOrigin + MAX_MEMORY_RANGE)
		maxAddr = (ULONG_PTR)pOrigin + MAX_MEMORY_RANGE;

	// Make room for MEMORY_BLOCK_SIZE bytes.
	maxAddr -= MEMORY_BLOCK_SIZE - 1;
#endif

#if defined(_M_X64) || defined(__x86_64__)
	// Alloc a new block above if not found.
	{
		LPVOID pAlloc = pOrigin;
		while ((ULONG_PTR)pAlloc >= minAddr) {
			pAlloc = FindPrevFreeRegion(pAlloc, (LPVOID)minAddr, si.dwAllocationGranularity, remote, hp);
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "FindPrevFreeRegion() returned %p", pAlloc);
			if (pAlloc == NULL)
				break;

			if (remote) {
				pBlock = (void*)ptrVirtualAllocEx(
					hp, pAlloc, MEMORY_BLOCK_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
			}
			else {
				pBlock = (void*)ptrVirtualAlloc(
					pAlloc, MEMORY_BLOCK_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
			}
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "FindPrevFreeRegion branch, allocated block=%p, remote=%d", pBlock, remote);
			if (pBlock != NULL)
				break;
		}
	}

	// Alloc a new block below if not found.
	if (pBlock == NULL) {
		LPVOID pAlloc = pOrigin;
		while ((ULONG_PTR)pAlloc <= maxAddr) {
			pAlloc = FindNextFreeRegion(pAlloc, (LPVOID)maxAddr, si.dwAllocationGranularity, remote, hp);
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "FindNextFreeRegion() returned %p", pAlloc);
			if (pAlloc == NULL)
				break;

			if (remote) {
				pBlock = (void*)ptrVirtualAllocEx(
					hp, pAlloc, MEMORY_BLOCK_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
			}
			else {
				pBlock = (void*)ptrVirtualAlloc(
					pAlloc, MEMORY_BLOCK_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
			}
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "FindNextFreeRegion branch, allocated block=%p, remote=%d", pBlock, remote);
			if (pBlock != NULL)
				break;
		}
	}
#else
	// In x86 mode, a memory block can be placed anywhere.
	pBlock = (void*)ptrVirtualAlloc(
		NULL, MEMORY_BLOCK_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
#endif
	return pBlock;
}

//-------------------------------------------------------------------------
LPVOID AllocateBuffer(LPVOID pOrigin, BOOL remote, DWORD pid) {
	HANDLE hp = NULL;
	if (remote) {
		// get handle to the process
		hp = ptrOpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, FALSE, pid);
		if (!hp) {
			DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrOpenProcess(), pid=%d", pid);
			return NULL;
		}
	}
	//DBG_PRINT(DBG_LEVEL_VERBOSE, "calling getMemoryBlock(), pid=%d", pid);
	void* pBlock = GetMemoryBlock(pOrigin, remote, hp);
	CloseHandle(hp);
	if (pBlock == NULL) {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed GetMemoryBlock(), pid=%d,res=%d", pid, GetLastError());
		return NULL;
	}

	//DBG_PRINT(DBG_LEVEL_VERBOSE, "pBlock=%p,remote=%d,pid=%d", pBlock, remote, pid);
	return pBlock;
}

//-------------------------------------------------------------------------
VOID FreeBuffer(LPVOID pBuffer) {
	if (pBuffer) {
		ptrVirtualFree(pBuffer, 0, MEM_RELEASE);
	}
}

//-------------------------------------------------------------------------
BOOL IsExecutableAddress(LPVOID pAddress) {
	MEMORY_BASIC_INFORMATION mi;
	if (ptrVirtualQuery(pAddress, &mi, sizeof(mi))) {
		//DBG_PRINT(DBG_LEVEL_VERBOSE, "VirtualQuery() address=%p, state=%x, protect=%x", pAddress, mi.State, mi.Protect);
	}
	else {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrVirtualQuery(), res=%d", GetLastError());
	}
	return (mi.State == MEM_COMMIT && (mi.Protect & PAGE_EXECUTE_FLAGS));
}
#pragma code_seg(pop, r1)
