/*!
* \file procutils_ptr.h
*
* \author valerino
* \date dicembre 2017
*
* import definitions for procutils
*/

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include <TlHelp32.h>

	/* @brief duplicated PROCESSENTRY32 structure from MSDN, they lack the *A definitioN! */
	typedef struct tagPROCESSENTRY32A {
		DWORD   dwSize;
		DWORD   cntUsage;
		DWORD   th32ProcessID;          // this process
		ULONG_PTR th32DefaultHeapID;
		DWORD   th32ModuleID;           // associated exe
		DWORD   cntThreads;
		DWORD   th32ParentProcessID;    // this process's parent process
		LONG    pcPriClassBase;         // Base priority of process's threads
		DWORD   dwFlags;
		CHAR    szExeFile[MAX_PATH];    // Path
	} PROCESSENTRY32A;

	/*
	the following pointers are available after calling procInitialize(), and remains valid also after calling procFinalize()
	*/
	typedef BOOL(WINAPI* OPENPROCESSTOKEN)(HANDLE ProcessHandle, DWORD DesiredAccess, PHANDLE TokenHandle);
	extern OPENPROCESSTOKEN ptrOpenProcessToken;

	typedef BOOL(WINAPI* ADJUSTTOKENPRIVILEGES)(HANDLE TokenHandle, BOOL DisableAllPrivileges, PTOKEN_PRIVILEGES NewState, DWORD BufferLength, PTOKEN_PRIVILEGES PreviousState, PDWORD ReturnLength);
	extern ADJUSTTOKENPRIVILEGES ptrAdjustTokenPrivileges;

	typedef BOOL(WINAPI* LOOKUPPRIVILEGEVALUEW)(PWCHAR lpSystemName, PWCHAR lpName, PLUID lpLuid);
	extern LOOKUPPRIVILEGEVALUEW ptrLookupPrivilegeValueW;

	typedef HANDLE(WINAPI* CREATETOOLHELP32SNAPSHOT)(DWORD dwFlags, DWORD th32ProcessID);
	extern CREATETOOLHELP32SNAPSHOT ptrCreateToolhelp32Snapshot;

	typedef BOOL(WINAPI* PROCESS32FIRSTA)(HANDLE hSnapshot, PROCESSENTRY32A* lppe);
	extern PROCESS32FIRSTA ptrProcess32First;

	typedef BOOL(WINAPI* PROCESS32NEXTA)(HANDLE hSnapshot, PROCESSENTRY32A* lppe);
	extern PROCESS32NEXTA ptrProcess32Next;

	typedef BOOL(WINAPI* MODULE32FIRSTW)(HANDLE hSnapshot, LPMODULEENTRY32W lpme);
	extern MODULE32FIRSTW ptrModule32FirstW;

	typedef BOOL(WINAPI* MODULE32NEXTW)(HANDLE hSnapshot, LPMODULEENTRY32W lpme);
	extern MODULE32NEXTW ptrModule32NextW;

	typedef BOOL(WINAPI* THREAD32FIRST)(HANDLE hSnapshot, THREADENTRY32* lpte);
	extern THREAD32FIRST ptrThread32First;

	typedef BOOL(WINAPI* THREAD32NEXT)(HANDLE hSnapshot, THREADENTRY32* lpte);
	extern THREAD32NEXT ptrThread32Next;

	typedef BOOL(WINAPI* CREATEPROCESSW)(PWCHAR lpApplicationName, PWCHAR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes, LPSECURITY_ATTRIBUTES lpThreadAttributes,
		BOOL bInheritHandles, DWORD dwCreationFlags, LPVOID lpEnvironment, PWCHAR lpCurrentDirectory, LPSTARTUPINFOW lpStartupInfo, LPPROCESS_INFORMATION lpProcessInformation);
	extern CREATEPROCESSW ptrCreateProcessW;

	typedef DWORD(WINAPI* GETCURRENTPROCESSID)(VOID);
	extern GETCURRENTPROCESSID ptrGetCurrentProcessId;

	typedef DWORD(WINAPI* GETCURRENTTHREADID)(VOID);
	extern GETCURRENTTHREADID ptrGetCurrentThreadId;

	typedef HANDLE(WINAPI* GETCURRENTPROCESS)(VOID);
	extern GETCURRENTPROCESS ptrGetCurrentProcess;

	typedef BOOL(WINAPI * ISWOW64PROCESS)(HANDLE hProcess, PBOOL Wow64Process);
	extern ISWOW64PROCESS ptrIsWow64Process;

	typedef BOOL(WINAPI* TERMINATEPROCESS)(HANDLE hp, UINT dwExitCode);
	extern TERMINATEPROCESS ptrTerminateProcess;

	typedef BOOL(WINAPI* EXPANDENVIRONMENTSTRINGSA)(PCHAR src, PCHAR dst, DWORD size);
	extern EXPANDENVIRONMENTSTRINGSA ptrExpandEnvironmentStringsA;

	typedef VOID(WINAPI* RTLEXITUSERTHREAD)(DWORD dwExitCode);
	extern RTLEXITUSERTHREAD ptrRtlExitUserThread;

	typedef BOOL(WINAPI* GETEXITCODETHREAD)(HANDLE hThread, LPDWORD lpExitCode);
	extern GETEXITCODETHREAD ptrGetExitCodeThread;

#ifdef __cplusplus
}
#endif