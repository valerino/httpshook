/*!
* \file httputils.cpp
*
* \author valerino
* \date novembre 2017
*
* http request utilities using winhttp
*/
#include <windows.h>
#include <stdint.h>
#include "httputils.h"
#include "memutils.h"
#include "fileutils.h"
#include "peutils.h"
#include "procutils.h"
#include "strutils.h"
#include "dbgutils.h"
#include "osutils.h"

// imported functions
typedef HRESULT(WINAPI* OBTAINUSERAGENTSTRING)(DWORD  dwOption, PCHAR *pcszUAOut, DWORD  *cbSize);
OBTAINUSERAGENTSTRING ptrObtainUserAgentString = NULL;

typedef BOOL(WINAPI* WINHTTPRECEIVERESPONSE)(HINTERNET hRequest, LPVOID lpReserved);
WINHTTPRECEIVERESPONSE ptrWinHttpReceiveResponse = NULL;

typedef BOOL(WINAPI* WINHTTPQUERYHEADERS)(HINTERNET hRequest, DWORD dwInfoLevel, PWCHAR pwszName, LPVOID lpBuffer, LPDWORD lpdwBufferLength, LPDWORD lpdwIndex);
WINHTTPQUERYHEADERS ptrWinHttpQueryHeaders = NULL;

typedef BOOL(WINAPI* WINHTTPCLOSEHANDLE)(HINTERNET hInternet);
WINHTTPCLOSEHANDLE ptrWinHttpCloseHandle = NULL;

typedef HINTERNET(WINAPI* WINHTTPOPEN)(PWCHAR pwszUserAgent, DWORD dwAccessType, PWCHAR pwszProxyName, PWCHAR pwszProxyBypass, DWORD dwFlags);
WINHTTPOPEN ptrWinHttpOpen = NULL;

typedef BOOL(WINAPI* WINHTTPSENDREQUEST)(HINTERNET hRequest, PWCHAR lpszHeaders, DWORD dwHeadersLength, LPVOID lpOptional, DWORD dwOptionalLength, DWORD dwTotalLength, DWORD_PTR dwContext);
WINHTTPSENDREQUEST ptrWinHttpSendRequest = NULL;

typedef HINTERNET(WINAPI* WINHTTPOPENREQUEST)(HINTERNET hConnect, PWCHAR pwszVerb, PWCHAR pwszObjectName, PWCHAR pwszVersion, PWCHAR pwszReferrer, PWCHAR ppwszAcceptTypes, DWORD dwFlags);
WINHTTPOPENREQUEST ptrWinHttpOpenRequest = NULL;

typedef BOOL(WINAPI* WINHTTPSETTIMEOUTS)(HINTERNET hInternet, int nResolveTimeout, int nConnectTimeout, int nSendTimeout, IN int nReceiveTimeout);
WINHTTPSETTIMEOUTS ptrWinHttpSetTimeouts = NULL;

typedef BOOL(WINAPI* WINHTTPSETOPTION) (HINTERNET hInternet, DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength);
WINHTTPSETOPTION ptrWinHttpSetOption = NULL;

typedef BOOL(WINAPI* WINHTTPCRACKURL)(PWCHAR pwszUrl, DWORD dwUrlLength, DWORD dwFlags, LPURL_COMPONENTS lpUrlComponents);
WINHTTPCRACKURL ptrWinHttpCrackUrl = NULL;

typedef HINTERNET(WINAPI* WINHTTPCONNECT)(HINTERNET hSession, PWCHAR pswzServerName, INTERNET_PORT nServerPort, DWORD dwReserved);
WINHTTPCONNECT ptrWinHttpConnect = NULL;

typedef BOOL(WINAPI* WINHTTPQUERYDATAAVAILABLE)(HINTERNET hRequest, LPDWORD lpdwNumberOfBytesAvailable);
WINHTTPQUERYDATAAVAILABLE ptrWinHttpQueryDataAvailable;

typedef BOOL(WINAPI* WINHTTPREADDATA)(HINTERNET hRequest, LPVOID lpBuffer, DWORD dwNumberOfBytesToRead, LPDWORD lpdwNumberOfBytesRead);
WINHTTPREADDATA ptrWinHttpReadData;

typedef BOOL(WINAPI* WINHTTPWRITEDATA)(HINTERNET hRequest, LPVOID lpBuffer, DWORD dwNumberOfBytesToWrite, LPDWORD lpdwNumberOfBytesWritten);
WINHTTPWRITEDATA ptrWinHttpWriteData;

typedef BOOL(WINAPI* WINHTTPADDREQUESTHEADERS)(HINTERNET hRequest, PWCHAR pwszHeaders, DWORD dwHeadersLength, IN DWORD dwModifiers);
WINHTTPADDREQUESTHEADERS ptrWinHttpAddRequestHeaders;

// for proper cleanup
HMODULE winhttp = NULL;
HMODULE urlmon = NULL;
int __httpInitializeCalled = 0;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

void httpFinalize() {
	if (__httpInitializeCalled == 0) {
		return;
	}
	__httpInitializeCalled--;

	if (__httpInitializeCalled == 0) {
		if (winhttp) {
			ptrFreeLibrary(winhttp);
			winhttp = NULL;
		}
		if (urlmon) {
			ptrFreeLibrary(urlmon);
			urlmon = NULL;
		}
		strFinalize();
		osFinalize();
	}
}

ULONG httpInitialize() {
	if (__httpInitializeCalled > 0) {
		return 0;
	}
	__httpInitializeCalled++;

	uint32_t res = strInitialize();
	if (res != 0) {
		httpFinalize();
		return res;
	}

	res = osInitialize();
	if (res != 0) {
		httpFinalize();
		return res;
	}

	// get winhttp
	winhttp = procFindModuleByHash(0xc5369a5b); // => w i n h t t p . d l l
	if (!winhttp) {
		char dll[] = { 'w','i','n','h','t','t','p','.','d','l','l','\0' };
		winhttp = ptrLoadLibraryA(dll);
		if (!winhttp) {
			res = GetLastError();
			httpFinalize();
			return res;
		}
	}

	// get urlmon
	urlmon = procFindModuleByHash(0x4181227c); // => u r l m o n . d l l
	if (!urlmon) {
		char dll[] = { 'u','r','l','m','o','n','.','d','l','l','\0' };
		urlmon = ptrLoadLibraryA(dll);
		if (!urlmon) {
			res = GetLastError();
			httpFinalize();
			return res;
		}
	}

	// get pointers
	dynamic_import_ptr ptrs[] = {
		{ (ULONG_PTR**)&ptrObtainUserAgentString, urlmon, 0x9c3672e8 }, // => ObtainUserAgentString
		{ (ULONG_PTR**)&ptrWinHttpReceiveResponse, winhttp, 0x6c3f3920 }, // => WinHttpReceiveResponse
		{ (ULONG_PTR**)&ptrWinHttpQueryHeaders, winhttp, 0xde67ac3c }, // => WinHttpQueryHeaders
		{ (ULONG_PTR**)&ptrWinHttpCloseHandle, winhttp, 0x9964b3dc }, // => WinHttpCloseHandle
		{ (ULONG_PTR**)&ptrWinHttpOpen, winhttp, 0xaf7f658e }, // => WinHttpOpen
		{ (ULONG_PTR**)&ptrWinHttpSendRequest, winhttp, 0x26d17a4e }, // => WinHttpSendRequest
		{ (ULONG_PTR**)&ptrWinHttpSetOption, winhttp, 0x8678c3f6 }, // => WinHttpSetOption
		{ (ULONG_PTR**)&ptrWinHttpCrackUrl, winhttp, 0x8ef04f02 }, // => WinHttpCrackUrl
		{ (ULONG_PTR**)&ptrWinHttpConnect, winhttp, 0x9f47a05e }, // => WinHttpConnect
		{ (ULONG_PTR**)&ptrWinHttpSetTimeouts, winhttp, 0x20b4c051 }, // => WinHttpSetTimeouts
		{ (ULONG_PTR**)&ptrWinHttpOpenRequest, winhttp, 0x1dd1d38d }, // => WinHttpOpenRequest
		{ (ULONG_PTR**)&ptrWinHttpQueryDataAvailable, winhttp, 0x710832cd }, // => WinHttpQueryDataAvailable
		{ (ULONG_PTR**)&ptrWinHttpReadData, winhttp, 0x80cc5bd7 }, // => WinHttpReadData
		{ (ULONG_PTR**)&ptrWinHttpWriteData, winhttp, 0xea74138b }, // => WinHttpWriteData
		{ (ULONG_PTR**)&ptrWinHttpAddRequestHeaders, winhttp, 0xe8a2144c }, // => WinHttpAddRequestHeaders
		{ 0,0,0 }
	};
	if (peResolveDynamicImports(ptrs) != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot resolve httputils imports!");
		httpFinalize();
		return ERROR_NOT_FOUND;
	}

	DBG_PRINT(DBG_LEVEL_INFO, "httputils initialized!");
	return 0;
}

void httpCloseHandles(http_handles* handles) {
	if (handles) {
		if (handles->req) {
			ptrWinHttpCloseHandle(handles->req);
		}

		if (handles->connect) {
			ptrWinHttpCloseHandle(handles->connect);
		}

		if (handles->session) {
			ptrWinHttpCloseHandle(handles->session);
		}
	}
}
uint32_t httpOpenHandles(const PWCHAR url, const PWCHAR referrer, const PWCHAR httpCommand, http_handles* handles) {
	if (!url || !httpCommand || !handles) {
		return ERROR_INVALID_PARAMETER;
	}
	memset(handles, 0, sizeof(http_handles));

	// get agent string
	char buf[512] = { 0 };
	PWCHAR wAgent;
	unsigned long sizeAgent = sizeof(buf);
	ptrObtainUserAgentString(0, (CHAR**)&buf, &sizeAgent);
	strToWchar(buf, &wAgent);

	// split url
	WCHAR hostName[MAX_PATH];
	WCHAR urlPath[MAX_PATH];
	URL_COMPONENTS urlC = { 0 };
	urlC.dwStructSize = sizeof(URL_COMPONENTS);
	urlC.dwUrlPathLength = MAX_PATH;
	urlC.lpszUrlPath = urlPath;
	urlC.dwHostNameLength = MAX_PATH;
	urlC.lpszHostName = hostName;
	if (!ptrWinHttpCrackUrl(url, 0, 0, &urlC)) {
		// invalid url
		DBG_PRINT(DBG_LEVEL_ERROR, "failed WinHttpCrackUrl()");
		memHeapFree(wAgent);
		return ERROR_INVALID_PARAMETER;
	}

	// create session
	DWORD accessType;
	uint32_t gle;
	if (osIsWindows81OrGreater()) {
		accessType = WINHTTP_ACCESS_TYPE_AUTOMATIC_PROXY;
	}
	else {
		accessType = WINHTTP_ACCESS_TYPE_DEFAULT_PROXY;
	}
	handles->session = ptrWinHttpOpen(wAgent, accessType, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0);
	memHeapFree(wAgent);
	if (!handles->session) {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed WinHttpOpen()");
		gle = GetLastError();
		return gle;
	}

	// connect
	handles->connect = ptrWinHttpConnect(handles->session, hostName, urlC.nPort, 0);
	if (!handles->connect) {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed WinHttpConnect()");
		ptrWinHttpCloseHandle(handles->session);
		handles->session = NULL;
		return ERROR_CONNECTION_REFUSED;
	}

	// open request
	handles->req = ptrWinHttpOpenRequest(handles->connect, httpCommand, urlPath, NULL, referrer, WINHTTP_DEFAULT_ACCEPT_TYPES,
		urlC.nScheme == INTERNET_SCHEME_HTTPS ? WINHTTP_FLAG_SECURE : 0);
	if (!handles->req) {
		gle = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed WinHttpOpenRequest()");
		ptrWinHttpCloseHandle(handles->connect);
		ptrWinHttpCloseHandle(handles->session);
		handles->req = NULL;
		handles->session = NULL;
		return gle;
	}

	// set timeouts
	BOOL b = ptrWinHttpSetTimeouts(handles->req, 0, 60 * 1000, 0, 0);
	return 0;
}

uint32_t httpQueryResponseStatus(http_handles* handles, uint32_t* status) {
	if (!handles) {
		return ERROR_INVALID_PARAMETER;
	}

	*status = NULL;
	BOOL b = ptrWinHttpReceiveResponse(handles->req, NULL);
	if (!b) {
		ULONG gle = GetLastError();
		return gle;
	}

	DWORD s;
	DWORD size = sizeof(DWORD);
	b = ptrWinHttpQueryHeaders(handles->req, WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER, NULL, &s, &size, NULL);
	if (!b) {
		ULONG gle = GetLastError();
		return gle;
	}

	*status = s;
	return 0;
}

uint32_t httpSendRequest(http_handles* handles, const PWCHAR headers, void* lpOptional, uint32_t dwOptionalLength, uint32_t dwTotalLength, uintptr_t dwContext) {
	if (!handles) {
		return ERROR_INVALID_PARAMETER;
	}

	// add headers if any
	if (headers) {
		PWCHAR h = headers;
		while (*h) {
			BOOL b = ptrWinHttpAddRequestHeaders(handles->req, h, -1, WINHTTP_ADDREQ_FLAG_ADD);
			h += strLenW(h) + 1;
		}
	}

	BOOL retry = FALSE;
	ULONG gle = NO_ERROR;
	do {
		// issue request
		retry = FALSE;
		BOOL b = ptrWinHttpSendRequest(handles->req, WINHTTP_NO_ADDITIONAL_HEADERS, 0, lpOptional, dwOptionalLength, dwTotalLength, dwContext);
		gle = GetLastError();
		if (gle == ERROR_WINHTTP_SECURE_FAILURE) {
			// reset security
			// TODO: proper https support ?!
			DBG_PRINT(DBG_LEVEL_WARNING, "ERROR_WINHTTP_SECURE_FAILURE, resetting security");
			DWORD flags = SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_CERT_WRONG_USAGE | SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID;
			ptrWinHttpSetOption(handles->req, WINHTTP_OPTION_SECURITY_FLAGS, &flags, sizeof(flags));
			// and reissue request
			retry = TRUE;
		}
		else if (gle == ERROR_WINHTTP_RESEND_REQUEST) {
			// initial resend may fail, so when this error is received we simply resend again
			DBG_PRINT(DBG_LEVEL_WARNING, "ERROR_WINHTTP_RESEND_REQUEST, sending again once");
			retry = TRUE;
		}
		else {
			// just return error
			break;
		}
	} while (retry);

	// done
	return gle;
}

uint32_t httpDownloadResponse(http_handles* handles, const PWCHAR outPath, uint8_t** response, uint32_t* respSize, uint32_t* httpStatus) {
	if (!handles || !httpStatus) {
		return ERROR_INVALID_PARAMETER;
	}
	*httpStatus = 0;

	// start downloading response and query status
	uint32_t res = httpQueryResponseStatus(handles, httpStatus);
	if (res != 0) {
		return res;
	}
	if (*httpStatus != HTTP_STATUS_OK) {
		res = ERROR_WINHTTP_INVALID_SERVER_RESPONSE;
		return res;
	}

	// if outfilepath is provided, response is downloaded to the specified path
	HANDLE hf = NULL;
	if (outPath) {
		// create new file and write data
		hf = CreateFile(outPath, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hf == INVALID_HANDLE_VALUE) {
			res = GetLastError();
			return res;
		}
	}
	else {
		// download to memory
		*response = NULL;
		*respSize = 0;
	}

	// read response in chunks
	uint8_t* respBuffer = NULL;
	uint32_t bufSize = 0;
	DWORD size = 0;
	while (TRUE) {
		BOOL b = ptrWinHttpQueryDataAvailable(handles->req, &size);
		if (!b) {
			res = GetLastError();
			break;
		}

		if (size == 0) {
			// finished
			res = 0;
			break;
		}

		if (hf) {
			// dump to file
			respBuffer = (PBYTE)memHeapAlloc(size + 2);
			if (!respBuffer) {
				res = ERROR_OUTOFMEMORY;
				break;
			}
			DWORD readBytes;
			b = ptrWinHttpReadData(handles->req, respBuffer, size, &readBytes);
			if (!b) {
				res = GetLastError();
				break;
			}
			DWORD writeBytes;
			b = WriteFile(hf, respBuffer, size, &writeBytes, NULL);
			if (!b) {
				res = GetLastError();
				break;
			}
			memHeapFree(respBuffer);
			respBuffer = NULL;
		}
		else {
			// read in memory
			ULONG offset = bufSize;
			bufSize += size;
			PBYTE respNew = (PBYTE)memHeapRealloc(respBuffer, bufSize + 2);
			if (!respNew) {
				res = ERROR_OUTOFMEMORY;
				break;
			}

			respBuffer = respNew;
			DWORD readBytes;
			b = ptrWinHttpReadData(handles->req, respNew + offset, size, &readBytes);
			if (!b) {
				res = GetLastError();
				break;
			}
		}
	}

	if (hf) {
		CloseHandle(hf);
	}

	if (res != 0) {
		if (respBuffer) {
			memHeapFree(respBuffer);
		}
		if (hf) {
			// delete dangling file
			DeleteFile(outPath);
		}
		*httpStatus = 0;
		return res;
	}

	if (response) {
		*response = respBuffer;
	}
	if (respSize) {
		*respSize = bufSize;
	}
	return 0;
}

uint32_t httpGet(const PWCHAR url, const PWCHAR referrer, const PWCHAR headers, const PWCHAR downloadPath, uint8_t**data, uint32_t* size, uint32_t* httpStatus) {
	// create handles
	http_handles handles;
	// GET
	WCHAR str_GET[] = { (WCHAR)'G', (WCHAR)'E', (WCHAR)'T', (WCHAR)'\0' };
	uint32_t res = httpOpenHandles(url, referrer, str_GET, &handles);
	if (res != 0) {
		return res;
	}

	// send the request
	res = httpSendRequest(&handles, headers, WINHTTP_NO_REQUEST_DATA, 0, 0, NULL);
	if (res != 0) {
		httpCloseHandles(&handles);
		return res;
	}

	// download response
	res = httpDownloadResponse(&handles, downloadPath, data, size, httpStatus);
	httpCloseHandles(&handles);
	return 0;
}

uint32_t httpPost(const PWCHAR url, const PWCHAR referrer, const PWCHAR headers, uint8_t* data, uint32_t size, http_handles* handles) {
	if (!headers || !handles) {
		// headers are mandatory
		return ERROR_INVALID_PARAMETER;
	}

	// open handles
	// POST
	WCHAR str_POST[] = { (WCHAR)'P', (WCHAR)'O', (WCHAR)'S', (WCHAR)'T', (WCHAR)'\0' };
	uint32_t res = httpOpenHandles(url, referrer, str_POST, handles);
	if (res != 0) {
		return res;
	}

	// send the request
	res = httpSendRequest(handles, headers, data, size, size, 0);
	if (res != 0) {
		// close the handles
		httpCloseHandles(handles);
		memset(handles, 0, sizeof(http_handles));
	}
	return res;
}
#pragma code_seg(pop, r1)
