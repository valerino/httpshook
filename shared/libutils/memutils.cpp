/*!
* \file memutils.h
*
* \author valerino
* \date novembre 2017
*
* memory allocation utilities
* TODO: extend with zeroing on free
*/

#include <windows.h>
#include <stdint.h>

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
BOOL memValidatePtr(const LPVOID buffer_bgn, SIZE_T buffer_size, const LPVOID field_bgn, SIZE_T field_size) {
	ULONGLONG start = (ULONGLONG)buffer_bgn;
	ULONGLONG end = start + buffer_size;

	ULONGLONG field_end = (ULONGLONG)field_bgn + field_size;

	if ((ULONGLONG)field_bgn < start) {
		return FALSE;
	}
	if (field_end >= end) {
		return FALSE;
	}
	return TRUE;
}

void* memHeapAlloc(uint32_t size) {
	uint8_t* p = (uint8_t*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, size + sizeof(uint32_t));
	if (!p) {
		SetLastError(ERROR_OUTOFMEMORY);
		return NULL;
	}
	return (void*)p;
}

void memHeapFree(void* ptr) {
	if (!ptr) {
		return;
	}
	HeapFree(GetProcessHeap(), 0, ptr);
}

void* memHeapRealloc(void* ptr, uint32_t newSize) {
	uint8_t* p = NULL;
	if (ptr) {
		p = (uint8_t*)HeapReAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, ptr, newSize);
	}
	else {
		// just allocate a new block
		p = (uint8_t*)memHeapAlloc(newSize);
	}

	if (!p) {
		SetLastError(ERROR_OUTOFMEMORY);
	}
	return (void*)p;
}
#pragma code_seg(pop, r1)

