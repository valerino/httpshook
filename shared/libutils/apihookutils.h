#pragma once
/*!
 * \file apihookutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * api hooking utilities, using minhook (https://github.com/TsudaKageyu/minhook)
 */

#include "minhook/include/minhook.h"

 /*!
  * \brief check if a function is already hooked
  *
  * \param void * address
  * \return BOOL
  */
BOOL apihkCheck(void* address);

/*!
  * \brief initializes api, must be called once per process
  *	\note subsequent calls after the first succesful call (in the same process) just increments a reference count and does nothing.
  * \return int MH_OK on success
  */
int apihkInitialize();

/*!
 * \brief finalizes api
 * \note subsequent calls in the same process just decrements a reference count, once it reaches 0 the api is finalized
 * \return void
 */
void apihkfinalize();

/*!
 * \brief install hook on the specified function address
 *
 * \param uint8_t * function the function to be hooked
 * \param uint8_t * replacement the function replacement (hook)
 * \param void * * hk on successful return, pointer to the trampoline (to be called in the hooked function code to call back the original function)
 * \param BOOL enable if TRUE, hook is automatically enabled immediately. either, apiHKEnableHook() must be called
 * \return int MH_OK on success
 */
int apihkInstallHook(uint8_t* function, uint8_t* replacement, void** hk, LPVOID trampolineMem, BOOL enable);

/*!
* \brief enable or disable hook
*
* \param uint8_t * function the function to be hooked (use MH_ALL_HOOKS to enable/disable all hooks placed IN THE PROCESS at once)
* \param BOOL enable
* \return int MH_OK on success
*/
int apihkEnableHook(uint8_t* function, BOOL enable, BOOL remote);

/*!
 * \brief remove function hook
 *
 * \param void * function the function to be restored
 * \return void
 */
void apihkUninstallHook(void* function);
