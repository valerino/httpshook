/*!
 * \file peutils.h
 *
 * \author valerino
 * \date novembre 2017
 *
 * PE file format utilities
 */
#pragma once
#include <stdint.h>

 /*!
  * \class defines a dynamic import
  *
  * \brief
  *
  * \author valerino
  * \date novembre 2017
  */
typedef struct _dynamic_import_ptr {
	ULONG_PTR** ptr;	/* address of the pointer to the resolved import */
	HMODULE hmod; /* module handle */
	uint32_t rot13_hash; /* the import hash */
} dynamic_import_ptr;

/*!
* \brief custom GetProcAddress() implementation
* \param module handle to a loaded module (base address)
* \param hash an hash obtained through hashRor13() (case insensitive, works on unicode strings too)
* \return pointer to function or NULL
* \note position independent (PIC) code, will be linked in '.text2' section
*/
extern "C" void* peGetProcAddress(HMODULE module, DWORD hash);

/*!
* \brief convert relative virtual address to raw file offset
*
* \param dwRva an RVA
* \param uiBaseAddress module base address which the RVA belongs to
* \return ULONG_PTR
* \note position independent (PIC) code, will be linked in '.text2' section
*/
extern "C" ULONG_PTR peRvaToOffset(DWORD dwRva, UINT_PTR uiBaseAddress);

/*!
 * \brief fill an array of dynamic_import_ptr
 *
 * \param dynamic_import_ptr * p on input, an array with rot_13 hashes and module handles set, with the last hash set to 0. on successfull output, all the corresponding pointers are resolved
 * \return extern uint32_t 0 on success
 */
extern "C" uint32_t peResolveDynamicImports(dynamic_import_ptr* p);


#define RELOC_32BIT_FIELD 3
#define RELOC_64BIT_FIELD 0xA

typedef struct _BASE_RELOCATION_ENTRY {
	WORD Offset : 12;
	WORD Type : 4;
} BASE_RELOCATION_ENTRY;

/*!
 * \brief get pointer to the IMAGE_NT_HEADERS (you can then cast to 32 or 64 version)
 *
 * \param const BYTE * pe_buffer the PE
 * \return BYTE*
 */
BYTE* peGetNtHeaders(const BYTE *pe_buffer);

/*!
 * \brief get PE architecture (IMAGE_FILE_MACHINE_*))
 *
 * \param const BYTE * pe_buffer the PE
 * \return WORD
 */
WORD peGetArchitecture(const BYTE *pe_buffer);

/*!
 * \brief check if it's a PE64
 *
 * \param const BYTE * pe_buffer the PE
 * \return BOOL
 */
BOOL peIs64bit(const BYTE *pe_buffer);

/*!
 * \brief get PE image base
 *
 * \param const BYTE * pe_buffer the PE
 * \return ULONGLONG
 */
ULONGLONG peGetImageBase(const BYTE *pe_buffer);

/*!
 * \brief get PE directory entry
 *
 * \param const BYTE * pe_buffer the PE
 * \param DWORD dir_id a directory entry id (i.e.  IMAGE_DIRECTORY_ENTRY_BASERELOC, etc...)
 * \return IMAGE_DATA_DIRECTORY*
 */
IMAGE_DATA_DIRECTORY* peGetDirectoryEntry(const BYTE *pe_buffer, DWORD dir_id);

/*!
* \brief set subsystem in a PE image
*
* \param BYTE * payload the PE
* \param WORD subsystem the subsystem to be set (i.e. IMAGE_SUBSYSTEM_WINDOWS_GUI)
* \return bool
*/
BOOL peSetSubsystem(BYTE* payload, WORD subsystem);

/*!
 * \brief sets a new image base in a PE image
 *
 * \param BYTE * payload the PE
 * \param ULONGLONG destImageBase the new imagebase
 * \return BOOL
 */
BOOL peSetImageBase(BYTE* payload, ULONGLONG destImageBase);

/*!
 * \brief get PE entrypoint relative virtual address
 *
 * \param const BYTE * pe_buffer the PE
 * \return DWORD
 */
DWORD peGetEntrypointRva(const BYTE *pe_buffer);