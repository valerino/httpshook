/*!
 * \file procutils.cpp
 *
 * \author valerino
 * \date novembre 2017
 *
 * process related utilities
 */

#include <windows.h>
#include <stdint.h>
#include "memutils.h"
#include "procutils.h"
#include "hashutils.h"
#include "procutils.h"
#include "peutils.h"
#include "strutils.h"
#include "dbgutils.h"
#include "bufferutils.h"
#include "procutils.h"
#include "osutils.h"
#include "refloader.h"
#include <shlwapi.h>
#include <Winternl.h>

static HMODULE advapi32 = NULL;
int __procInitializeCalled = 0;

OPENPROCESSTOKEN ptrOpenProcessToken = NULL;
ADJUSTTOKENPRIVILEGES ptrAdjustTokenPrivileges = NULL;
LOOKUPPRIVILEGEVALUEW ptrLookupPrivilegeValueW = NULL;
CREATETOOLHELP32SNAPSHOT ptrCreateToolhelp32Snapshot = NULL;
PROCESS32FIRSTA ptrProcess32First = NULL;
PROCESS32NEXTA ptrProcess32Next = NULL;
MODULE32FIRSTW ptrModule32FirstW = NULL;
MODULE32NEXTW ptrModule32NextW = NULL;
THREAD32FIRST ptrThread32First = NULL;
THREAD32NEXT ptrThread32Next = NULL;
CREATEPROCESSW ptrCreateProcessW = NULL;
GETCURRENTPROCESSID ptrGetCurrentProcessId = NULL;
GETCURRENTTHREADID ptrGetCurrentThreadId = NULL;
GETCURRENTPROCESS ptrGetCurrentProcess = NULL;
ISWOW64PROCESS ptrIsWow64Process = NULL;
TERMINATEPROCESS ptrTerminateProcess = NULL;
EXPANDENVIRONMENTSTRINGSA ptrExpandEnvironmentStringsA = NULL;
RTLEXITUSERTHREAD ptrRtlExitUserThread = NULL;
GETEXITCODETHREAD ptrGetExitCodeThread = NULL;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

extern "C" PVOID procGetPeb() {
#if defined(_WIN64)
	UINT64 uiPeb = __readgsqword(0x60);
	return (LPVOID)(UINT_PTR)uiPeb;
#else
	UINT32 uiPeb = __readfsdword(0x30);
	return (LPVOID)(UINT_PTR)uiPeb;
#endif
}

extern "C" HMODULE procFindModuleByHash(uint32_t hash) {
	// get current process peb
	PPEB peb = (PPEB)procGetPeb();

	// from here, we access the loaded modules linkedlist
	LDR_DATA_TABLE_ENTRY* module_ptr = (PLDR_DATA_TABLE_ENTRY)peb->Ldr->InMemoryOrderModuleList.Flink;
	LDR_DATA_TABLE_ENTRY* first_mod = module_ptr;
	do {
		// check ror13 hash
		if (hashRor13((uint8_t*)module_ptr->FullDllName.Buffer, module_ptr->FullDllName.Length) == hash) {
			// found !
			return (HMODULE)module_ptr->Reserved2[0];
		}

		// get flink and loop
		module_ptr = (PLDR_DATA_TABLE_ENTRY)module_ptr->Reserved1[0];
	} while (module_ptr && module_ptr != first_mod);

	// error occurred, not found!
	return NULL;
}

ULONG procInitialize() {
	if (__procInitializeCalled > 0) {
		return 0;
	}
	__procInitializeCalled++;

	// depends on string tools
	ULONG res = strInitialize();
	if (res != 0) {
		procFinalize();
		return res;
	}
	res = osInitialize();
	if (res != 0) {
		procFinalize();
		return res;
	}

	// get libraries
	advapi32 = procFindModuleByHash(0xc78a43f4); // = > a d v a p i 3 2 . d l l
	if (!advapi32) {
		char dll[] = { 'a','d','v','a','p','i','3','2','.','d','l','l','\0' };
		advapi32 = ptrLoadLibraryA(dll);
		if (!advapi32) {
			res = GetLastError();
			procFinalize();
			return res;
		}
	}

	// these are guaranteed to exist
	HMODULE kernel32 = procFindModuleByHash(KERNEL32DLL_HASH_UNICODE);
	HMODULE ntdll = procFindModuleByHash(NTDLL_HASH_UNICODE);

	// get pointers
	dynamic_import_ptr ptrs[] = {
		{ (ULONG_PTR**)&ptrOpenProcessToken, advapi32, 0xe79d18d6 }, // = > OpenProcessToken
		{ (ULONG_PTR**)&ptrAdjustTokenPrivileges, advapi32, 0xaa663ad2 }, // => AdjustTokenPrivileges
		{ (ULONG_PTR**)&ptrLookupPrivilegeValueW, advapi32, 0x1e0573ac }, // => LookupPrivilegeValueW
		{ (ULONG_PTR**)&ptrCreateToolhelp32Snapshot, kernel32, 0x8a62152f }, // => CreateToolhelp32Snapshot
		{ (ULONG_PTR**)&ptrProcess32First, kernel32, 0xd108ac7e }, // => Process32First
		{ (ULONG_PTR**)&ptrProcess32Next, kernel32, 0x25b55922 }, // => Process32Next
		{ (ULONG_PTR**)&ptrModule32FirstW, kernel32, 0x537710a0 }, // => Module32FirstW
		{ (ULONG_PTR**)&ptrModule32NextW, kernel32, 0xda11abf1 }, // => Module32NextW
		{ (ULONG_PTR**)&ptrThread32First, kernel32, 0x56faaac2 }, // => Thread32First
		{ (ULONG_PTR**)&ptrThread32Next, kernel32,  0x657dc9e0 }, // => Thread32Next
		{ (ULONG_PTR**)&ptrCreateProcessW, kernel32, 0xb4f0f46f}, // => CreateProcessW
		{ (ULONG_PTR**)&ptrGetCurrentProcessId, kernel32, 0x952a6aca }, // => GetCurrentProcessId
		{ (ULONG_PTR**)&ptrGetCurrentThreadId, kernel32, 0xc4d8ef61 }, // => GetCurrentThreadId
		{ (ULONG_PTR**)&ptrGetCurrentProcess, kernel32, 0x1a4b89aa }, // => GetCurrentProcess
		{ (ULONG_PTR**)&ptrIsWow64Process, kernel32, 0xa50dc580 }, // => IsWow64Process
		{ (ULONG_PTR**)&ptrTerminateProcess, kernel32, 0x7722b4b }, // => TerminateProcess
		{ (ULONG_PTR**)&ptrRtlExitUserThread, ntdll, 0x9dbc77ee }, // => RtlExitUserThread
		{ (ULONG_PTR**)&ptrExpandEnvironmentStringsA, kernel32, 0x78c1ba3a }, // => ExpandEnvironmentStringsA
		{ (ULONG_PTR**)&ptrGetExitCodeThread, kernel32, 0xb97d07cc }, // => GetExitCodeThread
		{ 0,0,0 }
	};
	if (peResolveDynamicImports(ptrs) != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot resolve procutils imports!");
		procFinalize();
		return ERROR_NOT_FOUND;
	}

	DBG_PRINT(DBG_LEVEL_INFO, "procutils initialized!");
	return 0;
}

void procFinalize() {
	if (__procInitializeCalled == 0) {
		return;
	}
	__procInitializeCalled--;

	if (__procInitializeCalled == 0) {
		if (advapi32) {
			ptrFreeLibrary(advapi32);
			advapi32 = NULL;
		}
		strFinalize();
		osFinalize();
	}
}

ULONG procExecute(const PWCHAR path, const PWCHAR params, BOOL hidden) {
	ULONG res = 0;
	if (!path) {
		return ERROR_INVALID_PARAMETER;
	}

	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFOW si = { 0 };
	si.cb = sizeof(STARTUPINFOW);
	si.dwFlags = STARTF_USESHOWWINDOW;
	if (hidden) {
		si.wShowWindow = SW_HIDE;
	}
	else {
		si.wShowWindow = SW_SHOW;
	}

	PWCHAR cmdLine = (PWCHAR)memHeapAlloc(1024);
	if (!cmdLine) {
		return ERROR_OUTOFMEMORY;
	}
	if (!params) {
		strPrintfW(cmdLine, 1024 / sizeof(WCHAR), path);
	}
	else {
		strPrintfW(cmdLine, 1024 / sizeof(WCHAR), L"%s %s", path, params);
	}

	if (!ptrCreateProcessW(NULL, cmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
		res = GetLastError();
		memHeapFree(cmdLine);
		return res;
	}
	memHeapFree(cmdLine);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	return 0;
}

ULONG procGetProcessesList(PLIST_ENTRY processes, PULONG num) {
	*num = 0;
	InitializeListHead(processes);

	// get processes snapshot
	HANDLE hs = ptrCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hs == INVALID_HANDLE_VALUE) {
		DBG_PRINT(DBG_LEVEL_ERROR, "failed CreateToolhelp32Snapshot()!");
		ULONG gle = GetLastError();
		return gle;
	}

	// walk snapshot
	ULONG count = 0;
	ULONG err;
	while (TRUE) {
		// allocate entry
		processEntry* entry = (processEntry*)memHeapAlloc(sizeof(processEntry));
		if (!entry) {
			err = ERROR_OUTOFMEMORY;
			break;
		}
		entry->proc.dwSize = sizeof(PROCESSENTRY32);

		BOOL b;
		if (count == 0) {
			b = ptrProcess32First(hs, &entry->proc);
		}
		else {
			b = ptrProcess32Next(hs, &entry->proc);
		}
		if (!b) {
			memHeapFree(entry);
			// end reached
			*num = count;
			if (count == 0) {
				DBG_PRINT(DBG_LEVEL_ERROR, "failed Process32First()");
				err = ERROR_NO_MORE_FILES;
			}
			else {
				err = 0;
			}
			break;
		}

		// insert in list
		InsertTailList(processes, &entry->chain);
		count++;
	}

	CloseHandle(hs);
	if (err != 0) {
		procFreeProcessesList(processes);
	}

	return err;
}

void procFreeProcessesList(PLIST_ENTRY processes) {
	while (!IsListEmpty(processes)) {
		PLIST_ENTRY p = RemoveHeadList(processes);
		processEntry* entry = CONTAINING_RECORD(p, processEntry, chain);
		memHeapFree(entry);
	}
}

ULONG procGetModuleEntryByHash(ULONG pid, uint32_t hash, ULONG flags, MODULEENTRY32* modEntry) {
	memset(modEntry, 0, sizeof(MODULEENTRY32));

	// get modules list
	ULONG num;
	LIST_ENTRY modules;
	ULONG err = procGetModulesList(&modules, pid, flags, &num);
	if (err != 0) {
		return err;
	}

	// walk entries
	LIST_ENTRY* current = modules.Flink;
	BOOL found = FALSE;
	while (TRUE) {
		moduleEntry* entry = CONTAINING_RECORD(current, moduleEntry, chain);

		// check hash (on the bare module name)
		PWCHAR p = strRightChrW(entry->mod.szModule, NULL, (WCHAR)'\\');
		if (p) {
			p++;
		}
		else {
			// already bare name
			p = entry->mod.szModule;
		}
		uint32_t h = hashRor13((uint8_t*)p, strLenW(p) * sizeof(WCHAR));
		if (h == hash) {
			memcpy(modEntry, &entry->mod, sizeof(MODULEENTRY32));
			found = TRUE;
			break;
		}

		// next
		if (current->Flink == &modules || current->Flink == NULL) {
			break;
		}
		current = current->Flink;
	}

	// free allocated list
	procFreeModulesList(&modules);
	if (!found) {
		return ERROR_NOT_FOUND;
	}

	return 0;
}

ULONG procGetModuleEntry(ULONG pid, const PWCHAR moduleName, ULONG flags, MODULEENTRY32* modEntry) {
	memset(modEntry, 0, sizeof(MODULEENTRY32));

	// get modules list
	ULONG num;
	LIST_ENTRY modules;
	ULONG err = procGetModulesList(&modules, pid, flags, &num);
	if (err != 0) {
		return err;
	}

	// walk entries
	LIST_ENTRY* current = modules.Flink;
	BOOL found = FALSE;
	while (TRUE) {
		moduleEntry* entry = CONTAINING_RECORD(current, moduleEntry, chain);

		// check hash (on the bare module name)
		PWCHAR p = strRightChrW(entry->mod.szModule, NULL, (WCHAR)'\\');
		if (p) {
			p++;
		}
		else {
			// already bare name
			p = entry->mod.szModule;
		}
		if (strInStringW(p, moduleName)) {
			memcpy(modEntry, &entry->mod, sizeof(MODULEENTRY32));
			found = TRUE;
			break;
		}

		// next
		if (current->Flink == &modules || current->Flink == NULL) {
			break;
		}
		current = current->Flink;
	}

	// free allocated list
	procFreeModulesList(&modules);
	if (!found) {
		return ERROR_NOT_FOUND;
	}

	return 0;
}

ULONG procFindProcessByHash(uint32_t hash, uint32_t* pids, uint32_t pidDwordSize, uint32_t* numPids) {
	if (pidDwordSize == 0 || !pids || !numPids) {
		return ERROR_INVALID_PARAMETER;
	}
	*numPids = 0;
	memset(pids, 0, pidDwordSize * sizeof(DWORD));
	uint32_t* p_pids = pids;
	int i = 0;

	// get processes list
	ULONG num;
	LIST_ENTRY processes;
	ULONG err = procGetProcessesList(&processes, &num);
	if (err != 0) {
		return ERROR_NOT_FOUND;
	}

	// walk entries
	LIST_ENTRY* current = processes.Flink;
	while (TRUE) {
		processEntry* entry = CONTAINING_RECORD(current, processEntry, chain);

		// check hash (on the bare module name)
		PCHAR p = strRightChrA(entry->proc.szExeFile, NULL, '\\');
		if (p) {
			p++;
		}
		else {
			// already bare name
			p = entry->proc.szExeFile;
		}

		// hash the name
		uint32_t h = hashRor13((uint8_t*)p, (uint32_t)strlen(p));
		if (h == hash) {
			// save this pid in our out array
			*p_pids = entry->proc.th32ProcessID;

			// next entry
			p_pids++;

			// check if we exhausted the buffer
			i++;
			if (i > (int)pidDwordSize) {
				break;
			}
		}

		// next
		if (current->Flink == &processes || current->Flink == NULL) {
			break;
		}
		current = current->Flink;
	}

	// free allocated list
	procFreeProcessesList(&processes);
	if (i == 0) {
		return ERROR_NOT_FOUND;
	}

	*numPids = i;
	return 0;
}

ULONG procGetThreadsList(PLIST_ENTRY threads, DWORD pid, PULONG num) {
	*num = 0;
	InitializeListHead(threads);

__again:
	// get snapshot
	HANDLE hs = ptrCreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, pid);
	if (hs == INVALID_HANDLE_VALUE) {
		ULONG gle = GetLastError();
		if (gle == ERROR_BAD_LENGTH) {
			// call again until succeeds
			goto __again;
		}
		else {
			return gle;
		}
	}

	// walk snapshot
	ULONG count = 0;
	ULONG n = 0;
	ULONG err;
	while (TRUE) {
		// allocate entry
		threadEntry* entry = (threadEntry*)memHeapAlloc(sizeof(threadEntry));
		if (!entry) {
			err = ERROR_OUTOFMEMORY;
			break;
		}
		entry->thread.dwSize = sizeof(THREADENTRY32);

		BOOL b;
		if (n == 0) {
			b = ptrThread32First(hs, &entry->thread);
		}
		else {
			b = ptrThread32Next(hs, &entry->thread);
		}
		if (!b) {
			memHeapFree(entry);
			if (GetLastError() == ERROR_NO_MORE_FILES) {
				*num = count;
				err = 0;
			}
			else {
				err = GetLastError();
			}
			break;
		}
		else {
			if (entry->thread.th32OwnerProcessID == pid) {
				// insert in list
				InsertTailList(threads, &entry->chain);
				count++;
			}
			else {
				// free, not belonging to pid
				memHeapFree(entry);
			}
		}
		n++;
	}

	CloseHandle(hs);
	if (err != 0) {
		procFreeThreadsList(threads);
	}
	return err;
}

void procFreeThreadsList(PLIST_ENTRY threads) {
	while (!IsListEmpty(threads)) {
		PLIST_ENTRY p = RemoveHeadList(threads);
		threadEntry* entry = CONTAINING_RECORD(p, threadEntry, chain);
		memHeapFree(entry);
	}
}

ULONG procGetModulesList(PLIST_ENTRY modules, DWORD pid, ULONG flags, PULONG num) {
	*num = 0;
	InitializeListHead(modules);

__again:
	// get snapshot
	HANDLE hs = ptrCreateToolhelp32Snapshot(flags, pid);
	if (hs == INVALID_HANDLE_VALUE) {
		ULONG gle = GetLastError();
		if (gle == ERROR_BAD_LENGTH) {
			// call again until succeeds
			goto __again;
		}
		else {
			return gle;
		}
	}

	// walk snapshot
	ULONG count = 0;
	ULONG err;
	while (TRUE) {
		// allocate entry
		moduleEntry* entry = (moduleEntry*)memHeapAlloc(sizeof(moduleEntry));
		if (!entry) {
			err = ERROR_OUTOFMEMORY;
			break;
		}
		entry->mod.dwSize = sizeof(MODULEENTRY32);

		BOOL b;
		if (count == 0) {
			b = ptrModule32FirstW(hs, &entry->mod);
		}
		else {
			b = ptrModule32NextW(hs, &entry->mod);
		}

		if (!b) {
			memHeapFree(entry);
			if (GetLastError() == ERROR_NO_MORE_FILES) {
				*num = count;
				err = 0;
			}
			else {
				err = GetLastError();
			}
			break;
		}
		else {
			// insert in list
			InsertTailList(modules, &entry->chain);
			count++;
		}
	}

	CloseHandle(hs);
	if (err != 0) {
		procFreeModulesList(modules);
	}
	return err;
}

void procFreeModulesList(PLIST_ENTRY modules) {
	while (!IsListEmpty(modules)) {
		PLIST_ENTRY p = RemoveHeadList(modules);
		moduleEntry* entry = CONTAINING_RECORD(p, moduleEntry, chain);
		memHeapFree(entry);
	}
}

ULONG procSetPrivilege(HANDLE token, const PWCHAR privilege, BOOL enable) {
	// lookup privilege
	LUID luid;
	if (!ptrLookupPrivilegeValueW(NULL, privilege, &luid)) {
		return GetLastError();
	}

	// enable or disable privilege
	TOKEN_PRIVILEGES tp = { 0 };
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (enable) {
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	}
	else {
		tp.Privileges[0].Attributes = 0;
	}

	if (!ptrAdjustTokenPrivileges(token, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL)) {
		return GetLastError();
	}

	return 0;
}

ULONG procSetCurrentProcessPrivilege(const PWCHAR privilege, BOOL enable) {
	// get this process token
	HANDLE tok = NULL;

	if (!ptrOpenProcessToken(ptrGetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &tok)) {
		return GetLastError();
	}

	// set privilege
	ULONG gle = procSetPrivilege(tok, privilege, enable);
	if (gle != 0) {
		CloseHandle(tok);
		return gle;
	}

	CloseHandle(tok);
	return 0;
}

uint8_t* procFindPatternByHash(DWORD pid, uint32_t moduleHash, DWORD flags, PBYTE toSearch, DWORD toSearchSize, uint8_t wildcard, BOOL useWildcard) {
	MODULEENTRY32W mod = { 0 };
	ULONG res = procGetModuleEntryByHash(pid, moduleHash, flags, &mod);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "procFindPatternByHash() target DLL hash DO NOT match!");
		return NULL;
	}
	else {
		DBG_PRINT(DBG_LEVEL_ERROR, "procFindPatternByHash() target DLL hash matches!");
	}

	uint8_t* found;
	if (useWildcard) {
		// wildcard search
		found = (PBYTE)bufferMemMemWildcard(mod.modBaseAddr, mod.modBaseSize, toSearch, toSearchSize, wildcard);
	}
	else {
		found = (PBYTE)bufferMemMem(mod.modBaseAddr, mod.modBaseSize, toSearch, toSearchSize);
	}
	if (!found) {
		DBG_PRINT(DBG_LEVEL_ERROR, "procFindPatternByHash() byte pattern not found!");
	}
	return found;
}

uint32_t procGetCurrentProcessHash() {
	CHAR process[MAX_PATH];
	ptrGetModuleFileNameA(GetModuleHandle(NULL), process, sizeof(process));
	PCHAR p = strRightChrA(process, NULL, '\\');
	p++;
	uint32_t h = hashRor13((uint8_t*)p, (uint32_t)strlen(p));
	DBG_PRINT(DBG_LEVEL_INFO, "injected into: %s, hash=%x", p, h);
	return h;
}

#pragma code_seg(pop, r1)
