/*!
* \file b64utils.cpp
*
* \author valerino
* \date novembre 2017
*
* base64 encoder ripped from BSD sources :)
*/

#ifdef _WIN32
#include <windows.h>
#include <wincrypt.h>
#endif
#include "fileutils.h"
#include "memutils.h"
#include "peutils.h"
#include "procutils.h"
#include "cryptutils.h"
#include "dbgutils.h"
#include "strutils.h"
#include "osutils.h"
#include <stdint.h>

int __b64InitializeCalled = 0;
HMODULE crypt32 = NULL;

typedef BOOL(WINAPI* CRYPTSTRINGTOBINARYA)(PCHAR pszString, DWORD cchString, DWORD dwFlags, BYTE *pbBinary, DWORD *pcbBinary, DWORD *pdwSkip, DWORD *pdwFlags);
CRYPTSTRINGTOBINARYA ptrCryptStringToBinaryA = NULL;
typedef BOOL(WINAPI* CRYPTBINARYTOSTRINGA)(const BYTE *pbBinary, DWORD cbBinary, DWORD dwFlags, PCHAR pszString, DWORD *pcchString);
CRYPTBINARYTOSTRINGA ptrCryptBinaryToStringA = NULL;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
void base64Finalize() {
	if (__b64InitializeCalled == 0) {
		return;
	}
	__b64InitializeCalled--;

	if (__b64InitializeCalled == 0) {
		if (crypt32) {
			ptrFreeLibrary(crypt32);
		}
		osFinalize();
	}
}

uint32_t base64Initialize() {
	if (__b64InitializeCalled > 0) {
		return 0;
	}
	__b64InitializeCalled++;

	uint32_t res = osInitialize();
	if (res != 0) {
		base64Finalize();
		return res;
	}

	// ensure we have crypt32
	crypt32 = procFindModuleByHash(0xc4e9bcb4); // => c r y p t 3 2 . d l l
	if (!crypt32) {
		char dll[] = { 'c','r','y','p','t','3','2','.','d','l','l','\0' };
		crypt32 = ptrLoadLibraryA(dll);
		if (!crypt32) {
			res = GetLastError();
			base64Finalize();
			return res;
		}
	}

	// get pointers
	dynamic_import_ptr ptrs[] = {
		{ (ULONG_PTR**)&ptrCryptBinaryToStringA, crypt32, 0x235f9b93 }, // => CryptBinaryToStringA
		{ (ULONG_PTR**)&ptrCryptStringToBinaryA, crypt32, 0x993b10cc }, // => CryptStringToBinaryA
		{ 0,0,0 }
	};
	if (peResolveDynamicImports(ptrs) != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "cannot resolve b64utils imports!");
		base64Finalize();
		return ERROR_NOT_FOUND;
	}
	return 0;
}

uint32_t base64Encode(uint8_t* data, uint32_t size, char** b64, uint32_t* b64size) {
	if (!data || !size || !b64 || !b64size) {
		return ERROR_INVALID_PARAMETER;
	}
	*b64size = 0;
	*b64 = NULL;

	// calculated needed size
	DWORD toAlloc = 0;
	BOOL res = ptrCryptBinaryToStringA(data, size, CRYPT_STRING_BASE64, NULL, &toAlloc);
	if (!res) {
		return ERROR_INVALID_DATA;
	}

	// allocate
	char* p = (char*)memHeapAlloc(toAlloc + 32);
	if (!p) {
		return ERROR_OUTOFMEMORY;
	}

	// convert
	res = ptrCryptBinaryToStringA(data, size, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, p, &toAlloc);
	if (!res) {
		memHeapFree(p);
		return ERROR_INVALID_DATA;
	}
	*b64 = p;
	*b64size = toAlloc;
	return 0;
}

uint32_t base64Decode(char* b64, uint8_t** decoded, uint32_t* decodedSize) {
	if (!decoded || !decodedSize || !b64) {
		return ERROR_INVALID_PARAMETER;
	}
	*decodedSize = 0;
	*decoded = NULL;

	// calculated needed size
	DWORD toAlloc = 0;
	BOOL res = ptrCryptStringToBinaryA(b64, 0, CRYPT_STRING_BASE64, NULL, &toAlloc, NULL, NULL);
	if (!res) {
		return ERROR_INVALID_DATA;
	}

	// allocate
	uint8_t* p = (uint8_t*)memHeapAlloc(toAlloc + 32);
	if (!p) {
		return ERROR_OUTOFMEMORY;
	}

	// convert
	res = ptrCryptStringToBinaryA(b64, 0, CRYPT_STRING_BASE64, p, &toAlloc, NULL, NULL);
	if (!res) {
		memHeapFree(p);
		return ERROR_INVALID_DATA;
	}
	*decoded = p;
	*decodedSize = toAlloc;
	return 0;
}
/* everything here will be linked into '.text2' section */
#pragma code_seg(pop, r1)
