/*!
 * \file refloader.cpp
 * \date 2017/10/31 19:27
 *
 * \author valerino
 *
 * \brief this is inspired by the metasploit reflective loader, heavily reworked to avoid export functions (DllMain only is used).
 *
 * to use the reflective loader to load a DLL in a remote process you should use one of the injectReflective* functions in inject.cpp with a DLL which must adehere to the following
 * in its DllMain():
 * 1) if fdwReason==DLLMAIN_RUN_PELOADER you must call reflectivePELoader() and return TRUE
 * 2) if fdwReason==DLLMAIN_RUN_LOADCHECK you may call a check function which basically should return TRUE if you want to continue with DLL initialization,
 *		or FALSE if you want to abort loading. This is not mandatory to implement a check function, just return TRUE if you don't have a check function.
 * everything else is handled by the injector function, which injects a stub code in the target process to call DllMain() and, in case, free the DLL memory in the target process
 * if startup has failed due to some reasong (DLLMAIN_RUN_LOADCHECK returns false, or any other error)
 *
 * alternatively, the reflective loader can be used to map and load a DLL in the current process. Just pass a DLL with the DllMain() as above, in a +RWX buffer as peBuffer parameter, and:
 * 1) call DllMain() first with fdwReason = DLLMAIN_RUN_PELOADER. This will map the DLL in the current process
 * 2) optionally, call DllMain() again with fdwReason=DLLMAIN_RUN_LOADCHECK if your DLL implements it, and check the return code. if it fails, the dll buffer can be freed.
 * 3) finally call DllMain() as normal, i.e. with fdwReason=DLL_PROCESS_ATTACH
 * \note: the containing dll must be compiled with FileAlignment=SectionAlignment and with all security checks turned off !!! (TODO: support alignment ?)
 *
*/
#include <windows.h>
#include <stdint.h>
#include <intrin.h>
#include "refloader_internal.h"
#include "peutils.h"
#include "refloader.h"
#include "hashutils.h"

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

extern "C" DWORD reflectivePEGetEntrypointRVA(VOID * peImage) {
#ifdef _WIN64
	DWORD targetArch = 2;
#else
	// win32
	DWORD targetArch = 1;
#endif

	// this is the base address
	UINT_PTR uiBaseAddress = (UINT_PTR)peImage;

	// get the File Offset of the modules NT Header
	PIMAGE_NT_HEADERS nthdr = (PIMAGE_NT_HEADERS)(uiBaseAddress + ((PIMAGE_DOS_HEADER)uiBaseAddress)->e_lfanew);

	// currenlty we can only process a PE file which is the same type as the one this fuction has
	// been compiled as, due to various offset in the PE structures being defined at compile time.
	if (nthdr->OptionalHeader.Magic == 0x010B) {
		// pe32
		if (targetArch != 1) {
			return 0;
		}
	}
	else if (nthdr->OptionalHeader.Magic == 0x020B) {
		// pe64
		if (targetArch != 2) {
			return 0;
		}
	}
	else {
		// not supported!
		return 0;
	}

	// return the entrypoint RVA
	return nthdr->OptionalHeader.AddressOfEntryPoint;
}

BOOL reflectivePELoader(ULONG_PTR peBuffer, ULONG_PTR* newBase) {
	if (newBase) {
		*newBase = NULL;
	}

	// the functions we need
	LOADLIBRARYA pLoadLibraryA = NULL;
	GETPROCADDRESS pGetProcAddress = NULL;
	VIRTUALALLOC pVirtualAlloc = NULL;
	NTFLUSHINSTRUCTIONCACHE pNtFlushInstructionCache = NULL;
	VIRTUALLOCK pVirtualLock = NULL;

	// this is the imagebase
	ULONG_PTR uiBaseAddress = (ULONG_PTR)peBuffer;

	//__debugbreak();
	// get the Process Enviroment Block
#ifdef _WIN64
	ULONG_PTR peb = __readgsqword(0x60);
#else
	ULONG_PTR peb = __readfsdword(0x30);
#endif

	// get the processes loaded modules. ref: http://msdn.microsoft.com/en-us/library/aa813708(VS.85).aspx
	ULONG_PTR loadedModulesList = (ULONG_PTR)((_PPEB)peb)->pLdr;

	// get the first entry of the InMemoryOrder module list
	ULONG_PTR module_entry = (ULONG_PTR)((PPEB_LDR_DATA)loadedModulesList)->InMemoryOrderModuleList.Flink;

	// search the imports we need from the loaded modules (kernel32)
	while (module_entry) {
		ULONG_PTR loaded_module_base = 0;
		ULONG_PTR nthdr = 0;
		ULONG_PTR exports = 0;
		ULONG_PTR uiExportDir = 0;
		ULONG_PTR uiNameArray = 0;
		ULONG_PTR uiNameOrdinals = 0;
		USHORT exportsToFind = 0;

		// get pointer to current modules name (unicode string)
		ULONG_PTR base_dll_name = (ULONG_PTR)((PLDR_DATA_TABLE_ENTRY)module_entry)->BaseDllName.pBuffer;

		// get module name size
		USHORT base_dll_name_size = ((PLDR_DATA_TABLE_ENTRY)module_entry)->BaseDllName.Length;

		// compute the hash of the module name (it's an unicode string)
		uint32_t module_name_hash = hashRor13((uint8_t*)base_dll_name, base_dll_name_size);

		// compare the hash with that of kernel32.dll
#ifdef _WIN64
		if ((DWORD)module_name_hash == KERNELBASEDLL_HASH_UNICODE) {
#else
		if ((DWORD)module_name_hash == KERNEL32DLL_HASH_UNICODE) {
#endif
			// get this modules base address
			loaded_module_base = (ULONG_PTR)((PLDR_DATA_TABLE_ENTRY)module_entry)->DllBase;

			// get the VA of the modules NT Header
			nthdr = loaded_module_base + ((PIMAGE_DOS_HEADER)loaded_module_base)->e_lfanew;

			// get the VA of the export directory
			exports = (ULONG_PTR)&((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
			uiExportDir = (loaded_module_base + ((PIMAGE_DATA_DIRECTORY)exports)->VirtualAddress);

			// get the VA for the array of name pointers
			uiNameArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNames);

			// get the VA for the array of name ordinals
			uiNameOrdinals = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNameOrdinals);

			// we need these many exports from kernel32
			exportsToFind = 4;

			// loop while we still have exports to find
			DWORD export_hash = 0;
			ULONG_PTR uiAddressArray = 0;
			while (exportsToFind > 0) {
				// compute the hash values for this function name (ansi string)
				export_hash = hashRor13((uint8_t*)(loaded_module_base + DEREF_32(uiNameArray)), 0);

				// if we have found a function we want we get its virtual address
				if (export_hash == LOADLIBRARYA_HASH || export_hash == GETPROCADDRESS_HASH || export_hash == VIRTUALALLOC_HASH || export_hash == VIRTUALLOCK_HASH) {
					// get the VA for the array of addresses
					uiAddressArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions);

					// use this functions name ordinal as an index into the array of name pointers
					uiAddressArray += (DEREF_16(uiNameOrdinals) * sizeof(DWORD));

					// store this functions VA
					if (export_hash == LOADLIBRARYA_HASH) {
						pLoadLibraryA = (LOADLIBRARYA)(loaded_module_base + DEREF_32(uiAddressArray));
					}
					else if (export_hash == GETPROCADDRESS_HASH) {
						pGetProcAddress = (GETPROCADDRESS)(loaded_module_base + DEREF_32(uiAddressArray));
					}
					else if (export_hash == VIRTUALALLOC_HASH) {
						pVirtualAlloc = (VIRTUALALLOC)(loaded_module_base + DEREF_32(uiAddressArray));
					}
					else if (export_hash == VIRTUALLOCK_HASH) {
						pVirtualLock = (VIRTUALLOCK)(loaded_module_base + DEREF_32(uiAddressArray));
					}

					// decrement our counter
					exportsToFind--;
				}

				// get the next exported function name
				uiNameArray += sizeof(DWORD);

				// get the next exported function name ordinal
				uiNameOrdinals += sizeof(WORD);
			}
		}
		else if ((DWORD)module_name_hash == NTDLL_HASH_UNICODE) {
			// get this modules base address
			loaded_module_base = (ULONG_PTR)((PLDR_DATA_TABLE_ENTRY)module_entry)->DllBase;

			// get the VA of the modules NT Header
			nthdr = loaded_module_base + ((PIMAGE_DOS_HEADER)loaded_module_base)->e_lfanew;

			// get the VA of the export directory
			exports = (ULONG_PTR)&((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
			uiExportDir = (loaded_module_base + ((PIMAGE_DATA_DIRECTORY)exports)->VirtualAddress);

			// get the VA for the array of name pointers
			uiNameArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNames);

			// get the VA for the array of name ordinals
			uiNameOrdinals = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNameOrdinals);

			// we need these many exports from ntdll
			exportsToFind = 1;

			// loop while we still have imports to find
			DWORD export_hash = 0;
			ULONG_PTR uiAddressArray = 0;
			while (exportsToFind > 0) {
				// compute the hash values for this function name
				export_hash = hashRor13((uint8_t*)(loaded_module_base + DEREF_32(uiNameArray)), 0);

				// if we have found a function we want we get its virtual address
				if (export_hash == NTFLUSHINSTRUCTIONCACHE_HASH) {
					// get the VA for the array of addresses
					uiAddressArray = (loaded_module_base + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions);

					// use this functions name ordinal as an index into the array of name pointers
					uiAddressArray += (DEREF_16(uiNameOrdinals) * sizeof(DWORD));

					// store this functions VA
					if (export_hash == NTFLUSHINSTRUCTIONCACHE_HASH) {
						pNtFlushInstructionCache = (NTFLUSHINSTRUCTIONCACHE)(loaded_module_base + DEREF_32(uiAddressArray));
					}

					// decrement our counter
					exportsToFind--;
				}

				// get the next exported function name
				uiNameArray += sizeof(DWORD);

				// get the next exported function name ordinal
				uiNameOrdinals += sizeof(WORD);
			}
		}

		// we stop searching when we have found everything we need.
		if (pLoadLibraryA && pGetProcAddress && pVirtualAlloc
			&& pVirtualLock
			&& pNtFlushInstructionCache)
			break;

		// get the next entry
		module_entry = DEREF(module_entry);
	}

	// we must now load the image in memory

	//__debugbreak();

	// get the VA of the NT Header for the PE to be loaded
	ULONG_PTR nthdr = uiBaseAddress + ((PIMAGE_DOS_HEADER)uiBaseAddress)->e_lfanew;

	/*
	// 1) we must be already mapped in the process, avoid virtualalloc() here.
	// 2) the following copy is not needed since we put filealignment = sectionalignment in the dlls we prepare ourself (saves space).

	// allocate all the memory for the DLL to be loaded into. we can load at any address because we will
	// relocate the image. Also zeros all memory and marks it as READ, WRITE and EXECUTE to avoid any problems.

	//ULONG_PTR newBaseAddress = (ULONG_PTR)pVirtualAlloc(NULL, ((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.SizeOfImage, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	// we must now copy over the headers (1:1 copy)
	ULONG_PTR cnt = ((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.SizeOfHeaders;
	ULONG_PTR src = uiBaseAddress;
	ULONG_PTR dst = newBaseAddress;
	while (cnt--) {
		*(BYTE *)dst++ = *(BYTE *)src++;
	}

	// this is the start of the first section header
	ULONG_PTR sections_VA = ((ULONG_PTR)&((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader + ((PIMAGE_NT_HEADERS)nthdr)->FileHeader.SizeOfOptionalHeader);

	// iterate through all sections, loading them into memory (considering alignment)
	WORD num_sections = ((PIMAGE_NT_HEADERS)nthdr)->FileHeader.NumberOfSections;
	while (num_sections--) {
		// dst is the VA for this section
		dst = (newBaseAddress + ((PIMAGE_SECTION_HEADER)sections_VA)->VirtualAddress);

		// src if the VA for this sections data
		src = (uiBaseAddress + ((PIMAGE_SECTION_HEADER)sections_VA)->PointerToRawData);

		// copy the section over
		cnt = ((PIMAGE_SECTION_HEADER)sections_VA)->SizeOfRawData;
		while (cnt--) {
			*(BYTE *)dst++ = *(BYTE *)src++;
		}

		// get the VA of the next section
		sections_VA += sizeof(IMAGE_SECTION_HEADER);
	}
	// continue with the new base address now
	uiBaseAddress = newBaseAddress;
	*/

	// prevent our image from being swapped to the pagefile
	pVirtualLock((LPVOID)uiBaseAddress, ((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.SizeOfImage);

	// process imports
	// we assume there is an import table to process
	ULONG_PTR imports = (ULONG_PTR)&((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];

	// this is the first entry in the import table
	ULONG_PTR import_VA = (uiBaseAddress + ((PIMAGE_DATA_DIRECTORY)imports)->VirtualAddress);

	// iterate through all imports until a null RVA is found (Characteristics is mis-named)
	while (((PIMAGE_IMPORT_DESCRIPTOR)import_VA)->Characteristics) {
		// use LoadLibraryA to load the imported module into memory
		ULONG_PTR libHModule = (ULONG_PTR)pLoadLibraryA((LPCSTR)(uiBaseAddress + ((PIMAGE_IMPORT_DESCRIPTOR)import_VA)->Name));
		if (!libHModule) {
			import_VA += sizeof(IMAGE_IMPORT_DESCRIPTOR);
			continue;
		}

		//__debugbreak();
		// uiValueD = VA of the OriginalFirstThunk
		ULONG_PTR original_first_thunk = (uiBaseAddress + ((PIMAGE_IMPORT_DESCRIPTOR)import_VA)->OriginalFirstThunk);

		// uiValueA = VA of the IAT (via first thunk not origionalfirstthunk)
		ULONG_PTR first_thunk = (uiBaseAddress + ((PIMAGE_IMPORT_DESCRIPTOR)import_VA)->FirstThunk);

		// iterate through all imported functions, importing by ordinal if no name present
		while (DEREF(first_thunk)) {
			// sanity check uiValueD as some compilers only import by FirstThunk
			if (original_first_thunk && ((PIMAGE_THUNK_DATA)original_first_thunk)->u1.Ordinal & IMAGE_ORDINAL_FLAG) {
				// get the VA of the module NT Header
				ULONG_PTR modNtHdr = libHModule + ((PIMAGE_DOS_HEADER)libHModule)->e_lfanew;

				// uiNameArray = the address of the modules export directory entry
				ULONG_PTR uiNameArray = (ULONG_PTR)&((PIMAGE_NT_HEADERS)modNtHdr)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];

				// get the VA of the export directory
				ULONG_PTR uiExportDir = (libHModule + ((PIMAGE_DATA_DIRECTORY)uiNameArray)->VirtualAddress);

				// get the VA for the array of addresses
				ULONG_PTR uiAddressArray = (libHModule + ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions);

				// use the import ordinal (- export ordinal base) as an index into the array of addresses
				uiAddressArray += ((IMAGE_ORDINAL(((PIMAGE_THUNK_DATA)original_first_thunk)->u1.Ordinal) - ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->Base) * sizeof(DWORD));

				// patch in the address for this imported function
				DEREF(first_thunk) = (libHModule + DEREF_32(uiAddressArray));
			}
			else {
				// get the VA of this functions import by name struct
				ULONG_PTR name_struct = (uiBaseAddress + DEREF(first_thunk));
				// use GetProcAddress and patch in the address for this imported function
				DEREF(first_thunk) = (ULONG_PTR)pGetProcAddress((HMODULE)libHModule, (LPCSTR)((PIMAGE_IMPORT_BY_NAME)name_struct)->Name);
			}

			// get the next imported function
			first_thunk += sizeof(ULONG_PTR);
			if (original_first_thunk) {
				original_first_thunk += sizeof(ULONG_PTR);
			}
		}

		// get the next import
		import_VA += sizeof(IMAGE_IMPORT_DESCRIPTOR);
	}

	// process relocations
	ULONG_PTR delta = uiBaseAddress - ((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.ImageBase;

	// uiValueB = the address of the relocation directory
	ULONG_PTR relocs = (ULONG_PTR)&((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC];

	// check if there are any relocations present
	if (((PIMAGE_DATA_DIRECTORY)relocs)->Size) {
		// UIValue C get the first entry (IMAGE_BASE_RELOCATION)
		ULONG_PTR reloc_VA = (uiBaseAddress + ((PIMAGE_DATA_DIRECTORY)relocs)->VirtualAddress);

		// and we itterate through all entries...
		while (((PIMAGE_BASE_RELOCATION)reloc_VA)->SizeOfBlock) {
			// uiValueA = the VA for this relocation block
			ULONG_PTR reloc_block = (uiBaseAddress + ((PIMAGE_BASE_RELOCATION)reloc_VA)->VirtualAddress);

			// uiValueB = number of entries in this relocation block
			ULONG_PTR reloc_entries = (((PIMAGE_BASE_RELOCATION)reloc_VA)->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(IMAGE_RELOC);

			// uiValueD is now the first entry in the current relocation block
			ULONG_PTR reloc_entry = reloc_VA + sizeof(IMAGE_BASE_RELOCATION);

			// we itterate through all the entries in the current block...
			while (reloc_entries--) {
				// perform the relocation, skipping IMAGE_REL_BASED_ABSOLUTE as required.
				// we dont use a switch statement to avoid the compiler building a jump table
				// which would not be very position independent!
				if (((PIMAGE_RELOC)reloc_entry)->type == IMAGE_REL_BASED_DIR64)
					*(ULONG_PTR *)(reloc_block + ((PIMAGE_RELOC)reloc_entry)->offset) += delta;
				else if (((PIMAGE_RELOC)reloc_entry)->type == IMAGE_REL_BASED_HIGHLOW)
					*(DWORD *)(reloc_block + ((PIMAGE_RELOC)reloc_entry)->offset) += (DWORD)delta;
				else if (((PIMAGE_RELOC)reloc_entry)->type == IMAGE_REL_BASED_HIGH)
					*(WORD *)(reloc_block + ((PIMAGE_RELOC)reloc_entry)->offset) += HIWORD(delta);
				else if (((PIMAGE_RELOC)reloc_entry)->type == IMAGE_REL_BASED_LOW)
					*(WORD *)(reloc_block + ((PIMAGE_RELOC)reloc_entry)->offset) += LOWORD(delta);

				// get the next entry in the current relocation block
				reloc_entry += sizeof(IMAGE_RELOC);
			}

			// get the next entry in the relocation directory
			reloc_VA += ((PIMAGE_BASE_RELOCATION)reloc_VA)->SizeOfBlock;
		}
	}

	// get entrypoint (DllMain)
	// ULONG_PTR entrypoint = (uiBaseAddress + ((PIMAGE_NT_HEADERS)nthdr)->OptionalHeader.AddressOfEntryPoint);

	// We must flush the instruction cache to avoid stale code being used which was updated by our relocation processing.
	pNtFlushInstructionCache((HANDLE)-1, NULL, 0);

	// done, return the base address too (must be freed only on error/cleanup)
	// also, the pointer at dll_buffer can be freed too now (depending who allocated it)
	if (newBase) {
		*newBase = uiBaseAddress;
	}
	return TRUE;
}
#pragma code_seg(pop, r1)
