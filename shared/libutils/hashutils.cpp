/*!
* \file hashutils.cpp
*
* \author valerino
* \date novembre 2017
*
* hashing utilities
*
*/

#include <windows.h>
#include <stdint.h>

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
extern "C" uint32_t hashRor13(uint8_t* c, uint32_t len) {
	register DWORD h = 0;
	int l = len;
	if (!c) {
		return 0;
	}
	while (1) {
		h = _rotr(h, 13);
		if (*c >= 'a') {
			h += *c - 0x20;
		}
		else {
			h += *c;
		}
		c++;
		if (l > 0) {
			len--;
			if (len == 0) {
				break;
			}
		}
		else {
			if (*c == '\0') {
				break;
			}
		}
	}
	return h;
}
#pragma code_seg(pop, r1)
