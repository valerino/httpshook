/*!
* \file apihookutils.h
*
* \author valerino
* \date novembre 2017
*
* api hooking utilities, using minhook (https://github.com/TsudaKageyu/minhook)
*/

#include <Windows.h>
#include <stdint.h>

#include "memutils.h"
#include "apihookutils.h"
#include "dbgutils.h"

int __apihkInitializeCalled = 0;

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

BOOL apihkCheck(void* address) {
	uint8_t* p = (uint8_t*)address;
	if (!address) {
		return FALSE;
	}
	if (p[0] == 0xe9) {
		return TRUE;
	}
	if (p[0] == 0xff && p[1] == 0x25) {
		return TRUE;

	}
	return FALSE;
}

int apihkInitialize() {
	if (__apihkInitializeCalled > 0) {
		return 0;
	}
	__apihkInitializeCalled++;

	// initialize minhook
	int res = MH_Initialize();
	return res;
}

void apihkfinalize() {
	if (__apihkInitializeCalled == 0) {
		return;
	}
	__apihkInitializeCalled--;

	if (__apihkInitializeCalled == 0) {
		// free minhook
		MH_Uninitialize();
	}
}

int apihkEnableHook(uint8_t* function, BOOL enable, BOOL remote) {
	if (!function) {
		return ERROR_INVALID_PARAMETER;
	}
	if (enable) {
		return MH_EnableHook(function, remote);
	}
	return MH_DisableHook(function, remote);
}

int apihkInstallHook(uint8_t* function, uint8_t* replacement, void** hk, LPVOID trampolineMem, BOOL enable) {
	if (!function || !replacement || !hk) {
		return ERROR_INVALID_PARAMETER;
	}
	DBG_PRINT(DBG_LEVEL_VERBOSE, "target:%p, hookfunc=%p, trampolineMem=%p", function, replacement, trampolineMem);
	int res = MH_CreateHook(function, replacement, hk, trampolineMem);
	if (res == MH_OK && enable) {
		DBG_PRINT(DBG_LEVEL_VERBOSE, "calling apihkEnableHook()");
		return apihkEnableHook(function, TRUE, trampolineMem ? TRUE : FALSE);
	}
	return res;
}

void apihkUninstallHook(void* function) {
	if (function) {
		MH_RemoveHook(function, FALSE);
	}
}
#pragma code_seg(pop, r1)
