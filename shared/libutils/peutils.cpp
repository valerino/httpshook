/*!
 * \file peutils.cpp
 *
 * \author valerino
 * \date novembre 2017
 *
 * pe file format utilities
 */

 /* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

#include <windows.h>
#include "peutils.h"
#include "hashutils.h"

extern "C" ULONG_PTR peRvaToOffset(DWORD dwRva, UINT_PTR uiBaseAddress) {
	PIMAGE_NT_HEADERS pNtHeaders = (PIMAGE_NT_HEADERS)(uiBaseAddress + ((PIMAGE_DOS_HEADER)uiBaseAddress)->e_lfanew);
	PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)((UINT_PTR)(&pNtHeaders->OptionalHeader) + pNtHeaders->FileHeader.SizeOfOptionalHeader);

	if (dwRva < pSectionHeader[0].PointerToRawData) {
		return dwRva;
	}

	// walk sections
	for (WORD wIndex = 0; wIndex < pNtHeaders->FileHeader.NumberOfSections; wIndex++) {
		if (dwRva >= pSectionHeader[wIndex].VirtualAddress && dwRva < (pSectionHeader[wIndex].VirtualAddress + pSectionHeader[wIndex].SizeOfRawData)) {
			// found
			return ((dwRva - pSectionHeader[wIndex].VirtualAddress) + pSectionHeader[wIndex].PointerToRawData);
		}
	}

	// not found!
	return 0;
}

extern "C" PVOID peGetProcAddress(HMODULE module, DWORD hash) {
	// get to the export directory
	IMAGE_DOS_HEADER* dos_header = (IMAGE_DOS_HEADER *)module;
	IMAGE_NT_HEADERS *nt_headers = (IMAGE_NT_HEADERS *)((char *)module + dos_header->e_lfanew);
	IMAGE_EXPORT_DIRECTORY *export_dir = (IMAGE_EXPORT_DIRECTORY *)((char *)module + nt_headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

	// get to the names/ordinals rva table
	PDWORD names = (DWORD *)((char *)module + export_dir->AddressOfNames);
	PDWORD funcs = (DWORD *)((char *)module + export_dir->AddressOfFunctions);
	PWORD ordinals = (WORD *)((char *)module + export_dir->AddressOfNameOrdinals);

	// walk table
	for (DWORD i = 0; i < export_dir->NumberOfNames; i++) {
		char *string = (char *)module + names[i];
		if (hash == hashRor13((uint8_t*)string, 0)) {
			// found function, get rva
			WORD ord = ordinals[i];
			DWORD func_rva = funcs[ord];

			// return base + function rva
			return (PVOID)((char *)module + func_rva);
		}
	}

	// not found!
	return NULL;
}

extern "C" uint32_t peResolveDynamicImports(dynamic_import_ptr* p) {
	dynamic_import_ptr* current = p;
	while (1) {
		*current->ptr = (ULONG_PTR*)peGetProcAddress(current->hmod, current->rot13_hash);
		if (*current->ptr == NULL) {
			// error resolving import
			return ERROR_NOT_FOUND;
		}

		// next entry
		current++;
		if (current->rot13_hash == 0) {
			// done
			break;
		}
	}
	return 0;
}

BYTE* peGetNtHeaders(const BYTE *pe_buffer) {
	if (pe_buffer == NULL) {
		return NULL;
	}

	IMAGE_DOS_HEADER *idh = (IMAGE_DOS_HEADER*)pe_buffer;
	if (idh->e_magic != IMAGE_DOS_SIGNATURE) {
		return NULL;
	}
	const LONG kMaxOffset = 1024;
	LONG pe_offset = idh->e_lfanew;

	if (pe_offset > kMaxOffset) return NULL;

	IMAGE_NT_HEADERS32 *inh = (IMAGE_NT_HEADERS32 *)(pe_buffer + pe_offset);
	if (inh->Signature != IMAGE_NT_SIGNATURE) {
		return NULL;
	}
	return (BYTE*)inh;
}

WORD peGetArchitecture(const BYTE *pe_buffer) {
	void *ptr = peGetNtHeaders(pe_buffer);
	if (ptr == NULL) {
		return 0;
	}

	IMAGE_NT_HEADERS32 *inh = static_cast<IMAGE_NT_HEADERS32*>(ptr);
	return inh->FileHeader.Machine;
}

BOOL peIs64bit(const BYTE *pe_buffer) {
	WORD arch = peGetArchitecture(pe_buffer);
	if (arch == IMAGE_FILE_MACHINE_AMD64) {
		return TRUE;
	}
	return FALSE;
}

ULONGLONG peGetImageBase(const BYTE *pe_buffer) {
	BOOL is64b = peIs64bit(pe_buffer);
	//update image base in the written content:
	BYTE* payload_nt_hdr = peGetNtHeaders(pe_buffer);
	if (payload_nt_hdr == NULL) {
		return 0;
	}
	ULONGLONG img_base = 0;
	if (is64b) {
		IMAGE_NT_HEADERS64* payload_nt_hdr64 = (IMAGE_NT_HEADERS64*)payload_nt_hdr;
		img_base = payload_nt_hdr64->OptionalHeader.ImageBase;
	}
	else {
		IMAGE_NT_HEADERS32* payload_nt_hdr32 = (IMAGE_NT_HEADERS32*)payload_nt_hdr;
		img_base = static_cast<ULONGLONG>(payload_nt_hdr32->OptionalHeader.ImageBase);
	}
	return img_base;
}

IMAGE_DATA_DIRECTORY* peGetDirectoryEntry(const BYTE *pe_buffer, DWORD dir_id) {
	if (dir_id >= IMAGE_NUMBEROF_DIRECTORY_ENTRIES) return NULL;

	BYTE* nt_headers = peGetNtHeaders((BYTE*)pe_buffer);
	if (nt_headers == NULL) return NULL;

	IMAGE_DATA_DIRECTORY* peDir = NULL;
	if (peIs64bit((BYTE*)pe_buffer)) {
		IMAGE_NT_HEADERS64* nt_headers64 = (IMAGE_NT_HEADERS64*)nt_headers;
		peDir = &(nt_headers64->OptionalHeader.DataDirectory[dir_id]);
	}
	else {
		IMAGE_NT_HEADERS32* nt_headers64 = (IMAGE_NT_HEADERS32*)nt_headers;
		peDir = &(nt_headers64->OptionalHeader.DataDirectory[dir_id]);
	}
	if (peDir->VirtualAddress == NULL) {
		return NULL;
	}
	return peDir;
}

/*!
* \brief set subsystem in a PE image
*
* \param BYTE * payload a PE
* \param WORD subsystem the subsystem to be set (i.e. IMAGE_SUBSYSTEM_WINDOWS_GUI)
* \return bool
*/
BOOL peSetSubsystem(BYTE* payload, WORD subsystem) {
	if (payload == NULL) {
		return FALSE;
	}

	BOOL is64b = peIs64bit(payload);
	BYTE* payload_nt_hdr = peGetNtHeaders(payload);
	if (payload_nt_hdr == NULL) {
		return FALSE;
	}
	if (is64b) {
		IMAGE_NT_HEADERS64* payload_nt_hdr64 = (IMAGE_NT_HEADERS64*)payload_nt_hdr;
		payload_nt_hdr64->OptionalHeader.Subsystem = subsystem;
	}
	else {
		IMAGE_NT_HEADERS32* payload_nt_hdr32 = (IMAGE_NT_HEADERS32*)payload_nt_hdr;
		payload_nt_hdr32->OptionalHeader.Subsystem = subsystem;
	}
	return TRUE;
}

BOOL peSetImageBase(BYTE* payload, ULONGLONG destImageBase) {
	BOOL is64b = peIs64bit(payload);
	//update image base in the written content:
	BYTE* payload_nt_hdr = peGetNtHeaders(payload);
	if (payload_nt_hdr == NULL) {
		return FALSE;
	}
	if (is64b) {
		IMAGE_NT_HEADERS64* payload_nt_hdr64 = (IMAGE_NT_HEADERS64*)payload_nt_hdr;
		payload_nt_hdr64->OptionalHeader.ImageBase = (ULONGLONG)destImageBase;
	}
	else {
		IMAGE_NT_HEADERS32* payload_nt_hdr32 = (IMAGE_NT_HEADERS32*)payload_nt_hdr;
		payload_nt_hdr32->OptionalHeader.ImageBase = (DWORD)destImageBase;
	}
	return TRUE;
}

DWORD peGetEntrypointRva(const BYTE *pe_buffer) {
	BOOL is64b = peIs64bit(pe_buffer);
	//update image base in the written content:
	BYTE* payload_nt_hdr = peGetNtHeaders(pe_buffer);
	if (payload_nt_hdr == NULL) {
		return 0;
	}
	DWORD img_base = 0;
	if (is64b) {
		IMAGE_NT_HEADERS64* payload_nt_hdr64 = (IMAGE_NT_HEADERS64*)payload_nt_hdr;
		img_base = payload_nt_hdr64->OptionalHeader.AddressOfEntryPoint;
	}
	else {
		IMAGE_NT_HEADERS32* payload_nt_hdr32 = (IMAGE_NT_HEADERS32*)payload_nt_hdr;
		img_base = static_cast<ULONGLONG>(payload_nt_hdr32->OptionalHeader.AddressOfEntryPoint);
	}
	return img_base;
}
#pragma code_seg(pop, r1)
