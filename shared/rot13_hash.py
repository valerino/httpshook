import codecs
import sys
import os
import pyperclip

rol = lambda val, r_bits, max_bits: \
	(val << r_bits%max_bits) & (2**max_bits-1) | \
	((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))
 
ror = lambda val, r_bits, max_bits: \
	((val & (2**max_bits-1)) >> r_bits%max_bits) | \
	(val << (max_bits-(r_bits%max_bits)) & (2**max_bits-1))

def rot13_hash(s):
	hash = 0
	for c in s:
		hash = ror(hash,13,32) | rol(hash,19,32)
		hash = hash + ord(c)
	
	return hash

def rot13_hash_2(s):
	hash = 0
	l = len(s)
	for c in s:
		hash = ror(hash,13,32) | rol(hash,19,32)
		if ord(c) >= ord('a'):
			hash = hash + ord(c) - 0x20
		else:
			hash = hash + ord(c)
	
	return hash

def print_hash(s, l, p, u, h):
	out_string = ""
	ss = ""
	if u == '-u':
		for c in s:
			if c != '\0':
				ss = ss+c
	else:
		ss=s

	if p == '-t':
		out_string = "{(ULONG_PTR**)&ptr%s, %s, %s}, // => %s" % (ss, l, h, s)
	elif p == '-g':
		out_string = ", %s); // => %s" % (h, s)
	else:
		# default -v
		out_string= "%s); // => %s" % (h, s)

	# print
	print(out_string)
	ss=""
	for c in out_string:
		if c == '\0':
			ss += ' '
		else:
			ss += c;
	
	# copy to clipboard
	pyperclip.copy(ss)
	
if len(sys.argv) >= 6:	
	l = sys.argv[1:][0] # HMODULE variable
	p = sys.argv[2:][0] # option (-t (table_entry), -g (getprocaddress string), -v (plain value)
	u = sys.argv[3:][0] # option (-u (unicode), -a (ansi)
	m = sys.argv[4:][0] # option (-m (meterpreter, upper/lowercase compensation), -d (default rot13)
	s = sys.argv[5:][0] # string to hash

	if u == '-u':
		# unicode
		ss = ""
		for c in s:
			ss+=c + '\0'
		s=ss

	if m == '-m':
		# meterpreter (upper/lowercase compensation)
		h = format(rot13_hash_2(s), '#04x')
	else:
		# default rot13
		h = format(rot13_hash(s), '#04x')

	print_hash(s, l, p, u, h)
	
else:
	print("usage: %s <hmodule_var> <printformat -t|-g|-v> <encoding -u|-a> <format -m meterpreter|-d default rot13> <string>" % sys.argv[0])
