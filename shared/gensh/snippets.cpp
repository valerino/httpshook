

typedef BOOL(WINAPI* HEAPFREE)(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem);
typedef LPVOID(WINAPI* RTLALLOCATEHEAP)(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
typedef HANDLE(WINAPI* GETPROCESSHEAP)(VOID);
typedef VOID(WINAPI* RTLCAPTURECONTEXT)(PCONTEXT ContextRecord);
typedef VOID(WINAPI* RTLRESTORECONTEXT)(PCONTEXT ContextRecord, PEXCEPTION_RECORD ExceptionRecord);
typedef BOOL(WINAPI* VIRTUALPROTECT)(LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, PDWORD flOldProtect);
typedef LPVOID(WINAPI* VIRTUALALLOC)(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
typedef BOOL(WINAPI* VIRTUALFREE)(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);

VIRTUALFREE ptrVirtualFree = (VIRTUALFREE)getprocaddress_custom(kernel32, 0x030633ac);
VIRTUALPROTECT ptrVirtualProtect = (VIRTUALPROTECT)getprocaddress_custom(kernel32, 0x7946c61b);



HMODULE kernelbase = module_find(0x351a452);
HMODULE kernel32 = _module_find(0x8fecd63f);
				//DWORD ptr = hash_ror13("VirtualAlloc"); // 0x91afca54
VIRTUALPROTECT ptrVirtualProtect = (VIRTUALPROTECT)_getprocaddress_custom(kernel32, 0x7946c61b); // _hash_rot13("VirtualProtect")
/DWORD ptr = hash_ror13("RtlAllocateHeap"); // 0x818a64c8
		DWORD ptr = hash_ror13("RtlCaptureContext"); // 0x818a64c8
		DWORD ptr = hash_ror13("RtlRestoreContext"); // 0x89103508
		ptr = hash_ror13("HeapAlloc"); // 0x2500383c
		ptr = hash_ror13("GetProcessHeap"); // 0xa80eecae
