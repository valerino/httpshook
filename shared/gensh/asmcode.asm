; implements calls in pure asm to avoid msvc15 limitations on x64 (can't inline)
; jrf, 2k17
;
PUBLIC call_virtualalloc_rwx

; all of this will go in .text2 section (among other position independent code)
.code
text2 SEGMENT READ EXECUTE alias ('.text2') 'CODE'

call_virtualalloc_rwx PROC
	; implemented in asm to avoid crashing due to missing stack canary (we can't compile the shellcode with /GS)
	; allocate memory, just calls virtual alloc with the given page flags
	; rcx = Kernel32!VirtualAlloc
	; rdx = dll size
	; r8 = page flags
	; 
	sub rsp,1000h						; try to avoid stack trashing moving the stack top, or eshims.dll stack protection triggers!
	mov rax, rcx						; rax = Kernel32!VirtualAlloc()
	xor rcx,rcx							; base address = 0
	mov r9, r8							; page flags
	mov r8, 3000h						; MEM_COMMIT|MEM_RESERVE
	call rax							; call VirtualAlloc()
	add rsp,1000h						; restore stack
	ret									; return to caller
call_virtualalloc_rwx ENDP

fix_stack PROC
	; fix stack so shell code on ret will land to the correct address
	; rcx = n to add to rsp
	add rsp, rcx						; fix rsp
	ret									; must return to the shellcode caller
fix_stack ENDP

shellcode_end PROC
	; dummy
	ret
shellcode_end ENDP

text2 ENDS
END
