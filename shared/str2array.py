import sys
import os
import pyperclip

def print_string(s, unicode):
	out_string = "// " + s + '\n'
	if unicode:
		out_string += 'WCHAR str_' + s + '[] = {'
	else:
		out_string += 'CHAR str_' + s + '[] = {'
	
	for c in s:
		if unicode:
			out_string += '(WCHAR)\'' + c + '\', ' 			
		else:
			out_string += '\'' + c + '\', ' 			
			
	
	# close
	if unicode:
		out_string = out_string + '(WCHAR)\'\\0\'};'
	else:
		out_string = out_string + '\'\\0\'};'

	# print
	print(out_string)
	
	# copy to clipboard
	pyperclip.copy(out_string)
	
if len(sys.argv) == 3:	
	s = sys.argv[2:][0] # the string
	u = sys.argv[1:][0] # ansi/unicode
	unicode = False
	if u == '-u':
		# unicode
		unicode = True

	print_string(s, unicode)
	
else:
	print("usage: %s <-u for unicode | -a for ansi> <string>" % sys.argv[0])
