#!/usr/bin/env python3
# embeds 32bit dll into 64bit agent dll.
# note: the 32bit dll must be already encrypted via encrypt_agent.py !

import os
import io
import argparse
import traceback
import binascii

def buffer_to_file(path, buffer):
	"""
	write a buffer to file, overwriting any existing file
	"""	
	with open(path, 'wb') as f:
		f.write(buffer)


def buffer_from_file(path):
	"""
	read whole file to bytearray buffer
	"""	
	with open(path, 'rb') as f:
		buffer = bytearray(f.read())
		
	return buffer

	
def buf_write(dst, offset, src, size):
	"""
	writes data into buffer at [dst] starting at [offset], from [src] of size [size]
	"""
	dst[offset:offset + size] = src
	

def replace_dll(args):
	"""
	embeds the 32bit DLL back into the 64bit dll
	"""
	# check parameters
	if args.dll is None or args.dll32 is None:
		raise RuntimeError('--dll and --dll32 is mandatory!')
	
	dll = args.dll[0]
	dll32 = args.dll32[0]

	# default to input path
	out = args.out[0] if args.out is not None else dll
	
	# read whole dll64 in memory
	buffer = buffer_from_file(dll)
		
	# read whole dll32 in memory
	buffer32 = buffer_from_file(dll32)

	# find tag (64bit dll)
	tag = b'This program cannot be run in DOS mode'	
	pos = buffer.index(tag, 0)

	# this will find the 32bit dll embedded
	pos = buffer.index(tag, pos + 1) - 78
	print('[i] 32bit DLL found at offset: ' + str(pos))

	# replace
	buf_write(buffer,pos,buffer32,len(buffer32))

	# rewrite binary
	buffer_to_file(out, buffer)
	print('[i] done, embedded 32bit DLL into %s' % (out))


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--dll', help='the Falcon Agent DLL, 64bit, input', nargs=1)
	parser.add_argument('--dll32', help='the Falcon Agent DLL, 32bit (must be already encrypted!)', nargs=1)
	parser.add_argument('--out', help='the Falcon Agent DLL or dropper EXE, output (path at --dll is overwritten if unspecified)', nargs=1)
	args = parser.parse_args()
	try:
		replace_dll(args)
		
	except Exception as e:
		traceback.print_exc()
		exit(1)
		
if __name__ == "__main__":
    main()
    exit(0)
