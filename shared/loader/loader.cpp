/*!
 * \file main.cpp
 *
 * \author valerino
 * \date novembre 2017
 *
 * DLL injection loader capable of using both CreateRemoteThread() and reflective DLL injection methods
 * for the reflective DLL method to work, the DLL must export a reflective loader (default "ReflectiveLoader")
 * NOTE: compile x86 version to use with x86 DLL and processes, and x64 version for x64 !!!
 */
#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <fileutils.h>
#include <procutils.h>
#include <strutils.h>
#include <injutils.h>
#include <hashutils.h>
#include <Shlwapi.h>

#pragma comment (lib, "shlwapi.lib")

#define INJ_API_REMOTETHREAD 0
#define INJ_API_APC 1

ULONG inject(PWCHAR path, ULONG pid, int api) {
	PWCHAR ext = PathFindExtension(path);
	ULONG res;
	if (StrStrI(ext, L"dll")) {
		// reflective injection
		uint32_t size;
		uint8_t* buffer = (uint8_t*)fileToBuffer(path, (PULONG)&size);
		if (!buffer) {
			res = GetLastError();
		}
		else {
			// we pass a copy of the DLL in DllParamStruct, to allow reinjection
			DllParamsStruct* params;
			res = injectAllocateDllParamsStruct(pid, buffer, size, NULL, 0, NULL, &params);
			if (res == 0) {
				res = injectReflectiveFromBuffer(buffer, size, pid, params, (api == INJ_API_APC));
				if (res != 0) {
					injectFreeDllParamsStruct(pid, params);
				}
			}
		}
	}
	else {
		// shellcode
		res = injectShellcodeFromFile(path, pid, TRUE, (api == INJ_API_APC));
	}
	return res;
}

void banner() {
	printf("usage: <pid|process(all instances) to inject into> <xxxx.bin for shellcode|xxxx.dll>\n"
		"\t[-a to use QueueUserAPC() instead of CreateRemoteThread()\n"
		"\t[dll is injected reflectively (must implement modified reflective loader)]\n"
		"\txxx.bin = shellcode, xxxx.dll = dll (both must match target arch)\n");
}

int main(int argc, char** argv) {
	if (argc < 3) {
		banner();
		return 1;
	}

	// initialize injection tools
	if (injectInitialize() != 0) {
		printf("error initializing injection tools!\n");
		return 1;
	}

	int inj_api = INJ_API_REMOTETHREAD;
	BOOL dllCopy = FALSE;
	for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], "-a") == 0) {
			inj_api = INJ_API_APC;
		}
	}

	PWCHAR wpath;
	ULONG res;
	strToWchar((char*)argv[2], &wpath);
	printf("dll/shellcode to inject=%S\n", wpath);
	if (inj_api == INJ_API_APC) {
		printf("injection api=QueueUserApc()\n");
	}
	else {
		printf("injection api=CreateRemoteThread()\n");
	}
	int pid = atoi(argv[1]);
	if (pid != 0) {
		printf("target pid=%d\n", pid);
		res = inject(wpath, pid, inj_api);
	}
	else {
		PCHAR process = argv[1];
		printf("trying injection in every istance of %s....\n", process);
		ULONG num;
		LIST_ENTRY processes;
		procGetProcessesList(&processes, &num);

		// walk entries
		LIST_ENTRY* current = processes.Flink;
		while (TRUE) {
			processEntry* entry = CONTAINING_RECORD(current, processEntry, chain);
			// both must be bare names
			PCHAR q = PathFindFileNameA(entry->proc.szExeFile);
			//printf("process=%S\n", q);
			if (lstrcmpiA(process, q) == 0) {
				pid = entry->proc.th32ProcessID;
				res = inject(wpath, pid, inj_api);
				printf("injection into pid=%d, process=%s, res=%d\n", pid, process, res);
			}

			// next
			if (current->Flink == &processes || current->Flink == NULL) {
				break;
			}
			current = current->Flink;
		}
		if (pid == 0) {
			printf("process %s not found!\n", process);
		}
	}
	return 0;
}
